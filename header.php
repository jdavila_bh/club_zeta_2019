<script src="http://connect.facebook.net/es_LA/all.js"></script>
<div id="fb-root"></div>

<?php
if (empty($_SESSION["id_usuario"])) {
?>
    <div class="contenedor_ingreso_login">
        <div id="contenedor_login_back">
            <div class="login pb20">
                <div class="row  ">
                    <div id="cerrar_login" class="text-right mt10 mr10">
                        <img src="images/login/cerrar.png" class="img-responsive" align="right">
                    </div>
                </div>


                <div class="row  ">
                    <div class="col-md-12 pl0 pr0">
                        <img src="images/logo.png" class="img-responsive center-block img_maxh_30p">
                        <span class="azul_oscuro bold">Servicios Alimenticios</span>
                        <hr class="mt20 mb30">
                    </div>
                </div>


                <div class="row  mb10">
                    <div class="col-md-12 pl0 pr0">
                        <span class="azul_oscuro size15">Ingresa tu cuenta</span>
                    </div>
                </div>

                <div class="row  ">
                    <div class="contenedor_input_ingreso_new col-md-12 pl0 pr0">
                        <input class="mt20" id="rut_log" type="text" placeholder="Rut" onkeypress="checkKey(event);"/>
                        <input class="mt20" id="clave" type="password" placeholder="Contraseña" onkeypress="checkKey(event);"/>
                    </div>
                </div>

                <div class="row  ">
                    <div class="col-md-12 pl0 pr0">
                        <div class="boton sombra" id="boton_ingresar_login">Ingresar</div>
                    </div>
                </div>
                <div class="row  ">
                    <div class="col-md-12 pl0 pr0">
                        <div class="boton sombra" id="boton_login_user_facebook"  onclick="loguea();">Conectar con</div>
                    </div>
                </div>
                <div class="row  mt25 pb20">
                    <div class="col-md-12 pl0 pr0">
                        <div id="registrar_text" class="estilo500 size15">Si no te encuentras registrado hazlo <a href="registro.php" >aquí</a></div>
                        <div id="olvidaste_contrasena" class=" bold size15">¿Olvidaste tu contraseña?</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>
<div class="contenedor_recuperar">
    <div id="contenedor_recuperar_back">
        <div  class="recuperar" >
            <div id="boton_cerrar_box"  class="text-right mt10 mr10">
                <img src="images/login/cerrar.png">
            </div>
            <div class="titulo_recuperar mb40">
                <img src="images/logo.png" class="img-responsive center-block img_maxh_30p">
                <span class="azul_oscuro bold">Servicios Alimenticios</span>
            </div>
            <hr class="mt40 mb40">
            <div class="row">
                <div class="col-md-12 pl0 pr0">
                    <span class="azul_oscuro size2 bold">INGRESA TU E-MAIL</span>
                </div>
            </div>

            <div class="row  ">
                <div class="col-md-12 pl0 pr0">
                    <p class>Para recuperar tu contraseña debes ingresar el correo electrónico que utilizaste para registrarte en Club Zeta.</p>
                </div>
            </div>
            <div class="row  ">
                <div class="col-md-12 pl0 pr0">
                    <input id="recuperar_mail" type="text" placeholder="Escribe tu e-mail aquí">
                </div>
            </div>
            <div class="row  mt35 pb30">
                <div class="col-md-6 pl0 pr0">
                    <div id="boton_recuperar" class="boto">Recuperar Contraseña</div>
                </div>
            
                <div class="col-md-6 pl0 pr0">
                    <div id="boton_cerrar_box2" class="boto">Cerrar</div>
                </div>
            </div>
        </div>
    </div>
</div>



<nav class="navbar navbar-default bg_blanco mb0">
    <div class="container">
        <div class="navbar-header ">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="inicio.html" class="navbar-brand"><img src="images/logo_header.png" class="img-responsive"/></a>
        </div>

        <div class="collapse navbar-collapse relative" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right nav-right w999">
                <li id="home_menu">
                    <a href="inicio.html" class="azul_oscuro" <?php echo ($menu_header==1)?'style="border-bottom: #76BA19 4px solid;padding-bottom: 16px;"':''; ?>>
                        Home
                    </a>
                </li>
                <li id="compra_menu" class="pointer" <?php echo ($menu_header == 2)?'style="border-bottom: #76BA19 4px solid;padding-bottom: 16px;"':'';?>>
                    <?php
                    echo (empty($_SESSION["id_usuario"]))?
                        '<a onclick="abrir_log();" class="azul_oscuro">Compra Online</a>'
                        :'<a href="home.html" class="azul_oscuro">Compra Online</a>';
                    ?>
                </li>
                <li id="quienes_menu" <?php echo ($menu_header == 4)?'style="border-bottom: #76BA19 4px solid;padding-bottom: 16px;"':'';?>>
                    <a href="quienes_somos.php" class="azul_oscuro">Quiénes Somos</a>
                </li>
                <li id="consejos_menu" <?php echo ($menu_header == 5)?'style="border-bottom: #76BA19 4px solid;padding-bottom: 16px;"':'';?>>
                    <a href="consejos.html" class="azul_oscuro">Tips Nutricionales</a>
                </li>
                <li id="consejos_menu" <?php echo ($menu_header == 6)?'style="border-bottom: #76BA19 4px solid;padding-bottom: 16px;"':'';?>>
                    <a href="kiosco_saludable.html" class="azul_oscuro">Kiosco Saludable</a>
                </li>
                <li id="contacto_menu" <?php echo ($menu_header == 3)?'style="border-bottom: #76BA19 4px solid;padding-bottom: 16px;"':''?>>
                    <a href="contacto.html" class="azul_oscuro">Contacto</a>
                </li>

                <li>
                    <?php if (empty($_SESSION["id_usuario"])) { ?>
                        <span id="ingresar_menu" style="margin-top: 8px; margin-left: 30px;display: inline-block;">
                            <a href="#" onclick="abrir_log();" class="verde"><img src="images/login/persona.png"  style="margin-top: 1px;margin-right: 20px;"> Iniciar Sesión</a>
                        </span> 
                        <!-- |
                        <span id="ingresar_menu">
                            <a href="registro.php" class="verde" >Registrarse</a>
                        </span> -->

                    <?php } else {?>
                        <span id="ingresar_menu" style="margin-top: 8px; margin-left: 30px;display: inline-block;">
                            <a href="/home.html" class="verde"><img src="images/login/persona.png"  style="margin-top: 1px;margin-right: 20px;"><?php echo '<span>Usuario</span>'; ?></a>
                        <!-- </span> |
                        <span id="ingresar_menu">
                            <a href="/cerrarsesion.html" class="verde">Desconectar</a>
                        </span> -->

                    <?php }?>
                </li>
            </ul>

        </div>
    </div>

</nav>






<?php
if ($miga_pan != 'HOME' and $miga_pan != '¿QUIÉNES SOMOS?' and $miga_pan != 'CONTACTO' and $miga_pan != 'REGISTRARSE' and $miga_pan != 'KIOSCO') {
    include('miga_pan.php');
    }
?>