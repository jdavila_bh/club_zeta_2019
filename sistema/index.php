<?php
include('../conex.php');
function format_rut($rut){
	$rut = str_replace('.','',$rut);
	$rut = str_replace('.','',$rut);
	$rut = str_replace('-','',$rut);
	$array_rut = str_split($rut);
	$largo = count($array_rut);
	$i=0;
	while($i<($largo-1)){
		$rut1.= $array_rut[$i];
		$i++;
	}
	$verificador = $array_rut[$i];
	
	return number_format($rut1,0,',','.').'-'.$verificador;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Comprobante</title>
<link rel="stylesheet" type="text/css" href="css/estilos.css"/>
<script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="js/validarut.js"></script>
<script>
$(document).ready(function(e) {
    llenar_ano();
});
//uso: onkeypress="return justNumbers(event);"
function justNumbers(e){
	var keynum = window.event ? window.event.keyCode : e.which;
	if((keynum == 8) || (keynum == 46)){
		return true;
	}else{
		return /\d/.test(String.fromCharCode(keynum));
	}
}
function enviar(oc,tipo){
	jQuery.ajax({
		type: "GET", 
		url: "mail_oc.php",
		data: 'oc='+oc+'&estado=1&tipo_mail='+tipo,
		success: function(a) {
			//console.log(a);
			if(a==0){
				alert('Nº de Orden no encontrada.');	
			}else{
				alert('Comprobante enviado a '+a+'.');
			}
		}
	});
} 
function ver(oc){
	jQuery.ajax({
		type: "GET", 
		url: "mail_oc.php",
		data: 'oc='+oc+'&estado=0',
		success: function(a) {
			//console.log(a);
			if(a==0){
				alert('Nº de Orden no encontrada.');	
			}else{
				$('#orden_vista').html(a);
			}
		}
	});
} 
function getVarsUrl(){
    var url= location.search.replace("?", "");
    var arrUrl = url.split("&");
    var urlObj={};   
    for(var i=0; i<arrUrl.length; i++){
        var x= arrUrl[i].split("=");
        urlObj[x[0]]=x[1]
    }
    return urlObj;
}
function llenar_ano(){
	var misVariablesGet = getVarsUrl();
	var minimo = 2010;
	var maximo = <?php echo date("Y");?>;	
	var i=0;
	var tope = maximo-minimo;
	while(i<=tope){
		if(misVariablesGet.a==maximo){
			$('#ano1').append('<option value="'+maximo+'" selected="selected">'+maximo+'</option>');
		}else{
			$('#ano1').append('<option value="'+maximo+'">'+maximo+'</option>');
		}
		
		maximo--;
		i++;
	}
}


function buscar(){
	var rut = $('#rut').val();
	var mes = $('#mes1').val();
	var ano = $('#ano1').val();
	if(rut==''){
		alert('Debes ingrear un rut antes de iniciar la busqueda.');
	}else{
		if(Rut(rut)){
			rut = $('#rut').val();
			top.location.href="index.php?r="+rut+"&m="+mes+"&a="+ano;
		}
	}
}

function buscar2(){
	var n_orden = $('#n_orden').val();
	var mes = $('#mes1').val();
	var ano = $('#ano1').val();
	if(n_orden==''){
		alert('Debes ingrear un Nº de orden antes de iniciar la busqueda.');
	}else{
		top.location.href="index.php?o="+n_orden+"&m="+mes+"&a="+ano;
	}
}

</script>

</head>

<body>




<h2>Buscar número de orden</h2>
<span>Mes</span>
<select id="mes1">
	<option value="01" <?php if(!empty($_GET['m'])){if($_GET['m']==01){echo 'selected="selected"';}} ?>>Enero</option>
    <option value="02" <?php if(!empty($_GET['m'])){if($_GET['m']==02){echo 'selected="selected"';}} ?>>Febrero</option>
    <option value="03" <?php if(!empty($_GET['m'])){if($_GET['m']==03){echo 'selected="selected"';}} ?>>Marzo</option>
    <option value="04" <?php if(!empty($_GET['m'])){if($_GET['m']==04){echo 'selected="selected"';}} ?>>Abril</option>
    <option value="05" <?php if(!empty($_GET['m'])){if($_GET['m']==05){echo 'selected="selected"';}} ?>>Mayo</option>
    <option value="06" <?php if(!empty($_GET['m'])){if($_GET['m']==06){echo 'selected="selected"';}} ?>>Junio</option>
    <option value="07" <?php if(!empty($_GET['m'])){if($_GET['m']==07){echo 'selected="selected"';}} ?>>Julio</option>
    <option value="08" <?php if(!empty($_GET['m'])){if($_GET['m']==08){echo 'selected="selected"';}} ?>>Agosto</option>
    <option value="09" <?php if(!empty($_GET['m'])){if($_GET['m']==09){echo 'selected="selected"';}} ?>>Septiembre</option>
    <option value="10" <?php if(!empty($_GET['m'])){if($_GET['m']==10){echo 'selected="selected"';}} ?>>Octubre</option>
    <option value="11" <?php if(!empty($_GET['m'])){if($_GET['m']==11){echo 'selected="selected"';}} ?>>Noviembre</option>
    <option value="12" <?php if(!empty($_GET['m'])){if($_GET['m']==12){echo 'selected="selected"';}} ?>>Diciembre</option>
</select>

<span>Año</span>
<select id="ano1"></select><br /><br />

<input id="rut" type="text" placeholder="Rut" value="<?php if(!empty($_GET['r'])){echo $_GET['r'];} ?>"/>
<button id="buscar" onclick="buscar()">Buscar</button><br /><br />

<input id="n_orden" type="text" placeholder="Nº Orden" onkeypress="return justNumbers(event);" value="<?php if(!empty($_GET['o'])){echo $_GET['o'];} ?>"/>
<button id="enviar" onclick="buscar2()">Buscar</button>

<?php
if(((!empty($_GET['r']))||(!empty($_GET['o'])))&&(!empty($_GET['a']))&&(!empty($_GET['m']))){
?>
<table id="table_orden" width="900" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <th>Nº Orden</th>
        <th>Rut</th>
        <th>Estado Orden</th>
        <th>Cantidad</th>
        <th>Total</th>
        <th>Fecha</th>
        <th>Ver Comprobante</th>
        <th>Enviar Apoderado</th>
        <th>Enviar Administrador</th>
      </tr>
      <?php
	  $mes = $_GET['m'];
	  $ano = $_GET['a'];
	  if($mes==12){
		  $mes=01;
		  $ano++;
	  }else{
		  $mes++;	  
	  }
	  
	  
      if(!empty($_GET['r'])){
          $rut_mod1 = format_rut($_GET['r']);
		  $rut_mod2 = str_replace('.','',$_GET['r']);
		  $rut_mod2 = str_replace('.','',$rut_mod2);
		  $rut_mod2 = str_replace('-','',$rut_mod2);
		  $query_orden="SELECT id_orden_compra, rut_orden_compra, estado_orden_compra, cantidad_orden_compra, monto_orden_compra, fecha_orden_compra 
						FROM orden_compra 
						WHERE (rut_orden_compra='".$rut_mod1."' OR rut_orden_compra='".$rut_mod2."') AND estado_orden_compra='PAGADO' AND fecha_orden_compra>='".$_GET['a']."-".$_GET['m']."-01' AND fecha_orden_compra<'".$ano."-".$mes."-01' ORDER BY  fecha_orden_compra DESC";
      }
	  if(!empty($_GET['o'])){
		  $query_orden="SELECT id_orden_compra, rut_orden_compra, estado_orden_compra, cantidad_orden_compra, monto_orden_compra, fecha_orden_compra 
						FROM orden_compra 
						WHERE id_orden_compra=".$_GET['o']." AND estado_orden_compra='PAGADO' AND fecha_orden_compra>='".$_GET['a']."-".$_GET['m']."-01' AND fecha_orden_compra<'".$ano."-".$mes."-01' ORDER BY  fecha_orden_compra DESC";
	  }
      
      $result_orden=mysql_query($query_orden,$link);
      while($row_orden=mysql_fetch_array($result_orden)){
          echo '<tr>
                    <td>'.utf8_decode($row_orden[0]).'</td>
                    <td>'.utf8_decode($row_orden[1]).'</td>
                    <td>'.utf8_decode($row_orden[2]).'</td>
                    <td>'.utf8_decode($row_orden[3]).'</td>
                    <td>$'.utf8_decode(number_format($row_orden[4],0,',','.')).'</td>
                    <td>'.utf8_decode($row_orden[5]).'</td>
                    <td><button id="ver" onclick="ver('.utf8_decode($row_orden[0]).')">Ver</button></td>
                    <td><button id="enviar" onclick="enviar('.utf8_decode($row_orden[0]).',1)">Enviar Comprobante Apoderado</button></td>
					<td><button id="enviar" onclick="enviar('.utf8_decode($row_orden[0]).',0)">Enviar Comprobante Administrador</button></td>
                </tr>';
      }
      ?>
  
</table>

<div id="orden_vista"></div>
<?php	
}
?>
</body>
</html>
