var cargando = {
    div: '<div id="cargando"><img src="../js/cargando/images/carga.gif" align="middle"><span>Cargando...</span></div>',
    div2: '<div id="cargando"><img src="js/cargando/images/carga.gif" align="middle"><span>Cargando...</span></div>',
    load:function(estado){
        if(estado)
        {
           $('#cargando').slideDown();
        }else{
           $('#cargando').slideUp();
        }
    },
    estado:function(){
        return $('#cargando').css('display');
    },
    evento:function(){
        $('#cargando').bind('click',function(){ $('#cargando').slideUp();});
    },

    init:function(){
         $(document).ready(function(){
            $("body").append(cargando.div2);
            cargando.evento();
         });

    }


}

// Inicializamos cargando
cargando.init();