function validarEmail(valor) {
  if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(valor)){
   	return true;
  } else {
    return false;
  }
}

var nav4 = window.Event ? true : false;
function acceptNum(evt){
// NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57
var key = nav4 ? evt.which : evt.keyCode;
return (key <= 13 || (key >= 48 && key <= 57));
}

//uso: onkeypress="return justNumbers(event);"
function justNumbers(e){
	var keynum = window.event ? window.event.keyCode : e.which;
	var telefono_num = $('#telefono').val().length;
	if(telefono_num < 8 ){
		if((keynum == 8) || (keynum == 46)){
			return true;
		}else{
			return /\d/.test(String.fromCharCode(keynum));
		}
	}else{
		return /\d/.test('');
	}
}