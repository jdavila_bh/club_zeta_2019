Código	Significado	Utilidad
200	OK	Úsalo para especificar que un recurso o colección existe
201	Created	Puedes usarlo para especificar que se creó un recurso. Se puede complementar con el header Location para especificar la URI hacia el nuevo recurso.
204	No Content	Representa un resultado exitoso pero sin retorno de algún dato (viene muy bien en DELETE).
304	No Modified	Indica que un recurso o colección no ha cambiado.
401	Unauthorized	Indica que el cliente debe estar autorizado primero antes de realizar operaciones con los recursos
404	Not Found	Ideal para especificar que el recurso buscado no existe
405	Method Not Allowed	Nos permite expresar que el método relacionado a la url no es permitido en la api
422	Unprocessable Entity	Va muy bien cuando los parámetros que se necesitaban en la petición no están completos o su sintaxis es la incorrecta para procesar la petición.
429	Too Many Requests	Se usa para expresarle al usuario que ha excedido su número de peticiones si es que existe una política de límites.
500	Internal Server Error	Te permite expresar que existe un error interno del servidor.
503	Service Unavailable	Este código se usa cuando el servidor esta temporalmente fuera de servicio.