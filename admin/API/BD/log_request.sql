

CREATE TABLE `log_request` (
  `id_log_request` bigint(21) NOT NULL,
  `uuid_log_request` varchar(500) NOT NULL,
  `addr_log_request` varchar(500) NOT NULL,
  `port_log_request` varchar(500) NOT NULL,
  `method_log_request` varchar(500) NOT NULL,
  `time_log_request` varchar(500) NOT NULL,
  `navigator_log_request` varchar(500) NOT NULL,
  `v_navigator_log_request` varchar(500) NOT NULL,
  `os_log_request` varchar(500) NOT NULL,
  `data_log_request` longtext NOT NULL,
  `date_log_request` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `log_request`
--
ALTER TABLE `log_request`
  ADD PRIMARY KEY (`id_log_request`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `log_request`
--
ALTER TABLE `log_request`
  MODIFY `id_log_request` bigint(21) NOT NULL AUTO_INCREMENT;
