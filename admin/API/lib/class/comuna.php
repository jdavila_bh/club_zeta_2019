<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 25/09/2017
 * Time: 15:23
 */

class comuna extends db{

    private $id;
    private $nombre;
    private $id_provincia;
    private $estado;
    private $fecha_ingreso;
    private $fecha_modificacion;

    private $bd;
    private $estadoSql;
    private $matrizDatos;

    function __construct()
    {
        $this->id = 0;
        $this->nombre = '';
        $this->id_provincia = 0;
        $this->fecha_ingreso = '';
        $this->fecha_modificacion = '';

        $this->estadoSql = 0;
        $this->matrizDatos = array();
    }


    function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'nombre' =>$this->getNombre(),
            'id_provincia' =>$this->getIdProvincia(),
            'estado' =>$this->getEstado(),
            'fecha_ingreso' => $this->getFechaIngreso(),
            'fecha_modificacion' => $this->getFechaModificacion()
        );
    }

    public function traerRegistro($id_registro)
    {

        $bd = new db();

        $tabla = 'comuna';
        $columnas = array(
            'id_comuna',
            'nombre_comuna',
            'id_provincia',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_comuna='.trim($id_registro));
        $bd->select();
        $bd->ejecutar();

        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_comuna);
            $this->setNombre(utf8_encode($row->nombre_comuna));
            $this->setIdProvincia($row->id_provincia);
            $this->setEstado($row->estado);
            $this->setFechaIngreso($row->fecha_ingreso);
            $this->setFechaModificacion($row->fecha_modificacion);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    public function traerTodosRegistro(array $filtroRef)
    {

        $filtro = '';

        if(isset($filtroRef['estado']))
        {
            $filtro.=' AND estado='.$filtroRef['estado'];
        }

        if(isset($filtroRef['id_provincia']))
        {
            $filtro.=' AND id_provincia='.$filtroRef['id_provincia'];
        }

        $bd = new db();

        $tabla = 'comuna';
        $columnas = array(
            'id_comuna',
            'nombre_comuna',
            'id_provincia',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 '.$filtro);
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->resultado);

        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();

    }

    public function estadoRegistro($id_registro,$nuevo_estado)
    {
        $bd = new db();

        $tabla = 'comuna';
        $valores = array(
            'estado' =>$nuevo_estado
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_comuna='.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    public function registrar()
    {
        $bd = new db();

        $tabla = 'comuna';
        $columnas = array(
            'nombre_comuna',
            'id_provincia',
            'estado'
        );
        $valores = array(
            '"'.trim($this->getNombre()).'"',
            '"'.trim($this->getIdProvincia()).'"',
            '"'.(int)trim($this->getEstado()).'"'
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if($bd->getResultadoInsert()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    public function modificar($id_registro)
    {
        $bd = new db();

        $tabla = 'comuna';
        $valores = array(
            'nombre_comuna' => '"' . trim($this->getNombre()) . '"',
            'id_provincia' => '"' . (int)trim($this->getIdProvincia()) . '"',
            'estado' => '"' . (int)trim($this->getEstado()) . '"',
            'fecha_modificacion' => '"'.date('Y-m-d H:i:s').'"'
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_comuna='.$id_registro);
        $bd->update();

        if ($bd->getResultadoUpdate() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return int
     */
    public function getIdProvincia()
    {
        return $this->id_provincia;
    }

    /**
     * @param int $id_provincia
     */
    public function setIdProvincia($id_provincia)
    {
        $this->id_provincia = $id_provincia;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return int
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param int $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return array
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param array $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }

    /**
     * @return string
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param string $fecha_ingreso
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return string
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * @param string $fecha_modificacion
     */
    public function setFechaModificacion($fecha_modificacion)
    {
        $this->fecha_modificacion = $fecha_modificacion;
    }


}