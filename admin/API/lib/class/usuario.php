<?php
@session_start();

class usuario extends db
{

    private $id;
    private $nombre;
    private $apellido_paterno;
    private $apellido_materno;
    private $rut;
    private $email;
    private $telefono;
    private $celular;
    private $clave;
    private $activo;
    private $recibir_info;
    private $recibir_recordatorio;
    private $fecha_creacion;


    private $bd;
    private $estadoSql;
    private $matrizDatos;

    function __construct()
{
    $this->id = 0;
    $this->nombre = '';
    $this->apellido_paterno = '';
    $this->apellido_materno = '';
    $this->rut = '';
    $this->email = '';
    $this->telefono = '';
    $this->celular = '';
    $this->clave = '';
    $this->activo = 0;
    $this->recibir_info = 0;
    $this->recibir_recordatorio = 0;
    $this->fecha_creacion = '';

    $this->estadoSql = 0;
    $this->matrizDatos = array();
}



    function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'apellido_paterno' => $this->getApellidoPaterno(),
            'apellido_materno' => $this->getApellidoMaterno(),
            'rut' => $this->getRut(),
            'email' => $this->getEmail(),
            'telefono' => $this->getTelefono(),
            'celular' => $this->getCelular(),
            'clave' => $this->getClave(),
            'activo' => $this->getActivo(),
            'recibir_info' => $this->getRecibirInfo(),
            'recibir_recordatorio' => $this->getRecibirRecordatorio(),
            'fecha_creacion' => $this->getFechaCreacion()
        );
    }


    public function traerRegistro($id_registro)
    {

        $bd = new db();

        $tabla = 'usuario';
        $columnas = array(
            'id_usuario',
            'nombre_usuario',
            'apellido_paterno_usuario',
            'apellido_materno_usuario',
            'rut_usuario',
            'email_usuario',
            'telefono_usuario',
            'celular_usuario',
            'clave_usuario',
            'activo_usuario',
            'recibir_info_usuario',
            'recibir_recordatorio_usuario',
            'fecha_creacion_usuario'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' id_usuario='.trim($id_registro));
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_usuario);
            $this->setNombre($row->nombre_usuario);
            $this->setApellidoPaterno($row->apellido_paterno_usuario);
            $this->setApellidoMaterno($row->apellido_materno_usuario);
            $this->setRut($row->rut_usuario);
            $this->setEmail($row->email_usuario);
            $this->setTelefono($row->telefono_usuario);
            $this->setCelular($row->celular_usuario);
            $this->setClave($row->clave_usuario);
            $this->setActivo($row->activo_usuario);
            $this->setRecibirInfo($row->recibir_info_usuario);
            $this->setRecibirRecordatorio($row->recibir_recordatorio_usuario);
            $this->setFechaCreacion($row->fecha_creacion_usuario);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();

    }



    public function traerTodosRegistro()
    {

        $bd = new db();

        $tabla = 'usuario';
        $columnas = array(
            'id_usuario',
            'nombre_usuario',
            'apellido_paterno_usuario',
            'apellido_materno_usuario',
            'rut_usuario',
            'email_usuario',
            'telefono_usuario',
            'celular_usuario',
            'clave_usuario',
            'activo_usuario',
            'recibir_info_usuario',
            'recibir_recordatorio_usuario',
            'fecha_creacion_usuario'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1');
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->resultado);

        }else{
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();

    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }/**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }/**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }/**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }/**
     * @return string
     */
    public function getApellidoPaterno()
    {
        return $this->apellido_paterno;
    }/**
     * @param string $apellido_paterno
     */
    public function setApellidoPaterno($apellido_paterno)
    {
        $this->apellido_paterno = $apellido_paterno;
    }/**
     * @return string
     */
    public function getApellidoMaterno()
    {
        return $this->apellido_materno;
    }/**
     * @param string $apellido_materno
     */
    public function setApellidoMaterno($apellido_materno)
    {
        $this->apellido_materno = $apellido_materno;
    }/**
     * @return string
     */
    public function getRut()
    {
        return $this->rut;
    }/**
     * @param string $rut
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    }/**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }/**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }/**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }/**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }/**
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }/**
     * @param string $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }/**
     * @return string
     */
    public function getClave()
    {
        return $this->clave;
    }/**
     * @param string $clave
     */
    public function setClave($clave)
    {
        $this->clave = $clave;
    }/**
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }/**
     * @param int $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }/**
     * @return int
     */
    public function getRecibirInfo()
    {
        return $this->recibir_info;
    }/**
     * @param int $recibir_info
     */
    public function setRecibirInfo($recibir_info)
    {
        $this->recibir_info = $recibir_info;
    }/**
     * @return int
     */
    public function getRecibirRecordatorio()
    {
        return $this->recibir_recordatorio;
    }/**
     * @param int $recibir_recordatorio
     */
    public function setRecibirRecordatorio($recibir_recordatorio)
    {
        $this->recibir_recordatorio = $recibir_recordatorio;
    }/**
     * @return string
     */
    public function getFechaCreacion()
    {
        return $this->fecha_creacion;
    }/**
     * @param string $fecha_creacion
     */
    public function setFechaCreacion($fecha_creacion)
    {
        $this->fecha_creacion = $fecha_creacion;
    }/**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }/**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }/**
     * @return int
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }/**
     * @param int $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }/**
     * @return array
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }/**
     * @param array $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }
}