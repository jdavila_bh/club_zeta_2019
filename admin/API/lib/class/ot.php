<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 25/09/2017
 * Time: 17:36
 */

class ot {

    private $id;
    private $id_cliente;
    private $solicitud;
    private $solicita;
    private $condicion_comercial;
    private $id_tipo_contrato;
    private $id_tipo_pago;
    private $uf_dia;
    private $valor_hora_uf;
    private $nfactura;
    private $timming_dias;
    private $hora_proyecto;
    private $comentario;
    private $nota;
    private $estado;
    private $fecha_ingreso;
    private $fecha_modificacion;

    private $bd;
    private $estadoSql;
    private $matrizDatos;
    private $ultimoId;
    private $filtroSql;

    function __construct()
    {
        $this->id = 0;
        $this->id_cliente = 0;
        $this->solicitud = '';
        $this->solicita = '';
        $this->condicion_comercial = '';
        $this->id_tipo_contrato = 0;
        $this->id_tipo_pago = 0;
        $this->uf_dia = 0;
        $this->valor_hora_uf = 0;
        $this->nfactura = 0;
        $this->timming_dias = 0;
        $this->hora_proyecto = 0;
        $this->comentario = '';
        $this->nota = '';
        $this->estado = 0;
        $this->fecha_ingreso = '';
        $this->fecha_modificacion = '';

        $this->estadoSql = 0;
        $this->matrizDatos = array();
        $this->filtroSql = array();
        $this->ultimoId = 0;
    }


    function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'id_cliente' => $this->getIdCliente(),
            'solicitud' =>$this->getSolicitud(),
            'solicita' =>$this->getSolicita(),
            'condicion_comercial' =>$this->getCondicionComercial(),
            'id_tipo_contrato' =>$this->getIdTipoContrato(),
            'id_tipo_pago' =>$this->getIdTipoPago(),
            'uf_dia' =>$this->getUfDia(),
            'valor_hora_uf' =>$this->getValorHoraUf(),
            'nfactura' =>$this->getNfactura(),
            'timming_dias' =>$this->getTimmingDias(),
            'hora_proyecto' =>$this->getHoraProyecto(),
            'comentario' =>$this->getComentario(),
            'nota' =>$this->getNota(),
            'estado' =>$this->getEstado(),
            'fecha_ingreso' =>$this->getFechaIngreso(),
            'fecha_modificacion' =>$this->getFechaModificacion()
        );
    }

    public function traerRegistro($id_registro)
    {

        $bd = new db();

        $tabla = 'ot';
        $columnas = array(
            'id_ot',
            'id_cliente',
            'solicitud_ot',
            'solicita_ot',

            'condicion_comercial_ot',
            'id_tipo_contrato',
            'id_tipo_pago',

            'uf_dia_ot',
            'valor_hora_uf_ot',
            'nfactura_ot',

            'timming_dias_ot',
            'hora_proyecto_ot',
            'comentario_ot',

            'nota_ot',
            'estado',
            'fecha_ingreso',

            'fecha_modificacion'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_ot='.trim($id_registro));
        $bd->select();
        $bd->ejecutar();

        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_ot);
            $this->setIdCliente($row->id_cliente);
            $this->setSolicitud($row->solicitud_ot);
            $this->setSolicita($row->solicita_ot);

            $this->setCondicionComercial($row->condicion_comercial_ot);
            $this->setIdTipoContrato($row->id_tipo_contrato);
            $this->setIdTipoPago($row->id_tipo_pago);

            $this->setUfDia($row->uf_dia_ot);
            $this->setValorHoraUf($row->valor_hora_uf_ot);
            $this->setNfactura($row->nfactura_ot);

            $this->setTimmingDias($row->timming_dias_ot);
            $this->setHoraProyecto($row->hora_proyecto_ot);
            $this->setComentario($row->comentario_ot);

            $this->setNota($row->nota_ot);
            $this->setEstado($row->estado);
            $this->setFechaIngreso($row->fecha_ingreso);

            $this->setFechaModificacion($row->fecha_modificacion);

        }else{
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    public function traerTodosRegistro()
    {
        $filtro = '';
        $filtro_ref = $this->getFiltroSql();

        if(isset($filtro_ref['estado']))
            $filtro.=' AND estado ='.$filtro_ref['estado'];


        $bd = new db();

        $tabla = 'ot';
        $columnas = array(
            'id_ot',
            'id_cliente',
            'solicitud_ot',
            'solicita_ot',

            'condicion_comercial_ot',
            'id_tipo_contrato',
            'id_tipo_pago',

            'uf_dia_ot',
            'valor_hora_uf_ot',
            'nfactura_ot',

            'timming_dias_ot',
            'hora_proyecto_ot',
            'comentario_ot',

            'nota_ot',
            'estado',
            'fecha_ingreso',

            'fecha_modificacion'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 '.$filtro);
        $bd->select();
        $bd->ejecutar();


        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->resultado);

        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    public function estadoRegistro($id_registro,$nuevo_estado)
    {
        $bd = new db();

        $tabla = 'ot';
        $valores = array(
            'estado' =>$nuevo_estado
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_ot = '.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    public function registrar()
    {
        $bd = new db();

        $tabla = 'ot';
        $columnas = array(
            'id_cliente',
            'solicitud_ot',
            'solicita_ot',
            'condicion_comercial_ot',

            'id_tipo_contrato',
            'id_tipo_pago',
            'uf_dia_ot',

            'valor_hora_uf_ot',
            'nfactura_ot',
            'timming_dias_ot',

            'hora_proyecto_ot',
            'comentario_ot',
            'nota_ot',

            'estado'
        );
        $valores = array(
            '"'.trim($this->getIdCliente()).'"',
            '"'.trim($this->getSolicitud()).'"',
            '"'.trim($this->getSolicita()).'"',
            '"'.trim($this->getCondicionComercial()).'"',

            '"'.(int)trim($this->getIdTipoContrato()).'"',
            '"'.(int)trim($this->getIdTipoPago()).'"',
            '"'.(int)trim($this->getUfDia()).'"',

            '"'.(int)trim($this->getValorHoraUf()).'"',
            '"'.(int)trim($this->getNfactura()).'"',
            '"'.(int)trim($this->getTimmingDias()).'"',

            '"'.(int)trim($this->getHoraProyecto()).'"',
            '"'.trim($this->getComentario()).'"',
            '"'.trim($this->getNota()).'"',

            '"'.(int)trim($this->getEstado()).'"'
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if($bd->getResultadoInsert()==1)
        {
            $this->setUltimoId($bd->last_id);
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    public function modificar($id_registro)
    {
        $bd = new db();

        $tabla = 'ot';
        $valores = array(
            'id_cliente' =>'"'.trim($this->getIdCliente()).'"',
            'solicitud_ot' =>'"'.trim($this->getSolicitud()).'"',
            'solicita_ot' =>'"'.trim($this->getSolicita()).'"',
            'condicion_comercial_ot' =>'"'.trim($this->getCondicionComercial()).'"',

            'id_tipo_contrato' =>'"'.(int)trim($this->getIdTipoContrato()).'"',
            'id_tipo_pago' =>'"'.(int)trim($this->getIdTipoPago()).'"',
            'uf_dia_ot' =>'"'.(int)trim($this->getUfDia()).'"',

            'valor_hora_uf_ot' =>'"'.(int)trim($this->getValorHoraUf()).'"',
            'nfactura_ot' =>'"'.(int)trim($this->getNfactura()).'"',
            'timming_dias_ot' =>'"'.(int)trim($this->getTimmingDias()).'"',

            'hora_proyecto_ot' =>'"'.(int)trim($this->getHoraProyecto()).'"',
            'comentario_ot' =>'"'.trim($this->getComentario()).'"',
            'nota_ot' =>'"'.trim($this->getNota()).'"',

            'estado' =>'"'.(int)trim($this->getEstado()).'"',
            'fecha_modificacion' =>'"'.date('Y-m-d H:i:s').'"',
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_ot='.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdCliente()
    {
        return $this->id_cliente;
    }

    /**
     * @param int $id_cliente
     */
    public function setIdCliente($id_cliente)
    {
        $this->id_cliente = $id_cliente;
    }

    /**
     * @return string
     */
    public function getSolicitud()
    {
        return $this->solicitud;
    }

    /**
     * @param string $solicitud
     */
    public function setSolicitud($solicitud)
    {
        $this->solicitud = $solicitud;
    }

    /**
     * @return string
     */
    public function getSolicita()
    {
        return $this->solicita;
    }

    /**
     * @param string $solicita
     */
    public function setSolicita($solicita)
    {
        $this->solicita = $solicita;
    }

    /**
     * @return string
     */
    public function getCondicionComercial()
    {
        return $this->condicion_comercial;
    }

    /**
     * @param string $condicion_comercial
     */
    public function setCondicionComercial($condicion_comercial)
    {
        $this->condicion_comercial = $condicion_comercial;
    }

    /**
     * @return int
     */
    public function getIdTipoContrato()
    {
        return $this->id_tipo_contrato;
    }

    /**
     * @param int $id_tipo_contrato
     */
    public function setIdTipoContrato($id_tipo_contrato)
    {
        $this->id_tipo_contrato = $id_tipo_contrato;
    }

    /**
     * @return int
     */
    public function getIdTipoPago()
    {
        return $this->id_tipo_pago;
    }

    /**
     * @param int $id_tipo_pago
     */
    public function setIdTipoPago($id_tipo_pago)
    {
        $this->id_tipo_pago = $id_tipo_pago;
    }

    /**
     * @return int
     */
    public function getUfDia()
    {
        return $this->uf_dia;
    }

    /**
     * @param int $uf_dia
     */
    public function setUfDia($uf_dia)
    {
        $this->uf_dia = $uf_dia;
    }

    /**
     * @return int
     */
    public function getValorHoraUf()
    {
        return $this->valor_hora_uf;
    }

    /**
     * @param int $valor_hora_uf
     */
    public function setValorHoraUf($valor_hora_uf)
    {
        $this->valor_hora_uf = $valor_hora_uf;
    }

    /**
     * @return int
     */
    public function getNfactura()
    {
        return $this->nfactura;
    }

    /**
     * @param int $nfactura
     */
    public function setNfactura($nfactura)
    {
        $this->nfactura = $nfactura;
    }

    /**
     * @return int
     */
    public function getTimmingDias()
    {
        return $this->timming_dias;
    }

    /**
     * @param int $timming_dias
     */
    public function setTimmingDias($timming_dias)
    {
        $this->timming_dias = $timming_dias;
    }

    /**
     * @return int
     */
    public function getHoraProyecto()
    {
        return $this->hora_proyecto;
    }

    /**
     * @param int $hora_proyecto
     */
    public function setHoraProyecto($hora_proyecto)
    {
        $this->hora_proyecto = $hora_proyecto;
    }

    /**
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * @param string $comentario
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    }

    /**
     * @return string
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * @param string $nota
     */
    public function setNota($nota)
    {
        $this->nota = $nota;
    }

    /**
     * @return int
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param int $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return string
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param string $fecha_ingreso
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return string
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * @param string $fecha_modificacion
     */
    public function setFechaModificacion($fecha_modificacion)
    {
        $this->fecha_modificacion = $fecha_modificacion;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return int
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param int $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return array
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param array $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }

    /**
     * @return mixed
     */
    public function getUltimoId()
    {
        return $this->ultimoId;
    }

    /**
     * @param mixed $ultimoId
     */
    public function setUltimoId($ultimoId)
    {
        $this->ultimoId = $ultimoId;
    }

    /**
     * @return array
     */
    public function getFiltroSql()
    {
        return $this->filtroSql;
    }

    /**
     * @param array $filtroSql
     */
    public function setFiltroSql($filtroSql)
    {
        $this->filtroSql = $filtroSql;
    }


}