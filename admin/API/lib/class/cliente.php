<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 25/09/2017
 * Time: 10:59
 */

class cliente {

    private $id;
    private $nombre;
    private $rut;
    private $direccion;
    private $id_region;
    private $id_provincia;
    private $id_comuna;
    private $telefono;
    private $celular;
    private $email;
    private $giro;
    private $estado;
    private $fecha_ingreso;
    private $fecha_modificacion;


    private $bd;
    private $estadoSql;
    private $matrizDatos;

    function __construct()
    {
        $this->id = 0;
        $this->nombre = '';
        $this->rut = '';
        $this->direccion = '';
        $this->id_region = 0;
        $this->id_provincia = 0;
        $this->id_comuna = 0;
        $this->telefono = '';
        $this->celular = '';
        $this->email = '';
        $this->giro = '';

        $this->estado = 0;
        $this->fecha_ingreso = '';
        $this->fecha_modificacion = '';

        $this->estadoSql = 0;
        $this->matrizDatos = array();
    }


    function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'nombre' =>$this->getNombre(),
            'rut' =>$this->getRut(),
            'direccion' =>$this->getDireccion(),
            'id_region' =>$this->getIdRegion(),
            'id_provincia' =>$this->getIdProvincia(),
            'id_comuna' =>$this->getIdComuna(),
            'telefono' =>$this->getTelefono(),
            'celular' =>$this->getCelular(),
            'email' =>$this->getEmail(),
            'giro' =>$this->getGiro(),
            'estado' =>$this->getEstado(),
            'fecha_ingreso' =>$this->getFechaIngreso(),
            'fecha_modificacion' =>$this->getFechaModificacion()
        );
    }


    public function traerRegistro($id_registro)
    {

        $bd = new db();

        $tabla = 'cliente';
        $columnas = array(
            'id_cliente',
            'nombre_cliente',
            'rut_cliente',
            'direccion_cliente',
            'id_region',
            'id_provincia',
            'id_comuna',
            'telefono_cliente',
            'celular_cliente',
            'email_cliente',
            'giro_cliente',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_cliente='.trim($id_registro));
        $bd->select();
        $bd->ejecutar();

        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId((int)$row->id_cliente);
            $this->setNombre(utf8_encode($row->nombre_cliente));
            $this->setRut($row->rut_cliente);
            $this->setDireccion(utf8_encode($row->direccion_cliente));
            $this->setIdRegion($row->id_region);
            $this->setIdProvincia($row->id_provincia);
            $this->setIdComuna($row->id_comuna);
            $this->setTelefono($row->telefono_cliente);
            $this->setCelular($row->celular_cliente);
            $this->setEmail($row->email_cliente);
            $this->setGiro(utf8_encode($row->giro_cliente));

            $this->setEstado($row->estado);
            $this->setFechaIngreso($row->fecha_ingreso);
            $this->setFechaModificacion($row->fecha_modificacion);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();

    }


    public function traerTodosRegistro($filtroRef)
    {

        $filtro = '';

        if(isset($filtroRef['estado']))
        {
            $filtro.=' AND estado='.$filtroRef['estado'];
        }

        $bd = new db();

        $tabla = 'cliente';
        $columnas = array(
            'id_cliente',
            'nombre_cliente',
            'rut_cliente',
            'direccion_cliente',
            'id_region',
            'id_provincia',
            'id_comuna',
            'telefono_cliente',
            'celular_cliente',
            'email_cliente',
            'giro_cliente',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 '.$filtro);
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->resultado);

        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();

    }

    public function estadoRegistro($id_registro,$nuevo_estado)
    {
        $bd = new db();

        $tabla = 'cliente';
        $valores = array(
            'estado' =>$nuevo_estado
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_cliente = '.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function registrar()
    {
        $bd = new db();

        $tabla = 'cliente';
        $columnas = array(
            'nombre_cliente',
            'rut_cliente',
            'direccion_cliente',

            'id_region',
            'id_provincia',
            'id_comuna',

            'telefono_cliente',
            'celular_cliente',
            'email_cliente',

            'giro_cliente',
            'estado'
        );
        $valores = array(
            '"'.trim($this->getNombre()).'"',
            '"'.trim($this->getRut()).'"',
            '"'.trim($this->getDireccion()).'"',

            '"'.(int)trim($this->getIdRegion()).'"',
            '"'.(int)trim($this->getIdProvincia()).'"',
            '"'.(int)trim($this->getIdComuna()).'"',

            '"'.trim($this->getTelefono()).'"',
            '"'.trim($this->getCelular()).'"',
            '"'.trim($this->getEmail()).'"',

            '"'.trim($this->getGiro()).'"',
            '"'.(int)trim($this->getEstado()).'"'
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if($bd->getResultadoInsert()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function modificar($id_registro)
    {
        $bd = new db();

        $tabla = 'cliente';
        $valores = array(
            'nombre_cliente' =>'"'.trim($this->getNombre()).'"',
            'rut_cliente' =>'"'.trim($this->getRut()).'"',
            'direccion_cliente' =>'"'.trim($this->getDireccion()).'"',

            'id_region' =>'"'.(int)trim($this->getIdRegion()).'"',
            'id_provincia' =>'"'.(int)trim($this->getIdProvincia()).'"',
            'id_comuna' =>  '"'.(int)trim($this->getIdComuna()).'"',

            'telefono_cliente' =>'"'.trim($this->getTelefono()).'"',
            'celular_cliente' =>'"'.$this->getCelular().'"',
            'email_cliente' =>'"'.$this->getEmail().'"',
            'giro_cliente' =>'"'.$this->getGiro().'"',
            'estado' =>'"'.(int)$this->getEstado().'"'
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_cliente='.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * @param string $rut
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    }

    /**
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param string $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return int
     */
    public function getIdRegion()
    {
        return $this->id_region;
    }

    /**
     * @param int $id_region
     */
    public function setIdRegion($id_region)
    {
        $this->id_region = $id_region;
    }

    /**
     * @return int
     */
    public function getIdProvincia()
    {
        return $this->id_provincia;
    }

    /**
     * @param int $id_provincia
     */
    public function setIdProvincia($id_provincia)
    {
        $this->id_provincia = $id_provincia;
    }

    /**
     * @return int
     */
    public function getIdComuna()
    {
        return $this->id_comuna;
    }

    /**
     * @param int $id_comuna
     */
    public function setIdComuna($id_comuna)
    {
        $this->id_comuna = $id_comuna;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param string $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getGiro()
    {
        return $this->giro;
    }

    /**
     * @param string $giro
     */
    public function setGiro($giro)
    {
        $this->giro = $giro;
    }

    /**
     * @return int
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param int $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return string
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param string $fecha_ingreso
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return string
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * @param string $fecha_modificacion
     */
    public function setFechaModificacion($fecha_modificacion)
    {
        $this->fecha_modificacion = $fecha_modificacion;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return int
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param int $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return array
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param array $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }




}