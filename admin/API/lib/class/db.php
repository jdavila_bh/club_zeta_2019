<?php
/**
 * Created by PhpStorm.
 * User: paul
 * Date: 30-05-15
 * Time: 14:14
 */
//error_reporting(0);

class db {


    //Datos de la conexion de la base de datos
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $baseDeDatos = DB_NOMBRE;

    public $estadoBD;

    public $link;

    //datos para hacer una consulta
    public $columnas;
    public $tabla;
    public $filtro;
    public $orden;
    public $agrupar;
    public $limit;

    public $valores; // Valores que se pasan en el INSERT
    public $updateValores;
    public $contenedorUpdateValores;

    public $consulta;
    public $resultado;
    public $registros;
	public $last_id;

    public $num_registros;

    public $resultadoDelete; // resultado despues de un delete si es 1 es que se realizo exitoso el delete
    public $resultadoUpdate;
    public $resultadoInsert;

    public function __construct(){

        $this->conexion();
        //instanciamos atributos base de datos
        $this->columnas='';
        $this->tabla='';
        $this->filtro='';
        $this->orden='';
        $this->agrupar='';
        $this->limit='';

        $this->valores='';
        $this->updateValores='';
        $this->contenedorUpdateValores=array();

        $this->consulta='';
        $this->resultado='';
        $this->registros='';
		$this->last_id=0;

        $this->num_registros=0;

        $this->resultadoDelete=0;
        $this->resultadoUpdate=0;
        $this->resultadoInsert=0;


    }

    public function getDatos()
    {
        return var_dump($this);
    }


    /**
     * Inicializamos la conexion a la base e datos
     */
    public function conexion()
    {
        $this ->link = mysql_connect($this->host,$this->user,$this->pass)or die('Error al inicializar la conexion a la base de datos');
        mysql_select_db($this->baseDeDatos,$this->link)or die('no se puedo seleccionar la base de datos');
        $this->estadoBD = "Conexion exitosa a la base de datos: ".$this->baseDeDatos;
    }

    public function setColumnas(array $columnas){

        $this->columnas = implode(",",$columnas);

    }
    public function setValores(array $valores){

        $this->valores = implode(",",$valores);

    }

    public function setUpdateValores(array $valores)
    {
        $this->updateValores='';
        $this->contenedorUpdateValores = array();

        foreach($valores as $key => $valores)
        {
            array_push($this->contenedorUpdateValores, $key.'='.$valores);

        }

        $this->updateValores = implode(',',$this->contenedorUpdateValores);


    }

    //modificadores
	
    public function setTabla($tabla){
        $this->tabla = $tabla;
    }

    public function setFiltro($filtro){
        $this->filtro=$filtro;

    }

    public function setOrden($orden){
        $this->orden=' ORDER BY '.$orden;

    }
    public function setAgrupar($agrupar){
        $this->agrupar=' GROUP BY '.$agrupar;
    }

    public function setLimit($inicio,$cantidad){
        $this->limit= ' LIMIT '.$inicio.','.$cantidad;
    }


    //Accesadores
	
    public function getSelect(){
        return "SELECT ".$this->columnas." FROM ".$this->tabla." WHERE ".$this->filtro." ".$this->agrupar." ".$this->orden.$this->limit;
    }

    public function getUpdateValores(){
        return $this->updateValores;
    }

    public function getIdUltimoRegistro(){
        return mysql_insert_id($this->link);

    }

    public function getRegistros(){

        return $this->registros;
    }

    //metodos

    public function select()
    {

        $this->consulta = "SELECT ".$this->columnas." FROM ".$this->tabla." WHERE ".$this->filtro." ".$this->agrupar." ".$this->orden.$this->limit;
    }

    public function delete(){
        $this->consulta = 'DELETE FROM '.$this->tabla.' WHERE '.$this->filtro;
        $this->resultado = mysql_query($this->consulta,$this->link);
        $this->resultadoDelete = mysql_affected_rows($this->link);
        if($this->resultadoDelete>0)
        {
            $this->resultadoDelete = 1;
        }else{
            $this->resultadoDelete = 0;
        }

    }

    public function insert(){

        $this->consulta = 'INSERT INTO '.$this->tabla.' ('.$this->columnas.')VALUES('.$this->valores.')';
        $this->resultado = mysql_query($this->consulta,$this->link);
        $this->resultadoInsert = mysql_affected_rows($this->link);
		$this->last_id = mysql_insert_id($this->link);
        if($this->resultadoInsert>0)
        {
            $this->resultadoInsert = 1;
        }else{
            $this->resultadoInsert = 0;
        }

    }

    public function getResultadoInsert()
    {
        return $this->resultadoInsert;

    }

    public function update(){

        $this->consulta = 'UPDATE '.$this->tabla.' SET '.$this->updateValores.'   WHERE '.$this->filtro;
        $this->resultado = mysql_query($this->consulta,$this->link) or die(json_encode(array('estado' => '0', 'query' => $this->consulta, 'message' => 'Ocurrió un error con la base de datos', 'error' => mysql_error())));;
        $this->resultadoUpdate = mysql_affected_rows($this->link);
        if($this->resultadoUpdate>0)
        {
            $this->resultadoUpdate = 1;
        }else{
            $this->resultadoUpdate = 0;
        }

    }

    public function getResultadoUpdate()
    {
        return $this->resultadoUpdate;

    }

    public function ejecutar()
    {
        $this->resultado = mysql_query($this->consulta,$this->link);
        $this->num_registros = mysql_num_rows($this->resultado);
    }

    public function resultado(){
        return $this->registros = mysql_fetch_array($this->resultado,MYSQL_BOTH);
    }

    public function resultadoArray(){
        return $this->registros = mysql_fetch_array($this->resultado);
    }

    public function resultadoAsociativo(){
        return $this->registros = mysql_fetch_array($this->resultado,MYSQL_ASSOC);

    }

    public function resultadoObjetual(){
        return $this->registros = mysql_fetch_object($this->resultado);

    }

    /**
     * @return string
     */
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * @param string $resultado
     */
    public function setResultado($resultado)
    {
        $this->resultado = $resultado;
    }


}