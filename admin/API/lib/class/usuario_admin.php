<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo 5
 * Date: 05-02-2018
 * Time: 12:12
 */
@session_start();

class usuario_admin extends db
{
    private $id;
    private $nombre;
    private $apellido;
    private $user;
    private $pass;
    private $rut;
    private $id_tipo;
    private $fecha_creacion;
    private $fecha_modificacion;
    private $email;
    private $detalle;
    private $grafico;
    private $colegios;
    private $comprobante;
    private $admusuarios;


    private $bd;
    private $estadoSql;
    private $matrizDatos;


    function __construct()
    {
        $this->id = 0;
        $this->nombre = '';
        $this->apellido = '';
        $this->user = '';
        $this->pass = '';
        $this->rut = '';
        $this->id_tipo = 0;
        $this->fecha_creacion = '';
        $this->fecha_modificacion = '';
        $this->email = '';
        $this->detalle = 0;
        $this->grafico = 0;
        $this->colegios = 0;
        $this->comprobante = 0;
        $this->admusuarios = 0;

        $this->estadoSql = 0;
        $this->matrizDatos = array();
    }


    /**
     * @return array
     */
    public function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'nombre' => $this->getNombre(),
            'apellido' => $this->getApellido(),
            'user' => $this->getUser(),
            'pass' => $this->getpass(),
            'rut' => $this->getRut(),
            'id_tipo' => $this->getIdTipo(),
            'fecha_creacion' => $this->getFechaCreacion(),
            'fecha_modificacion' => $this->getFechaModificacion(),
            'email' => $this->getEmail(),
            'detalle' => $this->getDetalle(),
            'grafico' => $this->getGrafico(),
            'colegios' => $this->getColegios(),
            'comprobante' => $this->getComprobante(),
            'admusuarios' => $this->getadmusuarios()
        );
    }

    /**
     * @param $id_registro
     * @return mixed
     */
    public function traerRegistro($id_registro)
    {
        $bd = new db();

        $tabla = 'usuario_admin';
        $columnas = array(
            'id_usuario_admin',
            'nombre_usuario_admin',
            'apellido_usuario_admin',
            'user_usuario_admin',
            'pass_usuario_admin',
            'rut_usuario_admin',
            'id_tipo_usuario_admin',
            'fecha_creacion_usuario_admin',
            'fecha_modificacion_usuario_admin',
            'email_usuario_admin',
            'detalle_compra',
            'graficos',
            'colegios',
            'comprobante',
            'admusuarios'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_usuario_admin=' . trim($id_registro));
        $bd->select();
        $bd->ejecutar();
        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_usuario_admin);
            $this->setNombre($row->nombre_usuario_admin);
            $this->setApellido($row->user_usuario_admin);
            $this->setPass($row->pass_usuario_admin);
            $this->setRut($row->rut_usuario_admin);
            $this->setIdTipo($row->id_tipo_usuario_admin);
            $this->setFechaCreacion($row->fecha_creacion_usuario_admin);
            $this->setFechaModificacion($row->fecha_modificacion_usuario_admin);
            $this->setEmail($row->email_usuario_admin);
            $this->setDetalle($row->detalle_compra);
            $this->setColegios($row->colegios);
            $this->setComprobante($row->comprobante);
            $this->setAdmusuarios($row->admusuarios);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }


    /**
     * @param $id_registro
     * @return mixed
     */
    public function traerTodosRegistro($id_registro)
    {

        $bd = new db();

        $tabla = 'usuario_admin';
        $columnas = array(
            'id_usuario_admin',
            'nombre_usuario_admin',
            'apellido_usuario_admin',
            'user_usuario_admin',
            //'pass_usuario_admin',
            'rut_usuario_admin',
            'id_tipo_usuario_admin',
            'fecha_creacion_usuario_admin',
            'fecha_modificacion_usuario_admin',
            'email_usuario_admin',
            'detalle_compra',
            'graficos',
            'colegios',
            'comprobante',
            'admusuarios'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 ');
        $bd->select();
        $bd->ejecutar();
        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->getResultado());

            /*
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_usuario_admin);
            $this->setNombre($row->nombre_usuario_admin);
            $this->setApellido($row->user_usuario_admin);
            $this->setPass($row->pass_usuario_admin);
            $this->setRut($row->rut_usuario_admin);
            $this->setIdTipo($row->id_tipo_usuario_admin);
            $this->setFechaCreacion($row->fecha_creacion_usuario_admin);
            $this->setFechaModificacion($row->fecha_modificacion_usuario_admin);
            $this->setEmail($row->email_usuario_admin);
            $this->setDetalle($row->detalle_compra);
            $this->setColegios($row->colegios);
            $this->setComprobante($row->comprobante);
            $this->setAdmusuarios($row->admusuarios);
            */
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    /**
     * @param $user
     * @param $pass
     * @return mixed
     */
    public function login($user, $pass)
    {
        $bd = new db();

        $tabla = 'usuario_admin';
        $columnas = array(
            'id_usuario_admin',
            'nombre_usuario_admin',
            'apellido_usuario_admin',
            'user_usuario_admin',
            'pass_usuario_admin',
            'rut_usuario_admin',
            'id_tipo_usuario_admin',
            'fecha_creacion_usuario_admin',
            'fecha_modificacion_usuario_admin',
            'email_usuario_admin',
            'detalle_compra',
            'graficos',
            'colegios',
            'comprobante',
            'admusuarios'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('user_usuario_admin="' . trim($user) . '" AND pass_usuario_admin="' . hash("whirlpool", (trim($pass))). '"');
        $bd->select();
        $bd->ejecutar();
        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();
            $this->activarSesionUsuario($row);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    /**
     * @param $registro
     */
    public function activarSesionUsuario($registro)
    {
        $_SESSION['sesion_usuario_activa'] = $registro;
        $_SESSION['sesion_usuario_activa_estado'] = 1;

        $_SESSION['permiso1'] = $this->getDetalle();
        $_SESSION['permiso2'] = $this->getGrafico();
        $_SESSION['permiso3'] = $this->getColegios();
        $_SESSION['permiso4'] = $this->getComprobante();
        $_SESSION['permiso5'] = $this->getAdmusuarios();

    }

    /**
     * @return mixed
     */
    public function traerSesionActiva()
    {
        return $_SESSION['sesion_usuario_activa'];
    }

    /**
     * @param $id_registro
     * @param $nueva_clave
     * @return mixed
     */
    public function claveRegistro($id_registro, $nueva_clave)
    {
        $bd = new db();

        $tabla = 'usuario_admin';
        $valores = array(
            'pass_usuario_admin' => "'" . md5($nueva_clave) . "'"
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_usuario_admin = ' . $id_registro);
        $bd->update();

        if ($bd->getResultadoUpdate() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    /**
     * @return mixed
     */
    public function registrar()
    {
        $bd = new db();

        $tabla = 'usuario_admin';
        $columnas = array(
            'id_usuario_admin',
            'nombre_usuario_admin',
            'apellido_usuario_admin',
            'user_usuario_admin',
            'pass_usuario_admin',
            'rut_usuario_admin',
            'id_tipo_usuario_admin',
            'fecha_creacion_usuario_admin',
            'fecha_modificacion_usuario_admin',
            'email_usuario_admin',
            'detalle_compra',
            'graficos',
            'colegios',
            'comprobante',
            'admusuarios'
        );

        $valores = array(
            '"' . trim($this->getNombre()) . '"',
            '"' . trim($this->getApellido()) . '"',
            '"' . trim($this->getRut()) . '"',
            '"' . trim($this->getEmail()) . '"',
            '"' . (int)trim($this->getIdTipo()) . '"',

            '"' . trim($this->getUser()) . '"',
            '"' . md5(trim($this->getPass())) . '"',

            '"' . trim(date('Y-m-d H:i:s')) . '"',
            //'"'.trim($this->getFechaModificacion()).'"',

            '"' . trim($this->getDetalle()) . '"',
            '"' . trim($this->getGrafico()) . '"',
            '"' . trim($this->getColegios()) . '"',
            '"' . trim($this->getComprobante()) . '"',
            '"' . trim($this->getAdmusuarios()) . '"',
            1
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if ($bd->getResultadoInsert() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    /**
     * @param $id_registro
     * @return mixed
     */
    public function modificar($id_registro)
    {
        $bd = new db();

        $tabla = 'usuario_admin';
        $valores = array(
            'nombre_usuario_admin' => '"' . trim($this->getNombre()) . '"',
            'apellido_usuario_admin' => '"' . trim($this->getApellido()) . '"',
            'user_usuario_admin' => '"' . trim($this->getUser()) . '"',
            'rut_usuario_admin' => '"' . trim($this->getRut()) . '"',
            'id_tipo_usuario_admin' => '"' . trim($this->getIdTipo()) . '"',

            'fecha_modificacion_usuario_admin' => '"' . date('Y-m-d H:i:s') . '"',
            'email_usuario_admin' => '"' . trim($this->getEmail()) . '"'
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_usuario_admin=' . $id_registro);
        $bd->update();

        if ($bd->getResultadoUpdate() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    /**
     * @param $userName
     * @return mixed
     */
    public function verificarUserName($userName)
    {
        $bd = new db();

        $tabla = 'usuario_admin';
        $columnas = array(
            'id_usuario_admin'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('user_usuario_admin="' . trim($userName) . '"');
        $bd->select();
        $bd->ejecutar();
        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);

        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    /**
     * @param $id_registro
     * @return mixed
     */
    public function modificar_accesos($id_registro)
    {
        $bd = new db();

        $tabla = 'usuario_admin';
        $valores = array(
            'fecha_modificacion_usuario_admin' => '"' . date('Y-m-d H:i:s') . '"',

            'detalle_compra' => '"' . trim($this->getDetalle()) . '"',
            'graficos' => '"' . trim($this->getGrafico()) . '"',
            'colegios' => '"' . trim($this->getColegios()) . '"',
            'comprobante' => '"' . trim($this->getComprobante()) . '"',
            'admusuarios' => '"' . trim($this->getAdmusuarios()) . '"',

        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_usuario_admin=' . $id_registro);
        $bd->update();

        if ($bd->getResultadoUpdate() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @param mixed $apellido
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return mixed
     */
    public function getIdTipo()
    {
        return $this->id_tipo;
    }

    /**
     * @return string
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * @param string $rut
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    }

    /**
     * @param mixed $id_tipo
     */
    public function setIdTipo($id_tipo)
    {
        $this->id_tipo = $id_tipo;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fecha_creacion;
    }

    /**
     * @param mixed $fecha_creacion
     */
    public function setFechaCreacion($fecha_creacion)
    {
        $this->fecha_creacion = $fecha_creacion;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * @param mixed $fecha_modificacion
     */
    public function setFechaModificacion($fecha_modificacion)
    {
        $this->fecha_modificacion = $fecha_modificacion;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * @param mixed $detalle
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;
    }

    /**
     * @return mixed
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * @param mixed $grafico
     */
    public function setGrafico($grafico)
    {
        $this->grafico = $grafico;
    }

    /**
     * @return mixed
     */
    public function getColegios()
    {
        return $this->colegios;
    }

    /**
     * @param mixed $colegios
     */
    public function setColegios($colegios)
    {
        $this->colegios = $colegios;
    }

    /**
     * @return mixed
     */
    public function getComprobante()
    {
        return $this->comprobante;
    }

    /**
     * @param mixed $comprobante
     */
    public function setComprobante($comprobante)
    {
        $this->comprobante = $comprobante;
    }

    /**
     * @return mixed
     */
    public function getAdmusuarios()
    {
        return $this->admusuarios;
    }

    /**
     * @param mixed $admusuarios
     */
    public function setAdmusuarios($admusuarios)
    {
        $this->admusuarios = $admusuarios;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return mixed
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param mixed $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return mixed
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param mixed $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }
}


