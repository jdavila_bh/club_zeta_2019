<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 25/09/2017
 * Time: 11:48
 */

class region extends db{

    private $id;
    private $nombre;
    private $iso;
    private $estado;

    private $bd;
    private $estadoSql;
    private $matrizDatos;

    function __construct()
    {
        $this->id = 0;
        $this->nombre = '';
        $this->iso = '';
        $this->estado = 0;

        $this->estadoSql = 0;
        $this->matrizDatos = array();
    }


    function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'nombre' =>$this->getNombre(),
            'iso' =>$this->getIso(),
            'estado' =>$this->getEstado()
        );
    }

    public function traerRegistro($id_registro)
    {

        $bd = new db();

        $tabla = 'region';
        $columnas = array(
            'id_region',
            'nombre_region',
            'ISO_3166_2_CL',
            'estado'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_region='.trim($id_registro));
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_region);
            $this->setNombre(utf8_encode($row->nombre_region));
            $this->setIso($row->ISO_3166_2_CL);
            $this->setEstado($row->estado);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function traerTodosRegistro(array $filtroRef)
    {

        $filtro = '';

        if(isset($filtroRef['estado']))
        {
            $filtro.=' AND estado='.$filtroRef['estado'];
        }

        $bd = new db();

        $tabla = 'region';
        $columnas = array(
            'id_region',
            'nombre_region',
            'ISO_3166_2_CL',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 '.$filtro);
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->resultado);

        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();

    }


    public function estadoRegistro($id_registro,$nuevo_estado)
    {
        $bd = new db();

        $tabla = 'region';
        $valores = array(
            'estado' =>$nuevo_estado
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_region = '.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }



    public function registrar()
    {
        $bd = new db();

        $tabla = 'region';
        $columnas = array(
            'nombre_region',
            'ISO_3166_2_CL',
            'estado'

        );
        $valores = array(
            '"'.trim($this->getNombre()).'"',
            '"'.trim($this->getIso()).'"',
            '"'.(int)trim($this->getEstado()).'"',
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if($bd->getResultadoInsert()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function modificar($id_registro)
    {
        $bd = new db();

        $tabla = 'region';
        $valores = array(
            'nombre_region' =>'"'.trim($this->getNombre()).'"',
            'ISO_3166_2_CL' =>'"'.trim($this->getIso()).'"',
            'estado' =>'"'.(int)trim($this->getEstado()).'"',
            'fecha_modificacion' => "'".date('Y-m-d H:i:s')."'"
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_region='.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * @param mixed $iso
     */
    public function setIso($iso)
    {
        $this->iso = $iso;
    }

    /**
     * @return int
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param int $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return mixed
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param mixed $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return mixed
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param mixed $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }




}