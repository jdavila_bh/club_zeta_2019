<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 25/09/2017
 * Time: 18:51
 */

class comentario_ot {

    private $id;
    private $id_ot;
    private $titulo;
    private $detalle;
    private $orden;
    private $estado;
    private $fecha_ingreso;
    private $fecha_modificacion;

    private $bd;
    private $estadoSql;
    private $matrizDatos;
    private $filtroSql;

    function __construct()
    {
        $this->id = 0;
        $this->id_ot = 0;
        $this->titulo = '';
        $this->detalle = '';
        $this->orden = 0;
        $this->estado = 0;
        $this->fecha_ingreso = '';
        $this->fecha_modificacion = '';

        $this->estadoSql = 0;
        $this->matrizDatos = array();
        $this->filtroSql = array();
    }


    function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'id_ot' =>$this->getIdOt(),
            'titulo' =>$this->getTitulo(),
            'detalle' =>$this->getDetalle(),
            'orden' =>$this->getOrden(),
            'estado' =>$this->getEstado(),
            'fecha_ingreso' =>$this->getFechaIngreso(),
            'fecha_modificacion' =>$this->getFechaModificacion()
        );
    }


    public function traerRegistro($id_registro)
    {

        $filtro = '';
        $filtro_ref = $this->getFiltroSql();

        if(isset($filtro_ref['id_ot']))
            $filtro .= ' AND id_ot='.$filtro_ref['id_ot'];

        if(isset($filtro_ref['orden_comentario_ot']))
            $filtro .= ' AND orden_comentario_ot='.$filtro_ref['orden_comentario_ot'];

        $bd = new db();

        $tabla = 'comentario_ot';
        $columnas = array(
            'id_comentario_ot',
            'id_ot',
            'titulo_comentario_ot',
            'detalle_comentario_ot',
            'orden_comentario_ot',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_comentario_ot='.(int)trim($id_registro).$filtro);
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_comentario_ot);
            $this->setIdOt($row->id_ot);
            $this->setTitulo($row->titulo_comentario_ot);
            $this->setDetalle($row->detalle_comentario_ot);
            $this->setOrden($row->orden_comentario_ot);
            $this->setEstado($row->estado);
            $this->setFechaIngreso($row->fecha_ingreso);
            $this->setFechaModificacion($row->fecha_modificacion);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function traerRegistroOt()
    {

        $filtro = '';
        $filtro_ref = $this->getFiltroSql();

        if(isset($filtro_ref['id_ot']))
            $filtro .= ' AND id_ot='.$filtro_ref['id_ot'];

        if(isset($filtro_ref['orden_comentario_ot']))
            $filtro .= ' AND orden_comentario_ot='.$filtro_ref['orden_comentario_ot'];

        $bd = new db();

        $tabla = 'comentario_ot';
        $columnas = array(
            'id_comentario_ot',
            'id_ot',
            'titulo_comentario_ot',
            'detalle_comentario_ot',
            'orden_comentario_ot',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 '.$filtro);
        $bd->select();
        $bd->ejecutar();

        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_comentario_ot);
            $this->setIdOt($row->id_ot);
            $this->setTitulo($row->titulo_comentario_ot);
            $this->setDetalle($row->detalle_comentario_ot);
            $this->setOrden($row->orden_comentario_ot);
            $this->setEstado($row->estado);
            $this->setFechaIngreso($row->fecha_ingreso);
            $this->setFechaModificacion($row->fecha_modificacion);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function traerTodosRegistro()
    {
        $filtro = '';
        $filtro_ref = $this->getFiltroSql();

        if(isset($filtro_ref['estado']))
            $filtro .= ' AND estado='.$filtro_ref['estado'];

        if(isset($filtro_ref['id_ot']))
            $filtro .= ' AND id_ot='.$filtro_ref['id_ot'];

        $bd = new db();

        $tabla = 'comentario_ot';
        $columnas = array(
            'id_comentario_ot',
            'id_ot',
            'titulo_comentario_ot',
            'detalle_comentario_ot',
            'orden_comentario_ot',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 '.$filtro);
        // ORDEN DE REGISTRO
        if(isset($filtro_ref['orden_registros']))
            $bd->setOrden($filtro_ref['orden_registros']);
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->resultado);

        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();

    }


    public function estadoRegistro($id_registro,$nuevo_estado)
    {
        $bd = new db();

        $tabla = 'comentario_ot';
        $valores = array(
            'estado' =>$nuevo_estado
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_comentario_ot = '.(int)trim($id_registro));
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }



    public function registrar()
    {
        $bd = new db();

        $tabla = 'comentario_ot';
        $columnas = array(
            'id_ot',
            'titulo_comentario_ot',
            'detalle_comentario_ot',
            'orden_comentario_ot',
            'estado'
        );
        $valores = array(
            '"'.(int)trim($this->getIdOt()).'"',
            '"'.trim($this->getTitulo()).'"',
            '"'.trim($this->getDetalle()).'"',
            '"'.trim($this->getOrden()).'"',
            '"'.(int)trim($this->getEstado()).'"'
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if($bd->getResultadoInsert()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }



    public function modificar($id_registro)
    {
        $bd = new db();

        $tabla = 'comentario_ot';
        $valores = array(
            'id_ot' =>'"'.(int)trim($this->getIdOt()).'"',
            'titulo_comentario_ot' =>'"'.trim($this->getTitulo()).'"',
            'detalle_comentario_ot' =>'"'.trim($this->getDetalle()).'"',
            'orden_comentario_ot' =>'"'.trim($this->getOrden()).'"',
            'estado' =>'"'.(int)trim($this->getEstado()).'"',
            'fecha_modificacion' =>'"'.date('Y-m-d H:i:s').'"'
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_comentario_ot='.(int)trim($id_registro));
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdOt()
    {
        return $this->id_ot;
    }

    /**
     * @param mixed $id_ot
     */
    public function setIdOt($id_ot)
    {
        $this->id_ot = $id_ot;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * @param mixed $detalle
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;
    }

    /**
     * @return int
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * @param int $orden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param mixed $fecha_ingreso
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * @param mixed $fecha_modificacion
     */
    public function setFechaModificacion($fecha_modificacion)
    {
        $this->fecha_modificacion = $fecha_modificacion;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return mixed
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param mixed $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return mixed
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param mixed $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }

    /**
     * @return array
     */
    public function getFiltroSql()
    {
        return $this->filtroSql;
    }

    /**
     * @param array $filtroSql
     */
    public function setFiltroSql($filtroSql)
    {
        $this->filtroSql = $filtroSql;
    }

}