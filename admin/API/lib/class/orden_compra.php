<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo 5
 * Date: 06-02-2018
 * Time: 13:05
 */
class orden_compra extends db{
    private $id;
    private $rut;
    private $monto;
    private $id_usuario;
    private $estado;
    private $sid;
    private $nalumno;
    private $calumno;
    private $id_colegio;
    private $producto;
    private $cantidad;
    private $fecha;
    private $fecha_pagado;
    private $tipo_transaccion;
    private $id_transaccion;
    private $tipo_pago;
    private $texto_tipo_pago;
    private $num_cuotas;
    private $fin_tarjeta;
    private $fecha_exp;
    private $mac;
    private $fecha_pagado_cap;
    private $ocupado;
    private $fecha_ingreso;
    private $commercecode;
    private $sessionid;
    private $vci;
    private $texto_vci;
    private $resp_code;
    private $texto_resp_code;
    private $detalle_rechazo;

    private $bd;
    private $estadoSql;
    private $matrizDatos;

    function __construct()
    {
        $this->id = 0;
        $this->rut = '';
        $this->monto = 0;
        $this->id_usuario = 0;
        $this->estado = '';
        $this->sid = 0;
        $this->nalumno = '';
        $this->calumno = '';
        $this->id_colegio = 0;
        $this->producto = 0;
        $this->cantidad = 0;
        $this->fecha = '';
        $this->fecha_pagado = '';
        $this->tipo_transaccion = '';
        $this->id_transaccion = '';
        $this->tipo_pago = '';
        $this->texto_tipo_pago = '';
        $this->num_cuotas = 0;
        $this->fin_tarjeta = '';
        $this->fecha_exp = '';
        $this->mac = '';
        $this->fecha_pagado_cap = '';
        $this->ocupado = 0;
        $this->fecha_ingreso = '';
        $this->commercecode = '';
        $this->sessionid = '';
        $this->vci = '';
        $this->texto_vci = '';
        $this->resp_code = '';
        $this->texto_resp_code = '';
        $this->detalle_rechazo = '';

        $this->estadoSql = 0;
        $this->matrizDatos = array();
    }

    public function format_rut($rut){
        $rut = str_replace('.','',$rut);
        $rut = str_replace('.','',$rut);
        $rut = str_replace('-','',$rut);
        $array_rut = str_split($rut);
        $largo = count($array_rut);
        $i=0;
        while($i<($largo-1)){
            $rut1.= $array_rut[$i];
            $i++;
        }
        $verificador = $array_rut[$i];

        return number_format($rut1,0,',','.').'-'.$verificador;
    }

    public function traerObjeto()
    {
        return array(
            'id' => (int)$this->getId(),
            'rut' => $this->getRut(),
            'monto' => (int)$this->getMonto(),
            'id_usuario' => (int)$this->getIdUsuario(),
            'estado' => $this->getEstado(),
            'sid' => (int)$this->getSid(),
            'nalumno' => $this->getNalumno(),
            'calumno' => $this->getCalumno(),
            'id_colegio' => (int)$this->getIdColegio(),
            'producto' => (int)$this->getProducto(),
            'cantidad' => (int)$this->getCantidad(),
            'fecha' => $this->getFecha(),
            'fecha_pagado' => $this->getFechaPagado(),
            'tipo_transaccion' => $this->getTipoTransaccion(),
            'id_transaccion' => $this->getIdTransaccion(),
            'tipo_pago' => $this->getTipoPago(),
            'texto_tipo_pago' => $this->getTextoTipoPago(),
            'num_cuotas' => (int)$this->getNumCuotas(),
            'fin_tarjeta' => $this->getFinTarjeta(),
            'fecha_exp' => $this->getFechaExp(),
            'mac' => $this->getMac(),
            'fecha_pagado_cap' => $this->getFechaPagadoCap(),
            'ocupado' => (int)$this->getOcupado(),
            'fecha_ingreso' => $this->getFechaIngreso(),
            'commercecode' => $this->getCommercecode(),
            'sessionid' => $this->getSessionid(),
            'vci' => $this->getVci(),
            'texto_vci' => $this->getTextoRespCode(),
            'resp_code' => $this->getRespCode(),
            'texto_resp_code' => $this->getTextoRespCode(),
            'detalle_rechazo' => $this->getDetalleRechazo()
        );
    }

    public function traerRegistro($id_registro)
    {
        $bd = new db();

        $tabla = 'orden_compra';
        $columnas = array(
            'id_orden_compra',
            'rut_orden_compra',
            'monto_orden_compra',
            'id_usuario',
            'estado_orden_compra',
            'sid_orden_compra',
            'nalumno_orden_compra',
            'calumno_orden_compra',
            'id_colegio',
            'producto_orden_compra',
            'cantidad_orden_compra',
            'fecha_orden_compra',
            'fecha_pagado_orden_compra',
            'tipo_transaccion_orden_compra',
            'id_transaccion_orden_compra',
            'tipo_pago_orden_compra',
            'texto_tipo_pago_orden_compra',
            'num_cuotas_orden_compra',
            'fin_tarjeta_orden_compra',
            'fecha_exp_orden_compra',
            'mac_orden_compra',
            'fecha_pagado_cap_orden_compra',
            'ocupado_orden_compra',
            'fecha_ingreso_orden_compra',
            'commercecode_orden_compra',
            'sessionid_orden_compra',
            'vci_orden_compra',
            'texto_vci_orden_compra',
            'resp_code_orden_compra',
            'texto_resp_code_orden_compra',
            'detalle_rechazo_orden_compra'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_orden_compra=' . trim($id_registro));
        $bd->select();
        $bd->ejecutar();
        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_orden_compra);
            $this->setRut($row->rut_orden_compra);
            $this->setMonto($row->monto_orden_compra);
            $this->setIdUsuario($row->id_usuario);
            $this->setEstado($row->estado_orden_compra);
            $this->setSid($row->sid_orden_compra);
            $this->setNalumno($row->nalumno_orden_compra);
            $this->setCalumno($row->calumno_orden_compra);
            $this->setIdColegio($row->id_colegio);
            $this->setProducto($row->producto_orden_compra);
            $this->setCantidad($row->cantidad_orden_compra);
            $this->setFecha($row->fecha_orden_compra);
            $this->setFechaPagado($row->fecha_pagado_orden_compra);
            $this->setTipoTransaccion($row->tipo_transaccion_orden_compra);
            $this->setTextoTipoPago($row->texto_tipo_pago_orden_compra);
            $this->setNumCuotas($row->num_cuotas_orden_compra);
            $this->setFinTarjeta($row->fin_tarjeta_orden_compra);
            $this->setFechaExp($row->fecha_exp_orden_compra);
            $this->setMac($row->mac_orden_compra);
            $this->setFechaPagado($row->fecha_pagado_cap_orden_compra);
            $this->setOcupado($row->ocupado_orden_compra);
            $this->setFechaIngreso($row->fecha_ingreso_orden_compra);
            $this->setCommercecode($row->commercecode_orden_compra);
            $this->setSessionid($row->sessionid_orden_compra);
            $this->setVci($row->vci_orden_compra);
            $this->setTextoVci($row->texto_vci_orden_compra);
            $this->setRespCode($row->resp_code_orden_compra);
            $this->setTextoRespCode($row->texto_resp_code_orden_compra);
            $this->setDetalleRechazo($row->detalle_rechazo_orden_compra);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    public function traerTodosRegistro()
    {

        $bd = new db();

        $tabla = 'orden_compra';
        $columnas = array(
            'id_orden_compra',
            'rut_orden_compra',
            'monto_orden_compra',
            'id_usuario',
            'estado_orden_compra',
            'sid_orden_compra',
            'nalumno_orden_compra',
            'calumno_orden_compra',
            'id_colegio',
            'producto_orden_compra',
            'cantidad_orden_compra',
            'fecha_orden_compra',
            'fecha_pagado_orden_compra',
            'tipo_transaccion_orden_compra',
            'id_transaccion_orden_compra',
            'tipo_pago_orden_compra',
            'texto_tipo_pago_orden_compra',
            'num_cuotas_orden_compra',
            'fin_tarjeta_orden_compra',
            'fecha_exp_orden_compra',
            'mac_orden_compra',
            'fecha_pagado_cap_orden_compra',
            'ocupado_orden_compra',
            'fecha_ingreso_orden_compra',
            'commercecode_orden_compra',
            'sessionid_orden_compra',
            'vci_orden_compra',
            'texto_vci_orden_compra',
            'resp_code_orden_compra',
            'texto_resp_code_orden_compra',
            'detalle_rechazo_orden_compra'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1');
        $bd->select();
        $bd->ejecutar();
        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->getResultado());

        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    public function traerRegistroRut($id_registro)
    {
        $bd = new db();

        $tabla = 'orden_compra';
        $columnas = array(
            'id_orden_compra',
            'rut_orden_compra',
            'monto_orden_compra',
            'id_usuario',
            'estado_orden_compra',
            'sid_orden_compra',
            'nalumno_orden_compra',
            'calumno_orden_compra',
            'id_colegio',
            'producto_orden_compra',
            'cantidad_orden_compra',
            'fecha_orden_compra',
            'fecha_pagado_orden_compra',
            'tipo_transaccion_orden_compra',
            'id_transaccion_orden_compra',
            'tipo_pago_orden_compra',
            'texto_tipo_pago_orden_compra',
            'num_cuotas_orden_compra',
            'fin_tarjeta_orden_compra',
            'fecha_exp_orden_compra',
            'mac_orden_compra',
            'fecha_pagado_cap_orden_compra',
            'ocupado_orden_compra',
            'fecha_ingreso_orden_compra',
            'commercecode_orden_compra',
            'sessionid_orden_compra',
            'vci_orden_compra',
            'texto_vci_orden_compra',
            'resp_code_orden_compra',
            'texto_resp_code_orden_compra',
            'detalle_rechazo_orden_compra'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);

        $rut_mod1 = $this->format_rut($id_registro);
        $rut_mod2 = str_replace('.','',$_GET['r']);
        $rut_mod2 = str_replace('.','',$rut_mod2);
        $rut_mod2 = str_replace('-','',$rut_mod2);

        $bd->setFiltro("(rut_orden_compra like '%".$rut_mod1."%' OR rut_orden_compra like '%".$rut_mod2."%') AND estado_orden_compra='PAGADO' ");
        $bd->select();
        $bd->ejecutar();

        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->getResultado());

        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }


    public function traerRegistroIdOrden($id_registro)
    {
        $bd = new db();

        $tabla = 'orden_compra';
        $columnas = array(
            'id_orden_compra',
            'rut_orden_compra',
            'monto_orden_compra',
            'id_usuario',
            'estado_orden_compra',
            'sid_orden_compra',
            'nalumno_orden_compra',
            'calumno_orden_compra',
            'id_colegio',
            'producto_orden_compra',
            'cantidad_orden_compra',
            'fecha_orden_compra',
            'fecha_pagado_orden_compra',
            'tipo_transaccion_orden_compra',
            'id_transaccion_orden_compra',
            'tipo_pago_orden_compra',
            'texto_tipo_pago_orden_compra',
            'num_cuotas_orden_compra',
            'fin_tarjeta_orden_compra',
            'fecha_exp_orden_compra',
            'mac_orden_compra',
            'fecha_pagado_cap_orden_compra',
            'ocupado_orden_compra',
            'fecha_ingreso_orden_compra',
            'commercecode_orden_compra',
            'sessionid_orden_compra',
            'vci_orden_compra',
            'texto_vci_orden_compra',
            'resp_code_orden_compra',
            'texto_resp_code_orden_compra',
            'detalle_rechazo_orden_compra'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(" id_orden_compra = 0".$id_registro." AND estado_orden_compra='PAGADO'");
        $bd->select();
        $bd->ejecutar();
        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->getResultado());
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * @param string $rut
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    }

    /**
     * @return int
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * @param int $monto
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    }

    /**
     * @return int
     */
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param string $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return int
     */
    public function getSid()
    {
        return $this->sid;
    }

    /**
     * @param int $sid
     */
    public function setSid($sid)
    {
        $this->sid = $sid;
    }

    /**
     * @return string
     */
    public function getNalumno()
    {
        return $this->nalumno;
    }

    /**
     * @param string $nalumno
     */
    public function setNalumno($nalumno)
    {
        $this->nalumno = $nalumno;
    }

    /**
     * @return string
     */
    public function getCalumno()
    {
        return $this->calumno;
    }

    /**
     * @param string $calumno
     */
    public function setCalumno($calumno)
    {
        $this->calumno = $calumno;
    }

    /**
     * @return int
     */
    public function getIdColegio()
    {
        return $this->id_colegio;
    }

    /**
     * @param int $id_colegio
     */
    public function setIdColegio($id_colegio)
    {
        $this->id_colegio = $id_colegio;
    }

    /**
     * @return int
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * @param int $producto
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;
    }

    /**
     * @return int
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param int $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return string
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return string
     */
    public function getFechaPagado()
    {
        return $this->fecha_pagado;
    }

    /**
     * @param string $fecha_pagado
     */
    public function setFechaPagado($fecha_pagado)
    {
        $this->fecha_pagado = $fecha_pagado;
    }

    /**
     * @return string
     */
    public function getTipoTransaccion()
    {
        return $this->tipo_transaccion;
    }

    /**
     * @param string $tipo_transaccion
     */
    public function setTipoTransaccion($tipo_transaccion)
    {
        $this->tipo_transaccion = $tipo_transaccion;
    }

    /**
     * @return string
     */
    public function getIdTransaccion()
    {
        return $this->id_transaccion;
    }

    /**
     * @param string $id_transaccion
     */
    public function setIdTransaccion($id_transaccion)
    {
        $this->id_transaccion = $id_transaccion;
    }

    /**
     * @return string
     */
    public function getTipoPago()
    {
        return $this->tipo_pago;
    }

    /**
     * @param string $tipo_pago
     */
    public function setTipoPago($tipo_pago)
    {
        $this->tipo_pago = $tipo_pago;
    }

    /**
     * @return string
     */
    public function getTextoTipoPago()
    {
        return $this->texto_tipo_pago;
    }

    /**
     * @param string $texto_tipo_pago
     */
    public function setTextoTipoPago($texto_tipo_pago)
    {
        $this->texto_tipo_pago = $texto_tipo_pago;
    }

    /**
     * @return int
     */
    public function getNumCuotas()
    {
        return $this->num_cuotas;
    }

    /**
     * @param int $num_cuotas
     */
    public function setNumCuotas($num_cuotas)
    {
        $this->num_cuotas = $num_cuotas;
    }

    /**
     * @return string
     */
    public function getFinTarjeta()
    {
        return $this->fin_tarjeta;
    }

    /**
     * @param string $fin_tarjeta
     */
    public function setFinTarjeta($fin_tarjeta)
    {
        $this->fin_tarjeta = $fin_tarjeta;
    }

    /**
     * @return string
     */
    public function getFechaExp()
    {
        return $this->fecha_exp;
    }

    /**
     * @param string $fecha_exp
     */
    public function setFechaExp($fecha_exp)
    {
        $this->fecha_exp = $fecha_exp;
    }

    /**
     * @return string
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * @param string $mac
     */
    public function setMac($mac)
    {
        $this->mac = $mac;
    }

    /**
     * @return string
     */
    public function getFechaPagadoCap()
    {
        return $this->fecha_pagado_cap;
    }

    /**
     * @param string $fecha_pagado_cap
     */
    public function setFechaPagadoCap($fecha_pagado_cap)
    {
        $this->fecha_pagado_cap = $fecha_pagado_cap;
    }

    /**
     * @return int
     */
    public function getOcupado()
    {
        return $this->ocupado;
    }

    /**
     * @param int $ocupado
     */
    public function setOcupado($ocupado)
    {
        $this->ocupado = $ocupado;
    }

    /**
     * @return string
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param string $fecha_ingreso
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return string
     */
    public function getCommercecode()
    {
        return $this->commercecode;
    }

    /**
     * @param string $commercecode
     */
    public function setCommercecode($commercecode)
    {
        $this->commercecode = $commercecode;
    }

    /**
     * @return string
     */
    public function getSessionid()
    {
        return $this->sessionid;
    }

    /**
     * @param string $sessionid
     */
    public function setSessionid($sessionid)
    {
        $this->sessionid = $sessionid;
    }

    /**
     * @return string
     */
    public function getVci()
    {
        return $this->vci;
    }

    /**
     * @param string $vci
     */
    public function setVci($vci)
    {
        $this->vci = $vci;
    }

    /**
     * @return string
     */
    public function getTextoVci()
    {
        return $this->texto_vci;
    }

    /**
     * @param string $texto_vci
     */
    public function setTextoVci($texto_vci)
    {
        $this->texto_vci = $texto_vci;
    }

    /**
     * @return string
     */
    public function getRespCode()
    {
        return $this->resp_code;
    }

    /**
     * @param string $resp_code
     */
    public function setRespCode($resp_code)
    {
        $this->resp_code = $resp_code;
    }

    /**
     * @return string
     */
    public function getTextoRespCode()
    {
        return $this->texto_resp_code;
    }

    /**
     * @param string $texto_resp_code
     */
    public function setTextoRespCode($texto_resp_code)
    {
        $this->texto_resp_code = $texto_resp_code;
    }

    /**
     * @return string
     */
    public function getDetalleRechazo()
    {
        return $this->detalle_rechazo;
    }

    /**
     * @param string $detalle_rechazo
     */
    public function setDetalleRechazo($detalle_rechazo)
    {
        $this->detalle_rechazo = $detalle_rechazo;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return int
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param int $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return array
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param array $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }




}