<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo 5
 * Date: 06-02-2018
 * Time: 11:01
 */
@session_start();
class colegio extends db{

    private $id;
    private $nombre;
    private $id_segmentacion;
    private $obligatorio;
    private $precio;
    private $logo;
    private $numero_centro_costo;
    private $nombre_administrador;
    private $mail_administrador;
    private $direccion;
    private $telefono;
    private $contacto;
    private $telefono_contacto;
    private $jop;
    private $correo_jop;
    private $estado;
    private $precio2;
    private $imagen_admin;
    private $precio_tramo1;
    private $precio_tramo2;
    private $precio_tramo3;
    private $fecha_creacion;

    private $bd;
    private $estadoSql;
    private $matrizDatos;

    function __construct()
    {
        $this->id = '';
        $this->nombre = '';
        $this->id_segmentacion = 0;
        $this->obligatorio = '';
        $this->precio = 0;
        $this->logo = $logo;
        $this->numero_centro_costo = 0;
        $this->nombre_administrador = '';
        $this->mail_administrador = '';
        $this->direccion = '';
        $this->telefono = '';
        $this->contacto = '';
        $this->telefono_contacto = '';
        $this->jop = '';
        $this->correo_jop = '';
        $this->estado = 0;
        $this->precio2 = 0;
        $this->imagen_admin = '';
        $this->precio_tramo1 = 0;
        $this->precio_tramo2 = 0;
        $this->precio_tramo3 = 0;
        $this->fecha_creacion = '';

        $this->estadoSql = 0;
        $this->matrizDatos = array();
    }


    public function traerObjeto()
    {
        return array(
            'id' =>$this->getId(),
            'nombre' =>$this->getNombre(),
            'id_segmentacion' =>$this->getIdSegmentacion(),
            'obligatorio' =>$this->getObligatorio(),
            'precio' =>$this->getPrecio(),
            'logo' =>$this->getLogo(),
            'numero_centro_costo' =>$this->getNumeroCentroCosto(),
            'nombre_administrador' =>$this->getNombreAdministrador(),
            'mail_administrador' =>$this->getMailAdministrador(),
            'direccion' =>$this->getDireccion(),
            'telefono' =>$this->getTelefono(),
            'contacto' =>$this->getContacto(),
            'telefono_contacto' =>$this->getTelefonoContacto(),
            'jop' =>$this->getJop(),
            'correo_jop' =>$this->getCorreoJop(),
            'estado' =>$this->getEstado(),
            'precio2' =>$this->getPrecio2(),
            'imagen_admin' =>$this->getImagenAdmin(),
            'precio_tramo1' =>$this->getPrecioTramo1(),
            'precio_tramo2' =>$this->getPrecioTramo2(),
            'precio_tramo3' =>$this->getPrecioTramo3(),
            'fecha_creacion' =>$this->getFechaCreacion()
        );
    }

    public function traerRegistro($id_registro)
    {
        $bd = new db();

        $tabla = 'colegio';
        $columnas = array(
            'id_colegio',
            'nombre_colegio',
            'id_segmentacion_colegio',
            'obligatorio_colegio',
            'precio_colegio',
            'logo_colegio',
            'numero_centro_costo_colegio',
            'nombre_administrador_colegio',
            'mail_administrador_colegio',
            'direccion_colegio',
            'telefono_colegio',
            'contacto_colegio',
            'telefono_contacto_colegio',
            'jop_colegio',
            'correo_jop_colegio',
            'estado_colegio',
            'precio2_colegio',
            'imagen_admin_colegio',
            'precio_tramo1',
            'precio_tramo2',
            'precio_tramo3',
            'fecha_creacion_colegio'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_colegio=' . trim($id_registro));
        $bd->select();
        $bd->ejecutar();
        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_colegio);
            $this->setNombre($row->nombre_colegio);
            $this->setIdSegmentacion($row->id_segmentacion_colegio);
            $this->setObligatorio($row->obligatorio_colegio);
            $this->setPrecio($row->precio_colegio);
            $this->setLogo($row->logo_colegio);
            $this->setNumeroCentroCosto($row->numero_centro_costo_colegio);
            $this->setNombreAdministrador($row->nombre_administrador_colegio);
            $this->setMailAdministrador($row->mail_administrador_colegio);
            $this->setDireccion($row->direccion_colegio);
            $this->setTelefono($row->telefono_colegio);
            $this->setContacto($row->contacto_colegio);
            $this->setTelefonoContacto($row->telefono_contacto_colegio);
            $this->setJop($row->jop_colegio);
            $this->setCorreoJop($row->correo_jop_colegio);
            $this->setEstado($row->estado_colegio);
            $this->setPrecio2($row->precio2_colegio);
            $this->setImagenAdmin($row->imagen_admin_colegio);
            $this->setPrecioTramo1($row->precio_tramo1);
            $this->setPrecioTramo2($row->precio_tramo2);
            $this->setPrecioTramo3($row->precio_tramo3);
            $this->setFechaCreacion($row->fecha_creacion_colegio);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    public function traerTodosRegistro()
    {

        $bd = new db();

        $tabla = 'colegio';
        $columnas = array(
            'id_colegio',
            'nombre_colegio',
            'id_segmentacion_colegio',
            'obligatorio_colegio',
            'precio_colegio',
            'logo_colegio',
            'numero_centro_costo_colegio',
            'nombre_administrador_colegio',
            'mail_administrador_colegio',
            'direccion_colegio',
            'telefono_colegio',
            'contacto_colegio',
            'telefono_contacto_colegio',
            'jop_colegio',
            'correo_jop_colegio',
            'estado_colegio',
            'precio2_colegio',
            'imagen_admin_colegio',
            'precio_tramo1',
            'precio_tramo2',
            'precio_tramo3',
            'fecha_creacion_colegio'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' estado_colegio NOT IN ("3") ');
        $bd->select();
        $bd->ejecutar();
        if ((int)$bd->num_registros > 0) {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->getResultado());

            /*
             $row = $bd->resultadoObjetual();

            $this->setId($row->id_colegio);
            $this->setNombre($row->nombre_colegio);
            $this->setIdSegmentacion($row->id_segmentacion_colegio);
            $this->setObligatorio($row->obligatorio_colegio);
            $this->setPrecio($row->precio_colegio);
            $this->setLogo($row->logo_colegio);
            $this->setNumeroCentroCosto($row->numero_centro_costo_colegio);
            $this->setNombreAdministrador($row->nombre_administrador_colegio);
            $this->setMailAdministrador($row->mail_administrador_colegio);
            $this->setDireccion($row->direccion_colegio);
            $this->setTelefono($row->telefono_colegio);
            $this->setContacto($row->contacto_colegio);
            $this->setTelefonoContacto($row->telefono_contacto_colegio);
            $this->setJop($row->jop_colegio);
            $this->setCorreoJop($row->correo_jop_colegio);
            $this->setEstado($row->estado_colegio);
            $this->setPrecio2($row->precio2_colegio);
            $this->setImagenAdmin($row->imagen_admin_colegio);
            $this->setPrecioTramo1($row->precio_tramo1);
            $this->setPrecioTramo2($row->precio_tramo2);
            $this->setPrecioTramo3($row->precio_tramo3);
            $this->setFechaCreacion($row->fecha_creacion_colegio);
            */

        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    public function registrar()
    {
        $bd = new db();

        $bd = new db();

        $tabla = 'colegio';
        $columnas = array(
            'nombre_colegio',
            'id_segmentacion_colegio',
            'obligatorio_colegio',
            'precio_colegio',
            'logo_colegio',
            'numero_centro_costo_colegio',
            'nombre_administrador_colegio',
            'mail_administrador_colegio',
            'direccion_colegio',
            'telefono_colegio',
            'contacto_colegio',
            'telefono_contacto_colegio',
            'jop_colegio',
            'correo_jop_colegio',
            'estado_colegio',
            'precio2_colegio',
            'imagen_admin_colegio',
            'precio_tramo1',
            'precio_tramo2',
            'precio_tramo3'
        );

        /*se asigna el mayor a tramo 1 y 2 */
        if($this->getPrecio() > $this->getPrecio2()) {
            $this->setPrecioTramo1((int)$this->getPrecio());
            $this->setPrecioTramo2((int)$this->getPrecio());
            $this->setPrecioTramo3((int)$this->getPrecio2());
        }else{

            $this->setPrecioTramo1((int)$this->getPrecio2());
            $this->setPrecioTramo2((int)$this->getPrecio2());
            $this->setPrecioTramo3((int)$this->getPrecio());
        }

        $valores = array(
            '"' . trim($this->getNombre()) . '"',
            '1',//'"' . (int)trim($this->getIdSegmentacion()) . '"',
            '"No"',//'"' . trim($this->getObligatorio()) . '"',
            '"' . (int)trim($this->getPrecio()) . '"',
            '"' . trim($this->getLogo()) . '"',
            '"' . (int)trim($this->getNumeroCentroCosto()) . '"',
            '"' . trim($this->getNombreAdministrador()) . '"',
            '"' . trim($this->getMailAdministrador()) . '"',
            '"' . trim($this->getDireccion()) . '"',
            '"' . trim($this->getTelefono()) . '"',
            '"' . trim($this->getContacto()) . '"',
            '"' . trim($this->getTelefonoContacto()) . '"',
            '"' . trim($this->getJop()) . '"',
            '"' . trim($this->getCorreoJop()) . '"',
            '1',//'"' . trim($this->getEstado()) . '"',
            '"' . (int)trim($this->getPrecio2()) . '"',
            '"' . trim($this->getImagenAdmin()) . '"',
            '"' . (int)trim($this->getPrecioTramo1()) . '"',
            '"' . (int)trim($this->getPrecioTramo2()) . '"',
            '"' . (int)trim($this->getPrecioTramo3()) . '"'
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if ($bd->getResultadoInsert() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    public function modificar($id_registro)
    {
        $bd = new db();

        /*se asigna el mayor a tramo 1 y 2 */
        if($this->getPrecio() > $this->getPrecio2()) {
            $this->setPrecioTramo1((int)$this->getPrecio());
            $this->setPrecioTramo2((int)$this->getPrecio());
            $this->setPrecioTramo3((int)$this->getPrecio2());
        }else{
            $this->setPrecioTramo1((int)$this->getPrecio2());
            $this->setPrecioTramo2((int)$this->getPrecio2());
            $this->setPrecioTramo3((int)$this->getPrecio());
        }

        $tabla = 'colegio';
        $valores = array(
            'nombre_colegio' => '"' . trim($this->getNombre()) . '"',
            'precio_colegio' => '"' . trim($this->getPrecio()) . '"',
            'logo_colegio' => '"' . trim($this->getLogo()) . '"',
            'numero_centro_costo_colegio' => '"' . trim($this->getNumeroCentroCosto()) . '"',
            'nombre_administrador_colegio' => '"' . trim($this->getNombreAdministrador()) . '"',
            'mail_administrador_colegio' => '"' . trim($this->getMailAdministrador()) . '"',
            'direccion_colegio' => '"' . trim($this->getDireccion()) . '"',
            'telefono_colegio' => '"' . trim($this->getTelefono()) . '"',
            'contacto_colegio' => '"' . trim($this->getContacto()) . '"',
            'telefono_contacto_colegio' => '"' . trim($this->getTelefonoContacto()) . '"',
            'jop_colegio' => '"' . trim($this->getJop()) . '"',
            'correo_jop_colegio' => '"' . trim($this->getCorreoJop()) . '"',
            'estado_colegio' => '"' . trim($this->getEstado()) . '"',
            'precio2_colegio' => '"' . trim($this->getPrecio2()) . '"',
            'imagen_admin_colegio' => '"' . trim($this->getImagenAdmin()) . '"',
            'precio_tramo1' => '"' . trim($this->getPrecioTramo1()) . '"',
            'precio_tramo2' => '"' . trim($this->getPrecioTramo2()) . '"',
            'precio_tramo3' => '"' . trim($this->getPrecioTramo3()) . '"'
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_colegio=' . $id_registro);
        $bd->update();

        if ($bd->getResultadoUpdate() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    public function eliminar($id_registro)
    {
        $bd = new db();
        $tabla = 'colegio';
        $valores = array(
            'estado_colegio' => '3'
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_colegio=' . $id_registro);
        $bd->update();

        if ($bd->getResultadoUpdate() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    public function cambiarEstado($id_registro, $estado)
    {
        $bd = new db();
        $tabla = 'colegio';
        $valores = array(
            'estado_colegio' => (int)$estado
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_colegio=' . $id_registro);
        $bd->update();

        if ($bd->getResultadoUpdate() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }
        return $this->getEstadoSql();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return int
     */
    public function getIdSegmentacion()
    {
        return $this->id_segmentacion;
    }

    /**
     * @param int $id_segmentacion
     */
    public function setIdSegmentacion($id_segmentacion)
    {
        $this->id_segmentacion = $id_segmentacion;
    }

    /**
     * @return string
     */
    public function getObligatorio()
    {
        return $this->obligatorio;
    }

    /**
     * @param string $obligatorio
     */
    public function setObligatorio($obligatorio)
    {
        $this->obligatorio = $obligatorio;
    }

    /**
     * @return int
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param int $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return int
     */
    public function getNumeroCentroCosto()
    {
        return $this->numero_centro_costo;
    }

    /**
     * @param int $numero_centro_costo
     */
    public function setNumeroCentroCosto($numero_centro_costo)
    {
        $this->numero_centro_costo = $numero_centro_costo;
    }

    /**
     * @return string
     */
    public function getNombreAdministrador()
    {
        return $this->nombre_administrador;
    }

    /**
     * @param string $nombre_administrador
     */
    public function setNombreAdministrador($nombre_administrador)
    {
        $this->nombre_administrador = $nombre_administrador;
    }

    /**
     * @return string
     */
    public function getMailAdministrador()
    {
        return $this->mail_administrador;
    }

    /**
     * @param string $mail_administrador
     */
    public function setMailAdministrador($mail_administrador)
    {
        $this->mail_administrador = $mail_administrador;
    }

    /**
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param string $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string
     */
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * @param string $contacto
     */
    public function setContacto($contacto)
    {
        $this->contacto = $contacto;
    }

    /**
     * @return string
     */
    public function getTelefonoContacto()
    {
        return $this->telefono_contacto;
    }

    /**
     * @param string $telefono_contacto
     */
    public function setTelefonoContacto($telefono_contacto)
    {
        $this->telefono_contacto = $telefono_contacto;
    }

    /**
     * @return string
     */
    public function getJop()
    {
        return $this->jop;
    }

    /**
     * @param string $jop
     */
    public function setJop($jop)
    {
        $this->jop = $jop;
    }

    /**
     * @return string
     */
    public function getCorreoJop()
    {
        return $this->correo_jop;
    }

    /**
     * @param string $correo_jop
     */
    public function setCorreoJop($correo_jop)
    {
        $this->correo_jop = $correo_jop;
    }

    /**
     * @return int
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param int $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return int
     */
    public function getPrecio2()
    {
        return $this->precio2;
    }

    /**
     * @param int $precio2
     */
    public function setPrecio2($precio2)
    {
        $this->precio2 = $precio2;
    }

    /**
     * @return string
     */
    public function getImagenAdmin()
    {
        return $this->imagen_admin;
    }

    /**
     * @param string $imagen_admin
     */
    public function setImagenAdmin($imagen_admin)
    {
        $this->imagen_admin = $imagen_admin;
    }

    /**
     * @return int
     */
    public function getPrecioTramo1()
    {
        return $this->precio_tramo1;
    }

    /**
     * @param int $precio_tramo1
     */
    public function setPrecioTramo1($precio_tramo1)
    {
        $this->precio_tramo1 = $precio_tramo1;
    }

    /**
     * @return int
     */
    public function getPrecioTramo2()
    {
        return $this->precio_tramo2;
    }

    /**
     * @param int $precio_tramo2
     */
    public function setPrecioTramo2($precio_tramo2)
    {
        $this->precio_tramo2 = $precio_tramo2;
    }

    /**
     * @return int
     */
    public function getPrecioTramo3()
    {
        return $this->precio_tramo3;
    }

    /**
     * @param int $precio_tramo3
     */
    public function setPrecioTramo3($precio_tramo3)
    {
        $this->precio_tramo3 = $precio_tramo3;
    }

    /**
     * @return string
     */
    public function getFechaCreacion()
    {
        return $this->fecha_creacion;
    }

    /**
     * @param string $fecha_creacion
     */
    public function setFechaCreacion($fecha_creacion)
    {
        $this->fecha_creacion = $fecha_creacion;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return int
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param int $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return array
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param array $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }


}