<?php
class mipdf extends FPDF
{
    private $ancho_col1;
    private $ancho_col2;
    private $ancho_col3;
    private $ancho_col4;
    private $ancho_col5;

    private $altura_celda;


    function init(){
        // TITULO DE CAJAS DE DATOS
        $this->titulo_cajas = array(
            'titulo1' => 'ARRIENDO DECORACIÓN',
            'titulo2' => 'ARRIENDO BODEGA',
            'titulo3' => 'ARRIENDO'

        );

        // TITULOS DATOS DEL LOS PRODUCTOS
        $this->titulo_datos_producto = array(
            'dato1' => 'Nombre',
            'dato2' => 'Categoria',
            'dato3' => 'Cantidad'
        );

        // TITULOS DATOS DEL LOS PRODUCTOS
        $this->titulo_datos_arriendo = array(
            'dato1' => 'Nombre',
            'dato2' => 'Categoria',
            'dato3' => 'Cantidad',
            'dato4' => 'Costo',
            'dato5' => 'Venta'
        );
    }

    function Header2($nombrer, $rutr, $giror, $direccion, $telefono_movil, $sucursal, $email)
    {
        $cont_esp =0;
        $anc = 95;
        $desp = 80;
        $hei = 4;
        $this -> image("../../modulos/pdf/images/logo.png",10,5,50);
        $this ->SetTextColor(0,0,0);

        if(!empty($nombrer))
        {
            $this -> setFont("Arial","B",8);
            $this -> Cell($anc,$hei,"",0,"","C");
            $this -> Cell($desp,$hei,$nombrer,0,"","L");
            $this -> Ln();
        }
        else
        {$cont_esp++;}

        if(!empty($rutr))
        {
            $this -> setFont("Arial","",8);
            $this -> Cell($anc,$hei,"",0,"","C");
            $this -> Cell($desp,$hei,$rutr,0,"","L");
            $this -> Ln();
        }
        else
        {$cont_esp++;}

        if(!empty($giror))
        {
            $this -> setFont("Arial","",8);
            $this -> Cell($anc,$hei,"",0,"","C");
            $this -> Cell($desp,$hei,$giror,0,"","L");
            $this -> Ln();
        }
        else
        {$cont_esp++;}

        if(!empty($direccion))
        {
            $this -> Cell($anc,$hei,"",0,"","C");
            $this -> Cell($desp,$hei, $direccion,0,"","L");
            $this -> Ln();
        }
        else
        {$cont_esp++;}

        if(!empty($telefono_movil))
        {
            $this -> Cell($anc,$hei,"",0,"","C");
            $this -> Cell($desp,$hei, $telefono_movil,0,"","L");
            $this -> Ln();
        }
        else
        {$cont_esp++;}

        if(!empty($sucursal))
        {
            $this -> Cell($anc,$hei,"",0,"","C");
            $this -> Cell($desp,$hei, $sucursal,0,"","L");
            $this -> Ln();
        }
        else
        {$cont_esp++;}

        if(!empty($email))
        {
            $this -> setTextColor(163,43,88);
            $this -> Cell($anc,$hei,"",0,"","C");
            $this -> Cell($desp,$hei, $email,0,"","L");
            $this -> Ln();
        }
        else
        {$cont_esp++;}

        for($i=0;$i<$cont_esp;$i++)
        {$this -> Ln();}

        $this -> setFont("Arial","",9);

    }

    function arriendo_decoracion($productos)
    {
        $this->Ln(10);
        $this->SetDrawColor(88,88,88);

        // TITULO CAJA
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol1()+$this->getAnchoCol2()+$this->getAnchoCol3(),$this->getAlturaCelda(),utf8_decode($this->titulo_cajas['titulo1']),1,1,'L',0);

        // FILA 1 // TITULOS
        // CAMPO UNO Col 1,2,3,4,5,6,7
        $this->setFillColor(186,10,37);
        $this->SetTextColor(255,255,255);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol1(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_producto['dato1']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol2(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_producto['dato2']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol3(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_producto['dato3']),1,1,"C",1);

        $this->setFillColor(255,255,255);
        $this->SetTextColor(0,0,0);
        foreach($productos as $key =>$value)
        {
            // FILA N // REVISTAS
            // CAMPO UNO Col 1,2,3,4,5,6,7
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol1(),$this->getAlturaCelda(),utf8_decode($productos[$key]['nombre']),1,0,"L");
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol2(),$this->getAlturaCelda(),utf8_decode($productos[$key]['categoria']),1,0,"L");
            $this->SetFont('Arial','',9);
            $this->Cell($this->getAnchoCol3(),$this->getAlturaCelda(),utf8_decode($productos[$key]['cantidad']),1,1,"C");
        }
    }

    function arriendo_bodega($productos)
    {
        $this->Ln(10);
        $this->SetDrawColor(88,88,88);

        // TITULO CAJA
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol1()+$this->getAnchoCol2()+$this->getAnchoCol3(),$this->getAlturaCelda(),utf8_decode($this->titulo_cajas['titulo2']),1,1,'L',0);

        // FILA 1 // TITULOS
        // CAMPO UNO Col 1,2,3,4,5,6,7
        $this->setFillColor(186,10,37);
        $this->SetTextColor(255,255,255);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol1(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_producto['dato1']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol2(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_producto['dato2']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol3(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_producto['dato3']),1,1,"C",1);

        $this->setFillColor(255,255,255);
        $this->SetTextColor(0,0,0);
        foreach($productos as $key =>$value)
        {
            // FILA N // REVISTAS
            // CAMPO UNO Col 1,2,3,4,5,6,7
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol1(),$this->getAlturaCelda(),utf8_decode($productos[$key]['nombre']),1,0,"L");
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol2(),$this->getAlturaCelda(),utf8_decode($productos[$key]['categoria']),1,0,"L");
            $this->SetFont('Arial','',9);
            $this->Cell($this->getAnchoCol3(),$this->getAlturaCelda(),utf8_decode($productos[$key]['cantidad']),1,1,"C");
        }
    }

    function arriendo_productos($productos)
    {
        $this->Ln(10);
        $this->SetDrawColor(88,88,88);

        // TITULO CAJA
        $this->SetTextColor(0,0,0);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol1()+(($this->getAnchoCol5())*2)+$this->getAnchoCol5()+20,$this->getAlturaCelda(),utf8_decode($this->titulo_cajas['titulo3']),1,1,'L',0);

        // FILA 1 // TITULOS
        // CAMPO UNO Col 1,2,3,4,5,6,7
        $this->setFillColor(186,10,37);
        $this->SetTextColor(255,255,255);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol1()+20,$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato1']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol5()*2,$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato2']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato3']),1,1,"C",1);
       // $this->SetFont('Arial','B',9);
       // $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato4']),1,0,"C",1);
       // $this->SetFont('Arial','B',9);
       // $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato5']),1,1,"C",1);

        $this->setFillColor(255,255,255);
        $this->SetTextColor(0,0,0);
        foreach($productos as $key =>$value)
        {
            // FILA N // REVISTAS
            // CAMPO UNO Col 1,2,3,4,5,6,7
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol1()+20,$this->getAlturaCelda(),utf8_decode($productos[$key]['nombre']),1,0,"L");
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol5()*2,$this->getAlturaCelda(),utf8_decode($productos[$key]['categoria']),1,0,"L");
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($productos[$key]['cantidad']),1,1,"C");
           // $this->SetFont('Arial','',7);
           // $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($productos[$key]['costo']),1,0);
           // $this->SetFont('Arial','',7);
           // $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($productos[$key]['venta']),1,1);

        }
    }


    function arriendo_productos_presupuesto($productos)
    {
        $this->Ln(10);
        $this->SetDrawColor(88,88,88);

        // TITULO CAJA
        $this->SetTextColor(0,0,0);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol1()+($this->getAnchoCol5()*4),$this->getAlturaCelda(),utf8_decode($this->titulo_cajas['titulo3']),1,1,'L',0);

        // FILA 1 // TITULOS
        // CAMPO UNO Col 1,2,3,4,5,6,7
        $this->setFillColor(186,10,37);
        $this->SetTextColor(255,255,255);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol1(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato1']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato2']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato3']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato4']),1,0,"C",1);
        $this->SetFont('Arial','B',9);
        $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($this->titulo_datos_arriendo['dato5']),1,1,"C",1);

        $this->setFillColor(255,255,255);
        $this->SetTextColor(0,0,0);
        foreach($productos as $key =>$value)
        {
            // FILA N // REVISTAS
            // CAMPO UNO Col 1,2,3,4,5,6,7
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol1(),$this->getAlturaCelda(),utf8_decode($productos[$key]['nombre']),1,0,"L");
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($productos[$key]['categoria']),1,0,"L");
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($productos[$key]['cantidad']),1,0,"L");
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($productos[$key]['costo']),1,0,"L");
            $this->SetFont('Arial','',7);
            $this->Cell($this->getAnchoCol5(),$this->getAlturaCelda(),utf8_decode($productos[$key]['venta']),1,1,"L");

        }
    }



    function Footer()
    {
        //$this -> Cell(100,5,"",0,1);
        //$this -> image("images/footer_pdf.jpg");


      /*
        $_SESSION['id_usuario_sesion']=         $this->id;
        $_SESSION['nombre_usuario_sesion']=     $this->nombre;
        $_SESSION['apellido_usuario_sesion']=   $this->apellido;
        $_SESSION['email_usuario_sesion']=      $this->email;
        $_SESSION['celular_usuario_sesion']=    $this->celular;
        $_SESSION['rut_usuario_sesion']=        $this->rut;
        $_SESSION['id_tipo_usuario_sesion']=    $this->id_tipo;
        $_SESSION['online_usuario_sesion']=     true;
      */

        $this->ln();
        $this->setFillColor(255,255,255);
        $this->SetTextColor(0,0,0);

        $this->SetFont('Arial','B',10);
        $this->Cell(30,6,utf8_decode('Nombre:'),0,0,"L",1);
        $this->SetFont('Arial','I',10);
        $this->Cell(50,6,utf8_decode($_SESSION['nombre_usuario_sesion'].' '.$_SESSION['apellido_usuario_sesion']),0,1,"L",1);

        $this->SetFont('Arial','B',10);
        $this->Cell(30,6,utf8_decode('Email:'),0,0,"L",1);
        $this->SetFont('Arial','I',10);
        $this->Cell(50,6,utf8_decode($_SESSION['email_usuario_sesion']),0,1,"L",1);

        $this->SetFont('Arial','B',10);
        $this->Cell(30,6,utf8_decode('Celular:'),0,0,"L",1);
        $this->SetFont('Arial','I',10);
        $this->Cell(50,6,utf8_decode($_SESSION['celular_usuario_sesion']),0,1,"L",1);
    }


    /**
     * @return mixed
     */
    public function getAnchoCol1()
    {
        return $this->ancho_col1;
    }

    /**
     * @param mixed $ancho_col1
     */
    public function setAnchoCol1($ancho_col1)
    {
        $this->ancho_col1 = $ancho_col1;
    }

    /**
     * @return mixed
     */
    public function getAnchoCol2()
    {
        return $this->ancho_col2;
    }

    /**
     * @param mixed $ancho_col2
     */
    public function setAnchoCol2($ancho_col2)
    {
        $this->ancho_col2 = $ancho_col2;
    }

    /**
     * @return mixed
     */
    public function getAnchoCol3()
    {
        return $this->ancho_col3;
    }

    /**
     * @param mixed $ancho_col3
     */
    public function setAnchoCol3($ancho_col3)
    {
        $this->ancho_col3 = $ancho_col3;
    }

    /**
     * @return mixed
     */
    public function getAnchoCol4()
    {
        return $this->ancho_col4;
    }

    /**
     * @param mixed $ancho_col4
     */
    public function setAnchoCol4($ancho_col4)
    {
        $this->ancho_col4 = $ancho_col4;
    }

    /**
     * @return mixed
     */
    public function getAnchoCol5()
    {
        return $this->ancho_col5;
    }

    /**
     * @param mixed $ancho_col5
     */
    public function setAnchoCol5($ancho_col5)
    {
        $this->ancho_col5 = $ancho_col5;
    }

    /**
     * @return mixed
     */
    public function getAlturaCelda()
    {
        return $this->altura_celda;
    }

    /**
     * @param mixed $altura_celda
     */
    public function setAlturaCelda($altura_celda)
    {
        $this->altura_celda = $altura_celda;
    }






}