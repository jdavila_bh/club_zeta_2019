<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 25/09/2017
 * Time: 15:42
 */

class tipo_usuario extends db{

    private $id;
    private $nombre;

    private $bd;
    private $estadoSql;
    private $matrizDatos;

    function __construct()
    {
        $this->id = 0;
        $this->nombre = '';

        $this->estadoSql = 0;
        $this->matrizDatos = array();
    }

    /**
     * @return array
     */
    function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'nombre' =>$this->getNombre(),
        );
    }

    /**
     * @param $id_registro
     * @return mixed
     */
    public function traerRegistro($id_registro)
    {

        $bd = new db();

        $tabla = 'tipo_usuario';
        $columnas = array(
            'id_tipo_usuario',
            'nombre_tipo_usuario'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_tipo_usuario='.trim($id_registro));
        $bd->select();
        $bd->ejecutar();

        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_tipo_usuario);
            $this->setNombre($row->nombre_tipo_usuario);
            $this->setEstado($row->estado);
            $this->setFechaIngreso($row->fecha_ingreso);
            $this->setFechaModificacion($row->fecha_modificacion);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    /**
     * @return mixed
     */
    public function traerTodosRegistro()
    {

        $bd = new db();

        $tabla = 'tipo_usuario';
        $columnas = array(
            'id_tipo_usuario',
            'nombre_tipo_usuario'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1');
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->resultado);

        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();

    }


    /**
     * @return mixed
     */
    public function registrar()
    {
        $bd = new db();

        $tabla = 'tipo_usuario';
        $columnas = array(
            'nombre_tipo_usuario'
        );
        $valores = array(
            '"'.trim($this->getNombre()).'"',
            '"'.(int)trim($this->getEstado()).'"',
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if($bd->getResultadoInsert()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    /**
     * @param $id_registro
     * @return mixed
     */
    public function modificar($id_registro)
    {
        $bd = new db();

        $tabla = 'tipo_usuario';
        $valores = array(
            'nombre_tipo_usuario' => '"' . trim($this->getNombre()) . '"'
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_tipo_usuario='.$id_registro);
        $bd->update();

        if ($bd->getResultadoUpdate() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return mixed
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param mixed $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return mixed
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param mixed $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }

}