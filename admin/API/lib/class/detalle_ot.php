<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 25/09/2017
 * Time: 18:30
 */

class detalle_ot {

    private $id;
    private $id_ot;
    private $cantidad;
    private $descripcion;
    private $uf;
    private $unitario;
    private $descuento;
    private $orden;
    private $estado;
    private $fecha_ingreso;
    private $fecha_modificacion;

    private $bd;
    private $estadoSql;
    private $matrizDatos;
    private $filtroSql;

    function __construct()
    {
        $this->id = 0;
        $this->id_ot = 0;
        $this->cantidad = 0;
        $this->descripcion = '';
        $this->uf = 0;
        $this->unitario = 0;
        $this->descuento = 0;
        $this->orden = 0;
        $this->estado = 0;
        $this->fecha_ingreso = '';
        $this->fecha_modificacion = '';

        $this->estadoSql = 0;
        $this->matrizDatos = array();
        $this->filtroSql = array();
    }

    function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'id_ot' =>$this->getIdOt(),
            'cantidad' =>$this->getCantidad(),
            'descripcion' =>$this->getDescripcion(),
            'uf' =>$this->getUf(),
            'unitario' =>$this->getUnitario(),
            'descuento' =>$this->getDescuento(),
            'orden' => $this->getOrden(),
            'estado' =>$this->getEstado(),
            'fecha_ingreso' =>$this->getFechaIngreso(),
            'fecha_modificacion' =>$this->getFechaModificacion()
        );
    }


    public function traerRegistro($id_registro)
    {

        $filtro = '';
        $filtro_ref = $this->getFiltroSql();

        if(isset($filtro_ref['id_ot']))
            $filtro .= ' AND id_ot='.$filtro_ref['id_ot'];

        if(isset($filtro_ref['orden_detalle_ot']))
            $filtro .= ' AND orden_detalle_ot='.$filtro_ref['orden_detalle_ot'];


        $bd = new db();

        $tabla = 'detalle_ot';
        $columnas = array(
            'id_detalle_ot',
            'id_ot',
            'cantidad_detalle_ot',

            'descripcion_detalle_ot',
            'uf_detalle_ot',
            'unitario_detalle_ot',
            'descuento_detalle_ot',

            'orden_detalle_ot',

            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_detalle_ot='.trim($id_registro).$filtro);
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_detalle_ot);
            $this->setIdOt($row->id_ot);
            $this->setCantidad($row->cantidad_detalle_ot);

            $this->setDescripcion($row->descripcion_detalle_ot);
            $this->setUf($row->uf_detalle_ot);
            $this->setUnitario($row->unitario_detalle_ot);
            $this->setDescuento($row->descuento_detalle_ot);

            $this->setOrden($row->orden_detalle_ot);

            $this->setEstado($row->estado);
            $this->setFechaIngreso($row->fecha_ingreso);
            $this->setFechaModificacion($row->fecha_modificacion);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function traerRegistroOt()
    {

        $filtro = '';
        $filtro_ref = $this->getFiltroSql();

        if(isset($filtro_ref['id_ot']))
            $filtro .= ' AND id_ot='.$filtro_ref['id_ot'];

        if(isset($filtro_ref['orden_detalle_ot']))
            $filtro .= ' AND orden_detalle_ot='.$filtro_ref['orden_detalle_ot'];


        $bd = new db();

        $tabla = 'detalle_ot';
        $columnas = array(
            'id_detalle_ot',
            'id_ot',
            'cantidad_detalle_ot',

            'descripcion_detalle_ot',
            'uf_detalle_ot',
            'unitario_detalle_ot',
            'descuento_detalle_ot',

            'orden_detalle_ot',

            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 '.$filtro);
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_detalle_ot);
            $this->setIdOt($row->id_ot);
            $this->setCantidad($row->cantidad_detalle_ot);

            $this->setDescripcion($row->descripcion_detalle_ot);
            $this->setUf($row->uf_detalle_ot);
            $this->setUnitario($row->unitario_detalle_ot);
            $this->setDescuento($row->descuento_detalle_ot);

            $this->setOrden($row->orden_detalle_ot);

            $this->setEstado($row->estado);
            $this->setFechaIngreso($row->fecha_ingreso);
            $this->setFechaModificacion($row->fecha_modificacion);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }



    public function traerTodosRegistro()
    {

        $filtro = '';
        $filtro_ref = $this->getFiltroSql();

        if(isset($filtro_ref['estado']))
            $filtro .= ' AND estado='.$filtro_ref['estado'];

        if(isset($filtro_ref['id_ot']))
            $filtro .= ' AND id_ot='.$filtro_ref['id_ot'];


        $bd = new db();

        $tabla = 'detalle_ot';
        $columnas = array(
            'id_detalle_ot',
            'id_ot',
            'cantidad_detalle_ot',

            'descripcion_detalle_ot',
            'uf_detalle_ot',
            'unitario_detalle_ot',
            'descuento_detalle_ot',

            'orden_detalle_ot',

            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 '.$filtro);
        // ORDEN DE REGISTRO
        if(isset($filtro_ref['orden_sql']))
        $bd->setOrden($filtro_ref['orden_sql']);

        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->resultado);

        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();

    }


    public function estadoRegistro($id_registro,$nuevo_estado)
    {
        $bd = new db();

        $tabla = 'detalle_ot';
        $valores = array(
            'estado' =>$nuevo_estado
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_detalle_ot = '.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function registrar()
    {
        $bd = new db();

        $tabla = 'detalle_ot';
        $columnas = array(
            'id_ot',
            'cantidad_detalle_ot',

            'descripcion_detalle_ot',
            'uf_detalle_ot',
            'unitario_detalle_ot',
            'descuento_detalle_ot',

            'orden_detalle_ot',

            'estado'
        );
        $valores = array(
            '"'.(int)trim($this->getIdOt()).'"',
            '"'.(int)trim($this->getCantidad()).'"',

            '"'.trim($this->getDescripcion()).'"',
            '"'.(int)trim($this->getuf()).'"',
            '"'.(int)trim($this->getUnitario()).'"',
            '"'.(int)trim($this->getDescuento()).'"',

            '"'.(int)trim($this->getOrden()).'"',

            '"'.(int)trim($this->getEstado()).'"'
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if($bd->getResultadoInsert()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }



    public function modificar($id_registro)
    {
        $bd = new db();

        $tabla = 'detalle_ot';
        $valores = array(
            'id_ot' =>'"'.(int)trim($this->getIdOt()).'"',
            'cantidad_detalle_ot' =>'"'.(int)trim($this->getCantidad()).'"',

            'descripcion_detalle_ot' =>'"'.trim($this->getDescripcion()).'"',
            'uf_detalle_ot' =>'"'.(int)trim($this->getUf()).'"',
            'unitario_detalle_ot' =>'"'.(int)trim($this->getUnitario()).'"',
            'descuento_detalle_ot' =>'"'.(int)trim($this->getDescuento()).'"',

            'orden_detalle_ot' =>'"'.(int)trim($this->getOrden()).'"',

            'estado' =>'"'.(int)trim($this->getEstado()).'"',
            'fecha_modificacion' =>'"'.date('Y-m-d H:i:s').'"'
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_detalle_ot='.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }




    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdOt()
    {
        return $this->id_ot;
    }

    /**
     * @param int $id_ot
     */
    public function setIdOt($id_ot)
    {
        $this->id_ot = $id_ot;
    }

    /**
     * @return int
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param int $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return int
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param int $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }



    /**
     * @return int
     */
    public function getUnitario()
    {
        return $this->unitario;
    }

    /**
     * @param int $unitario
     */
    public function setUnitario($unitario)
    {
        $this->unitario = $unitario;
    }

    /**
     * @return int
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * @param int $descuento
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    }


    /**
     * @return int
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * @param int $orden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    }

    /**
     * @return int
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param int $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return string
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param string $fecha_ingreso
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return string
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * @param string $fecha_modificacion
     */
    public function setFechaModificacion($fecha_modificacion)
    {
        $this->fecha_modificacion = $fecha_modificacion;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return int
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param int $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return array
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param array $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }

    /**
     * @return array
     */
    public function getFiltroSql()
    {
        return $this->filtroSql;
    }

    /**
     * @param array $filtroSql
     */
    public function setFiltroSql($filtroSql)
    {
        $this->filtroSql = $filtroSql;
    }






}