<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 25/09/2017
 * Time: 16:56
 */

class tipo_pago extends db{


    private $id;
    private $nombre;
    private $estado;
    private $fecha_ingreso;
    private $fecha_modificacion;

    private $bd;
    private $estadoSql;
    private $matrizDatos;

    function __construct()
    {
        $this->id = 0;
        $this->nombre = '';
        $this->estado = 0;
        $this->fecha_ingreso = '';
        $this->fecha_modificacion = '';

        $this->estadoSql = 0;
        $this->matrizDatos = array();
    }

    function traerObjeto()
    {
        return array(
            'id' => $this->getId(),
            'nombre' =>$this->getNombre(),
            'estado' =>$this->getEstado(),
            'fecha_ingreso' =>$this->getFechaIngreso(),
            'fecha_modificacion' =>$this->getFechaModificacion()
        );
    }

    public function traerRegistro($id_registro)
    {

        $bd = new db();

        $tabla = 'tipo_pago';
        $columnas = array(
            'id_tipo_pago',
            'nombre_tipo_pago',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );


        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_tipo_pago='.trim($id_registro));
        $bd->select();
        $bd->ejecutar();

        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $row = $bd->resultadoObjetual();

            $this->setId($row->id_tipo_usuario);
            $this->setNombre($row->nombre_tipo_usuario);
            $this->setEstado($row->estado);
            $this->setFechaIngreso($row->fecha_ingreso);
            $this->setFechaModificacion($row->fecha_modificacion);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function traerTodosRegistro(array $filtroRef)
    {
        $filtro = '';

        if(isset($filtroRef['estado']))
        {
            $filtro.=' AND estado='.$filtroRef['estado'];
        }

        $bd = new db();

        $tabla = 'tipo_pago';
        $columnas = array(
            'id_tipo_pago',
            'nombre_tipo_pago',
            'estado',
            'fecha_ingreso',
            'fecha_modificacion'
        );

        $bd->setColumnas($columnas);
        $bd->setTabla($tabla);
        $bd->setFiltro(' 1 '.$filtro);
        $bd->select();
        $bd->ejecutar();
        if((int)$bd->num_registros>0)
        {
            $this->setEstadoSql(1);
            $this->setMatrizDatos($bd->resultado);

        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();

    }


    public function estadoRegistro($id_registro,$nuevo_estado)
    {
        $bd = new db();

        $tabla = 'tipo_pago';
        $valores = array(
            'estado' =>$nuevo_estado
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_tipo_pago = '.$id_registro);
        $bd->update();

        if($bd->getResultadoUpdate()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }

    public function registrar()
    {
        $bd = new db();

        $tabla = 'tipo_pago';
        $columnas = array(
            'nombre_tipo_pago',
            'estado'
        );
        $valores = array(
            '"'.trim($this->getNombre()).'"',
            '"'.(int)trim($this->getEstado()).'"',
        );
        $bd->setColumnas($columnas);
        $bd->setValores($valores);
        $bd->setTabla($tabla);
        $bd->insert();

        if($bd->getResultadoInsert()==1)
        {
            $this->setEstadoSql(1);
        }else{
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }


    public function modificar($id_registro)
    {
        $bd = new db();

        $tabla = 'tipo_pago';
        $valores = array(
            'nombre_tipo_pago' => '"' . trim($this->getNombre()) . '"',
            'estado' => '"' . (int)trim($this->getEstado()) . '"',
            'fecha_modificacion' => "'".date('Y-m-d H:i:s')."'"
        );

        $bd->setUpdateValores($valores);
        $bd->setTabla($tabla);
        $bd->setFiltro('id_tipo_pago=' . $id_registro);
        $bd->update();

        if ($bd->getResultadoUpdate() == 1) {
            $this->setEstadoSql(1);
        } else {
            $this->setEstadoSql(0);
        }

        return $this->getEstadoSql();
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param mixed $fecha_ingreso
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return mixed
     */
    public function getFechaModificacion()
    {
        return $this->fecha_modificacion;
    }

    /**
     * @param mixed $fecha_modificacion
     */
    public function setFechaModificacion($fecha_modificacion)
    {
        $this->fecha_modificacion = $fecha_modificacion;
    }

    /**
     * @return mixed
     */
    public function getBd()
    {
        return $this->bd;
    }

    /**
     * @param mixed $bd
     */
    public function setBd($bd)
    {
        $this->bd = $bd;
    }

    /**
     * @return mixed
     */
    public function getEstadoSql()
    {
        return $this->estadoSql;
    }

    /**
     * @param mixed $estadoSql
     */
    public function setEstadoSql($estadoSql)
    {
        $this->estadoSql = $estadoSql;
    }

    /**
     * @return mixed
     */
    public function getMatrizDatos()
    {
        return $this->matrizDatos;
    }

    /**
     * @param mixed $matrizDatos
     */
    public function setMatrizDatos($matrizDatos)
    {
        $this->matrizDatos = $matrizDatos;
    }


}