<?php
/**
* @author Luis Ramos <luisenrique545@gmail.com>
* Date: 18/01/2018	11:32	(dd-mm-yyyy)
* 
* Modelo de datos del modulo Empresas
*/

class empresa extends db{
	private $id;
	private $nombre;
	private $rut;
	private $direccion;
	private $telefono;
	private $giro;
	private $logo;
	private $estado;
	private $fecha_ingreso;
	private $fecha_modificacion;

	private $arrayDeDatos;

	private $db;
	private $estadoSQL;

	function __construct(){
		$this->id = 0;
		$this->nombre = '';
		$this->rut = '';
		$this->direccion = '';
		$this->telefono = '';
		$this->giro = '';
		$this->logo = '';
		$this->estado = 0;
		$this->fecha_ingreso = '';
		$this->fecha_modificacion = '';

		$this->arrayDeDatos = array();

		$this->estadoSQL = 0;
	}
	
	/**
	 * [obtenerEmpresas - Devuelve todos los registros guardandolos en un atributo]
	 * @return [int] [Retorna 0 o 1 dependiendo del resultado del SQL]
	 */
	public function obtenerEmpresas(){
		$this->db = new db;
		$this->db->setTabla('empresa'); //Se elige la tabla
		$this->db->setColumnas( //Se eligen columnas para la consulta
			array(
				'id_empresa', 
				'nombre_empresa', 
				'rut_empresa', 
				'direccion_empresa', 
				'telefono_empresa', 
				'giro_empresa', 
				'logo_empresa',
				'estado', 
				'fecha_ingreso', 
				'fecha_modificacion'
			)
		);
		$this->db->setFiltro(1);
		$this->db->select();
		$this->db->ejecutar();
		if ($this->db->num_registros>0) {
			$this->setEstadoSQL(1);
			$this->setArrayDeDatos($this->db->getResultado());
		} else {
			$this->setEstadoSQL(0);
		}
		return $this->getEstadoSQL();	
	}

	/**
	 * [agregarEmpresa - Agrega un registro]
	 * @return [int] [Retorna 0 o 1 dependiendo del resultado del SQL]
	 */
	public function agregarEmpresa(){
		$this->db = new db;
		$this->db->setTabla('empresa'); //Se elige la tabla
		$this->db->setColumnas( //Se eligen columnas para la consulta
			array(
				'nombre_empresa', 
				'rut_empresa', 
				'direccion_empresa', 
				'telefono_empresa', 
				'giro_empresa', 
				'estado'
			)
		);
		$this->db->setValores( //Setea los valores para preparar el Insert
			array(
				'"'.trim($this->getNombre()).'"',
	           	'"'.trim($this->getRut()).'"',
	           	'"'.trim($this->getDireccion()).'"',
	           	'"'.trim($this->getTelefono()).'"',
	           	'"'.trim($this->getGiro()).'"',
	           	'"'.(int)trim($this->getEstado()).'"'
			)
		);
		$this->db->insert();

		if ($this->db->getResultadoInsert()==1) {
			$this->setEstadoSQL(1);
		} else {
			$this->setEstadoSQL(0);
		}

		return $this->getEstadoSQL();	
	}

	public function modificarEmpresa(){
		$this->db = new db;
		$this->db->setTabla('empresa');
		$this->db->setFiltro('id_empresa='.$this->getId());
		/*$this->db->setColumnas( //Se eligen columnas para la consulta
			array(
				'nombre_empresa' => , 
				'rut_empresa', 
				'direccion_empresa', 
				'telefono_empresa', 
				'giro_empresa', 
				'estado',
				'fecha_modificacion'
			)
		);*/
		$this->db->setUpdateValores( //Setea los valores para preparar el Insert
			array(
				'nombre_empresa' => '"'.trim($this->getNombre()).'"',
	           	'rut_empresa' => '"'.trim($this->getRut()).'"',
	           	'direccion_empresa' => '"'.trim($this->getDireccion()).'"',
	           	'telefono_empresa' => '"'.trim($this->getTelefono()).'"',
	           	'giro_empresa' => '"'.trim($this->getGiro()).'"',
	           	'fecha_modificacion' => '"'.$this->getFechaModificacion().'"'
			)
		);

		$this->db->update();

		if ($this->db->getResultadoUpdate()==1) {
			$this->setEstadoSQL(1);
		} else {
			$this->setEstadoSQL(0);
		}

		return $this->getEstadoSQL();	

	}

	public function cambiarEstado($id_registro, $nuevo_estado){
		$this->db = new db;
		$this->db->setTabla('empresa');
		$this->db->setFiltro('id_empresa='.$id_registro);
		$this->db->setColumnas( //Se eligen columnas para la consulta
			array(
				'estado'
			)
		);
		$this->db->setUpdateValores( //Setea los valores para preparar el Insert
			array(
           		'estado' => $nuevo_estado
       		)
		);
		$this->db->update();

		if ($this->db->getResultadoUpdate()==1) {
			$this->setEstadoSQL(1);
		} else {
			$this->setEstadoSQL(0);
		}

		return $this->getEstadoSQL();
	}

	/**
	 * [setId - Setea valor al atributo]
	 * @param [int] $id [ID de Empresa]
	 */
	public function setId($id){
		$this->id = $id;
	}

	/**
	 * [getId - Retorna el valor del atributo]
	 * @return [int] [ID de Empresa]
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * [setNombre - Setea valor al atributo]
	 * @param [string] $nombre [Nombre de Empresa]
	 */
	public function setNombre($nombre){
		$this->nombre = $nombre;
	}

	/**
	 * [getNombre - Retorna el valor del atributo]
	 * @return [string] [Nombre de Empresa]
	 */
	public function getNombre(){
		return $this->nombre;
	}

	/**
	 * [setRut - Setea valor al atributo]
	 * @param [string] $rut [RUT de Empresa]
	 */
	public function setRut($rut){
		$this->rut = $rut;
	}

	/**
	 * [getRut - Retorna el valor del atributo]
	 * @return [string] [Rut de Empresa]
	 */
	public function getRut(){
		return $this->rut;
	}

	/**
	 * [setDireccion - Setea valor al atributo]
	 * @param [string] $direccion [Direccion de Empresa]
	 */
	public function setDireccion($direccion){
		$this->direccion = $direccion;
	}

	/**
	 * [getDireccion - Retorna el valor del atributo]
	 * @return [string] [Direccion de Empresa]
	 */
	public function getDireccion(){
		return $this->direccion;
	}

	/**
	 * [setTelefono - Setea valor al atributo]
	 * @param [string] $telefono [Telefono de Empresa]
	 */
	public function setTelefono($telefono){
		$this->telefono = $telefono;
	}

	/**
	 * [getTelefono - Retorna el valor del atributo]
	 * @return [string] [Telefono de Empresa]
	 */
	public function getTelefono(){
		return $this->telefono;
	}

	/**
	 * [setGiro - Setea valor al atributo]
	 * @param [string] $giro [Giro de Empresa]
	 */
	public function setGiro($giro){
		$this->giro = $giro;
	}

	/**
	 * [getGiro - Retorna el valor del atributo]
	 * @return [string] [Giro de Empresa]
	 */
	public function getGiro(){
		return $this->giro;
	}

	/**
	 * [setLogo - Setea valor al atributo]
	 * @param [string] $logo [Logo de Empresa]
	 */
	public function setLogo($logo){
		$this->logo = $logo;
	}

	/**
	 * [getLogo - Retorna el valor del atributo]
	 * @return [string] [Logo de Empresa]
	 */
	public function getLogo(){
		return $this->logo;
	}

	/**
	 * [setEstado - Setea valor al atributo]
	 * @param [int] $estado [Estado de Empresa]
	 */
	public function setEstado($estado){
		$this->estado = $estado;
	}

	/**
	 * [getEstado - Retorna el valor del atributo]
	 * @return [int] [Estado de Empresa]
	 */
	public function getEstado(){
		return $this->estado;
	}

	/**
	 * [setFechaIngreso - Setea valor al atributo]
	 * @param [string] $fecha_ingreso [Fecha Ingreso de Empresa]
	 */
	public function setFechaIngreso($fecha_ingreso){
		$this->fecha_ingreso = $fecha_ingreso;
	}

	/**
	 * [getFechaIngreso - Retorna el valor del atributo]
	 * @return [string] [Fecha Ingreso de Empresa]
	 */
	public function getFechaIngreso(){
		return $this->fecha_ingreso;
	}

	/**
	 * [setFechaModificacion - Setea valor al atributo]
	 * @param [string] $fecha_modificacion [Fecha Modificacion de Empresa]
	 */
	public function setFechaModificacion($fecha_modificacion){
		$this->fecha_modificacion = $fecha_modificacion;
	}

	/**
	 * [getFechaModificacion - Retorna el valor del atributo]
	 * @return [string] [Fecha Ingreso de Empresa]
	 */
	public function getFechaModificacion(){
		return $this->fecha_modificacion;
	}

	/**
	 * [setEstadoSQL - Setea valor al atributo]
	 * @param [int] $id [Status del SQL]
	 */
	public function setEstadoSQL($estadoSQL){
		$this->estadoSQL = $estadoSQL;
	}

	/**
	 * [getEstadoSQL - Retorna el valor del atributo]
	 * @return [int] [Status del SQL]
	 */
	public function getEstadoSQL(){
		return $this->estadoSQL;
	}

	/**
	 * [setArrayDeDatos - Setea valor al atributo]
	 * @param [array] $arrayDeDatos [Arreglo con todos los registros]
	 */
	public function setArrayDeDatos($arrayDeDatos){
		$this->arrayDeDatos = $arrayDeDatos;
	}

	/**
	 * [getArrayDeDatos - Retorna el valor del atributo]
	 * @return [array] [Arreglo con todos los registros]
	 */
	public function getArrayDeDatos(){
		return $this->arrayDeDatos;
	}
}

?>