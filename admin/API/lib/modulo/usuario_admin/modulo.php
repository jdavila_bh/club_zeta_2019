<?php
@session_start();
include_once('lib/class/usuario_admin.php');
include_once('lib/class/tipo_usuario.php');

$metodo_fun = array(
    'verificarUsuario'=>'POST',
    'traerSesionActiva'=>'POST',
    //'cambiarEstado'=>'POST',
    'cambiarClave'=>'POST',
    'registrarUsuario'=>'POST',
    'modificarUsuario'=>'POST',
    'traerTodosUsuarios'=>'POST',
    'traerUsuarios'=>'POST',
    'modificar_accesos'=>'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
    $datos = array(
        'codigo' => 404,
        'estado' => 0,
        'mensaje'=> 'Metodo no registrado en el modulo.',
        'metodo' => $funcion_modulo,
        'method' => $metodo
    );
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'verificarUsuario':

            $usuario = new usuario_admin();

            if($usuario->login($user,$pass)==1)
            {
                $datos['estado'] = 1;
                $datos['sesionActiva'] = $usuario->traerSesionActiva();
            }else{
                $datos['estado'] = 0;
            }

            break;

        case 'traerSesionActiva':

            $usuario = new usuario_admin();

            if($usuario->traerSesionActiva()!=null)
            {
                $datos['estado'] =1;
                $datos['sesion_activa']= $usuario->traerSesionActiva();
            }else{
                $datos['estado'] =0;
            }

            break;

        case 'cambiarClave':

            $usuario = new usuario_admin();
            if((int)$usuario->claveRegistro($id_registro,$nueva_clave)==1)
            {
                $datos['estado'] =1;
                $datos['msg'] = 'Cambio de clave exitoso.';
            }else{
                $datos['estado'] =0;
                $datos['msg'] = 'Problemas al cambiar la clave.';
            }

            break;

        case 'registrarUsuario':

            $usuario = new usuario_admin();

            $estadoUsuario = $usuario->verificarUserName($user);

            $usuario->setNombre(utf8_decode($nombre));
            $usuario->setApellido(utf8_decode($apellidop));
            $usuario->setUser(utf8_decode($user));
            $usuario->setPass(utf8_decode($pass));
            $usuario->setEmail(utf8_decode($email));
            $usuario->setRut(utf8_decode($rut));
            $usuario->setIdTipo(utf8_decode($tipo));
            $usuario->setEmail(utf8_decode($email));

            $usuario->setDetalle(utf8_decode($detalle));
            $usuario->setGrafico(utf8_decode($grafico));
            $usuario->setComprobante(utf8_decode($colegios));
            $usuario->setAdmusuarios(utf8_decode($admusuarios));

            if($estadoUsuario==0)
            {
                if($usuario->registrar()==1)
                {
                    $datos['estado'] = 1;
                    $datos['msg'] = 'Usuario registrado.';
                }else{
                    $datos['estado'] = 0;
                    $datos['msg'] = 'Problemas al registrar el usuario.';
                }
            }else{
                $datos['estado'] = 2;
                $datos['msg'] = 'Usuario ya existe.';
            }

            break;


        case 'modificarUsuario':

            $usuario = new usuario_admin();

            $usuario->traerRegistro($id_registro);

            $usuario->setNombre(utf8_decode($nombre));
            $usuario->setApellido(utf8_decode($apellidop));
            $usuario->setUser(utf8_decode($user));
            $usuario->setPass(utf8_decode($pass));
            $usuario->setEmail(utf8_decode($email));
            $usuario->setRut(utf8_decode($rut));
            $usuario->setEmail(utf8_decode($email));

            if($usuario->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al Actualizar el registro.';
            }

            break;


        case 'traerTodosUsuarios':


            $usuario = new usuario_admin();
            $tipo_usuario = new tipo_usuario();

            $datos['registros'] = array();

            if($usuario->traerTodosRegistro())
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registros encontrados.';

                while($row = mysql_fetch_object($usuario->getMatrizDatos()))
                {

                    $tipo_usuario->traerRegistro($row->id_tipo_usuario);

                    $datos['registros'][] = array(
                        'id' => $row->id_usuario_admin,
                        'nombre' => utf8_encode($row->nombre_usuario_admin),
                        'apellido' => utf8_encode($row->apellido_usuario_admin),
                        'user' => $row->user_usuario_admin,
                        'rut' => $row->rut_usuario_admin,
                        'id_tipo_usuario' => $row->id_tipo_usuario_admin,
                        'fecha_creacion' => $row->fecha_creacion_usuario_admin,
                        'fecha_modificacion' => $row->fecha_modificacion_usuario_admin,
                        'email' => $row->email_usuario_admin,

                        'detalle' => $row->detalle_compra,
                        'graficos' => $row->graficos,
                        'colegios' => $row->colegios,
                        'comprobante' => $row->comprobante,
                        'admusuarios' => $row->admusuarios,

                        'tipo_usuario_obj' => $tipo_usuario->traerObjeto()
                    );
                }
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al traer el registro.';
            }

            break;

        case 'traerUsuarios':


            $usuario = new usuario_admin();
            $tipo_usuario = new tipo_usuario();

            $datos['registros'] = array();

            if($usuario->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro encontrado.';

                $tipo_usuario->traerRegistro($usuario->getIdTipo());

                $datos['registros'][] = array(
                    'id' => $usuario->getIdTipo(),
                    'nombre' => $usuario->getNombre(),
                    'apellido' => $usuario->getApellido(),
                    'user' => $usuario->getUser(),
                    'rut' => $usuario->getRut(),
                    'id_tipo_usuario' => $usuario->getIdTipo(),
                    'fecha_creacion' => $usuario->getFechaCreacion(),
                    'fecha_modificacion' => $usuario->getFechaModificacion(),
                    'email' => $usuario->getEmail(),

                    'detalle' => $usuario->getDetalle(),
                    'graficos' => $usuario->getGrafico(),
                    'colegios' => $usuario->getColegios(),
                    'comprobante' => $usuario->getComprobante(),
                    'admusuarios' => $usuario->getAdmusuarios(),

                    'tipo_usuario_obj' => $tipo_usuario->traerObjeto()
                );



            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al encontrar el Registro.';
            }

            break;
        case 'modificar_accesos':

            $usuario = new usuario_admin();

            $usuario->traerRegistro($id_registro);

            $usuario->setDetalle($detalle);
            $usuario->setGrafico($graficos);
            $usuario->setColegios($colegios);
            $usuario->setComprobante($comprobantes);
            $usuario->setAdmusuarios($admusuarios);

            if($usuario->modificar_accesos($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al Actualizar el registro.';
            }

            break;
        default:

            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );

            break;

    }
}
?>