<?php
@session_start();
include_once('lib/class/provincia.php');
include_once('lib/class/region.php');

$metodo_fun = array(
        'registrarProvincia' => 'POST',
        'modificarProvincia' => 'POST',
        'traerTodosProvincias' => 'POST',
        'traerProvincia' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarProvincia':

            $provincia = new provincia();

            $provincia->setNombre(utf8_decode($nombre));
            $provincia->setIdRegion(utf8_decode($id_region));


            if($provincia->registrar()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al ingresar el registro.';
            }

        break;

        case 'modificarProvincia':

            $provincia = new provincia();

            $provincia->traerRegistro($id_registro);

            $provincia->setNombre(utf8_decode($nombre));
            $provincia->setIdRegion(utf8_decode($id_region));

            if($provincia->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosProvincias':

            $provincia = new provincia();

            $datos['registros'] = array();

            // FILTROS
            $filtrosql = array();

            if(isset($estado))
                $filtrosql['estado'] = $estado;
            if(isset($id_region))
                $filtrosql['id_region'] = $id_region;

            $estadoConsulta = $provincia->traerTodosRegistro($filtrosql);

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($provincia->getMatrizDatos()))
                {

                    $region = new region();
                    $region->traerRegistro($row->id_region);


                    $datos['registros'][] = array(
                        'id' => $row->id_provincia,
                        'nombre' => utf8_encode($row->nombre_provincia),
                        'id_region' => $row->id_region,
                        'regionObj' => $region->traerObjeto(),
                        'estado' => $row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }


            }else{
                $datos['estado'] = 0;
            }

        break;

        case 'traerProvincia':


            $provincia = new provincia();

            $datos['registros'] = array();

            if($provincia->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $datos['registros'] = array(
                    'id' => $provincia->getId(),
                    'nombre' => utf8_encode($provincia->getNombre()),
                    'id_region' => $provincia->getIdRegion(),
                    'estado' => $provincia->getEstado()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;

        case 'cambiarEstado':

            $provincia = new provincia();
            if((int)$provincia->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;

        default:
            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );
        break;

    }
}
?>