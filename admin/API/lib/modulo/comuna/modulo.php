<?php
@session_start();
include_once('lib/class/comuna.php');
include_once('lib/class/provincia.php');

$metodo_fun = array(
        'registrarComuna' => 'POST',
        'modificarComuna' => 'POST',
        'traerTodosComunas' => 'POST',
        'traerComuna' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarComuna':

            $comuna = new comuna();

            $comuna->setNombre(utf8_decode($nombre));
            $comuna->setIdProvincia(utf8_decode($id_provincia));


            if($comuna->registrar()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al ingresar el registro.';
            }

        break;

        case 'modificarComuna':

            $comuna = new comuna();

            $comuna->traerRegistro($id_registro);

            $comuna->setNombre(utf8_decode($nombre));
            $comuna->setIdProvincia(utf8_decode($id_provincia));

            if($comuna->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosComunas':

            $comuna = new comuna();

            $datos['registros'] = array();

            // FILTROS
            $filtrosql = array();

            if(isset($estado))
                $filtrosql['estado'] = $estado;
            if(isset($id_provincia))
                $filtrosql['id_provincia'] = $id_provincia;

            $estadoConsulta = $comuna->traerTodosRegistro($filtrosql);

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($comuna->getMatrizDatos()))
                {
                    $provincia = new provincia();
                    $provincia->traerRegistro($row->id_provincia);
                    $datos['registros'][] = array(
                        'id' => $row->id_comuna,
                        'nombre' => utf8_encode($row->nombre_comuna),
                        'id_provincia' => $row->id_provincia,
                        'provinciaObj' => $provincia->traerObjeto(),
                        'estado' => $row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }


            }else{
                $datos['estado'] = 0;
            }

        break;

        case 'traerComunas':


            $comuna = new comuna();

            $datos['registros'] = array();

            if($comuna->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $datos['registros'] = array(
                    'id' => $comuna->getId(),
                    'nombre' => utf8_encode($comuna->getNombre()),
                    'id_provincia' => $comuna->getIdProvincia(),
                    'estado' => $comuna->getEstado(),
                    'fecha_ingreso' => $comuna->getFechaIngreso(),
                    'fecha_modificacion' => $comuna->getFechaModificacion()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;

        case 'cambiarEstado':

            $comuna = new comuna();
            if((int)$comuna->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;

        default:
            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );
        break;

    }
}
?>