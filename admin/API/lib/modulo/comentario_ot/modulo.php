<?php
@session_start();
include_once('lib/class/comentario_ot.php');

$metodo_fun = array(
        'registrarComentarioOt' => 'POST',
        'modificarComentarioOt' => 'POST',
        'traerTodosComentarioOt' => 'POST',
        'traerComentarioOt' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarComentarioOt':

            $comentario_ot = new comentario_ot(); // TRAER REGISTROS FILTRADOS ESTADO 1
            $comentario_ot2 = new comentario_ot(); // CAMBIA ESTADO A 0
            $comentario_ot3 = new comentario_ot(); // VERFICAR SI EXISTE
            $comentario_ot4 = new comentario_ot(); // INGRESO O MODIFICACION


            /* DESACTIVAR DETALLE */
            $filtro_ref = array();

            $filtro_ref['estado'] = 1; // OBTENEMOS TODO EL DETALLE ACTIVO
            $filtro_ref['id_ot'] = $id_registro; // ID OT INGRESA DETALLE
            $filtro_ref['orden_sql'] = 'orden_comentario_ot ASC';


            $comentario_ot->setFiltroSql($filtro_ref);
            $estado_sql = $comentario_ot->traerTodosRegistro();



            if($estado_sql==1)
            {

                while($row = mysql_fetch_object($comentario_ot->getMatrizDatos()))
                {
                    $comentario_ot2->estadoRegistro($row->id_comentario_ot,0);
                }

            }
            /* FIN - DESACTIVAR DETALLE */

            // INGRESANDO O MODIFICANDO PRODUCTOS
            $cantidad_comentario= count($titulo);
            $cantidad_comentario_reg = 0;
            $cantidad_comentario_mod = 0;

            for($i=0 ; $i<count($titulo);$i++)
            {

                $filtro_ref = array();
                $filtro_ref['id_ot'] = $id_registro;
                $filtro_ref['orden_comentario_ot'] = $orden[$i];

                $comentario_ot3->setFiltroSql($filtro_ref); // FILTRAMOS POR OT y ORDEN
                $estado_sql = $comentario_ot3->traerRegistroOt(); //VERIFICAMOS SI EXISTE PARA INGRESAR NUEVO O ACTUALIZAR

                if($estado_sql) // VERIFICAMOS SI EXISTE
                {
                    // MODIFICAMOS REGISTRO EXISTENTE
                    $comentario_ot4->setIdOt(utf8_decode($id_registro));
                    $comentario_ot4->setTitulo(utf8_decode($titulo[$i]));
                    $comentario_ot4->setDetalle(utf8_decode($detalle[$i]));
                    $comentario_ot4->setOrden((int)$orden[$i]);
                    $comentario_ot4->setEstado(1);


                    if($comentario_ot4->modificar($comentario_ot3->getId())==1)
                    {
                        $cantidad_prod_mod++;
                    }

                }else{

                    // INGRESAMOS NUEVO REGISTRO
                    $comentario_ot4->setIdOt(utf8_decode($id_registro));
                    $comentario_ot4->setTitulo(utf8_decode($titulo[$i]));
                    $comentario_ot4->setDetalle(utf8_decode($detalle[$i]));
                    $comentario_ot4->setOrden((int)$orden[$i]);
                    $comentario_ot4->setEstado(1);


                    if($comentario_ot4->registrar()==1)
                    {
                        $cantidad_prod_reg++;
                    }
                }
            }
            

                $datos['estado'] = 1;
                $datos['cantidad_registrados'] = $cantidad_comentario_reg;
                $datos['cantidad_modificados'] = $cantidad_comentario_mod;
                $datos['cantidad_total'] = $cantidad_comentario_reg+$cantidad_comentario_mod;
                $datos['registros_recibidos'] = $cantidad_comentario;

                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';

        break;

        case 'modificarComentarioOt':

            $comentario_ot = new comentario_ot();

            $comentario_ot->traerRegistro($id_registro);

            $comentario_ot->setIdOt(utf8_decode($id_ot));
            $comentario_ot->setTitulo(utf8_decode($titulo));
            $comentario_ot->setDetalle(utf8_decode($detalle));
            $comentario_ot->setOrden(utf8_decode($orden));
            $comentario_ot->setEstado(1);

            if($comentario_ot->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosComentarioOt':

            $comentario_ot = new comentario_ot();

            $datos['registros'] = array();

            // FILTROS
            $filtrosql = array();

            if(isset($estado))
                $filtrosql['estado'] = $estado; //ACTIVO 1 - INACTIVO 0
            if(isset($id_ot))
                $filtrosql['id_ot'] = $id_ot; // ID OT
            if(isset($orden_registros))
                $filtrosql['orden_registros'] = $orden_registros; // order by ASC

            $comentario_ot->setFiltroSql($filtrosql);
            $estadoConsulta = $comentario_ot->traerTodosRegistro();

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($comentario_ot->getMatrizDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => (int)$row->id_comentario_ot,
                        'id_ot' => (int)$row->id_ot,
                        'titulo' => utf8_decode($row->titulo_comentario_ot),
                        'detalle' => utf8_decode($row->detalle_comentario_ot),
                        'orden' => (int)$row->orden_comentario_ot,
                        'estado' => (int)$row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }


            }else{
                $datos['estado'] = 0;
            }

        break;

        case 'traerDetalleOt':


            $comentario_ot = new comentario_ot();

            $datos['registros'] = array();

            if($comentario_ot->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $datos['registros'] = array(
                    'id' => (int)$comentario_ot->getId(),
                    'id_ot' => (int)$comentario_ot->getIdOt(),
                    'titulo' => utf8_encode($comentario_ot->getTitulo()),
                    'detalle' => utf8_encode($comentario_ot->getDetalle()),
                    'orden' => (int)$comentario_ot->getOrden(),
                    'estado' => (int)$comentario_ot->getEstado(),
                    'fecha_ingreso' => $comentario_ot->getFechaIngreso(),
                    'fecha_modificacion' => $comentario_ot->getFechaModificacion()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;

        case 'cambiarEstado':

            $comentario_ot = new comentario_ot();
            if((int)$comentario_ot->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;

        default:
            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );
        break;

    }
}
?>