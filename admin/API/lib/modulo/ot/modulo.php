<?php
@session_start();
include_once('lib/class/ot.php');
include_once('lib/class/cliente.php');
include_once('lib/class/tipo_contrato.php');
include_once('lib/class/tipo_pago.php');


$metodo_fun = array(
        'registrarOt' => 'POST',
        'modificarOt' => 'POST',
        'traerTodosOts' => 'POST',
        'traerOt' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarOt':

            $ot = new ot();

            $ot->setIdCliente((int)$id_cliente);
            $ot->setSolicitud(utf8_decode($solicitud));
            $ot->setSolicita(utf8_decode($solicita));
            $ot->setComentario(utf8_decode($comentario));
            $ot->setNota(utf8_decode($nota));

            if($ot->registrar()==1)
            {
                $datos['estado'] = 1;
                $datos['id_ot'] = $ot->getUltimoId();
                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al ingresar el registro.';
            }

        break;

        case 'modificarOt':

            $ot = new ot();

            $ot->traerRegistro($id_registro);

            //UPDATE CLIENTE
            if(isset($id_cliente))
            $ot->setIdCliente($id_cliente);
            if(isset($solicitud))
            $ot->setSolicitud(utf8_decode($solicitud));
            if(isset($solicita))
            $ot->setSolicita(utf8_decode($solicita));
            if(isset($comentario))
            $ot->setComentario(utf8_decode($comentario));
            if(isset($nota))
            $ot->setNota(utf8_decode($nota));

            //UPDATE DETALLE
            if(isset($id_tipo_pago))
            $ot->setIdTipoPago(utf8_decode($id_tipo_pago));
            if(isset($nfactura))
            $ot->setNfactura(utf8_decode($nfactura));

            if(isset($uf_dia))
            $ot->setUfDia(utf8_decode($uf_dia));

            if(isset($id_tipo_contrato))
                $ot->setIdTipoContrato(utf8_decode($id_tipo_contrato));

            if(isset($timing_dia))
            $ot->setTimmingDias(utf8_decode($timing_dia));

            if(isset($valor_hora))
            $ot->setValorHoraUf(utf8_decode($valor_hora));
            if(isset($timing_hora))
            $ot->setHoraProyecto(utf8_decode($timing_hora));



            if($ot->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosOts':

            $ot = new ot();

            $datos['registros'] = array();

            // FILTROS
            $filtro_ref = array();

            if(isset($estado))
                $filtro_ref['estado'] = $estado;

            $ot->setFiltroSql($filtro_ref);

            $estadoConsulta = $ot->traerTodosRegistro($filtrosql);

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($ot->getMatrizDatos()))
                {
                    $cliente = new cliente();
                    $cliente->traerRegistro((int)$row->id_cliente);

                    $tipo_contrato = new tipo_contrato();
                    $tipo_contrato->traerRegistro((int)$row->id_tipo_contrato);

                    $tipo_pago = new tipo_pago();
                    $tipo_pago->traerRegistro((int)$row->id_tipo_pago);

                    $datos['registros'][] = array(
                        'id' => $row->id_ot,
                        'id_cliente' => $row->id_cliente,
                        'objCliente' => $cliente->traerObjeto(),
                        'solicitud' => utf8_encode($row->solicitud_ot),
                        'solicita' => utf8_encode($row->solicita_ot),
                        'condicion_comercial' => utf8_encode($row->condicion_comercial_ot),
                        'id_tipo_contrato' => (int)$row->id_tipo_contrato,
                        'objTipoContrato' => $tipo_contrato->traerObjeto(),
                        'id_tipo_pago' => (int)$row->id_tipo_pago,
                        'objTipoPago' => $tipo_pago->traerObjeto(),
                        'uf_dia' => (int)$row->uf_dia_ot,
                        'valor_hora_uf' => (int)$row->valor_hora_uf_ot,
                        'nfactura' => $row->nfactura_ot,
                        'timming_dias' => (int)$row->timming_dias_ot,
                        'hora_proyecto' => (int)$row->hora_proyecto_ot,
                        'comentario' => utf8_encode($row->comentario_ot),
                        'nota' => utf8_encode($row->nota_ot),
                        'estado' => (int)$row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }


            }else{
                $datos['estado'] = 0;
            }

        break;

        case 'traerOt':


            $ot = new ot();

            $datos['registros'] = array();

            if($ot->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $datos['registros'] = array(
                    'id' =>(int)$ot->getId(),
                    'id_cliente' => (int)$ot->getIdCliente(),
                    'solicitud' => utf8_encode($ot->getSolicitud()),
                    'solicita' => utf8_encode($ot->getSolicita()),
                    'condicion_comercial' => utf8_encode($ot->getCondicionComercial()),
                    'id_tipo_contrato' => (int)$ot->getIdTipoContrato(),
                    'id_tipo_pago' => (int)$ot->getIdTipoPago(),
                    'uf_dia' => (int)$ot->getUfDia(),
                    'valor_hora' => (int)$ot->getValorHoraUf(),
                    'nfactura' => $ot->getNfactura(),
                    'timming_dias' => (int)$ot->getTimmingDias(),
                    'hora_proyecto' => (int)$ot->getHoraProyecto(),
                    'comentario' => utf8_encode($ot->getComentario()),
                    'nota' => utf8_encode($ot->getNota()),
                    'estado' => $ot->getEstado(),
                    'fecha_ingreso' => $ot->getFechaIngreso(),
                    'fecha_modificacion' => $ot->getFechaModificacion()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;

        case 'cambiarEstado':

            $ot = new ot();
            if((int)$ot->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;

        default:
            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );
        break;

    }
}
?>