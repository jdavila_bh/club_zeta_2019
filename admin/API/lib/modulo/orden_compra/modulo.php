<?php
@session_start();
include_once('lib/class/orden_compra.php');
include_once('lib/class/colegio.php');
include_once('lib/class/usuario.php');

$metodo_fun = array(
    'enviarMail'=>'POST',
    'traerRegistro'=>'POST',
    'traerTodosRegistro'=>'POST',
    'traerRegistroRut'=>'POST',
    'traerRegistroIdOrden'=>'POST',
    'traerRegistroVerComprobante'=>'POST',
    'mailprueba'=>'POST'


);

if($metodo!=$metodo_fun[$funcion_modulo])
{
    $datos = array(
        'codigo' => 404,
        'estado' => 0,
        'mensaje'=> 'Metodo no registrado en el modulo.',
        'metodo' => $funcion_modulo,
        'method' => $metodo
    );
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'traerTodosRegistros':
            $oc = new orden_compra();

            $datos['registros'] = array();

            if($oc->traerTodosRegistro())
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registros encontrados.';

                while($row = mysql_fetch_object($oc->getMatrizDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => $row->id_orden_compra,
                        'rut' => $row->rut_orden_compra,
                        'monto' => $row->monto_orden_compra,
                        'id_usuario' => $row->id_usuario,
                        'estado' => $row->estado_orden_compra,
                        'sid' => $row->sid_orden_compra,
                        'nalumno' => $row->nalumno_orden_compra,
                        'calumno' => $row->calumno_orden_compra,
                        'id_colegio' => $row->id_colegio,
                        'producto' => $row->producto_orden_compra,
                        'cantidad' => $row->cantidad_orden_compra,
                        'fecha' => $row->fecha_orden_compra,
                        'fecha_pagado' => $row->fecha_pagado_orden_compra,
                        'tipo_transaccion' => $row->tipo_transaccion_orden_compra,
                        'texto_tipo_pago' => $row->texto_tipo_pago_orden_compra,
                        'num_cuotas' => $row->num_cuotas_orden_compra,
                        'fin_tarjeta' => $row->fin_tarjeta_orden_compra,
                        'fecha_exp' => $row->fecha_exp_orden_compra,
                        'mac' => $row->mac_orden_compra,
                        'fecha_pagado' => $row->fecha_pagado_cap_orden_compra,
                        'ocupado' => $row->ocupado_orden_compra,
                        'fecha_ingreso' => $row->fecha_ingreso_orden_compra,
                        'commercecode' => $row->commercecode_orden_compra,
                        'sessionid' => $row->sessionid_orden_compra,
                        'vci' => $row->vci_orden_compra,
                        'texto_vci' => $row->texto_vci_orden_compra,
                        'resp_code' => $row->resp_code_orden_compra,
                        'texto_resp_code' => $row->texto_resp_code_orden_compra,
                        'detalle_rechazo' => $row->detalle_rechazo_orden_compra
                    );
                }
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al traer el registro.';
            }

            break;

        case 'traerRegistro':

            $oc = new orden_compra();
            $datos['registros'] = array();

            if($oc->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro encontrado.';

                $datos['registros'][] = array(
                    'id' => $oc->getId(),
                    'rut' => $oc->getRut(),
                    'monto' => $oc->getMonto(),
                    'id_usuario' => $oc->getIdUsuario(),
                    'estado' => $oc->getEstado(),
                    'sid' =>$oc->getSid(),
                    'nalumno' => $oc->getNalumno(),
                    'calumno' => $oc->getCalumno(),
                    'id_colegio' => $oc->getIdColegio(),
                    'producto' => $oc->getProducto(),
                    'cantidad' => $oc->getCantidad(),
                    'fecha' => $oc->getFecha(),
                    'fecha_pagado' => $oc->getFechaPagado(),
                    'tipo_transaccion' => $oc->getTipoTransaccion(),
                    'texto_tipo_pago' => $oc->getTextoTipoPago(),
                    'num_cuotas' => $oc->getNumCuotas(),
                    'fin_tarjeta' => $oc->getFinTarjeta(),
                    'fecha_exp' => $oc->getFechaExp(),
                    'mac' => $oc->getMac(),
                    'fecha_pagado' => $oc->getFechaPagado(),
                    'ocupado' => $oc->getOcupado(),
                    'fecha_ingreso' => $oc->getFechaIngreso(),
                    'commercecode' => $oc->getCommercecode(),
                    'sessionid' => $oc->getSessionid(),
                    'vci' => $oc->getVci(),
                    'texto_vci' => $oc->getTextoVci(),
                    'resp_code' => $oc->getRespCode(),
                    'texto_resp_code' => $oc->getTextoRespCode(),
                    'detalle_rechazo' => $oc->getDetalleRechazo()
                );
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al encontrar el Registro.';
            }

            break;

        case 'traerRegistroRut':

            $oc = new orden_compra();
            $datos['registros'] = array();

            if($oc->traerRegistroRut($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registros encontrados.';

                while($row = mysql_fetch_object($oc->getMatrizDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => $row->id_orden_compra,
                        'rut' => $row->rut_orden_compra,
                        'monto' => $row->monto_orden_compra,
                        'id_usuario' => $row->id_usuario,
                        'estado' => $row->estado_orden_compra,
                        'sid' => $row->sid_orden_compra,
                        'nalumno' => $row->nalumno_orden_compra,
                        'calumno' => $row->calumno_orden_compra,
                        'id_colegio' => $row->id_colegio,
                        'producto' => $row->producto_orden_compra,
                        'cantidad' => $row->cantidad_orden_compra,
                        'fecha' => $row->fecha_orden_compra,
                        'fecha_pagado' => $row->fecha_pagado_orden_compra,
                        'tipo_transaccion' => $row->tipo_transaccion_orden_compra,
                        'texto_tipo_pago' => $row->texto_tipo_pago_orden_compra,
                        'num_cuotas' => $row->num_cuotas_orden_compra,
                        'fin_tarjeta' => $row->fin_tarjeta_orden_compra,
                        'fecha_exp' => $row->fecha_exp_orden_compra,
                        'mac' => $row->mac_orden_compra,
                        'fecha_pagado' => $row->fecha_pagado_cap_orden_compra,
                        'ocupado' => $row->ocupado_orden_compra,
                        'fecha_ingreso' => $row->fecha_ingreso_orden_compra,
                        'commercecode' => $row->commercecode_orden_compra,
                        'sessionid' => $row->sessionid_orden_compra,
                        'vci' => $row->vci_orden_compra,
                        'texto_vci' => $row->texto_vci_orden_compra,
                        'resp_code' => $row->resp_code_orden_compra,
                        'texto_resp_code' => $row->texto_resp_code_orden_compra,
                        'detalle_rechazo' => $row->detalle_rechazo_orden_compra
                    );
                }
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al traer el registro.';
            }

            break;
        case 'traerRegistroIdOrden':

            $oc = new orden_compra();
            $datos['registros'] = array();

            if($oc->traerRegistroIdOrden($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registros encontrados.';

                while($row = mysql_fetch_object($oc->getMatrizDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => $row->id_orden_compra,
                        'rut' => $row->rut_orden_compra,
                        'monto' => $row->monto_orden_compra,
                        'id_usuario' => $row->id_usuario,
                        'estado' => $row->estado_orden_compra,
                        'sid' => $row->sid_orden_compra,
                        'nalumno' => $row->nalumno_orden_compra,
                        'calumno' => $row->calumno_orden_compra,
                        'id_colegio' => $row->id_colegio,
                        'producto' => $row->producto_orden_compra,
                        'cantidad' => $row->cantidad_orden_compra,
                        'fecha' => $row->fecha_orden_compra,
                        'fecha_pagado' => $row->fecha_pagado_orden_compra,
                        'tipo_transaccion' => $row->tipo_transaccion_orden_compra,
                        'texto_tipo_pago' => $row->texto_tipo_pago_orden_compra,
                        'num_cuotas' => $row->num_cuotas_orden_compra,
                        'fin_tarjeta' => $row->fin_tarjeta_orden_compra,
                        'fecha_exp' => $row->fecha_exp_orden_compra,
                        'mac' => $row->mac_orden_compra,
                        'fecha_pagado' => $row->fecha_pagado_cap_orden_compra,
                        'ocupado' => $row->ocupado_orden_compra,
                        'fecha_ingreso' => $row->fecha_ingreso_orden_compra,
                        'commercecode' => $row->commercecode_orden_compra,
                        'sessionid' => $row->sessionid_orden_compra,
                        'vci' => $row->vci_orden_compra,
                        'texto_vci' => $row->texto_vci_orden_compra,
                        'resp_code' => $row->resp_code_orden_compra,
                        'texto_resp_code' => $row->texto_resp_code_orden_compra,
                        'detalle_rechazo' => $row->detalle_rechazo_orden_compra
                    );
                }
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al traer el registro.';
            }

            break;
        case 'traerRegistroVerComprobante':

            $oc = new orden_compra();
            $datos['registros'] = array();

            if($oc->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro encontrado.';

                $colegio = new colegio();
                $colegio->traerRegistro($oc->getIdColegio());

                $usuario = new usuario();
                $usuario->traerRegistro($oc->getIdUsuario());

                $datos['registros'][] = array(
                    'id_usuario' => $oc->getIdUsuario(),
                    'nombre_usuario' => $usuario->getNombre()." ".$usuario->getApellidoPaterno()." ".$usuario->getApellidoMaterno(),
                    'sid' =>$oc->getSid(),
                    'rut' => $oc->getRut(),
                    'fecha' => $oc->getFecha(),
                    'cantidad' => producto($oc->getCantidad()),
                    'nalumno' => $oc->getNalumno(),
                    'calumno' => $oc->getCalumno(),
                    'precio_unidad' => precio_unidad($oc->getMonto(), $oc->getCantidad()),
                    'id_colegio' => $oc->getIdColegio(),
                    'nombre_colegio' => $colegio->getNombre(),
                    'monto' => monto($oc->getMonto()),
                    'id' => $oc->getId(),
                    'tipo_pago' => tipoPago($oc->getTipoPago()),
                    'num_cuotas' => cuotas($oc->getNumCuotas()),
                    'tipo_cuotas' =>tipo_cuota($oc->getTipoPago()),
                    'tarjeta' => tarjeta($oc->getTipoPago()),
                    'fin_tarjeta' => nroTarjeta($oc->getFinTarjeta()),

                    'estado' => $oc->getEstado(),
                    'producto' => $oc->getProducto(),
                    'fecha_pagado' => $oc->getFechaPagado(),
                    'tipo_transaccion' => $oc->getTipoTransaccion(),
                    'texto_tipo_pago' => tipoPago($oc->getTextoTipoPago()),
                    'fecha_exp' => $oc->getFechaExp(),
                    'mac' => $oc->getMac(),
                    'fecha_pagado' => $oc->getFechaPagado(),
                    'ocupado' => $oc->getOcupado(),
                    'fecha_ingreso' => $oc->getFechaIngreso(),
                    'commercecode' => $oc->getCommercecode(),
                    'sessionid' => $oc->getSessionid(),
                    'vci' => $oc->getVci(),
                    'texto_vci' => $oc->getTextoVci(),
                    'resp_code' => $oc->getRespCode(),
                    'texto_resp_code' => $oc->getTextoRespCode(),
                    'detalle_rechazo' => $oc->getDetalleRechazo()

                );
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al encontrar el Registro.';
            }

            break;
        case 'mailprueba':
            echo mail("johan@bhstudios.com","prueba ","Email message","From: johan@hbstudios.com");
            break;
        case 'enviarMail':

            include('../../PHPMailer/PHPMailerAutoload.php');
            //$id_registro;
            //$tipo;
            /*envio de correo*/

            $oc = new orden_compra();
            $datos['registros'] = array();

            if($oc->traerRegistro($id_registro))
            {

                $datos['estado'] = 1;
                $datos['msg'] = 'Registro encontrado.';

                $colegio = new colegio();
                $colegio->traerRegistro($oc->getIdColegio());

                $usuario = new usuario();
                $usuario->traerRegistro($oc->getIdUsuario());

                //$para_user = $usuario->getEmail(); // USUARIO
                $para_user = 'johan@bhstudios.com'; // USUARIO
                $para_bh = 'formulario@bhstudios.com';// BH
                //$para_cole = $colegio->getMailAdministrador(); // COLEGIO
                $para_cole = 'johan@bhstudios.com'; // COLEGIO

                /*header correo */

                $header = "Mime-Version: 1.0 \r\n";
                $header .= "Content-Type: text/html; charset=iso-8859-1\r\n";
                $header .= 'From: pagos@clubzeta.com';

                /*Body correo*/
                $mensaje = '
                        <table style="font-family: \'Arial\', sans-serif;border-collapse: collapse;border-spacing: 0;color: #444444;width: 750px; background-image: url(\'../images/comprobante/fondo.png\');background-repeat: no-repeat;background-position: center;background-size: 350px; background-color: #ffffff;">
                            <tr>
                                <td colspan="3">
                                    <table style=" width: 100%;">
                                        <tr>
                                            <td colspan="2" style="font-size:28px;padding-top: 10px;padding-right: 10px;padding-left: 10px;">Talonario válido sólo para 2018</td>
                                            <td style="vertical-align: bottom;text-align: left; background-image: url(\'../images/comprobante/baner1.jpg\');background-repeat: no-repeat;"><img src="../images/comprobante/logo.png" style="width: 80px; margin-top: 10px;"><span class="azul_oscuro size-sub-titulo">Servicio de alimentación</span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: center;" >
                                    <span style="font-size:28px;color: #74B913;">Comprobante</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: center;padding-bottom: 20px;padding-top: 20px;padding-right: 20px;padding-left: 20px;">
                                    <span style="color: #0A4187;">Imprima y presente este comprobante en su casino. Impresión no válida para canje de almuerzo, se debe retirar cuponeras correspodientes en casino. Canje al portador de este documento.</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Nombre Comprador:</span>&nbsp;<span id="ver_nombre">'.$usuario->getNombre()." ".$usuario->getApellidoPaterno()." ".$usuario->getApellidoMaterno().'</span></td>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Código de autorización:</span>&nbsp;<span id="ver_cod_aut">'.$oc->getSid().'</span></td>
                                <td rowspan="7" style="text-align: center;vertical-align: bottom;"><img src="../images/comprobante/pagado.png" class="" style=" width: 100px;"></td>
                            </tr>
                            <tr>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Rut:</span>&nbsp;<span id="ver_rut">'.$oc->getRut().'</span></td>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Tipo de Transacción:</span>&nbsp;<span id="ver_tipo_tran">VENTA</span></td>

                            </tr>
                            <tr>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Fecha:</span>&nbsp;<span id="fecha">'.$oc->getFecha().'</span></td>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Producto:</span>&nbsp;<span id="ver_prod">'.producto($oc->getCantidad()).'</span></td>

                            </tr>
                            <tr>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Alumno:</span>&nbsp;<span id="ver_nomb_alum">'.$oc->getNalumno().'</span></td>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Precio Unitario:</span>&nbsp;<span id="ver_precio_uni">'.precio_unidad($oc->getMonto(), $oc->getCantidad()).'</span></td>

                            </tr>
                            <tr>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Nº orden:</span>&nbsp;<span id="nro_orden">'.$oc->getId().'</span></td>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Monto total:</span>&nbsp;<span id="ver_monto_total">'.monto($oc->getMonto()).'</span></td>

                            </tr>
                            <tr>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Número de Cuotas:</span>&nbsp;<span id="ver_nro_cuotas">'.cuotas($oc->getNumCuotas()).'</span></td>
                                <td>&nbsp;</td>

                            </tr>
                            <tr>
                                <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">'.tarjeta($oc->getTipoPago()).'</span>&nbsp;<span id="nro_tarjeta">'.nroTarjeta($oc->getFinTarjeta()).'</span></td>
                                <td>&nbsp;</td>

                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3"style="text-align: left;padding-bottom: 20px;padding-top: 20px;padding-right: 30px;padding-left: 30px;">
                                    <span>"En caso de requerir y/o Reembolsos, favor de contactar a Departamento Finanzas al teléfono (+56-2) 29 57 41 53 (Anexo 231) o al Mail finanzas@clubzeta.cl.</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3" bgcolor="#0A4187" style="background-color: #0A4187;text-align: center;padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><a href="www.clubzeta.cl" class="" style="color: #FFFFFF;text-decoration: none;">www.clubzeta.cl</a></td>
                            </tr>
                        </table>';

                $mail = new PHPMailer;


                /*
                $mail->IsSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'mail.bhstudios.com';                 // Specify main and backup server
                $mail->Port = 465;                                    // Set the SMTP port
                //$mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'johan@bhstudios.com';          // SMTP username
                $mail->Password = 'bhstudio2018';           // SMTP password
                //$mail->SMTPSecure = 'tls';                          // Enable encryption, 'ssl' also accepted
                */
                $mail->From = 'pagos@clubzeta.cl';

                $mail->IsHTML(true);                                  // Set email format to HTML

                $de = ($tipo==1)?'Usuario':"Administrador";

                $mail->Subject = 'Comprobante Club Zeta - '.$de;
                $mail->Body    = $mensaje ;
                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                $mail->FromName = 'Comprobante de Compra';


                if($tipo==1){//usuario
                    //var_dump("<script>console.log('ENVIO DE CORREO Cliente')</script>");
                    $mail->AddAddress($para_user); // CLIENTE
                    $mail->addCC($para_bh); // BH

                    if($mail->Send()){
                    //if(1==1){
                        $datos['estado'] = 1;
                        $datos['msg'] = 'Correo enviado al Cliente.';
                    }else{
                        $datos['estado'] = 0;
                        $datos['msg'] = 'Falla Correo NO enviado al Cliente.';
                    }
                }else{//administrador
                    //var_dump("<script>console.log('ENVIO DE CORREO Admin')</script>");
                    $mail->AddAddress($para_cole); // COLEGIO
                    $mail->addCC($para_bh); // BH

                    /*
                     for($i=0;$i<count($para_cole);$i++){
                        $mail->addCC($para_cole); // COLEGIO
                        if($i==0)
                        {
                            $correos_cole = $para_cole;
                        }else{
                            $correos_cole .= ','.$para_cole;
                        }
                    }
                    */

                    if($mail->Send()){
                    //if(1==1){
                        $datos['estado'] = 1;
                        $datos['msg'] = 'Correo enviado al Administrador';
                    }else{
                        $datos['estado'] = 0;
                        $datos['msg'] = 'Problemas al enviar el Mail al Administrador.';
                    }
                }

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al encontrar el Registro.';
            }

            break;

        default:

            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );

            break;

    }
}

function precio_unidad($monto, $cantida )
{
    return number_format(($monto/$cantida),0,',','.');
}

function tipoPago($id)
{
    if ($id == "VD") {
        $valor = "Redcompra";
    } else {
        $valor = "Crédito";
    }
    return $valor;
}

function monto($monto){
    return "Ch $".number_format($monto,0,',','.');
}

function cuotas($cuotas)
{
    if ($cuotas < 10) {
        $numero_cuotas = '0' . $cuotas;
    } else {
        $numero_cuotas = $cuotas;
    }
    return $numero_cuotas;
}

function tipo_cuota($tipo)
{
    $tipo_cuota = "";
    switch ($tipo) {
        case 'VN':
            $tipo_cuota = "Sin Cuotas";
            break;

        case 'VC':
            $tipo_cuota = "Cuotas normales";
            break;

        case 'SI':
            $tipo_cuota = "Sin Interés";
            break;

        case 'CI':
            $tipo_cuota = "Cuotas Comercio";
            break;

        case 'VD':
            $tipo_cuota = "Débito";
            break;
    }
    return $tipo_cuota;
}

function tarjeta($tarjeta)
{
    if ($row_no[14] == "VD") {
        $tipo = "Debito";
    } else {
        $tipo = "Crédito";
    }
    return "Tarjeta de " . $tipo . ":";
}
function nroTarjeta($nro)
{
    return "XXXX-XXXX-XXXX-".$nro;
}

function producto ($cant)
{
    return $cant." Cuponeras restaurant";
}
?>