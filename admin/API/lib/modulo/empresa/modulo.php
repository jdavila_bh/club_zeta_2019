<?php 
/**
* @author Luis Ramos <luisenrique545@gmail.com>
* Date: 18/01/2018  11:52   (dd-mm-yyyy)
* 
*/

@session_start();
include_once('lib/class/empresa.php');

$metodo_fun = array(
    'obtenerEmpresas' => 'POST',
    'agregarEmpresa' => 'POST',
    'modificarEmpresa' => 'POST',
    'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
		'codigo' => 404,
		'estado' => 0,
		'mensaje'=> 'Metodo no registrado en el modulo.',
		'metodo' => $funcion_modulo,
		'method' => $metodo
	);	
}else{

	extract($_REQUEST);

	switch($funcion_modulo)
    {

        case 'obtenerEmpresas':

            $empresa = new empresa();


            $datos['registros'] = array();


            $estadoConsulta = $empresa->obtenerEmpresas();

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($empresa->getArrayDeDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => $row->id_empresa,
                        'nombre' => $row->nombre_empresa,
                        'rut' => $row->rut_empresa,
                        'direccion' => $row->direccion_empresa,
                        'telefono' => $row->telefono_empresa,
                        'giro' => $row->giro_empresa,
                        'logo' => $row->logo_empresa,
                        'estado' => $row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }
            }else{
                $datos['estado'] = 0;
            }
        break;

        case 'agregarEmpresa':

            $empresa = new empresa();

            $empresa->setNombre(utf8_decode($nombre));
            $empresa->setRut($rut);
            $empresa->setDireccion(utf8_decode($direccion));
            $empresa->setTelefono($telefono);
            $empresa->setGiro(utf8_decode($giro));

            if($empresa->agregarEmpresa()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al ingresar el registro.';
            }

        break;

        case 'modificarEmpresa':

            $empresa = new empresa();

            $empresa->setId($id);
            $empresa->setNombre(utf8_decode($nombre));
            $empresa->setRut($rut);
            $empresa->setDireccion(utf8_decode($direccion));
            $empresa->setTelefono($telefono);
            $empresa->setGiro(utf8_decode($giro));
            $empresa->setFechaModificacion(date('Y-m-d H:i:s'));

            //cho $empresa->getFechaModificacion();

            if($empresa->modificarEmpresa()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Modificado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al modificar el registro.';
            }

        break;

        case 'cambiarEstado':

            $empresa = new empresa();

            if($empresa->cambiarEstado($id_registro, $nuevo_estado)==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Estado cambiado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al cambiar el estado.';
            }

        break;


        default:

            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );
        break;
    }
}

?>