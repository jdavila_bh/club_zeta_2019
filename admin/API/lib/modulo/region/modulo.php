<?php
@session_start();
include_once('lib/class/region.php');

$metodo_fun = array(
        'registrarRegion' => 'POST',
        'modificarRegion' => 'POST',
        'traerTodosRegiones' => 'POST',
        'traerRegion' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarRegion':

            $region = new region();

            $region->setNombre(utf8_decode($nombre));
            $region->setIso(utf8_decode($iso));


            if($region->registrar()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al ingresar el registro.';
            }

        break;


        case 'modificarRegion':

            $region = new region();

            $region->traerRegistro($id_registro);

            $region->setNombre(utf8_decode($nombre));
            $region->setIso(utf8_decode($iso));

            if($region->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosRegiones':

            $region = new region();


            $datos['registros'] = array();


            // FILTROS
            $filtrosql = array();

            if(isset($estado))
                $filtrosql['estado'] = $estado;

            $estadoConsulta = $region->traerTodosRegistro($filtrosql);

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($region->getMatrizDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => $row->id_region,
                        'nombre' => utf8_encode($row->nombre_region),
                        'estado' => $row->estado,
                        'iso' => $row->ISO_3166_2_CL,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }


            }else{
                $datos['estado'] = 0;
            }

        break;


        case 'traerRegion':


            $region = new region();

            $datos['registros'] = array();

            if($region->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $datos['registros'] = array(
                    'id' => $region->getId(),
                    'nombre' => utf8_encode($region->getNombre()),
                    'estado' => $region->getEstado(),
                    'iso' => $region->getIso()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;


        case 'cambiarEstado':

            $region = new region();
            if((int)$region->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;


        default:

            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );

            break;

    }
}
?>