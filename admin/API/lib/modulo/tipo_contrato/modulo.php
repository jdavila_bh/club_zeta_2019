<?php
@session_start();
include_once('lib/class/tipo_contrato.php');

$metodo_fun = array(
        'registrarTipoContrato' => 'POST',
        'modificarTipoContrato' => 'POST',
        'traerTodosTipoContratos' => 'POST',
        'traerTipoContrato' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarTipoContrato':

            $tipoContrato = new tipo_contrato();

            $tipoContrato->setNombre(utf8_decode($nombre));

            if($tipoContrato->registrar()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al ingresar el registro.';
            }

        break;


        case 'modificarTipoContrato':

            $tipoContrato = new tipo_contrato();

            $tipoContrato->traerRegistro($id_registro);

            $tipoContrato->setNombre(utf8_decode($nombre));

            if($tipoContrato->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosTipoContratos':

            $tipoContrato = new tipo_contrato();

            $datos['registros'] = array();

            // FILTROS
            $filtrosql = array();

            if(isset($estado))
                $filtrosql['estado'] = $estado;

            $estadoConsulta = $tipoContrato->traerTodosRegistro($filtrosql);

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($tipoContrato->getMatrizDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => $row->id_tipo_contrato,
                        'nombre' => utf8_encode($row->nombre_tipo_contrato),
                        'estado' => $row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }


            }else{
                $datos['estado'] = 0;
            }

        break;


        case 'traerTipoContrato':


            $tipoContrato = new tipo_contrato();

            $datos['registros'] = array();

            if($tipoContrato->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $datos['registros'] = array(
                    'id' => $tipoContrato->getId(),
                    'nombre' => utf8_encode($tipoContrato->getNombre()),
                    'estado' => $tipoContrato->getEstado(),
                    'fecha_ingreso' => $tipoContrato->getFechaIngreso(),
                    'fecha_modificacion' => $tipoContrato->getFechaModificacion()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;


        case 'cambiarEstado':

            $tipoContrato = new tipo_contrato();
            if((int)$tipoContrato->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;


        default:

            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );

            break;

    }
}
?>