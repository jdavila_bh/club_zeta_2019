<?php
@session_start();
include_once('lib/class/detalle_ot.php');

$metodo_fun = array(
        'registrarDetalleOt' => 'POST',
        'modificarDetalleOt' => 'POST',
        'traerTodosDetalleOt' => 'POST',
        'traerDetalleOt' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarDetalleOt':

            $detalle_ot = new detalle_ot(); // TRAER REGISTROS FILTRADOS ESTADO 1
            $detalle_ot2 = new detalle_ot(); // CAMBIA ESTADO A 0
            $detalle_ot3 = new detalle_ot(); // VERFICAR SI EXISTE
            $detalle_ot4 = new detalle_ot(); // INGRESO O MODIFICACION


            /* DESACTIVAR DETALLE */
            $filtro_ref = array();

            $filtro_ref['estado'] = 1; // OBTENEMOS TODO EL DETALLE ACTIVO
            $filtro_ref['id_ot'] = $id_registro; // ID OT INGRESA DETALLE
            $filtro_ref['orden_sql'] = 'orden_detalle_ot ASC';


            $detalle_ot->setFiltroSql($filtro_ref);
            $estado_sql = $detalle_ot->traerTodosRegistro();



            if($estado_sql==1)
            {

                while($row = mysql_fetch_object($detalle_ot->getMatrizDatos()))
                {
                    $detalle_ot2->estadoRegistro($row->id_detalle_ot,0);
                }

            }
            /* FIN - DESACTIVAR DETALLE */

            // INGRESANDO O MODIFICANDO PRODUCTOS
            $cantidad_prod = count($cantidad);
            $cantidad_prod_reg = 0;
            $cantidad_prod_mod = 0;

            for($i=0 ; $i<count($cantidad);$i++)
            {

                $filtro_ref = array();
                $filtro_ref['id_ot'] = $id_registro;
                $filtro_ref['orden_detalle_ot'] = $orden[$i];

                $detalle_ot3->setFiltroSql($filtro_ref); // FILTRAMOS POR OT y ORDEN
                $estado_sql = $detalle_ot3->traerRegistroOt(); //VERIFICAMOS SI EXISTE PARA INGRESAR NUEVO O ACTUALIZAR

                if($estado_sql) // VERIFICAMOS SI EXISTE
                {
                    // MODIFICAMOS REGISTRO EXISTENTE
                    $detalle_ot4->setIdOt(utf8_decode($id_registro));
                    $detalle_ot4->setCantidad((int)$cantidad[$i]);
                    $detalle_ot4->setDescripcion(utf8_decode($descripcion[$i]));
                    $detalle_ot4->setUf((int)$cant_uf[$i]);
                    $detalle_ot4->setUnitario((int)$unitario[$i]);
                    $detalle_ot4->setDescuento((int)$descuento[$i]);
                    $detalle_ot4->setOrden((int)$orden[$i]);
                    $detalle_ot4->setEstado(1);


                    if($detalle_ot4->modificar($detalle_ot3->getId())==1)
                    {
                        $cantidad_prod_mod++;
                    }

                }else{

                    // INGRESAMOS NUEVO REGISTRO
                    $detalle_ot4->setIdOt(utf8_decode($id_registro));
                    $detalle_ot4->setCantidad((int)$cantidad[$i]);
                    $detalle_ot4->setDescripcion(utf8_decode($descripcion[$i]));
                    $detalle_ot4->setUf((int)$cant_uf[$i]);
                    $detalle_ot4->setUnitario((int)$unitario[$i]);
                    $detalle_ot4->setDescuento((int)$descuento[$i]);
                    $detalle_ot4->setOrden((int)$orden[$i]);
                    $detalle_ot4->setEstado(1);


                    if($detalle_ot4->registrar()==1)
                    {
                        $cantidad_prod_reg++;
                    }
                }
            }


                $datos['estado'] = 1;
                $datos['cantidad_registrados'] = $cantidad_prod_reg;
                $datos['cantidad_modificados'] = $cantidad_prod_mod;
                $datos['cantidad_total'] = $cantidad_prod_reg+$cantidad_prod_mod;
                $datos['registros_recibidos'] = $cantidad_prod;

                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';

        break;

        case 'modificarDetalleOt':

            $detalle_ot = new detalle_ot();

            $detalle_ot->traerRegistro($id_registro);

            $detalle_ot->setIdOt(utf8_decode($id_ot));
            $detalle_ot->setCantidad(utf8_decode($cantidad));
            $detalle_ot->setDescripcion(utf8_decode($descripcion));
            $detalle_ot->setUf(utf8_decode($uf));
            $detalle_ot->setUnitario(utf8_decode($unitario));
            $detalle_ot->setDescuento(utf8_decode($descuento));

            if($detalle_ot->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosDetalleOt':

            $detalle_ot = new detalle_ot();

            $datos['registros'] = array();

            // FILTROS
            $filtrosql = array();

            if(isset($estado))
                $filtrosql['estado'] = $estado;
            if(isset($id_ot))
                $filtrosql['id_ot'] = $id_ot;

             $filtrosql['orden_sql'] = 'orden_detalle_ot ASC';

            $detalle_ot->setFiltroSql($filtrosql);
            $estadoConsulta = $detalle_ot->traerTodosRegistro();

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($detalle_ot->getMatrizDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => (int)$row->id_detalle_ot,
                        'id_ot' => (int)$row->id_ot,
                        'cantidad' => (int)$row->cantidad_detalle_ot,
                        'descripcion' => utf8_encode($row->descripcion_detalle_ot),
                        'cant_uf' => $row->uf_detalle_ot,
                        'unitario' => (int)$row->unitario_detalle_ot,
                        'descuento' => (int)$row->descuento_detalle_ot,
                        'orden' => (int)$row->orden_detalle_ot,
                        'estado' => (int)$row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }


            }else{
                $datos['estado'] = 0;
            }

        break;

        case 'traerDetalleOt':


            $detalle_ot = new detalle_ot();

            $datos['registros'] = array();

            if($detalle_ot->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $datos['registros'] = array(
                    'id' => (int)$detalle_ot->getId(),
                    'id_ot' => (int)$detalle_ot->getIdOt(),
                    'cantidad' => (int)$detalle_ot->getCantidad(),
                    'descripcion' => utf8_encode($detalle_ot->getDescripcion()),
                    'cant_uf' => $detalle_ot->getUf(),
                    'unitario' => (int)$detalle_ot->getUnitario(),
                    'descuento' => (int)$detalle_ot->getDescuento(),
                    'orden' => (int)$detalle_ot->getOrden(),
                    'estado' => (int)$detalle_ot->getEstado(),
                    'fecha_ingreso' => $detalle_ot->getFechaIngreso(),
                    'fecha_modificacion' => $detalle_ot->getFechaModificacion()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;

        case 'cambiarEstado':

            $detalle_ot = new detalle_ot();
            if((int)$detalle_ot->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;

        default:
            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );
        break;

    }
}
?>