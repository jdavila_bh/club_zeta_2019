<?php
@session_start();
include_once('lib/class/tipo_usuario.php');

$metodo_fun = array(
        'registrarTipoUsuario' => 'POST',
        'modificarTipoUsuario' => 'POST',
        'traerTodosTipoUsuarios' => 'POST',
        'traerTipoUsuario' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarTipoUsuario':

            $tipo_usuario = new tipo_usuario();

            $tipo_usuario->setNombre(utf8_decode($nombre));


            if($tipo_usuario->registrar()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Tipo Usuario registrado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al registrar el Tipo Usuario.';
            }

        break;


        case 'modificarTipoUsuario':

            $tipo_usuario = new tipo_usuario();

            $tipo_usuario->traerRegistro($id_registro);

            $tipo_usuario->setNombre(utf8_decode($nombre));

            if($tipo_usuario->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosTipoUsuarios':

            $tipoUsuario = new tipo_usuario();


            $datos['tipo_usuarios'] = array();
            $estado = $tipoUsuario->traerTodosRegistro();

            if($estado==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($tipoUsuario->getMatrizDatos()))
                {
                    $datos['tipo_usuarios'][] = array(
                        'id' => $row->id_tipo_usuario,
                        'nombre' => $row->nombre_tipo_usuario,
                        'estado' => $row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }


            }else{
                $datos['estado'] = 0;
            }

        break;


        case 'traerTipoUsuario':


            $tipo_usuario = new tipo_usuario();

            $datos['registros'] = array();

            if($tipo_usuario->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $datos['registros'] = array(
                    'id' => $tipo_usuario->getId(),
                    'nombre' => utf8_encode($tipo_usuario->getNombre()),
                    'estado' => $tipo_usuario->getEstado(),
                    'fecha_ingreso' => $tipo_usuario->getFechaIngreso(),
                    'fecha_modificacion' => $tipo_usuario->getFechaModificacion()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;


        case 'cambiarEstado':

            $tipo_usuario = new tipo_usuario();
            if((int)$tipo_usuario->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;


        default:

            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );

            break;

    }
}
?>