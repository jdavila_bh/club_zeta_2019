<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 13/10/2017
 * Time: 10:41
 */

class PDF_OT extends TCPDF{


    public $imagen_file =  'lib/modulo/pdf/images/logo1.png';
    public $ext_imagen_file = 'png';

    public $size_header = 10;
    public $bold_header = 'N';
    public $style_header = 'helvetica';

    public $col_w = 0;
    public $col_h = 0;
    public $pos_x = 0;
    public $pos_y = 0;

    public $html_header = '';


    //Page header
    public function Header()
    {
        // Logo
        $image_file =
        $this->Image($this->getImagenFile(), 15, 10, 60, '', $this->getExtImagenFile(), '', 'T', false, 72, '', false, false, 0, false, false, false);
        // Set font
        $this->writeHTMLCell($this->getColW(), $this->getColH(), $this->getPosX(), $this->pos_y, $this->getHtmlHeader(), 0, 1, 0, true, 'J', true);

    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    /**
     * @return string
     */
    public function getImagenFile()
    {
        return $this->imagen_file;
    }

    /**
     * @param string $imagen_file
     */
    public function setImagenFile($imagen_file)
    {
        $this->imagen_file = $imagen_file;
    }

    /**
     * @return string
     */
    public function getExtImagenFile()
    {
        return $this->ext_imagen_file;
    }

    /**
     * @param string $ext_imagen_file
     */
    public function setExtImagenFile($ext_imagen_file)
    {
        $this->ext_imagen_file = $ext_imagen_file;
    }

    /**
     * @return int
     */
    public function getSizeHeader()
    {
        return $this->size_header;
    }

    /**
     * @param int $size_header
     */
    public function setSizeHeader($size_header)
    {
        $this->size_header = $size_header;
    }

    /**
     * @return string
     */
    public function getBoldHeader()
    {
        return $this->bold_header;
    }

    /**
     * @param string $bold_header
     */
    public function setBoldHeader($bold_header)
    {
        $this->bold_header = $bold_header;
    }

    /**
     * @return string
     */
    public function getStyleHeader()
    {
        return $this->style_header;
    }

    /**
     * @param string $style_header
     */
    public function setStyleHeader($style_header)
    {
        $this->style_header = $style_header;
    }

    /**
     * @return string
     */
    public function getEmpresaTxt()
    {
        return $this->empresa_txt;
    }

    /**
     * @return int
     */
    public function getColW()
    {
        return $this->col_w;
    }

    /**
     * @param int $col_w
     */
    public function setColW($col_w)
    {
        $this->col_w = $col_w;
    }

    /**
     * @return int
     */
    public function getColH()
    {
        return $this->col_h;
    }

    /**
     * @param int $col_h
     */
    public function setColH($col_h)
    {
        $this->col_h = $col_h;
    }

    /**
     * @return int
     */
    public function getPosX()
    {
        return $this->pos_x;
    }

    /**
     * @param int $pos_x
     */
    public function setPosX($pos_x)
    {
        $this->pos_x = $pos_x;
    }

    /**
     * @return int
     */
    public function getPosY()
    {
        return $this->pos_y;
    }

    /**
     * @param int $pos_y
     */
    public function setPosY($pos_y)
    {
        $this->pos_y = $pos_y;
    }

    /**
     * @return string
     */
    public function getHtmlHeader()
    {
        return $this->html_header;
    }

    /**
     * @param string $html_header
     */
    public function setHtmlHeader($html_header)
    {
        $this->html_header = $html_header;
    }



}