<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 12/05/2017
 * Time: 8:47
 */

extract($_REQUEST);

$decoded = base64_decode($PdfDte);
$file = date('YmdHisu').'.pdf';
file_put_contents($file, $decoded);

if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    exit;
}