<?php
require_once('config/tcpdf_include.php');
require_once('config/PDF_OT.php');
include('config.php');

include_once('lib/class/ot.php');
include_once('lib/class/cliente.php');

include_once('lib/class/region.php');
include_once('lib/class/provincia.php');
include_once('lib/class/comuna.php');

include_once('lib/class/tipo_contrato.php');
include_once('lib/class/tipo_pago.php');

include_once('lib/class/detalle_ot.php');
include_once('lib/class/comentario_ot.php');




extract($_REQUEST);

// HEADER
$pdf = new PDF_OT(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setColW(100);
$pdf->setColH(10);
$html = <<<EOF
<span style="color:#666666;font-size:12px;font-weight:bold;">BH STUDIOS ASESORÍAS Y COMPUTACIÓN LTDA.</span><br>
<span style="color:#666666;font-size:12px;font-weight:bold;">76.079.830-4</span><br>
<span style="color:#666666;font-size:12px;">Publicidad</span><br>
<span style="color:#666666;font-size:12px;">Monseñor Sotero Sanz 100, Of.403, Providencia - Chile</span><br>
<span style="color:#666666;font-size:12px;">Tel: <a href="">+562 3245 1870</a> / <a href="">+562 3245 1871</a></span><br>
<span style="color:#666666;font-size:12px;">www.bhstudios.com</span>
EOF;
$pdf->setPosX(100);
$pdf->setPosY(10);
$pdf->setHtmlHeader($html);


// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists('config/lib/lang/eng.php')) {
    require_once('config/lib/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

$pdf->SetFont('helvetica', '', 10, '', true);
// Add a page
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


/* DATOS DEL PRESUPUESTO */

$ot = new ot();
$ot->traerRegistro($n_presupuesto);

$cliente = new cliente();
$cliente->traerRegistro($ot->getIdCliente());

$region = new region();
$region->traerRegistro($cliente->getIdRegion());

$provincia = new provincia();
$provincia->traerRegistro($cliente->getIdProvincia());

$comuna = new comuna();
$comuna->traerRegistro($cliente->getIdComuna());

$n_presupuesto = $ot->getId();
$nombre_cliente = $cliente->getNombre();
$rut_cliente = $cliente->getRut();
$direccion_cliente = $cliente->getDireccion();
$region_cliente = $region->getNombre();
$ciudad_cliente = $provincia->getNombre();
$comuna_cliente = $comuna->getNombre();
$telefono_cliente = $cliente->getTelefono();
$celular_cliente = $cliente->getCelular();
$giro_cliente = $cliente->getGiro();

$html ='';
$html .='<br>';
$html .=
    <<<EOD
    <p style="font-size: 14px;"><b><u>Nº Presupuesto: $n_presupuesto</u></b></p>
    <br>
    <table border="0" cellpadding="2">
    <tr>
        <td width="150" height="20"><strong>Nombre Cliente:</strong></td>
        <td>$nombre_cliente</td>
    </tr>
    <tr>
        <td height="20"><strong>Rut:</strong></td>
        <td>$rut_cliente</td>
    </tr>
    <tr>
        <td height="20"><strong>Dirección:</strong></td>
        <td>$direccion_cliente</td>
    </tr>
    <tr>
        <td height="20"><strong>Región:</strong></td>
        <td>$region_cliente</td>
    </tr>
    <tr>
        <td height="20"><strong>Ciudad:</strong></td>
        <td>$ciudad_cliente</td>
    </tr>
    <tr>
        <td height="20"><strong>Comuna:</strong></td>
        <td>$comuna_cliente</td>
    </tr>
    <tr>
        <td height="20"><strong>Telefono:</strong></td>
        <td>$telefono_cliente</td>
    </tr>
    <tr>
        <td height="20"><strong>Celular:</strong></td>
        <td>$celular_cliente</td>
    </tr>
    <tr>
        <td height="20"><strong>Giro:</strong></td>
        <td>$giro_cliente</td>
    </tr>
    </table>

EOD;

// Datos del cliente
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

/* FIN - DATOS DEL PRESUPUESTO */

/* DETALLE DEL PRESUPUESTO */

$detalle_ot = new detalle_ot();
$filtro_ref = array();
$filtro_ref['id_ot'] = $ot->getId();
$filtro_ref['orden_sql'] = 'orden_detalle_ot ASC';
$detalle_ot->setFiltroSql($filtro_ref);
$estado_consulta = $detalle_ot->traerTodosRegistro();

$html_detalle = '';
$sub_total = 0;
$neto = 0;
$iva = 0;
$total_desc = 0;
$total_final = 0;

if($estado_consulta)
{
    while($row = mysql_fetch_object($detalle_ot->getMatrizDatos()))
    {

        $cant_uf_detalle = 0;
        $cant_uf_detalle = $row->uf_detalle_ot;
        if($cant_uf_detalle==0)
        $cant_uf_detalle = '-';

        if($row->uf_detalle_ot>0)
        {
            $html_detalle.='<tr>
            <td align="center">'.$row->cantidad_detalle_ot.'</td>
            <td align="left">'.utf8_encode($row->descripcion_detalle_ot).'
            </td>
            <td align="center">'.$cant_uf_detalle.'</td>
            <td align="rigth">'.number_format($row->unitario_detalle_ot,0,',','.').'</td>
            <td align="rigth">'.(int)$row->descuento_detalle_ot.'%</td>
            <td align="rigth">'.number_format((($row->unitario_detalle_ot*$row->cantidad_detalle_ot*$cant_uf_detalle)-($row->unitario_detalle_ot*$row->cantidad_detalle_ot*$cant_uf_detalle)*(int)$row->descuento_detalle_ot/100),0,',','.').'</td>
            </tr>
            ';
            $total_desc += (($row->unitario_detalle_ot*$row->cantidad_detalle_ot*$cant_uf_detalle)*(int)$row->descuento_detalle_ot/100);
            $sub_total+=round($row->unitario_detalle_ot*$row->cantidad_detalle_ot*$cant_uf_detalle,0);
            $neto+=round($row->unitario_detalle_ot*$row->cantidad_detalle_ot*$cant_uf_detalle,0);
        }else{
            $html_detalle.='<tr>
            <td align="center">'.$row->cantidad_detalle_ot.'</td>
            <td align="left">'.utf8_encode($row->descripcion_detalle_ot).'
            </td>
            <td align="center">'.$cant_uf_detalle.'</td>
            <td align="rigth">'.number_format($row->unitario_detalle_ot,0,',','.').'</td>
            <td align="rigth">'.(int)$row->descuento_detalle_ot.'%</td>
            <td align="rigth">'.number_format((($row->unitario_detalle_ot*$row->cantidad_detalle_ot)-($row->unitario_detalle_ot*$row->cantidad_detalle_ot)*(int)$row->descuento_detalle_ot/100),0,',','.').'</td>
            </tr>
            ';
            $total_desc+=(($row->unitario_detalle_ot*$row->cantidad_detalle_ot)*(int)$row->descuento_detalle_ot/100);
            $sub_total+=round($row->unitario_detalle_ot*$row->cantidad_detalle_ot,0);
            $neto+=round($row->unitario_detalle_ot*$row->cantidad_detalle_ot,0);
        }

    }
        $neto = $neto - $total_desc;
        $total_desc = number_format($total_desc,0,',','.');
        $iva=round($neto*19/100,0);
        $total_final = $neto+$iva;
        $iva=number_format(round($neto*19/100,0),0,',','.');
        $total_final = number_format($total_final,0,',','.');

        $sub_total = number_format(round($sub_total),0,',','.');
        $neto = number_format(round($neto),0,',','.');
}


$html2 ='';
$html2 .='<br><br><br>';
$html2 .=
<<<EOD

<table border="1" cellpadding="2">
<tr>
    <td align="center" width="40" height="18" bgcolor="#FDC700">Cant</td>
    <td align="center" width="350" bgcolor="#FDC700">Descripción</td>
    <td align="center" width="40" bgcolor="#FDC700">UF</td>
    <td align="center" width="90" bgcolor="#FDC700">Unitario</td>
    <td align="center" width="40" bgcolor="#FDC700">Desc</td>
    <td align="center" width="90" bgcolor="#FDC700">Neto</td>
</tr>
$html_detalle
<tr>
    <td colspan="3" rowspan="5"></td>
    <td align="rigth" bgcolor="#FDC700">Subtotal</td>
    <td bgcolor="#FDC700"></td>
    <td align="rigth" bgcolor="#FDC700">$sub_total</td>
</tr>
<tr>
    <td align="rigth" bgcolor="#FDC700">Descuento</td>
    <td bgcolor="#FDC700"></td>
    <td align="rigth" bgcolor="#FDC700">$total_desc</td>
</tr>
<tr>
    <td align="rigth" bgcolor="#FDC700">Neto</td>
    <td bgcolor="#FDC700"></td>
    <td align="rigth" bgcolor="#FDC700">$neto</td>
</tr>
<tr>
    <td align="rigth" bgcolor="#FDC700">Impuesto</td>
    <td align="rigth" bgcolor="#FDC700">19%</td>
    <td align="rigth" bgcolor="#FDC700">$iva</td>
</tr>
<tr>
    <td colspan="2" align="left" bgcolor="#FDC700">Total Proyecto</td>
    <td align="rigth" bgcolor="#FDC700">$total_final</td>
</tr>

</table>
EOD;

$pdf->writeHTMLCell(0, 0, '', '', $html2, 0, 1, 0, true, '', true);
/* FIN - DETALLE DEL PRESUPUESTO */


/* LEGAL PRESUPUESTO */
$comentario = utf8_encode($ot->getComentario());
$nota = utf8_encode($ot->getNota());

$html3 ='';
$html3 .='<br><br><br>';
$html3 .=
<<<EOD
<p style="color:#666666;font-size:12px;font-weight:bold;">Condiciones Comerciales</p>
<p style="font-size:12px;">
$comentario
</p>
<p style="font-size:12px;">
$nota
</p>
EOD;

$pdf->writeHTMLCell(0, 0, '', '', $html3, 0, 1, 0, true, '', true);
/* FIN - LEGAL PRESUPUESTO */



/* COMENTARIO PRESUPUESTO */
$comentario = new comentario_ot();

$filtro_ref = array();
$filtro_ref['id_ot'] = $ot->getId();
$filtro_ref['estado'] = 1;
$filtro_ref['orden_sql'] = 'orden_comentario_ot ASC';
$comentario->setFiltroSql($filtro_ref);
$estado_consulta = $comentario->traerTodosRegistro();

if($estado_consulta==1)
{
// PAGINA 2
$pdf->AddPage();

$html_comentario = '';


if($estado_consulta)
{
    while($row = mysql_fetch_object($comentario->getMatrizDatos()))
    {

        $html_comentario.='
        <p style="color:#666666;font-size:12px;font-weight:bold;">'.utf8_encode($row->titulo_comentario_ot).' :</p>
        <p style="font-size:12px;">
                '.utf8_encode($row->detalle_comentario_ot).'
        </p>
        ';
    }

}

$html4 ='';
$html4 .='<br><br><br>';
$html4 .=
    <<<EOD
$html_comentario
EOD;

$pdf->writeHTMLCell(0, 0, '', '', $html4, 0, 1, 0, true, '', true);
}
/* FIN - LEGAL PRESUPUESTO */


// SALIDA PDF
if((int)$pdf_estado==1)
{
    //$pdf->Output($n_presupuesto.'_presupuesto.pdf', 'I');
    $pdf->Output('../pdf_ot/'.$n_presupuesto.'_presupuesto.pdf', 'F');
}else{
    //$pdf->Output($n_presupuesto.'_presupuesto.pdf', 'D');
    $pdf->Output('../pdf_ot/'.$n_presupuesto.'_presupuesto.pdf', 'F');
}