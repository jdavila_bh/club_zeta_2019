<?php
require_once('config/tcpdf_include.php');
require_once('config/PDF_OT.php');
include('config.php');

// HEADER
$pdf = new PDF_OT(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setColW(100);
$pdf->setColH(10);
$html = <<<EOF
<span style="color:#666666;font-size:12px;font-weight:bold;">BH STUDIOS ASESORÍAS Y COMPUTACIÓN LTDA.</span><br>
<span style="color:#666666;font-size:12px;">Publicidad</span><br>
<span style="color:#666666;font-size:12px;">Monseñor Sotero Sanz 100, Of.403, Providencia - Chile</span><br>
<span style="color:#666666;font-size:12px;">Tel: <a href="callto:+562 3245 1870">+562 3245 1870</a> / <a href="callto:+562 3245 1871">+562 3245 1871</a></span>
EOF;
$pdf->setPosX(100);
$pdf->setPosY(10);
$pdf->setHtmlHeader($html);


// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists('config/lib/lang/eng.php')) {
    require_once('config/lib/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

$pdf->SetFont('helvetica', '', 10, '', true);
// Add a page
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


/* DATOS DEL PRESUPUESTO */
$n_presupuesto = 1000;
$nombre_cliente = 'INMOBILIARIA DEISA LTDA.';
$rut_cliente = '76.810.960-5 ';
$direccion_cliente = 'Padre Mariano 181';
$region_cliente = 'Metropolitana';
$ciudad_cliente = 'Santiago';
$comuna_cliente = 'Providencia';
$telefono_cliente = '2234 78100';
$celular_cliente = '';
$giro_cliente = 'Inmobiliaria';

$html ='';
$html .='<br>';
$html .=
    <<<EOD
    <p>Nº Presupuesto: $n_presupuesto</p>
    <br>
    <table border="0" cellpadding="2">
    <tr>
        <td width="150" height="20">Nombre Cliente:</td>
        <td>$nombre_cliente</td>
    </tr>
    <tr>
        <td height="20">Rut:</td>
        <td>$rut_cliente</td>
    </tr>
    <tr>
        <td height="20">Dirección:</td>
        <td>$direccion_cliente</td>
    </tr>
    <tr>
        <td height="20">Región:</td>
        <td>$region_cliente</td>
    </tr>
    <tr>
        <td height="20">Ciudad:</td>
        <td>$ciudad_cliente</td>
    </tr>
    <tr>
        <td height="20">Comuna:</td>
        <td>$comuna_cliente</td>
    </tr>
    <tr>
        <td height="20">Telefono:</td>
        <td>$telefono_cliente</td>
    </tr>
    <tr>
        <td height="20">Celular:</td>
        <td>$celular_cliente</td>
    </tr>
    <tr>
        <td height="20">Giro:</td>
        <td>$giro_cliente</td>
    </tr>
    </table>

EOD;

// Datos del cliente
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

/* FIN - DATOS DEL PRESUPUESTO */

/* DETALLE DEL PRESUPUESTO */
$html2 ='';
$html2 .='<br><br><br>';
$html2 .=
<<<EOD

<table border="1" cellpadding="2">
<tr>
    <td align="center" width="60" height="18" bgcolor="#FDC700">Cantidad</td>
    <td align="center" width="350" bgcolor="#FDC700">Descripción</td>
    <td align="center" width="60" bgcolor="#FDC700">UF</td>
    <td align="center" width="90" bgcolor="#FDC700">Unitario</td>
    <td align="center" width="90" bgcolor="#FDC700">Neto</td>
</tr>
<tr>
    <td align="center">1</td>
    <td align="left">Servicio de transformación sitio web www.deisa.cl
        cambio de sección e incorporación de nueva sección
        ambas autoadministrables con contenido dinámico
        y adaptado multiplataforma

    </td>
    <td align="center">-</td>
    <td align="rigth">479.000</td>
    <td align="rigth">479.000</td>
</tr>
<tr>
    <td align="center">1</td>
    <td align="left">Servicio de transformación sitio web www.deisa.cl
        cambio de sección e incorporación de nueva sección
        ambas autoadministrables con contenido dinámico
        y adaptado multiplataforma

    </td>
    <td align="center">-</td>
    <td align="rigth">479.000</td>
    <td align="rigth">479.000</td>
</tr>
<tr>
    <td align="center">1</td>
    <td align="left">Servicio de transformación sitio web www.deisa.cl
        cambio de sección e incorporación de nueva sección
        ambas autoadministrables con contenido dinámico
        y adaptado multiplataforma

    </td>
    <td align="center">-</td>
    <td align="rigth">479.000</td>
    <td align="rigth">479.000</td>
</tr>

<tr>
    <td colspan="2" rowspan="4"></td>
    <td align="rigth" bgcolor="#FDC700">Subtotal</td>
    <td bgcolor="#FDC700"></td>
    <td align="rigth" bgcolor="#FDC700">479.000</td>
</tr>
<tr>
    <td align="rigth" bgcolor="#FDC700">Neto</td>
    <td bgcolor="#FDC700"></td>
    <td align="rigth" bgcolor="#FDC700">479.000</td>
</tr>
<tr>
    <td align="rigth" bgcolor="#FDC700">Impuesto</td>
    <td bgcolor="#FDC700">19%</td>
    <td align="rigth" bgcolor="#FDC700">91.010</td>
</tr>
<tr>
    <td colspan="2" align="left" bgcolor="#FDC700">Total Proyecto</td>
    <td align="rigth" bgcolor="#FDC700">570.010</td>
</tr>

</table>
EOD;

$pdf->writeHTMLCell(0, 0, '', '', $html2, 0, 1, 0, true, '', true);

/* FIN - DETALLE DEL PRESUPUESTO */


/* LEGAL PRESUPUESTO */
$html3 ='';
$html3 .='<br><br><br>';
$html3 .=
<<<EOD
<p style="color:#666666;font-size:12px;font-weight:bold;">Condición Comercial</p>
<p style="font-size:12px;">
Una vez aprobado el presupuesto el cliente debera generar una OC que se entenderá como formalización del encargo
 Se contara como inicio de actividades o dia 1, una vez que el cliente proporcione todo el material, textos, claves de
 acceso, imágenes, logos, tipografias, restricciones, medidas, etc.
</p>
<p style="font-size:12px;">
Sr. Cliente para que BH STUDIOS de curso a los servicios solicitados por su empresa, deberá completar esta orden de
trabajo, aceptando, el prespuesto según codigo. Se le enviará el presente docuemtno para respaldar la factura que emitiremos.
Cancelar a : BH STUDIOS ASESORIAS Y COMPUTACION LTDA., Banco: Santander, Cuenta N°: 352974-6
</p>
EOD;

$pdf->writeHTMLCell(0, 0, '', '', $html3, 0, 1, 0, true, '', true);
/* FIN - LEGAL PRESUPUESTO */


// PAGINA 2

$pdf->AddPage();

/* COMENTARIO PRESUPUESTO */
$html4 ='';
$html4 .='<br><br><br>';
$html4 .=
    <<<EOD
    <p style="color:#666666;font-size:12px;font-weight:bold;">Propuesta y Requerimientos :</p>
<p style="font-size:12px;">
Sitio Club Contacto<br>
Actualización mensual de:<br>
-Sección Energía de Hoy<br>
-Sección Tecnología del Mañana<br>
-Sección Ventana al Mundo Eléctrico<br>
-Sección Editorial<br>
-Sección Conectados<br>
-Sección Capacitación / Voltaje<br>
-Concursos por target: Proyectista, Instalador y Distribuidor<br>
-Concurso transversal<br>
-5 Banners linkeables zona superior<br>
</p>
EOD;

$pdf->writeHTMLCell(0, 0, '', '', $html4, 0, 1, 0, true, '', true);
/* FIN - LEGAL PRESUPUESTO */


// SALIDA PDF
$pdf->Output('example_001.pdf', 'I');