<?php
@session_start();
include_once('lib/class/tipo_pago.php');

$metodo_fun = array(
        'registrarTipoPago' => 'POST',
        'modificarTipoPago' => 'POST',
        'traerTodosTipoPagos' => 'POST',
        'traerTipoPago' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarTipoPago':

            $tipoPago = new tipo_pago();

            $tipoPago->setNombre(utf8_decode($nombre));

            if($tipoPago->registrar()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al ingresar el registro.';
            }

        break;

        case 'modificarTipoPago':

            $tipoPago = new tipo_pago();

            $tipoPago->traerRegistro($id_registro);

            $tipoPago->setNombre(utf8_decode($nombre));

            if($tipoPago->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosTipoPagos':

            $tipoPago = new tipo_pago();

            $datos['registros'] = array();

            // FILTROS
            $filtrosql = array();

            if(isset($estado))
                $filtrosql['estado'] = $estado;

            $estadoConsulta = $tipoPago->traerTodosRegistro($filtrosql);

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($tipoPago->getMatrizDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => $row->id_tipo_pago,
                        'nombre' => utf8_encode($row->nombre_tipo_pago),
                        'estado' => $row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }

            }else{
                $datos['estado'] = 0;
            }

        break;

        case 'traerTipoPago':


            $tipoPago = new tipo_pago();

            $datos['registros'] = array();

            if($tipoPago->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $datos['registros'] = array(
                    'id' => $tipoPago->getId(),
                    'nombre' => utf8_encode($tipoPago->getNombre()),
                    'estado' => $tipoPago->getEstado(),
                    'fecha_ingreso' => $tipoPago->getFechaIngreso(),
                    'fecha_modificacion' => $tipoPago->getFechaModificacion()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;

        case 'cambiarEstado':

            $tipoPago = new tipo_pago();
            if((int)$tipoPago->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;

        default:

            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );

        break;
    }
}
?>