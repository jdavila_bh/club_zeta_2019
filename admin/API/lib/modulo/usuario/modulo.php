<?php
@session_start();
include_once('lib/class/usuario.php');
include_once('lib/class/tipo_usuario.php');

$metodo_fun = array(
    'verificarUsuario'=>'POST',
    'traerSesionActiva'=>'POST',
    'cambiarEstado'=>'POST',
    'cambiarClave'=>'POST',
    'registrarUsuario'=>'POST',
    'modificarUsuario'=>'POST',
    'traerTodosUsuarios'=>'POST',
    'traerUsuarios'=>'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
    $datos = array(
        'codigo' => 404,
        'estado' => 0,
        'mensaje'=> 'Metodo no registrado en el modulo.',
        'metodo' => $funcion_modulo,
        'method' => $metodo
    );
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'verificarUsuario':

            $usuario = new usuario();

            if($usuario->login($user,$pass)==1)
            {
                $datos['estado'] = 1;
                $datos['sesionActiva'] = $usuario->traerSesionActiva();
            }else{
                $datos['estado'] = 0;
            }

            break;

        case 'traerSesionActiva':

            $usuario = new usuario();
            if($usuario->traerSesionActiva()!=null)
            {
                $datos['estado'] =1;
                $datos['sesion_activa']= $usuario->traerSesionActiva();
            }else{
                $datos['estado'] =0;
            }

            break;


        case 'cambiarEstado':

            $usuario = new usuario();
            if((int)$usuario->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
                $datos['msg'] = 'Cambio de estado Exitoso.';
            }else{
                $datos['estado'] =0;
                $datos['msg'] = 'Problema al cambiar el estado.';
            }

            break;

        case 'cambiarClave':

            $usuario = new usuario();
            if((int)$usuario->claveRegistro($id_registro,$nueva_clave)==1)
            {
                $datos['estado'] =1;
                $datos['msg'] = 'Cambio de clave exitoso.';
            }else{
                $datos['estado'] =0;
                $datos['msg'] = 'Problemas al cambiar la clave.';
            }

            break;

        case 'registrarUsuario':

            $usuario = new usuario();

            $estadoUsuario = $usuario->verificarUserName($user);

            $usuario->setNombre(utf8_decode($nombre));
            $usuario->setApellidop(utf8_decode($apellidop));
            $usuario->setApellidom(utf8_decode($apellidom));
            $usuario->setRut(utf8_decode($rut));
            $usuario->setEmail(utf8_decode($email));
            $usuario->setDireccion(utf8_decode($direccion));
            $usuario->setTelefono(utf8_decode($telefono));
            $usuario->setCelular(utf8_decode($celular));
            $usuario->setUser(utf8_decode($user));
            $usuario->setPass(utf8_decode($pass));
            $usuario->setIdTipo(utf8_decode($tipo));



            if($estadoUsuario==0)
            {
                if($usuario->registrar()==1)
                {
                    $datos['estado'] = 1;
                    $datos['msg'] = 'Usuario registrado.';
                }else{
                    $datos['estado'] = 0;
                    $datos['msg'] = 'Problemas al registrar el usuario.';
                }
            }else{
                $datos['estado'] = 2;
                $datos['msg'] = 'Usuario ya existe.';
            }

            break;


        case 'modificarUsuario':

            $usuario = new usuario();

            $usuario->traerRegistro($id_registro);

            $usuario->setNombre(utf8_decode($nombre));
            $usuario->setApellidop(utf8_decode($apellidop));
            $usuario->setApellidom(utf8_decode($apellidom));
            $usuario->setRut($rut);
            $usuario->setEmail($email);
            $usuario->setDireccion(utf8_decode($direccion));
            $usuario->setTelefono($telefono);
            $usuario->setCelular($celular);
            $usuario->setIdTipo($tipo);

            if($usuario->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al Actualizar el registro.';
            }

            break;


        case 'traerTodosUsuarios':


            $usuario = new usuario();
            $tipo_usuario = new tipo_usuario();

            $datos['registros'] = array();

            if($usuario->traerTodosRegistro())
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registros encontrados.';

                while($row = mysql_fetch_object($usuario->getMatrizDatos()))
                {

                    $tipo_usuario->traerRegistro($row->id_tipo_usuario);

                    $datos['registros'][] = array(
                        'id' => $row->id_usuario,
                        'nombre' => utf8_encode($row->nombre_usuario),
                        'apellidop' => utf8_encode($row->apellidop_usuario),
                        'apellidom' => utf8_encode($row->apellidom_usuario),
                        'rut' => $row->rut_usuario,
                        'email' => $row->email_usuario,
                        'direccion' => utf8_encode($row->direccion_usuario),
                        'telefono' => $row->telefono_usuario,
                        'celular' => $row->celular_usuario,
                        'user' => $row->user_usuario,
                        'id_tipo_usuario' => $row->id_tipo_usuario,
                        'tipo_usuario_obj' => $tipo_usuario->traerObjeto(),
                        'estado' => $row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );

                }

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al traer el registro.';
            }

            break;

        case 'traerUsuarios':


            $usuario = new usuario();
            $tipo_usuario = new tipo_usuario();

            $datos['registros'] = array();

            if($usuario->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro encontrado.';

                $tipo_usuario->traerRegistro($usuario->getIdTipo());

                $datos['registros'] = array(
                    'id' => $usuario->getId(),
                    'nombre' => utf8_encode($usuario->getNombre()),
                    'apellidop' => utf8_encode($usuario->getApellidop()),
                    'apellidom' => utf8_encode($usuario->getApellidom()),
                    'rut' => $usuario->getRut(),
                    'email' => $usuario->getEmail(),
                    'direccion' => utf8_encode($usuario->getDireccion()),
                    'telefono' => $usuario->getTelefono(),
                    'celular' => $usuario->getCelular(),
                    'user' => $usuario->getUser(),
                    'id_tipo_usuario' => $usuario->getIdTipo(),
                    'tipo_usuario_obj' => $tipo_usuario->traerObjeto(),
                    'estado' => $usuario->getEstado(),
                    'fecha_ingreso' => $usuario->getFechaIngreso(),
                    'fecha_modificacion' => $usuario->getFechaModificacion()
                );



            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al encontrar el Registro.';
            }

            break;

        default:

            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );

            break;

    }
}
?>