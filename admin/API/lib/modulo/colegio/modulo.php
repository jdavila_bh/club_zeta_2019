<?php
@session_start();
include_once('lib/class/colegio.php');

$metodo_fun = array(
    'registrarColegio'=>'POST',
    'modificarColegio'=>'POST',
    'eliminarColegio'=>'POST',
    'cambiarEstado'=>'POST',
    'traerTodosRegistros'=>'POST',
    'traerRegistros'=>'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
    $datos = array(
        'codigo' => 404,
        'estado' => 0,
        'mensaje'=> 'Metodo no registrado en el modulo.',
        'metodo' => $funcion_modulo,
        'method' => $metodo
    );
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarColegio':

            $colegio = new colegio();


            $colegio->setNombre(utf8_decode($nombre));
            //$colegio->setIdSegmentacion(1);
            $colegio->setObligatorio('No');
            $colegio->setPrecio((int)utf8_decode($precio));
            //$colegio->setLogo(utf8_decode($logo));
            $colegio->setNumeroCentroCosto((int)utf8_decode($nemro_centro_costo));
            $colegio->setNombreAdministrador(utf8_decode($nombreAdministrador));
            $colegio->setMailAdministrador(utf8_decode($maileAdministrador));
            $colegio->setDireccion(utf8_decode($direccion));
            $colegio->setTelefono(utf8_decode($telefono));
            $colegio->setContacto(utf8_decode($contacto));
            $colegio->setTelefonoContacto(utf8_decode($telefonoContancto));
            $colegio->setJop(utf8_decode($jop));
            $colegio->setCorreoJop(utf8_decode($correoJop));
            $colegio->setEstado(utf8_decode($estado));
            $colegio->setPrecio2(utf8_decode($precio2));
            //$colegio->setImagenAdmin(utf8_decode($imagenAdmin));
            //$colegio->setPrecioTramo1(utf8_decode($preciotramo1));
            //$colegio->setPrecioTramo2(utf8_decode($preciotramo2));
            //$colegio->setPrecioTramo3(utf8_decode($preciotramo3));


            if($colegio->registrar()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Colegio registrado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al registrar el Colegio.';
            }
            break;


        case 'modificarColegio':

            $colegio = new colegio();

            $colegio->traerRegistro($id_registro);

            $colegio->setNombre(utf8_decode($nombre));
            //$colegio->setIdSegmentacion(1);
            $colegio->setObligatorio('No');
            $colegio->setPrecio((int)utf8_decode($precio));
            //$colegio->setLogo(utf8_decode($logo));
            $colegio->setNumeroCentroCosto((int)utf8_decode($nemro_centro_costo));
            $colegio->setNombreAdministrador(utf8_decode($nombreAdministrador));
            $colegio->setMailAdministrador(utf8_decode($maileAdministrador));
            $colegio->setDireccion(utf8_decode($direccion));
            $colegio->setTelefono(utf8_decode($telefono));
            $colegio->setContacto(utf8_decode($contacto));
            $colegio->setTelefonoContacto(utf8_decode($telefonoContancto));
            $colegio->setJop(utf8_decode($jop));
            $colegio->setCorreoJop(utf8_decode($correoJop));
            $colegio->setEstado((int)utf8_decode($estado));
            $colegio->setPrecio2((int)utf8_decode($precio2));
            //$colegio->setImagenAdmin(utf8_decode($imagenAdmin));
            //$colegio->setPrecioTramo1(utf8_decode($preciotramo1));
            //$colegio->setPrecioTramo2(utf8_decode($preciotramo2));
            //$colegio->setPrecioTramo3(utf8_decode($preciotramo3));

            if($colegio->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al Actualizar el registro.';
            }

            break;


        case 'traerTodosRegistros':


            $colegio = new colegio();

            $datos['registros'] = array();

            if($colegio->traerTodosRegistro())
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registros encontrados.';

                while($row = mysql_fetch_object($colegio->getMatrizDatos()))
                {
                    $datos['registros'][] = array(
                        'id' => $row->id_colegio,
                        'nombre' => utf8_encode($row->nombre_colegio),
                        'id_segmentacion' => $row->id_segmentacion_colegio,
                        'obligatorio' => $row->obligatorio_colegio,
                        'precio' => (int)$row->precio_colegio,
                        'logo' => $row->logo_colegio,
                        'numero_centro_costo' => (int)$row->numero_centro_costo_colegio,
                        'nombre_administrador' => utf8_encode($row->nombre_administrador_colegio),
                        'mail_administrador' => utf8_encode($row->mail_administrador_colegio),
                        'direccion' => utf8_encode($row->direccion_colegio),
                        'telefono' => utf8_encode($row->telefono_colegio),
                        'contacto' => utf8_encode($row->contacto_colegio),
                        'telefono_contacto' => utf8_encode($row->telefono_contacto_colegio),
                        'jop' => utf8_encode($row->jop_colegio),
                        'correo_jop' => utf8_encode($row->correo_jop_colegio),
                        'estado' => $row->estado_colegio,
                        'precio2' => $row->precio2_colegio,
                        'imagen_admin' => utf8_encode($row->imagen_admin_colegio),
                        'precio_tramo1' =>(int) $row->precio_tramo1,
                        'precio_tramo2' => (int)$row->precio_tramo2,
                        'precio_tramo3' => (int)$row->precio_tramo3,
                        'fechacreacion' => $row->fecha_creacion_colegio
                    );
                }
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al traer el registro.';
            }

            break;

        case 'traerRegistros':

            $colegio = new colegio();
            $datos['registros'] = array();

            if($colegio->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro encontrado.';

                $datos['registros'][] = array(
                    'id' => $colegio->getId(),
                    'nombre' => utf8_encode($colegio->getNombre()),
                    'id_segmentacion' => $colegio->getIdSegmentacion(),
                    'obligatorio' => $colegio->getObligatorio(),
                    'precio' => (int)$colegio->getPrecio(),
                    'logo' => $colegio->getLogo(),
                    'numero_centro_costo' => (int)$colegio->getNumeroCentroCosto(),
                    'nombre_administrador' => utf8_encode($colegio->getNombreAdministrador()),
                    'mail_administrador' => utf8_encode($colegio->getMailAdministrador()),
                    'direccion' => utf8_encode($colegio->getDireccion()),
                    'telefono' => utf8_encode($colegio->getTelefono()),
                    'contacto' => utf8_encode($colegio->getContacto()),
                    'telefono_contacto' => utf8_encode($colegio->getTelefonoContacto()),
                    'jop' => utf8_encode($colegio->getJop()),
                    'correo_jop' => utf8_encode($colegio->getCorreoJop()),
                    'estado' => $colegio->getEstado(),
                    'precio2' => $colegio->getPrecio2(),
                    'imagen_admin' => utf8_encode($colegio->getImagenAdmin()),
                    'precio_tramo1' =>(int) $colegio->getPrecioTramo1(),
                    'precio_tramo2' => (int)$colegio->getPrecioTramo2(),
                    'precio_tramo3' => (int)$colegio->getPrecioTramo3(),
                    'fechacreacion' => $colegio->getFechaCreacion()
                );
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al encontrar el Registro.';
            }

            break;
        case 'eliminarColegio':

            $colegio = new colegio();

            if($colegio->eliminar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Eliminado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al Eliminado el registro.';
            }

            break;

        case 'cambiarEstado':

            $colegio = new colegio();

            if($colegio->cambiarEstado($id_registro,$estado))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problema al Actualizado el registro.';
            }

            break;
        default:

            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );

            break;

    }
}
?>