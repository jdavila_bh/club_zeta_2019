<?php
@session_start();
include_once('lib/class/cliente.php');
include_once('lib/class/region.php');
include_once('lib/class/provincia.php');
include_once('lib/class/comuna.php');

$metodo_fun = array(
        'registrarCliente' => 'POST',
        'modificarCliente' => 'POST',
        'traerTodosClientes' => 'POST',
        'traerCliente' => 'POST',
        'cambiarEstado' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
			'codigo' => 404,
			'estado' => 0,
			'mensaje'=> 'Metodo no registrado en el modulo.',
			'metodo' => $funcion_modulo,
			'method' => $metodo
			);	
}else{

    extract($_REQUEST);

    switch($funcion_modulo)
    {

        case 'registrarCliente':

            $cliente = new cliente();

            $cliente->setNombre(utf8_decode($nombre));
            $cliente->setRut(utf8_decode($rut));
            $cliente->setDireccion(utf8_decode($direccion));
            $cliente->setIdRegion(utf8_decode($id_region));
            $cliente->setIdProvincia(utf8_decode($id_provincia));
            $cliente->setIdComuna(utf8_decode($id_comuna));
            $cliente->setTelefono(utf8_decode($telefono));
            $cliente->setcelular(utf8_decode($celular));
            $cliente->setEmail(utf8_decode($email));
            $cliente->setGiro(utf8_decode($giro));


            if($cliente->registrar()==1)
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Ingresado Satisfactoriamente.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al ingresar el registro.';
            }

        break;

        case 'modificarCliente':

            $cliente = new cliente();

            $cliente->traerRegistro($id_registro);

            $cliente->setNombre(utf8_decode($nombre));
            $cliente->setRut(utf8_decode($rut));
            $cliente->setDireccion(utf8_decode($direccion));
            $cliente->setIdRegion(utf8_decode($id_region));
            $cliente->setIdProvincia(utf8_decode($id_provincia));
            $cliente->setIdComuna(utf8_decode($id_comuna));
            $cliente->setTelefono(utf8_decode($telefono));
            $cliente->setcelular(utf8_decode($celular));
            $cliente->setEmail(utf8_decode($email));
            $cliente->setGiro(utf8_decode($giro));

            if($cliente->modificar($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Actualizado.';
            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al actualizar el registro.';
            }

        break;

        case 'traerTodosClientes':

            $cliente = new cliente();

            $datos['registros'] = array();

            // FILTROS
            $filtrosql = array();

            if(isset($estado))
                $filtrosql['estado'] = $estado;

            $estadoConsulta = $cliente->traerTodosRegistro($filtrosql);

            if($estadoConsulta==1)
            {
                $datos['estado'] = 1;

                while($row = mysql_fetch_object($cliente->getMatrizDatos()))
                {
                    $region = new region();
                    $region->traerRegistro((int)$row->id_region);
                    $provincia = new provincia();
                    $provincia->traerRegistro((int)$row->id_provincia);
                    $comuna = new comuna();
                    $comuna->traerRegistro((int)$row->id_comuna);

                    $datos['registros'][] = array(
                        'id' => (int)$row->id_cliente,
                        'nombre' => utf8_encode($row->nombre_cliente),
                        'rut' => utf8_encode($row->rut_cliente),
                        'direccion' => utf8_encode($row->direccion_cliente),

                        'id_region' => (int)$row->id_region,
                        'regionObj' => $region->traerObjeto(),

                        'id_provincia' => (int)$row->id_provincia,
                        'provinciaObj' => $comuna->traerObjeto(),

                        'id_comuna' => (int)$row->id_comuna,
                        'comunaObj' => $comuna->traerObjeto(),

                        'telefono' => $row->telefono_cliente,
                        'celular' => $row->celular_cliente,
                        'email' => $row->email_cliente,
                        'giro' => utf8_encode($row->giro_cliente),
                        'estado' => (int)$row->estado,
                        'fecha_ingreso' => $row->fecha_ingreso,
                        'fecha_modificacion' => $row->fecha_modificacion
                    );
                }


            }else{
                $datos['estado'] = 0;
            }

        break;

        case 'traerCliente':


            $cliente = new cliente();

            $datos['registros'] = array();

            if($cliente->traerRegistro($id_registro))
            {
                $datos['estado'] = 1;
                $datos['msg'] = 'Registro Encontrado.';


                $region = new region();
                $region->traerRegistro((int)$cliente->getIdRegion());
                $provincia = new provincia();
                $provincia->traerRegistro((int)$cliente->getIdProvincia());
                $comuna = new comuna();
                $comuna->traerRegistro((int)$cliente->getIdComuna());

                $datos['registros'] = array(
                    'id' => (int)$cliente->getId(),
                    'nombre' => utf8_encode($cliente->getNombre()),
                    'rut' => utf8_encode($cliente->getRut()),
                    'direccion' => utf8_encode($cliente->getDireccion()),

                    'id_region' => (int)$cliente->getIdRegion(),
                    'regionObj' => $region->traerObjeto(),

                    'id_provincia' => (int)$cliente->getIdProvincia(),
                    'provinciaObj' => $comuna->traerObjeto(),

                    'id_comuna' => (int)$cliente->getIdComuna(),
                    'comunaObj' => $comuna->traerObjeto(),

                    'telefono' => $cliente->getTelefono(),
                    'celular' => $cliente->getCelular(),
                    'email' => $cliente->getEmail(),
                    'giro' => utf8_encode($cliente->getGiro()),
                    'estado' => (int)$cliente->getEstado(),
                    'fecha_ingreso' => $cliente->getFechaIngreso(),
                    'fecha_modificacion' => $cliente->getFechaModificacion()
                );

            }else{
                $datos['estado'] = 0;
                $datos['msg'] = 'Problemas al traer el registro.';
            }

        break;

        case 'cambiarEstado':

            $cliente = new cliente();
            if((int)$cliente->estadoRegistro($id_registro,$nuevo_estado)==1)
            {
                $datos['estado'] =1;
            }else{
                $datos['estado'] =0;
            }

        break;

        default:
            $datos = array(
                'code' => 404,
                'msg' => 'Metodo no encontrado, vuelva a intentarlo',
                'status' => 2
            );
        break;

    }
}
?>