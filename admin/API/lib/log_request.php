<?php
include('funciones.php');
include('class/UUID.php');

$UUID_REG = strtoupper(UUID::v4());

$bd_log = new db();
$navigator_ua=getBrowser();

//ESTRUCTURA TABLA DATOS
$columnas  = array(
    "uuid_log_request",
    "addr_log_request",
    "port_log_request",
    "method_log_request",
    "time_log_request",

    "navigator_log_request",
    "v_navigator_log_request",
    "os_log_request",
    "data_log_request"
);

$bd_log->setColumnas($columnas);
$bd_log->setTabla('log_request');
$bd_log->setFiltro('1');
$bd_log->setAgrupar('id_log_request');
$bd_log->setOrden('id_log_request');

$valores = array(
    "'" . $UUID_REG . "'",
    "'" . $_SERVER["REMOTE_ADDR"] . "'",
    "'" . $_SERVER["REMOTE_PORT"] . "'",
    "'" . $_SERVER["REQUEST_METHOD"] . "'",
    "'" . $_SERVER["REQUEST_TIME"] . "'",
    "'" . $navigator_ua['name'] . "'",
    "'" . $navigator_ua['version'] . "'",
    "'" . $navigator_ua['platform'] . "'",
    "'" . print_r($_REQUEST,true) . "'"
);

$bd_log->setValores($valores);
$bd_log->insert();


?>