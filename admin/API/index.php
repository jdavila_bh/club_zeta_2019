<?php
session_start();
include('lib/header.php');
include('lib/conex.php');

include('lib/class/PHPMailer-compress/class.phpmailer.php');
include('lib/class/qr/phpqrcode/qrlib.php');

include('lib/class.php');

include('lib/SMTP.php');
include('lib/log_request.php');

$datos = array(); // RESPONSE

$peticion = explode('/',$_GET['PATH_INFO']); // CAPTURAMOS RUTA
$funcion_modulo = $peticion[1]; // METODO DEL MODULO
$id_reg_cap = $peticion[2]; // REGISTRO
// Obtener recurso
$recurso = array_shift($peticion); // SACAMOS EL RECURSO QUE APUNTA // MODULO

// MODULOS
$recursos_existentes = array(
    'usuario_admin',
    'colegio',
    'orden_compra',

    'usuario',
    'tipo_usuario',
    'cliente',
    'empresa',

    'region',
    'provincia',
    'comuna',
    'tipo_contrato',
    'tipo_pago',
    'ot',
    'detalle_ot',
    'comentario_ot',

    'pdf'
    );

// Comprobar si existe el recurso
if(!in_array($recurso, $recursos_existentes)){

    $datos['status'] = array(
        'codigo'=>405,
        'mensaje'=>'Metodo no existe, vuelva intentarlo'
   );

}else{

    // METODO UTILIZADO GET,POST,PUT,DELETE,PATCH
    $metodo = strtoupper($_SERVER['REQUEST_METHOD']);
	//$metodo = $_POST['method'];
    // LINK MODULO
    $_REQUEST = valid_xss_clean($_REQUEST);
    include('lib/modulo/'.$recurso.'/modulo.php');
}


// RESPONSE API
echo json_encode($datos);