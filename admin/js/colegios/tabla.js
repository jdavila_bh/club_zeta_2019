$(document).ready(function(){
   tabla_colegios();

});

function tabla_colegios()
{
    $('#tabla_registros').html('');
    var datos=[];

    datos.modulo = 'colegios';
    datos.metodo = 'traerTodosRegistros';
    datos.data = data;

    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            cargando.load(0);

            var titulos = ['ID', 'NOMBRE COLEGIO','LOGO', 'N.C.C.', 'NOMBRE ADMIN', 'MAIL ADMIN','FOTO ADMIN',
                            'DIRECCION COLEGIO', 'TELEFONO COLEGIO', 'MAIL CONTACTO', 'TELEFONO CONTACTO',
                            'JOP', 'MAIL JOP','PRECIO U. ','PRECIO U.2 '];
            var html = '';
            var div_tabla = 'tabla_registros';
            html+= armar_tabla_head(titulos,div_tabla);

            if(resp.estado==1)
            {
                for(var key in resp.registros)
                {
                    html+='<tr>';
                    html+='<td>'+resp.registros[key].id+'</td>';
                    html+='<td>'+resp.registros[key].rut+' </td>';
                    html+='<td>'+resp.registros[key].estado+'</td>';
                    html+='<td>'+resp.registros[key].cantidad+'</td>';
                    html+='<td>'+resp.registros[key].monto+'</td>';
                    html+='<td>'+resp.registros[key].fecha+'</td>';
                    html+='<td><button id="btn_ver" class="btn fa fa-eye" onclick="ver_comprobante(\''+resp.registros[key].id+'\');"/></td>';
                    html+='<td><button id="btn_meil_apod" class="btn fa fa-envelope" onclick="enviar_mail(\''+resp.registros[key].id+'\',\'1\')"> Enviar apoderado</td>';
                    html+='<td><button id="btn_mail_admi" class="btn fa fa-envelope-o"  onclick="enviar_mail(\''+resp.registros[key].id+'\',\'2\')"> Enviar Adminstrador</td>';
                    html+='</tr>';
                }
            }
            $('#tabla_registro').html(html);
            dataTable(div_tabla);
        }
    });
}

function tabla_registros_orden()
{
    var f_rut = $('#f_orden').val();
    var data = {};
    $('#tabla_registros').html('');

    if(f_rut!=-1) data.id_registro = f_rut;

    var datos = {};

    datos.modulo = 'orden_compra';
    datos.metodo = 'traerRegistroIdOrden';
    datos.data = data;

    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){
            cargando.load(0);
            var titulos = ['N° Orden ','Rut','Estado Orden', 'Cantidad','Total','Fecha','Ver Comprobante','Enviar apoderado','Enviar Administrador'];
            var html = '';
            var div_tabla = 'tabla_registros';
            html+= armar_tabla_head(titulos,div_tabla);

            if(resp.estado==1)
            {
                for(var key in resp.registros)
                {
                    html+='<tr>';
                    html+='<td>'+resp.registros[key].id+'</td>';
                    html+='<td>'+resp.registros[key].rut+' </td>';
                    html+='<td>'+resp.registros[key].estado+'</td>';
                    html+='<td>'+resp.registros[key].cantidad+'</td>';
                    html+='<td>'+resp.registros[key].monto+'</td>';
                    html+='<td>'+resp.registros[key].fecha+'</td>';
                    html+='<td><button id="btn_ver" class="btn fa fa-eye" onclick="ver_comprobante(\''+resp.registros[key].id+'\');"/></td>';
                    html+='<td><button id="btn_meil_apod" class="btn fa fa-envelope"> Enviar apoderado</td>';
                    html+='<td><button id="btn_mail_admi" class="btn fa fa-envelope-o"> Enviar Adminstrador</td>';
                    html+='</tr>';
                }
            }
            $('#tabla_registro').html(html);
            dataTable(div_tabla);
        }
    });
}

function imprimir(){
    cargando.load();
    var objeto=document.getElementById('compra_exitosa');  //obtenemos el objeto a imprimir
    var ventana=window.open('','_blank');  //abrimos una ventana vacía nueva
    ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
    setTimeout("",10000);
    ventana.document.close();  //cerramos el documento
    ventana.print();  //imprimimos la ventana
    ventana.close();  //cerramos la ventana
    $('#boton_imprimmir').show();
    $('#boton_volver_comprar').show();
}

/*
 @Tipo: 1=Apoderado
        2=Administrador
 */
function enviar_mail(id_orden, tipo) {
    cargando.load(true);
    var data = {};
    data.id_registro = id_orden;
    data.tipo = tipo;

    var datos = {};

    datos.modulo = 'orden_compra';
    datos.metodo = 'enviarMail';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){
            cargando.load(false);
            if(resp.estado==1) {
                ok(resp.msg);
            }else{
                error(resp.msg);
            }
        }
    });
}

/* jkdfkaljsdhfkjasdhf */

function ver_pdf(num)
{
    var data = {};

    data.pdf_estado = 1;
    data.n_presupuesto = num;

    var datos = {};

    datos.modulo = 'pdf';
    datos.metodo = 'genPdf';
    datos.data = data;


    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp) {

            cargando.load(0);
            $.fancybox({type:'iframe', href:"../pdf_ot/"+num+"_presupuesto.pdf", width: 800, height: 600});

        }
    });
}

function descargar_pdf(num)
{
    var data = {};

    data.pdf_estado = 2;
    data.n_presupuesto = num;

    var datos = {};

    datos.modulo = 'pdf';
    datos.metodo = 'genPdf';
    datos.data = data;


    cargando.load(1);
    $.ajax({
        type: 'POST',
        url: URL_WBS,
        success: function(resp) {
            cargando.load(0);
            downloadURI('../pdf_ot/'+num+'_presupuesto.pdf',num+'_presupuesto.pdf');

        },
        data: datos,
        dataType: 'json'
    });
}
