
$(document).ready(function(){
    listaRegiones('region',0);
    //listaProvincias('provincia',0);
    //listaComunas('comuna',0);
});


function traerCiudades()
{
    var datos = {};

    datos.estado = 1;
    datos.id_region = $('#region').val();
    datos.id_provincia = 0;
    listaProvincias('provincia',0,datos);
    listaComunas('comuna',0,datos);
}

function traerComunas()
{
    var datos = {};

    datos.estado = 1;
    datos.id_provincia = $('#provincia').val();
    listaComunas('comuna',0,datos);
}



function registrar()
{

    var nombre = $.trim($('#nombre').val());
    var rut = $.trim($('#rut').val());
    var direccion = $.trim($('#direccion').val());

    var region = $.trim($('#region').val());
    var provincia = $.trim($('#provincia').val());
    var comuna = $.trim($('#comuna').val());

    var telefono = $.trim($('#telefono').val());
    var celular = $.trim($('#celular').val());

    var email = $.trim($('#email').val());

    var giro = $.trim($('#giro').val());


    if(nombre=='')
    {
        error(txt_tipo_cliente.texto1);
        $('#nombre').select().focus();
        return false;
    }
    if(!Rut(rut,'rut'))
    {
        $('#rut').select().focus();
        return false;
    }
    if(direccion=='')
    {
        error(txt_tipo_cliente.texto2);
        $('#direccion').select().focus();
        return false;
    }


    if(region==0)
    {
        error(txt_tipo_cliente.texto3);
        $('#region').select().focus();
        return false;
    }

    if(provincia==0)
    {
        error(txt_tipo_cliente.texto4);
        $('#provincia').select().focus();
        return false;
    }

    if(comuna==0)
    {
        error(txt_tipo_cliente.texto5);
        $('#comuna').select().focus();
        return false;
    }

	/*
    if(telefono=='')
    {
        error(txt_tipo_cliente.texto6);
        $('#telefono').select().focus();
        return false;
    }

    if(celular=='')
    {
        error(txt_tipo_cliente.texto7);
        $('#celular').select().focus();
        return false;
    }
	*/
	
    if(validarEmail(email))
    {
        error(txt_tipo_cliente.texto8);
        $('#email').select().focus();
        return false;
    }

    if(giro=='')
    {
        error(txt_tipo_cliente.texto9);
        $('#giro').select().focus();
        return false;
    }


    var data = {};

    data.nombre = nombre;
    data.rut = rut;
    data.direccion = direccion;


    data.id_region = region;
    data.id_provincia = provincia;
    data.id_comuna = comuna;

    data.telefono = telefono;
    data.celular = celular;

    data.email = email;
    data.giro = giro;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'registrarCliente';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

                if(resp.estado == 1)
                {
                    $('#nombre').val('');
                    $('#rut').val('');
                    $('#direccion').val('');

                    $('#region').val(0);
                    $('#provincia').val(0);
                    $('#comuna').val(0);

                    $('#telefono').val('');
                    $('#celular').val('');

                    $('#email').val('');

                    $('#giro').val('');

                    ok('Cliente Registrado');

                }else{
                    error('Problemas al registrar el cliente.');
                }

        }
    });


}
