
$(document).ready(function(){
    tabla_registros();
    listaRegiones('region',0);
});



function traerCiudades()
{
    var datos = {};

    datos.estado = 1;
    datos.id_region = $('#region').val();
    datos.id_provincia = 0;
    listaProvincias('provincia',0,datos);
    listaComunas('comuna',0,datos);
}

function traerComunas()
{
    var datos = {};

    datos.estado = 1;
    datos.id_provincia = $('#provincia').val();
    listaComunas('comuna',0,datos);
}

function tabla_registros()
{

    var data = {};


    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'traerTodosClientes';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            var color_estados = ['danger','success'];
            var titulos = ['Id','Nombre','Rut', 'Dirección','Region','Ciudad','Comuna','Telefono','Celular','Email','Giro','Fecha Ingreso','Estado','Modificar'];
            var html = '';
            var div_tabla = 'tabla_registros';
            html+= armar_tabla_head(titulos,div_tabla);

            if(resp.estado==1)
            {

                for(var key in resp.registros)
                {

                html+='<tr>';
                html+='<td>'+resp.registros[key].id+'</td>';
                html+='<td id="nombre_reg_'+resp.registros[key].id+'">'+resp.registros[key].nombre+'</td>';
                html+='<td id="rut_reg_'+resp.registros[key].id+'">'+resp.registros[key].rut+'</td>';
                html+='<td id="direccion_reg_'+resp.registros[key].id+'">'+resp.registros[key].direccion+'</td>';

                html+='<td id="region_reg_'+resp.registros[key].id+'">'+resp.registros[key].regionObj.nombre+'</td>';
                html+='<td id="provincia_reg_'+resp.registros[key].id+'">'+resp.registros[key].provinciaObj.nombre+'</td>';
                html+='<td id="comuna_reg_'+resp.registros[key].id+'">'+resp.registros[key].comunaObj.nombre+'</td>';
                html+='<td id="telefono_reg_'+resp.registros[key].id+'">'+resp.registros[key].telefono+'</td>';

                html+='<td id="celular_reg_'+resp.registros[key].id+'">'+resp.registros[key].celular+'</td>';
                html+='<td id="email_reg_'+resp.registros[key].id+'">'+resp.registros[key].email+'</td>';
                html+='<td id="giro_reg_'+resp.registros[key].id+'">'+resp.registros[key].giro+'</td>';
                html+='<td>'+resp.registros[key].fecha_ingreso+'</td>';

                html+='<td><button id="reg_'+resp.registros[key].id+'" class="btn btn-'+color_estados[resp.registros[key].estado]+' menu-icon fa fa fa-check boton" onclick="estado_registro('+resp.registros[key].id+','+resp.registros[key].estado+')"></button></td>';
                html+='<td><button class="btn btn-info menu-icon fa fa-pencil boton" onclick="editar_registro('+resp.registros[key].id+')"></button></td>';
                html+='</tr>';
                }

            }

            $('#tabla_registro').html(html);
            dataTable(div_tabla);
        }
    });

}


function editar_registro(id)
{
    $('#id_registro').html(id);

    var data = {};

    data.id_registro = id;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'traerCliente';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {

                $('#nombre').val(resp.registros.nombre);
                $('#rut').val(resp.registros.rut);
                $('#direccion').val(resp.registros.direccion);

                //$('#region').val(resp.registros.region);
                //$('#provincia').val(resp.registros.provincia);
                //$('#comuna').val(resp.registros.comuna);

                listaRegiones2('region','provincia','comuna',resp.registros.id_region,resp.registros.id_provincia,resp.registros.id_comuna);


                $('#telefono').val(resp.registros.telefono);
                $('#celular').val(resp.registros.celular);

                $('#email').val(resp.registros.email);
                $('#giro').val(resp.registros.giro);

                ver_div('#tabla_editar_registro');
            }else{
                error('Problema al traer el registro.');
            }
        }
    });


}

function editar_registro_guardar()
{
    var id_registro = $.trim($('#id_registro').html());

    var nombre = $.trim($('#nombre').val());
    var rut = $.trim($('#rut').val());
    var direccion = $.trim($('#direccion').val());

    var region = $.trim($('#region').val());
    var provincia = $.trim($('#provincia').val());
    var comuna = $.trim($('#comuna').val());

    var region_texto = $.trim($('#region option:selected').text());
    var provincia_texto = $.trim($('#provincia option:selected').text());
    var comuna_texto = $.trim($('#comuna option:selected').text());

    var telefono = $.trim($('#telefono').val());
    var celular = $.trim($('#celular').val());

    var email = $.trim($('#email').val());

    var giro = $.trim($('#giro').val());


    if(nombre=='')
    {
        error(txt_tipo_cliente.texto1);
        $('#nombre').select().focus();
        return false;
    }
    if(!Rut(rut,'rut'))
    {
        $('#rut').select().focus();
        return false;
    }
    if(direccion=='')
    {
        error(txt_tipo_cliente.texto2);
        $('#direccion').select().focus();
        return false;
    }


    if(region==0)
    {
        error(txt_tipo_cliente.texto3);
        $('#region').select().focus();
        return false;
    }

    if(provincia==0)
    {
        error(txt_tipo_cliente.texto4);
        $('#provincia').select().focus();
        return false;
    }

    if(comuna==0)
    {
        error(txt_tipo_cliente.texto5);
        $('#comuna').select().focus();
        return false;
    }
	/*
    if(telefono=='')
    {
        error(txt_tipo_cliente.texto6);
        $('#telefono').select().focus();
        return false;
    }

    if(celular=='')
    {
        error(txt_tipo_cliente.texto7);
        $('#celular').select().focus();
        return false;
    }
	*/

    if(validarEmail(email))
    {
        error(txt_tipo_cliente.texto8);
        $('#email').select().focus();
        return false;
    }

    if(giro=='')
    {
        error(txt_tipo_cliente.texto9);
        $('#giro').select().focus();
        return false;
    }


    var data = {};

    data.id_registro = id_registro;

    data.nombre = nombre;
    data.rut = rut;
    data.direccion = direccion;


    data.id_region = region;
    data.id_provincia = provincia;
    data.id_comuna = comuna;

    data.telefono = telefono;
    data.celular = celular;

    data.email = email;
    data.giro = giro;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'modificarCliente';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){


                if(resp.estado == 1)
                {
                    $('#nombre').val('');
                    $('#rut').val('');
                    $('#direccion').val('');
                    $('#region').val(0);
                    $('#provincia').val(0);
                    $('#comuna').val(0);
                    $('#telefono').val('');
                    $('#celular').val('');
                    $('#email').val('');
                    $('#giro').val('');

                    ok('Registro Modificado');
                    $.fancybox.close();

                    var tag = [
                        "nombre_reg_",
                        "rut_reg_",
                        "direccion_reg_",

                        "region_reg_",
                        "provincia_reg_",
                        "comuna_reg_",

                        "telefono_reg_",
                        "celular_reg_",
                        "email_reg_",
                        "giro_reg_"
                    ];
                    var value = [
                        nombre,
                        rut,
                        direccion,

                        region_texto,
                        provincia_texto,
                        comuna_texto,

                        telefono,
                        celular,
                        email,
                        giro
                    ];

                    actualizarTablaReg(tag,value,id_registro);

                }else{
                    error('Problema al actualizar registro.');
                }

        }
    });
}


function actualizarTablaReg(tag,value,id_registro)
{

    for(var key in tag)
    {
        $('#'+tag[key]+id_registro).html(value[key]);
    }

}


function estado_registro(id,estado)
{

    if(estado)
    estado=0;
    else
    estado=1;

    var data = {};

    data.id_registro = id;
    data.nuevo_estado = estado;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'cambiarEstado';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                var color_estado = ['btn-success','btn-danger'];
                var color_estado2 = ['btn-danger','btn-success'];

                $('#reg_'+id).removeClass(color_estado[estado]).addClass(color_estado2[estado]).attr('onclick','estado_registro('+id+','+estado+')');

                ok('Cambios de estado, exitoso.');

            }else{
                ok('Problemas al cambiar el estado, vuelva a intentarlo.');
            }
        }
    });

}