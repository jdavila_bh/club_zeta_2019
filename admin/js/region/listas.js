

function listaRegiones(div,id)
{

    var datos = {};

    datos.modulo = 'region';
    datos.metodo = 'traerTodosRegiones';

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            $('#'+div).html('<option value="0">Seleccione una región</option>');

            if(resp.estado == 1)
            {
                for(var key in resp.registros)
                    $('#'+div).append('<option value="'+resp.registros[key].id+'">'+resp.registros[key].nombre+'</option>');

                if(id>0)
                {
                    $('#'+div).val(id);
                }

            }



        }
    });

}

function listaRegiones2(div,div2,div3,id,id2,id3)
{

    var datos = {};

    datos.modulo = 'region';
    datos.metodo = 'traerTodosRegiones';

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            $('#'+div).html('<option value="0">Seleccione una región</option>');

            if(resp.estado == 1)
            {
                for(var key in resp.registros)
                    $('#'+div).append('<option value="'+resp.registros[key].id+'">'+resp.registros[key].nombre+'</option>');

                if(id>0)
                {
                    $('#'+div).val(id);
                }

                listaProvincias2(div2,div3,id2,id3);

            }



        }
    });

}




