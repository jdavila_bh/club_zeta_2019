var txt_tipo_usuario = {};

// VISTA Agregar Usuario
txt_tipo_usuario.texto1 = 'Seleccione Tipo Usuario';

// Verificación de campos al ingresar usuario
txt_tipo_usuario.texto2 = 'Debe ingresar el Nombre.';
txt_tipo_usuario.texto3 = 'Debe ingresar el Apellido Parterno.';
txt_tipo_usuario.texto4 = 'Debe ingresar el Apellido Materno.';
txt_tipo_usuario.texto5 = 'Debe ingresar el e-mail.';
txt_tipo_usuario.texto6 = 'Debe ingresar la Dirección.';
txt_tipo_usuario.texto7 = 'Debe ingresar el telefono.';
txt_tipo_usuario.texto8 = 'Debe ingresar el Celular.';
txt_tipo_usuario.texto9 = 'El nombre de usuario debe tener un minimo de 5 caracteres.';
txt_tipo_usuario.texto10 = 'El pass de usuario debe tener un minimo de 6 caracteres.';
txt_tipo_usuario.texto11 = 'El pass y el pass2 debe ser iguales.';
txt_tipo_usuario.texto12 = 'Debe seleccionar un tipo de usuario.';


//Resultado de ingreso
txt_tipo_usuario.texto13 = 'Nombre de usuario ya existe.' ;
txt_tipo_usuario.texto14 = 'Usuario Creado satisfactoriamente';
txt_tipo_usuario.texto15 = 'Problema al crear el usuario, vuelva a intentarlo.';

txt_tipo_usuario.texto16 = 'Usuario modificado satisfactoriamente';
txt_tipo_usuario.texto17 = 'Problema al modificado el usuario, vuelva a intentarlo.';