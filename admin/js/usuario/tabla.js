
$(document).ready(function(){
    tabla_registros();
    listaTipoUsuarios('tipo',0);
});

function tabla_registros()
{

    var data = {};

    var datos = {};

    datos.modulo = 'usuario';
    datos.metodo = 'traerTodosUsuarios';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            var color_estados = ['danger','success'];
            var titulos = ['Id','Nombre','Apellido P', 'Apellido M','Rut','Email','Dirección','Telefono','Celular','User','Tipo','Fecha Ingreso','Estado','Modificar','Clave'];
            var html = '';
            var div_tabla = 'tabla_registros_usuarios';
            html+= armar_tabla_head(titulos,div_tabla);

            if(resp.estado==1)
            {

                for(var key in resp.registros)
                {

                html+='<tr>';
                html+='<td>'+resp.registros[key].id+'</td>';
                html+='<td id="nombre_reg_'+resp.registros[key].id+'">'+resp.registros[key].nombre+'</td>';
                html+='<td id="apellidop_reg_'+resp.registros[key].id+'">'+resp.registros[key].apellidop+'</td>';
                html+='<td id="apellidom_reg_'+resp.registros[key].id+'">'+resp.registros[key].apellidom+'</td>';
                html+='<td id="rut_reg_'+resp.registros[key].id+'">'+resp.registros[key].rut+'</td>';
                html+='<td id="email_reg_'+resp.registros[key].id+'">'+resp.registros[key].email+'</td>';
                html+='<td id="direccion_reg_'+resp.registros[key].id+'">'+resp.registros[key].direccion+'</td>';
                html+='<td id="telefono_reg_'+resp.registros[key].id+'">'+resp.registros[key].telefono+'</td>';
                html+='<td id="celular_reg_'+resp.registros[key].id+'">'+resp.registros[key].celular+'</td>';
                html+='<td id="user_reg_'+resp.registros[key].id+'">'+resp.registros[key].user+'</td>';
                html+='<td id="tipo_reg_'+resp.registros[key].id+'">'+resp.registros[key].tipo_usuario_obj.nombre+'</td>';
                html+='<td>'+resp.registros[key].fecha_ingreso+'</td>';



                html+='<td><button id="reg_'+resp.registros[key].id+'" class="btn btn-'+color_estados[resp.registros[key].estado]+' menu-icon fa fa fa-check boton" onclick="estado_registro('+resp.registros[key].id+','+resp.registros[key].estado+')"></button></td>';
                html+='<td><button class="btn btn-info menu-icon fa fa-pencil boton" onclick="editar_registro('+resp.registros[key].id+')"></button></td>';
                html+='<td><button class="btn btn-warning menu-icon fa fa-pencil boton" onclick="editar_clave_registro('+resp.registros[key].id+')"></button></td>';
                html+='</tr>';
                }

            }

            $('#tabla_registro').html(html);
            dataTable(div_tabla);


        }
    });

}


function editar_clave_registro(id)
{
    $('#id_usuario_hidden').val(id);
    ver_div('#contenedor_cambio_clave');
}

function guardar_clave(){
    var id_registro = $.trim($('#id_usuario_hidden').val());
    var pass = $.trim($('#clave_nueva').val());
    var pass2 = $.trim($('#clave_nueva2').val());

    if(pass.length<6)
    {
        error(txt_tipo_usuario.texto10);
        $('#pass').select().focus();
        return false;
    }
    if(pass!=pass2)
    {
        error(txt_tipo_usuario.texto11);
        $('#pass').select().focus();
        return false;
    }

    var data = {};

    data.id_registro = id_registro;
    data.nueva_clave = pass;

    var datos = {};

    datos.modulo = 'usuario';
    datos.metodo = 'cambiarClave';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                $.fancybox.close();
                $('#clave_nueva').val('');
                $('#clave_nueva2').val('');
                ok('Cambios de Clave, exitoso.');

            }else{
                ok('Problemas al cambiar la clave, vuelva a intentarlo.');
            }
        }
    });


}


function editar_registro(id)
{
    $('#id_usuario').html(id);

    var data = {};

    data.id_registro = id;

    var datos = {};

    datos.modulo = 'usuario';
    datos.metodo = 'traerUsuarios';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {

                $('#nombre').val(resp.registros.nombre);
                $('#apellidop').val(resp.registros.apellidop);
                $('#apellidom').val(resp.registros.apellidom);
                $('#rut').val(resp.registros.rut);
                $('#email').val(resp.registros.email);
                $('#direccion').val(resp.registros.direccion);
                $('#telefono').val(resp.registros.telefono);
                $('#celular').val(resp.registros.celular);

                $('#tipo').val(resp.registros.id_tipo_usuario);

                ver_div('#tabla_editar_usuario');
            }else{
                error('Problema al traer los datos del usuario');
            }
        }
    });


}

function editar_registro_guardar()
{

    var id_registro = $.trim($('#id_usuario').html());
    var nombre = $.trim($('#nombre').val());
    var apellidop = $.trim($('#apellidop').val());
    var apellidom = $.trim($('#apellidom').val());
    var rut = $.trim($('#rut').val());
    var email = $.trim($('#email').val());
    var direccion = $.trim($('#direccion').val());
    var telefono = $.trim($('#telefono').val());
    var celular = $.trim($('#celular').val());

    var tipo = $.trim($('#tipo').val());
    var tipo_txt = $('#tipo option:selected').text();


    if(nombre=='')
    {
        error(txt_tipo_usuario.texto2);
        $('#nombre').select().focus();
        return false;
    }
    if(apellidop=='')
    {
        error(txt_tipo_usuario.texto3);
        $('#apellidop').select().focus();
        return false;
    }
    if(apellidom=='')
    {
        error(txt_tipo_usuario.texto4);
        $('#apellidom').select().focus();
        return false;
    }
    if(!Rut(rut,'rut'))
    {
        $('#rut').select().focus();
        return false;
    }
    if(validarEmail(email))
    {
        error(txt_tipo_usuario.texto5);
        $('#email').select().focus();
        return false;
    }
    if(direccion=='')
    {
        error(txt_tipo_usuario.texto6);
        $('#direccion').select().focus();
        return false;
    }
    if(telefono=='')
    {
        error(txt_tipo_usuario.texto7);
        $('#telefono').select().focus();
        return false;
    }
    if(celular=='')
    {
        error(txt_tipo_usuario.texto8);
        $('#celular').select().focus();
        return false;
    }

    if(tipo==0)
    {
        error(txt_tipo_usuario.texto12);
        $('#tipo').select().focus();
        return false;
    }

    var data = {};

    data.id_registro = id_registro;
    data.nombre = nombre;
    data.apellidop = apellidop;
    data.apellidom = apellidom;
    data.rut = rut;
    data.email = email;
    data.direccion = direccion;
    data.telefono = telefono;
    data.celular = celular;

    data.tipo = tipo;

    var datos = {};

    datos.modulo = 'usuario';
    datos.metodo = 'modificarUsuario';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){


                if(resp.estado == 1)
                {
                    $('#nombre').val('');
                    $('#apellidop').val('');
                    $('#apellidom').val('');
                    $('#rut').val('');
                    $('#email').val('');
                    $('#direccion').val('');
                    $('#telefono').val('');
                    $('#celular').val('');

                    $('#tipo').val(0);

                    ok(txt_tipo_usuario.texto16);
                    $.fancybox.close();

                    var tag = [
                        "nombre_reg_",
                        "apellidop_reg_",
                        "apellidom_reg_",
                        "rut_reg_",
                        "email_reg_",
                        "direccion_reg_",
                        "telefono_reg_",
                        "celular_reg_",
                        "tipo_reg_"
                    ];
                    var value = [
                        nombre,
                        apellidop,
                        apellidom,
                        rut,
                        email,
                        direccion,
                        telefono,
                        celular,
                        tipo_txt
                    ];

                    actualizarTablaReg(tag,value,id_registro);

                }else{
                    error(txt_tipo_usuario.texto17);
                }

        }
    });
}


function actualizarTablaReg(tag,value,id_registro)
{

    for(var key in tag)
    {
        $('#'+tag[key]+id_registro).html(value[key]);
    }

}


function estado_registro(id,estado)
{

    if(estado)
    estado=0;
    else
    estado=1;

    var data = {};

    data.id_registro = id;
    data.nuevo_estado = estado;

    var datos = {};

    datos.modulo = 'usuario';
    datos.metodo = 'cambiarEstado';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                var color_estado = ['btn-success','btn-danger'];
                var color_estado2 = ['btn-danger','btn-success'];

                $('#reg_'+id).removeClass(color_estado[estado]).addClass(color_estado2[estado]).attr('onclick','estado_registro('+id+','+estado+')');

                ok('Cambios de estado, exitoso.');

            }else{
                ok('Problemas al cambiar el estado, vuelva a intentarlo.');
            }
        }
    });

}