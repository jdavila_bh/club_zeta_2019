
$(document).ready(function(){
    listaTipoUsuarios('tipo',0);
});


function registrar()
{

    var nombre = $.trim($('#nombre').val());
    var apellidop = $.trim($('#apellidop').val());
    var apellidom = $.trim($('#apellidom').val());
    var rut = $.trim($('#rut').val());
    var email = $.trim($('#email').val());
    var direccion = $.trim($('#direccion').val());
    var telefono = $.trim($('#telefono').val());
    var celular = $.trim($('#celular').val());

    var user = $.trim($('#user').val());

    var pass = $.trim($('#pass').val());
    var pass2 = $.trim($('#pass2').val());

    var tipo = $.trim($('#tipo').val());


    if(nombre=='')
    {
        error(txt_tipo_usuario.texto2);
        $('#nombre').select().focus();
        return false;
    }
    if(apellidop=='')
    {
        error(txt_tipo_usuario.texto3);
        $('#apellidop').select().focus();
        return false;
    }
    if(apellidom=='')
    {
        error(txt_tipo_usuario.texto4);
        $('#apellidom').select().focus();
        return false;
    }
    if(!Rut(rut,'rut'))
    {
        $('#rut').select().focus();
        return false;
    }
    if(validarEmail(email))
    {
        error(txt_tipo_usuario.texto5);
        $('#email').select().focus();
        return false;
    }
    if(direccion=='')
    {
        error(txt_tipo_usuario.texto6);
        $('#direccion').select().focus();
        return false;
    }
    if(telefono=='')
    {
        error(txt_tipo_usuario.texto7);
        $('#telefono').select().focus();
        return false;
    }
    if(celular=='')
    {
        error(txt_tipo_usuario.texto8);
        $('#celular').select().focus();
        return false;
    }
    if(user.length<5)
    {
        error(txt_tipo_usuario.texto9);
        $('#user').select().focus();
        return false;
    }
    if(pass.length<6)
    {
        error(txt_tipo_usuario.texto10);
        $('#pass').select().focus();
        return false;
    }
    if(pass!=pass2)
    {
        error(txt_tipo_usuario.texto11);
        $('#pass').select().focus();
        return false;
    }
    if(tipo==0)
    {
        error(txt_tipo_usuario.texto12);
        $('#tipo').select().focus();
        return false;
    }

    var data = {};

    data.nombre = nombre;
    data.apellidop = apellidop;
    data.apellidom = apellidom;
    data.rut = rut;
    data.email = email;
    data.direccion = direccion;
    data.telefono = telefono;
    data.celular = celular;

    data.user = user;
    data.pass = pass;
    data.tipo = tipo;

    var datos = {};

    datos.modulo = 'usuario';
    datos.metodo = 'registrarUsuario';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            if(resp.estado == 2)
            {
                error(txt_tipo_usuario.texto13);
            }else{
                if(resp.estado == 1)
                {
                    $('#nombre').val('');
                    $('#apellidop').val('');
                    $('#apellidom').val('');
                    $('#rut').val('');
                    $('#email').val('');
                    $('#direccion').val('');
                    $('#telefono').val('');
                    $('#celular').val('');

                    $('#user').val('');

                    $('#pass').val('');
                    $('#pass2').val('');

                    $('#tipo').val(0);

                    ok(txt_tipo_usuario.texto14);

                }else{
                    error(txt_tipo_usuario.texto15);
                }
            }
        }
    });


}
