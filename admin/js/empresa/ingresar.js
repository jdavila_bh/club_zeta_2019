$(document).ready(function()
{
    
});

// AGREGAR
function registrar()
{
    var nombre = $.trim($('#nombre').val());
    var direccion = $.trim($('#direccion').val());
    var telefono = $.trim($('#telefono').val());
    var giro = $.trim($('#giro').val());

    if(!Rut($.trim($('#rut').val()),'rut'))
    {
        $('#rut').select().focus();
        return false;
    }

    var data = {};

    data.nombre = nombre;
    data.rut = $.trim($('#rut').val());
    data.direccion = direccion;
    data.telefono = telefono;
    data.giro = giro;

    var estadoLoop = 0; //Verifica si el ciclo rompe o no

    $.each( data, function( key, value ) {
      if (value=='') {
        $('#'+key).select().focus();
        error("Debe Ingresar "+$('#'+key).attr('placeholder')+'.');
        estadoLoop = 1;
        return false; //Rompe el ciclo each
      }
    });

    if (estadoLoop==1) {
        return false; //Rompe el codigo si uno de los datos del loop esta vacio
    }

    var datos = {};

    datos.modulo = 'empresa';
    datos.metodo = 'agregarEmpresa';
    datos.data = data;

    cargando.load();

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            cargando.load();
            if(resp.estado==1)
            {
                ok("Registro creado satisfactoriamente.");
                $(':input').val('');
            }else{
                error("problema al crear el registro, vuelva a intentarlo.");
            }
            tabla_registros();
        }
    });
}

