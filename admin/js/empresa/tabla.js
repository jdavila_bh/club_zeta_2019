$(document).ready(function(){
    tabla_registros(); // CARGA TABLA DATOS
});


function tabla_registros()
{

    var data = {};

    var datos = {};

    datos.modulo = 'empresa';
    datos.metodo = 'obtenerEmpresas';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){
            var color_estados = ['danger','success'];
            var titulos = ['ID','Nombre','RUT','Dirección','Teléfono','Giro','Fecha de Ingreso','Fecha de Modificación','Estado','Modificar'];
            var html = '';
            var div_tabla = 'tabla_registros';
            html+= armar_tabla_head(titulos,div_tabla);

            if(resp.estado==1)
            {
                for(var key in resp.registros)
                {
                    html+='<tr>';
                    html+='<td>'+resp.registros[key].id+'</td>';
                    html+='<td id="nombre_reg_'+resp.registros[key].id+'">'+resp.registros[key].nombre+'</td>';
                    html+='<td id="rut_reg_'+resp.registros[key].id+'">'+resp.registros[key].rut+'</td>';
                    html+='<td id="direccion_reg_'+resp.registros[key].id+'">'+resp.registros[key].direccion+'</td>';
                    html+='<td id="telefono_reg_'+resp.registros[key].id+'">'+resp.registros[key].telefono+'</td>';
                    html+='<td id="giro_reg_'+resp.registros[key].id+'">'+resp.registros[key].giro+'</td>';
                    html+='<td>'+resp.registros[key].fecha_ingreso+'</td>';
                    html+='<td>'+resp.registros[key].fecha_modificacion+'</td>';
                    html+='<td><button id="reg_'+resp.registros[key].id+'" class="btn btn-'+color_estados[resp.registros[key].estado]+' menu-icon fa fa fa-check boton" onclick="estado_registro('+resp.registros[key].id+','+resp.registros[key].estado+')"></button></td>';
                    html+='<td><button class="btn btn-info menu-icon fa fa-pencil boton" onclick="modal_editar('+resp.registros[key].id+')"></button></td>';
                    html+='</tr>';
                }
            }

            $('#tabla_registro').html(html);
            dataTable(div_tabla);
        }
    });
}


// MODIFICAR DATO
function modal_editar(id){
    cargando.load();
    $('#id_registro_mod').val(id);
    $('#editar_registro').modal();
  // $('#editar_registro').on('shown.bs.modal', function (e) {
        $('#nombre_mod').val($('#nombre_reg_'+id).html());
        $('#rut_mod').val($('#rut_reg_'+id).html());
        $('#direccion_mod').val($('#direccion_reg_'+id).html());
        $('#telefono_mod').val($('#telefono_reg_'+id).html());
        $('#giro_mod').val($('#giro_reg_'+id).html());
   //});

    //cambiar_botones(false);
    //$(document).scrollTop(0);
    cargando.load();
}


function estado_registro(id,estado)
{

    if(estado)
        estado=0;
    else
        estado=1;

    var data = {};

    data.id_registro = id;
    data.nuevo_estado = estado;

    var datos = {};

    datos.modulo = 'empresa';
    datos.metodo = 'cambiarEstado';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                var color_estado = ['btn-success','btn-danger'];
                var color_estado2 = ['btn-danger','btn-success'];

                $('#reg_'+id).removeClass(color_estado[estado]).addClass(color_estado2[estado]).attr('onclick','estado_registro('+id+','+estado+')');

                ok('Cambios de estado, exitoso.');

            }else{
                error('Problemas al cambiar el estado, vuelva a intentarlo.');
            }
        }
    });

}