$(document).ready(function()
{
    
});

// MODIFICAR
function modificar_registro()
{
    var id = $.trim($('#id_registro_mod').val());
    var nombre = $.trim($('#nombre_mod').val());
    var rut = $.trim($('#rut_mod').val());
    var direccion = $.trim($('#direccion_mod').val());
    var telefono = $.trim($('#telefono_mod').val());
    var giro = $.trim($('#giro_mod').val());

    var data = {};

    data.id = id;
    data.nombre = nombre;
    data.rut = rut;
    data.direccion = direccion;
    data.telefono = telefono;
    data.giro = giro;

    var estadoLoop = 0; //Verifica si el ciclo rompe o no

    $.each( data, function( key, value ) {
      if (value=='') {
        $('#'+key).select().focus();
        error("Debe Ingresar "+$('#'+key).attr('placeholder')+'.');
        estadoLoop = 1;
        return false; //Rompe el ciclo each
      }
    });

    if (estadoLoop==1) {
        return false; //Rompe el codigo si uno de los datos del loop esta vacio
    }

    var datos = {};

    datos.modulo = 'empresa';
    datos.metodo = 'modificarEmpresa';
    datos.data = data;

    cargando.load();

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            cargando.load();
            if(resp.estado==1)
            {
                ok("Registro creado satisfactoriamente.");
                $(':input').val('');
                $('#editar_registro').modal('toggle');
            }else{
                error("problema al crear el registro, vuelva a intentarlo.");
            }
            tabla_registros();
        }
    });
}

