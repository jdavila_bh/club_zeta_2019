/*
var estados_ot_color = ['warning','success','danger'];
var estados_ot_text = ['Pendiente','Aceptada','Rechazada'];
*/

$(document).ready(function(){
   //buscar por rut
    $("#btn_rut").click(function(){
        tabla_registros_rut();
    });
    //buscar por nro orden
    $("#btn_orden").click(function(){
        tabla_registros_orden();
    });

    //imrpimir Comprobante
    $("#btn_imp_cmprb").click(function(){
        //printDiv('compra_exitosa');
        imprimir();
    });
    //cerrar ver imprimir comrpobante
    $("#btn_ver_cerrar").click(function(){
        $('#verComprobante').modal('hide');
    });
});



function ver_comprobante(id_registro){
    cargando.load();
    $('#verComprobante').modal();
    var data = {};
    data.id_registro = id_registro;
    var datos = {};

    datos.modulo = 'orden_compra';
    datos.metodo = 'traerRegistroVerComprobante';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){
            cargando.load(0);
            if(resp.estado==1)
            {
                $('#verComprobante').find('#ver_nombre').html(resp.registros[0].nombre_usuario);
                $('#ver_cod_aut').html(resp.registros[0].sid);
                $('#ver_rut').html(resp.registros[0].rut);
                $('#ver_tipo_tran').html("VENTA");
                $('#fecha').html(resp.registros[0].fecha);
                $('#ver_prod').html(resp.registros[0].cantidad);
                $('#ver_nomb_alum').html(resp.registros[0].nalumno);
                $('#ver_precio_uni').html(resp.registros[0].precio_unidad);
                $('#ver_colegio').html(resp.registros[0].nombre_colegio);
                $('#ver_monto_total').html(resp.registros[0].monto);
                $('#nro_orden').html(resp.registros[0].id);
                $('#ver_tipo_pago').html(resp.registros[0].tipo_pago);
                $('#ver_nro_cuotas').html(resp.registros[0].num_cuotas);
                $('#ver_tipo_cuotas').html(resp.registros[0].tipo_cuotas);
                $('#ver_tarjeta').html(resp.registros[0].tarjeta);
                $('#nro_tarjeta').html(resp.registros[0].fin_tarjeta);
                $('#verComprobante').modal('show');
            }
        }
    });
    cargando.load();
}

function tabla_registros_rut()
{
    var f_rut = $('#f_rut').val();
    var data = {};
    $('#tabla_registros').html('');

    if(f_rut!=-1)data.id_registro = f_rut;

    var datos = {};

    datos.modulo = 'orden_compra';
    datos.metodo = 'traerRegistroRut';
    datos.data = data;

    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            cargando.load(0);

            var titulos = ['N° Orden ','Rut','Estado Orden', 'Cantidad','Total','Fecha','Ver Comprobante','Enviar apoderado','Enviar Administrador'];
            var html = '';
            var div_tabla = 'tabla_registros';
            html+= armar_tabla_head(titulos,div_tabla);

            if(resp.estado==1)
            {
                for(var key in resp.registros)
                {
                    html+='<tr>';
                    html+='<td>'+resp.registros[key].id+'</td>';
                    html+='<td>'+resp.registros[key].rut+' </td>';
                    html+='<td>'+resp.registros[key].estado+'</td>';
                    html+='<td>'+resp.registros[key].cantidad+'</td>';
                    html+='<td>'+resp.registros[key].monto+'</td>';
                    html+='<td>'+resp.registros[key].fecha+'</td>';
                    html+='<td><button id="btn_ver" class="btn fa fa-eye" onclick="ver_comprobante(\''+resp.registros[key].id+'\');"/></td>';
                    html+='<td><button id="btn_meil_apod" class="btn fa fa-envelope" onclick="enviar_mail(\''+resp.registros[key].id+'\',\'1\')"> Enviar apoderado</td>';
                    html+='<td><button id="btn_mail_admi" class="btn fa fa-envelope-o"  onclick="enviar_mail(\''+resp.registros[key].id+'\',\'2\')"> Enviar Adminstrador</td>';
                    html+='</tr>';
                }
            }
            $('#tabla_registro').html(html);
            dataTable(div_tabla);
        }
    });
}

function tabla_registros_orden()
{
    var f_rut = $('#f_orden').val();
    var data = {};
    $('#tabla_registros').html('');

    if(f_rut!=-1) data.id_registro = f_rut;

    var datos = {};

    datos.modulo = 'orden_compra';
    datos.metodo = 'traerRegistroIdOrden';
    datos.data = data;

    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){
            cargando.load(0);
            var titulos = ['N° Orden ','Rut','Estado Orden', 'Cantidad','Total','Fecha','Ver Comprobante','Enviar apoderado','Enviar Administrador'];
            var html = '';
            var div_tabla = 'tabla_registros';
            html+= armar_tabla_head(titulos,div_tabla);

            if(resp.estado==1)
            {
                for(var key in resp.registros)
                {
                    html+='<tr>';
                    html+='<td>'+resp.registros[key].id+'</td>';
                    html+='<td>'+resp.registros[key].rut+' </td>';
                    html+='<td>'+resp.registros[key].estado+'</td>';
                    html+='<td>'+resp.registros[key].cantidad+'</td>';
                    html+='<td>'+resp.registros[key].monto+'</td>';
                    html+='<td>'+resp.registros[key].fecha+'</td>';
                    html+='<td><button id="btn_ver" class="btn fa fa-eye" onclick="ver_comprobante(\''+resp.registros[key].id+'\');"/></td>';
                    html+='<td><button id="btn_meil_apod" class="btn fa fa-envelope"> Enviar apoderado</td>';
                    html+='<td><button id="btn_mail_admi" class="btn fa fa-envelope-o"> Enviar Adminstrador</td>';
                    html+='</tr>';
                }
            }
            $('#tabla_registro').html(html);
            dataTable(div_tabla);
        }
    });
}

function imprimir(){
    cargando.load();
    var objeto=document.getElementById('compra_exitosa');  //obtenemos el objeto a imprimir
    var ventana=window.open('','_blank');  //abrimos una ventana vacía nueva
    ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
    setTimeout("",10000);
    ventana.document.close();  //cerramos el documento
    ventana.print();  //imprimimos la ventana
    ventana.close();  //cerramos la ventana
    $('#boton_imprimmir').show();
    $('#boton_volver_comprar').show();
}

/*
 @Tipo: 1=Apoderado
        2=Administrador
 */
function enviar_mail(id_orden, tipo) {
    cargando.load(true);
    var data = {};
    data.id_registro = id_orden;
    data.tipo = tipo;

    var datos = {};

    datos.modulo = 'orden_compra';
    datos.metodo = 'enviarMail';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){
            cargando.load(false);
            if(resp.estado==1) {
                ok(resp.msg);
            }else{
                error(resp.msg);
            }
        }
    });
}

/* jkdfkaljsdhfkjasdhf */

function ver_pdf(num)
{
    var data = {};

    data.pdf_estado = 1;
    data.n_presupuesto = num;

    var datos = {};

    datos.modulo = 'pdf';
    datos.metodo = 'genPdf';
    datos.data = data;


    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp) {

            cargando.load(0);
            $.fancybox({type:'iframe', href:"../pdf_ot/"+num+"_presupuesto.pdf", width: 800, height: 600});

        }
    });
}

function descargar_pdf(num)
{
    var data = {};

    data.pdf_estado = 2;
    data.n_presupuesto = num;

    var datos = {};

    datos.modulo = 'pdf';
    datos.metodo = 'genPdf';
    datos.data = data;


    cargando.load(1);
    $.ajax({
        type: 'POST',
        url: URL_WBS,
        success: function(resp) {
            cargando.load(0);
            downloadURI('../pdf_ot/'+num+'_presupuesto.pdf',num+'_presupuesto.pdf');

        },
        data: datos,
        dataType: 'json'
    });
}
