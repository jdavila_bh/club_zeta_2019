var estados_ot_color = ['warning','success','danger'];
var estados_ot_text = ['Pendiente','Aceptada','Rechazada'];


$(document).ready(function(){
    tabla_registros();
});


function tabla_registros()
{

    var estado = $('#estado_ot').val();

    var data = {};

    if(estado!=-1)
    data.estado = estado;

    var datos = {};

    datos.modulo = 'ot';
    datos.metodo = 'traerTodosOts';
    datos.data = data;

    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            cargando.load(0);

            var color_estados = ['danger','success'];
            var titulos = ['Id','Cliente','Solicitud', 'Solicita','NºFactura','Fecha Ingreso','Ver PDF','Desc. PDF','Cambiar Estado','Modificar'];
            var html = '';
            var div_tabla = 'tabla_registros';
            html+= armar_tabla_head(titulos,div_tabla);

            if(resp.estado==1)
            {

                for(var key in resp.registros)
                {

                html+='<tr>';
                html+='<td>'+resp.registros[key].id+'</td>';
                html+='<td id="cliente_reg_'+resp.registros[key].id+'">'+resp.registros[key].objCliente.nombre+' <span class="label label-info pointer" onclick="ver_detalle_cliente('+resp.registros[key].id_cliente+')"><span class="fa fa-search"></span></span></td>';
                html+='<td id="solicitud_reg_'+resp.registros[key].id+'">'+resp.registros[key].solicitud+'</td>';
                html+='<td id="solicita_reg_'+resp.registros[key].id+'">'+resp.registros[key].solicita+'</td>';

                html+='<td><span id="nfactura_reg_'+resp.registros[key].id+'">'+resp.registros[key].nfactura+'</span> <span class="label label-warning pointer" onclick="cambiar_n_factura('+resp.registros[key].id+')"><span class="fa fa-pencil"></span></span></td>';
                html+='<td>'+resp.registros[key].fecha_ingreso+'</td>';

                html+='<td><button class="btn btn-info menu-icon fa fa-eye boton" onclick="ver_pdf('+resp.registros[key].id+')"></button></td>';
                html+='<td><button class="btn btn-success menu-icon fa fa-download boton" onclick="descargar_pdf('+resp.registros[key].id+')"></button></td>';

                html+='<td align="center"><p class="label label-'+estados_ot_color[resp.registros[key].estado]+'" id="estado_ot_'+resp.registros[key].id+'">'+estados_ot_text[resp.registros[key].estado]+'</p><br><button id="reg_'+resp.registros[key].id+'" class="btn btn-warning menu-icon fa fa fa-list boton" onclick="cambiar_estado('+resp.registros[key].id+','+resp.registros[key].estado+')"></button></td>';
                if(resp.registros[key].estado==0)
                html+='<td><button class="btn btn-info menu-icon fa fa-pencil boton" onclick="editar_registro('+resp.registros[key].id+')"></button></td>';
                else
                html+='<td></td>';
                html+='</tr>';
                }

            }

            $('#tabla_registro').html(html);
            dataTable(div_tabla);
        }
    });

}

function cambiar_n_factura(id_reg)
{
    $('#titulo_box').html('Cambiar Numero Factura');
    var n_factura = $.trim($('#nfactura_reg_'+id_reg).html());

    var html = '';
    html+='<div class="row">';
    html+='<div class="col-md-4" align="center">Nº Factura:</div>';
    html+='<div class="col-md-4" align="center"><input type="text" id="n_factura_n" onkeypress="return justNumbers(event)" value="'+n_factura+'" ></button></div>';
    html+='<div class="col-md-4" ><button class="btn btn-success" onclick="updateFactura('+id_reg+')">Actualizar</button></div>';
    html+='</div>';

    $('#texto_box').html(html);

    $('#myModal').modal('show');
}

function updateFactura(id_reg)
{
    var n_factura = $.trim($('#n_factura_n').val());

    var data = {};

    data.id_registro = id_reg;
    data.nfactura = n_factura;

    var datos = {};

    datos.modulo = 'ot';
    datos.metodo = 'modificarOt';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp) {

        if(resp.estado==1)
        {
            $('#nfactura_reg_'+id_reg).html(n_factura);
            $('#myModal').modal('hide');
        }

        }
    });
}


function cambiar_estado(id_reg)
{
    $('#titulo_box').html('Cambiar Estado Presupuesto');

    var html = '';
    html+='<table class="table table-responsive">';

    html+='<tr>';
    html+='<td align="center">Por Validar:</td>';
    html+='<td align="center">Aceptada:</td>';
    html+='<td align="center">Rechazada:</td>';
    html+='</tr>';
    html+='<tr>';
    html+='<td align="center"><button class="btn btn-warning menu-icon fa fa fa-check boton center-block" onclick="estado_registro('+id_reg+',0)"></button></td>';
    html+='<td align="center"><button class="btn btn-success menu-icon fa fa fa-check boton center-block" onclick="estado_registro('+id_reg+',1)"></button></td>';
    html+='<td align="center"><button class="btn btn-danger menu-icon fa fa fa-check boton center-block" onclick="estado_registro('+id_reg+',2)"></button></td>';
    html+='</tr>';
    html+='</table>';

    $('#texto_box').html(html);

    $('#myModal').modal('show');
}


function ver_detalle_cliente(num)
{
    var data = {};

    data.id_registro = num;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'traerCliente';
    datos.data = data;


    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            cargando.load(0);

            console.log(resp);

            if(resp.estado ==1)
            {
                $('#titulo_box').html('Información Cliente');

                var html = '';
                html+='<table class="table table-responsive">';
                html+='<tr>';
                html+='<td>Nombre Cliente:</td>';
                html+='<td>'+resp.registros.nombre+'</td>';
                html+='</tr>';
                html+='<td>Rut:</td>';
                html+='<td>'+resp.registros.rut+'</td>';
                html+='</tr>';
                html+='<td>Dirección:</td>';
                html+='<td>'+resp.registros.direccion+'</td>';
                html+='</tr>';
                html+='<td>Region:</td>';
                html+='<td>'+resp.registros.regionObj.nombre+'</td>';
                html+='</tr>';
                html+='<td>Ciudad:</td>';
                html+='<td>'+resp.registros.provinciaObj.nombre+'</td>';
                html+='</tr>';
                html+='<td>Comuna:</td>';
                html+='<td>'+resp.registros.comunaObj.nombre+'</td>';
                html+='</tr>';
                html+='<td>Telefono:</td>';
                html+='<td>'+resp.registros.telefono+'</td>';
                html+='</tr>';
                html+='<td>Celular:</td>';
                html+='<td>'+resp.registros.celular+'</td>';
                html+='</tr>';
                html+='<td>Email:</td>';
                html+='<td>'+resp.registros.email+'</td>';
                html+='</tr>';
                html+='<td>Giro:</td>';
                html+='<td>'+resp.registros.giro+'</td>';
                html+='</tr>';
                html+='</table>';

                $('#texto_box').html(html);
                $('#myModal').modal('show');
            }
        }
    });
}

function ver_pdf(num)
{
    var data = {};

    data.pdf_estado = 1;
    data.n_presupuesto = num;

    var datos = {};

    datos.modulo = 'pdf';
    datos.metodo = 'genPdf';
    datos.data = data;


    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp) {

            cargando.load(0);
            $.fancybox({type:'iframe', href:"../pdf_ot/"+num+"_presupuesto.pdf", width: 800, height: 600});

        }
        });
}

function descargar_pdf(num)
{
    var data = {};

    data.pdf_estado = 2;
    data.n_presupuesto = num;

    var datos = {};

    datos.modulo = 'pdf';
    datos.metodo = 'genPdf';
    datos.data = data;


    cargando.load(1);
    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp) {
            cargando.load(0);
            downloadURI('../pdf_ot/'+num+'_presupuesto.pdf',num+'_presupuesto.pdf');

        }
    });
}


function editar_registro(id)
{
    cargando.load();
    $.get("vistas/modificar_ot.php?id_registro="+id+"", function( data ) {
        $("#content-wrapper" ).html( data );
    });
}

function editar_registro_guardar()
{
    var id_registro = $.trim($('#id_registro').html());

    var nombre = $.trim($('#nombre').val());
    var rut = $.trim($('#rut').val());
    var direccion = $.trim($('#direccion').val());

    var region = $.trim($('#region').val());
    var provincia = $.trim($('#provincia').val());
    var comuna = $.trim($('#comuna').val());

    var region_texto = $.trim($('#region option:selected').text());
    var provincia_texto = $.trim($('#provincia option:selected').text());
    var comuna_texto = $.trim($('#comuna option:selected').text());

    var telefono = $.trim($('#telefono').val());
    var celular = $.trim($('#celular').val());

    var email = $.trim($('#email').val());

    var giro = $.trim($('#giro').val());


    if(nombre=='')
    {
        error(txt_tipo_cliente.texto1);
        $('#nombre').select().focus();
        return false;
    }
    if(!Rut(rut,'rut'))
    {
        $('#rut').select().focus();
        return false;
    }
    if(direccion=='')
    {
        error(txt_tipo_cliente.texto2);
        $('#direccion').select().focus();
        return false;
    }


    if(region==0)
    {
        error(txt_tipo_cliente.texto3);
        $('#region').select().focus();
        return false;
    }

    if(provincia==0)
    {
        error(txt_tipo_cliente.texto4);
        $('#provincia').select().focus();
        return false;
    }

    if(comuna==0)
    {
        error(txt_tipo_cliente.texto5);
        $('#comuna').select().focus();
        return false;
    }

    if(telefono=='')
    {
        error(txt_tipo_cliente.texto6);
        $('#telefono').select().focus();
        return false;
    }

    if(celular=='')
    {
        error(txt_tipo_cliente.texto7);
        $('#celular').select().focus();
        return false;
    }

    if(validarEmail(email))
    {
        error(txt_tipo_cliente.texto8);
        $('#email').select().focus();
        return false;
    }

    if(giro=='')
    {
        error(txt_tipo_cliente.texto9);
        $('#giro').select().focus();
        return false;
    }


    var data = {};

    data.id_registro = id_registro;

    data.nombre = nombre;
    data.rut = rut;
    data.direccion = direccion;


    data.id_region = region;
    data.id_provincia = provincia;
    data.id_comuna = comuna;

    data.telefono = telefono;
    data.celular = celular;

    data.email = email;
    data.giro = giro;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'modificarCliente';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){


                if(resp.estado == 1)
                {
                    $('#nombre').val('');
                    $('#rut').val('');
                    $('#direccion').val('');
                    $('#region').val(0);
                    $('#provincia').val(0);
                    $('#comuna').val(0);
                    $('#telefono').val('');
                    $('#celular').val('');
                    $('#email').val('');
                    $('#giro').val('');

                    ok('Registro Modificado');
                    $.fancybox.close();

                    var tag = [
                        "nombre_reg_",
                        "rut_reg_",
                        "direccion_reg_",

                        "region_reg_",
                        "provincia_reg_",
                        "comuna_reg_",

                        "telefono_reg_",
                        "celular_reg_",
                        "email_reg_",
                        "giro_reg_"
                    ];
                    var value = [
                        nombre,
                        rut,
                        direccion,

                        region_texto,
                        provincia_texto,
                        comuna_texto,

                        telefono,
                        celular,
                        email,
                        giro
                    ];

                    actualizarTablaReg(tag,value,id_registro);

                }else{
                    error('Problema al actualizar registro.');
                }

        }
    });
}


function actualizarTablaReg(tag,value,id_registro)
{

    for(var key in tag)
    {
        $('#'+tag[key]+id_registro).html(value[key]);
    }

}


function estado_registro(id,estado)
{

    var data = {};

    data.id_registro = id;
    data.nuevo_estado = estado;

    var datos = {};

    datos.modulo = 'ot';
    datos.metodo = 'cambiarEstado';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                $('#myModal').modal('hide');
                ok('Cambios de estado, exitoso.');

                for(var key in estados_ot_color)
                $('#estado_ot_'+id).removeClass('label-'+estados_ot_color[key]);
                $('#estado_ot_'+id).addClass('label-'+estados_ot_color[estado]);
                $('#estado_ot_'+id).html(estados_ot_text[estado]);
            }else{
                error('Problemas al cambiar el estado, vuelva a intentarlo.');
            }
        }
    });

}