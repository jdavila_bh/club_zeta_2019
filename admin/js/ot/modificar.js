
var clienteBuscar = [];
var clienteSel = {};
var clienteSelEstado = 0;
var valoresPresupuesto = {};

valoresPresupuesto.subtotal=0;
valoresPresupuesto.neto=0;
valoresPresupuesto.descuento=0;
valoresPresupuesto.impuesto=0;
valoresPresupuesto.total=0;

var comentariosPresupuesto = {};

var presupuesto_n = 0;

$(document).ready(function(){
    listaClienteBusqueda();
    //indicador_uf('uf_dia');
    listaFormaPago('formapago');
    listaTipoContrato('tipocontrato');

    calcularValores(); // calcula productos


    /* MODIFCAR OT */
    var id_ot = $('#id_ot').val();
    traerInfoOt(id_ot);
});


function traerInfoOt(id_ot)
{
    var data = {};

    data.id_registro = id_ot;

    var datos = {};

    datos.modulo = 'ot';
    datos.metodo = 'traerOt';

    datos.data = data;

    cargando.load(1);

    $.ajax({
        type:'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            cargando.load(0);
            if(resp.estado==1)
            {

                // Llenar Info Cliente
                presupuesto_n = id_ot;
                traerCliente2(resp.registros.id_cliente);
                traerDetalleOt(id_ot);
                traerComentarioOt(id_ot);

                $('#solicitud').val(resp.registros.solicitud);
                $('#solicitadopor').val(resp.registros.solicita);

                $('#comentario').val(resp.registros.comentario);
                $('#nota').val(resp.registros.nota);

                $('#formapago').val(resp.registros.id_tipo_pago);
                $('#uf_dia').val(resp.registros.uf_dia);
                $('#valorhora').val(resp.registros.valor_hora);

                $('#tipocontrato').val(resp.registros.id_tipo_contrato);

                $('#nfactura').val(resp.registros.nfactura);
                $('#timingdia').val(resp.registros.timming_dias);
                $('#timinghora').val(resp.registros.hora_proyecto);



            }else{
                error('Problema al actualizar el Presupuesto, vuelva a intentarlo.');
            }
        }

    });
}

function traerDetalleOt(id_registro)
{

    var data = {};

    data.id_ot = id_registro;
    data.estado = 1;

    var datos = {};

    datos.modulo = 'detalle_ot';
    datos.metodo = 'traerTodosDetalleOt';

    datos.data = data;

    $.ajax({
        type:'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {

            for(var key in resp.registros)
            {
                agregarItem2(resp.registros[key].cantidad,resp.registros[key].descripcion,resp.registros[key].cant_uf,resp.registros[key].unitario,resp.registros[key].descuento);
            }


            }else{
                ok('No se encontraron detalles para cargar.');
            }
        }

    });
}

function traerComentarioOt(id_registro)
{
    var data = {};

    data.id_ot = id_registro;
    data.estado = 1;
    data.orden_registros = 'orden_comentario_ot ASC';

    var datos = {};

    datos.modulo = 'comentario_ot';
    datos.metodo = 'traerTodosComentarioOt';

    datos.data = data;

    $.ajax({
        type:'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {

                for(var key in resp.registros)
                {
                    agregarComentario2(resp.registros[key].titulo,resp.registros[key].detalle);
                }


            }else{
                ok('No se encontraron comentarios para cargar.');
            }
        }

    });
}


function validarPaso1()
{
    var solicitud = $.trim($('#solicitud').val());
    var solicitadopor = $.trim($('#solicitadopor').val());
    var comentario = $.trim($('#comentario').val());
    var nota = $.trim($('#nota').val());
    var cliente = clienteSel;


    if(clienteSelEstado==0)
    {
        error('Debe buscar un cliente y seleccionarlo.');
        $('#nombre').select().focus();
        return false;
    }

    if(solicitud=='')
    {
        error('Debe ingresar la Solicitud.');
        $('#solicitud').select().focus();
        return false;
    }

    if(solicitadopor=='')
    {
        error('Debe ingresar quien solicita.');
        $('#solicitadopor').select().focus();
        return false;
    }

    if(comentario=='')
    {
        error('Debe ingresar el comentario.');
        $('#comentario').select().focus();
        return false;
    }

    if(nota=='')
    {
        error('Debe ingresar la nota.');
        $('#nota').select().focus();
        return false;
    }
}

function paso1()
{
    var solicitud = $.trim($('#solicitud').val());
    var solicitadopor = $.trim($('#solicitadopor').val());
    var comentario = $.trim($('#comentario').val());
    var nota = $.trim($('#nota').val());
    var cliente = clienteSel;
    //clienteSelEstado -> variable que almacena si se ha seleccionado un cliente.



    if(clienteSelEstado==0)
    {
        error('Debe buscar un cliente y seleccionarlo.');
        $('#nombre').select().focus();
        return false;
    }

    if(solicitud=='')
    {
        error('Debe ingresar la Solicitud.');
        $('#solicitud').select().focus();
        return false;
    }

    if(solicitadopor=='')
    {
        error('Debe ingresar quien solicita.');
        $('#solicitadopor').select().focus();
        return false;
    }

    if(comentario=='')
    {
        error('Debe ingresar el comentario.');
        $('#comentario').select().focus();
        return false;
    }

    if(nota=='')
    {
        error('Debe ingresar la nota.');
        $('#nota').select().focus();
        return false;
    }

    if(presupuesto_n!=0)
    {
        $('#titulo_box').html('Presupuesto Generado');
        $('#texto_box').html('<p>Existe un presupuesto abierto, necesita generar uno nuevo.</p>');

        var html = '';
        html+='<button type="button" class="btn btn-success" onclick="ingresarOt()">Nuevo Presupuesto</button>';
        html+='<button type="button" class="btn btn-danger" onclick="updateClienteOt()" data-dismiss="modal">Mantener Presupuesto</button>';
        $('#footer_box').html(html);
        $('#myModal').modal('show');
    }else{
        ingresarOt();
    }


}

function updateClienteOt()
{
    // DATOS VALORES
    var id_cliente = clienteSel.id;
    var solicitud = $('#solicitud').val();
    var solicita = $('#solicitadopor').val();
    var comentario = $('#comentario').val();
    var nota = $('#nota').val();

    var data = {};

    data.id_registro = presupuesto_n;

    data.id_cliente = id_cliente;
    data.solicitud = solicitud;
    data.solicita = solicita;
    data.comentario = comentario;
    data.nota = nota;

    var datos = {};

    datos.modulo = 'ot';
    datos.metodo = 'modificarOt';

    datos.data = data;

    $.ajax({
        type:'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                ok('Presupuesto actualizado.');

            }else{
                error('Problema al actualizar el Presupuesto, vuelva a intentarlo.');
            }
        }

    });
}

function updateOt()
{
    // DATOS VALORES
    var id_tipo_pago = $('#formapago').val();
    var nfactura = $('#nfactura').val();
    var uf_dia = $('#uf_dia').val();
    var id_tipo_contrato = $('#tipocontrato').val();
    var timingdia = $('#timingdia').val();
    var valorhora = $('#valorhora').val();
    var timinghora = $('#timinghora').val();

    var data = {};

    data.id_registro = presupuesto_n;

    data.id_tipo_pago = id_tipo_pago;
    data.nfactura = nfactura;
    data.uf_dia = uf_dia;
    data.id_tipo_contrato = id_tipo_contrato;
    data.timing_dia = timingdia;
    data.valor_hora = valorhora;
    data.timing_hora = timinghora;

    var datos = {};

    datos.modulo = 'ot';
    datos.metodo = 'modificarOt';

    datos.data = data;

    $.ajax({
        type:'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                ok('Presupuesto actualizado.');

            }else{
                error('Problema al actualizar el Presupuesto, vuelva a intentarlo.');
            }
        }

    });

}

function guardarDetalle()
{
    var pro_cantidad = [];
    var pro_descripcion = [];
    var pro_cant_uf = [];
    var pro_unitario = [];
    var pro_descuento = [];
    var pro_neto = [];
    var pro_orden = [];

    var cantidad_pro = $('.cantidad').length;

    for(var i=0;i<cantidad_pro;i++)
    {
        var orden_detalle = $('.detalle_item').eq(i).attr('id').split('_');

        var cantidad = parseInt($('.cantidad').eq(i).val());
        var descripcion = $('.descripcion').eq(i).val();
        var cant_uf = parseInt($('.cant_uf').eq(i).val());
        var unitario = parseInt($('.unitario').eq(i).val());
        var descuento = parseInt($('.descuento').eq(i).val());
        var neto = parseInt($('.neto').eq(i).val());


        pro_cantidad.push(cantidad);
        pro_descripcion.push(descripcion);
        pro_cant_uf.push(cant_uf);
        pro_unitario.push(unitario);
        pro_descuento.push(descuento);
        pro_neto.push(neto);
        pro_orden.push(parseInt(orden_detalle[1]));

    }

    var data = {};

    data.id_registro = presupuesto_n;
    data.cantidad = pro_cantidad;
    data.descripcion = pro_descripcion;
    data.cant_uf = pro_cant_uf;
    data.unitario = pro_unitario;
    data.descuento = pro_descuento;
    data.neto = pro_neto;
    data.orden = pro_orden;

    var datos = {};

    datos.modulo = 'detalle_ot';
    datos.metodo = 'registrarDetalleOt';

    datos.data = data;
    $.ajax({
        type:'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                ok('Detalle del Presupuesto actualizado.');

            }else{
                error('Problema al actualizar el Presupuesto, vuelva a intentarlo.');
            }
        }

    });


}

function guardarComentario()
{

    var comentario_titulo = [];
    var comentario_descripcion = [];
    var comentario_orden = [];

    var cantidad_comentario = $('.titulo_comentario').length;

    for(var i=0;i<cantidad_comentario;i++)
    {
        var orden_detalle = $('.comentario_presupuesto').eq(i).attr('id').split('_');

        var titulo_comentario = $('.titulo_comentario').eq(i).val();
        var detalle_comentario = $('.detalle_comentario').eq(i).val();



        comentario_titulo.push(titulo_comentario);
        comentario_descripcion.push(detalle_comentario);
        comentario_orden.push(parseInt(orden_detalle[1]));

    }

    var data = {};

    data.id_registro = presupuesto_n;
    data.titulo = comentario_titulo;
    data.detalle = comentario_descripcion;
    data.orden = comentario_orden;

    var datos = {};

    datos.modulo = 'comentario_ot';
    datos.metodo = 'registrarComentarioOt';

    datos.data = data;
    $.ajax({
        type:'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                ok('Presupuesto actualizado.');

                vista('ver_ot');

            }else{
                error('Problema al actualizar el Presupuesto, vuelva a intentarlo.');
            }
        }

    });
}


function ingresarOt()
{
    var solicitud = $.trim($('#solicitud').val());
    var solicitadopor = $.trim($('#solicitadopor').val());
    var comentario = $.trim($('#comentario').val());
    var nota = $.trim($('#nota').val());
    var cliente = clienteSel;

    var data = {};

    data.id_cliente = clienteSel.id;
    data.solicitud = solicitud;
    data.solicita = solicitadopor;
    data.comentario = comentario;
    data.nota = nota;


    var datos = {};

    datos.modulo = 'ot';
    datos.metodo = 'registrarOt';

    datos.data = data;

    $.ajax({
        type:'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                ok('Presupuesto Generado.');
                presupuesto_n = resp.id_ot;
                $('#n_presupuesto').html('- PRESUPUESTO Nº: '+presupuesto_n);
                $('#myModal').modal('hide');
            }else{
                error('Problema al generar el Presupuesto, vuelva a intentarlo.');
            }
        }

    });
}

function listaClienteBusqueda()
{

    var data = {};
    data.estado = 1;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'traerTodosClientes';
    datos.data =  data;


    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){


            if(resp.estado==1)
            {

                for(var key in resp.registros)
                {
                clienteBuscar.push({obj:resp.registros[key],label:'['+resp.registros[key].rut+'] '+resp.registros[key].nombre});
                }

                $( "#nombre" ).autocomplete({
                    source: clienteBuscar,
                    select: function( event, ui ) {
                        //console.log('select');
                        //console.log(ui);
                        traerCliente(ui);
                    }
                });
            }

        }
    });

}



function traerCliente(registro)
{
    var data = {};

    data.id_registro = registro.item.obj.id;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'traerCliente';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url:URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){
            console.log('Datos cliente');

            if(resp.estado==1)
            {

            clienteSelEstado = resp.estado;
            clienteSel = resp.registros;

            $('#nombre_cli').html(resp.registros.nombre);
            $('#rut_cli').html(resp.registros.rut);
            $('#direccion_cli').html(resp.registros.direccion);
            $('#region_cli').html(resp.registros.regionObj.nombre);
            $('#ciudad_cli').html(resp.registros.provinciaObj.nombre);
            $('#comuna_cli').html(resp.registros.comunaObj.nombre);
            $('#telefono_cli').html(resp.registros.telefono);
            $('#celular_cli').html(resp.registros.celular);
            $('#giro_cli').html(resp.registros.giro);
            }
        }

    });

}

function traerCliente2(registro)
{
    var data = {};

    data.id_registro = registro;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'traerCliente';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url:URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){
            console.log('Datos cliente');

            if(resp.estado==1)
            {

                clienteSelEstado = resp.estado;
                clienteSel = resp.registros;

                $('#nombre_cli').html(resp.registros.nombre);
                $('#rut_cli').html(resp.registros.rut);
                $('#direccion_cli').html(resp.registros.direccion);
                $('#region_cli').html(resp.registros.regionObj.nombre);
                $('#ciudad_cli').html(resp.registros.provinciaObj.nombre);
                $('#comuna_cli').html(resp.registros.comunaObj.nombre);
                $('#telefono_cli').html(resp.registros.telefono);
                $('#celular_cli').html(resp.registros.celular);
                $('#giro_cli').html(resp.registros.giro);
            }
        }

    });

}


function menu_presupuesto(num)
{

    switch(num)
    {
        case 1:



            $('.paso1').removeClass('paso-top').removeClass('paso-bottom').addClass('paso-top');
            $('.paso2').removeClass('paso-top').removeClass('paso-bottom').addClass('paso-bottom');
            $('.paso3').removeClass('paso-top').removeClass('paso-bottom').addClass('paso-bottom');
            $('.paso-flecha').removeClass('paso-top').removeClass('paso-bottom');
            $('.paso-flecha').eq(0).addClass('paso-top');
            $('.paso-flecha').eq(1).addClass('paso-bottom');
            $('.paso1_datos').slideDown();
            $('.paso2_datos').slideUp();
            $('.paso3_datos').slideUp();

        break;

        case 2:

            if(paso1()==false) // Validacion de Campos
                return false;

            $('.paso1').removeClass('paso-top').removeClass('paso-bottom').addClass('paso-top');
            $('.paso2').removeClass('paso-top').removeClass('paso-bottom').addClass('paso-top');
            $('.paso3').removeClass('paso-top').removeClass('paso-bottom').addClass('paso-bottom');
            $('.paso-flecha').removeClass('paso-top').removeClass('paso-bottom');
            $('.paso-flecha').addClass('paso-top');
            $('.paso1_datos').slideUp();
            $('.paso2_datos').slideDown();
            $('.paso3_datos').slideUp();

        break;

        case 3:

            if(validarPaso1()==false) // Validacion de Campos si salta del paso1 al 3
            {
                return false;
            }else {
                updateOt();
                guardarDetalle();
            }



            $('.paso1').removeClass('paso-top').removeClass('paso-bottom').addClass('paso-top');
            $('.paso2').removeClass('paso-top').removeClass('paso-bottom').addClass('paso-top');
            $('.paso3').removeClass('paso-top').removeClass('paso-bottom').addClass('paso-top');
            $('.paso-flecha').removeClass('paso-top').removeClass('paso-bottom');
            $('.paso-flecha').addClass('paso-top');
            $('.paso1_datos').slideUp();
            $('.paso2_datos').slideUp();
            $('.paso3_datos').slideDown();

        break;
    }

}


function calcularHoras()
{
    var timmingdia = $('#timingdia').val();
    var timminghora = 0;

    if(isNaN(timmingdia))
    {
        timminghora = 0;
    }else{
        timminghora = timmingdia*24;
    }
    $('#timinghora').val(timminghora);
}


/* DETALLE PRESUPUESTO */


function agregarItem()
{
    var cantidad = $('.detalle_item').length;
    var estado = 0;


    do{
        cantidad++;

        if($('#row_'+cantidad).length==0)
        estado = 1;
        else
        estado = 0;

    }while(estado==0);

    var html = '';

    html+='<tr class="detalle_item" id="row_'+(cantidad)+'">';
    html+='<td><input type="text" class="cantidad form-control" onkeyup="calcularValores()" onkeypress="return justNumbers(event);"></td>';
    html+='<td><textarea class="descripcion form-control"></textarea> </td>';
    html+='<td><input type="text" class="cant_uf form-control" onkeyup="calcularValores()"  onkeypress="return justNumbers(event);"></td>';
    html+='<td><input type="text" class="unitario form-control" onkeyup="calcularValores()" onkeypress="return justNumbers(event);"></td>';
    html+='<td><input type="text" class="descuento form-control" onkeyup="calcularValores()" onkeypress="return justNumbers(event);"></td>';
    html+='<td><input type="text" class="neto form-control" onkeypress="return justNumbers(event);"></td>';
    html+='<td><div class="btn btn-danger" onclick="removeRow('+(cantidad)+')">X</div></td>';
    html+='</tr>';

    $('.detalle_item:last').closest('tr').after(html);

    calcularValores();
}
var estadoagregarItem2 = 0;
function agregarItem2(cantidad_pro,descripcion,cant_uf,unitario,descuento)
{
    var cantidad = $('.detalle_item').length;
    var estado = 0;


    do{
        cantidad++;

        if($('#row_'+cantidad).length==0)
            estado = 1;
        else
            estado = 0;

    }while(estado==0);



    if(estadoagregarItem2==0)
    {
        $('.cantidad').eq(0).val(cantidad_pro);
        $('.descripcion').eq(0).html(descripcion);
        $('.cant_uf').eq(0).val(cant_uf);
        $('.unitario').eq(0).val(unitario);
        $('.descuento').eq(0).val(descuento);

        estadoagregarItem2++;

    }else{
        var html = '';

        html+='<tr class="detalle_item" id="row_'+(cantidad)+'">';
        html+='<td><input type="text" class="cantidad form-control" value="'+cantidad_pro+'" onkeyup="calcularValores()" onkeypress="return justNumbers(event);"></td>';
        html+='<td><textarea class="descripcion form-control">'+descripcion+'</textarea> </td>';
        html+='<td><input type="text" class="cant_uf form-control" value="'+cant_uf+'" onkeyup="calcularValores()"  onkeypress="return justNumbers(event);"></td>';
        html+='<td><input type="text" class="unitario form-control" value="'+unitario+'" onkeyup="calcularValores()" onkeypress="return justNumbers(event);"></td>';
        html+='<td><input type="text" class="descuento form-control" value="'+descuento+'" onkeyup="calcularValores()" onkeypress="return justNumbers(event);"></td>';
        html+='<td><input type="text" class="neto form-control" onkeypress="return justNumbers(event);"></td>';
        html+='<td><div class="btn btn-danger" onclick="removeRow('+(cantidad)+')">X</div></td>';
        html+='</tr>';

        $('.detalle_item:last').closest('tr').after(html);
    }

    calcularValores();
}

function removeRow(num)
{
    $('#row_'+num).remove();
    calcularValores();
}

function calcularValores()
{

    var valorFinal = 0;
    var valorDescuentoFinal = 0;
    var valorUfDia = $('#uf_dia').val();
    var largo = $('.detalle_item').length;

    for(var i=0; i<largo; i++)
    {

        /* CANTIDAD */
        var valor1 = parseInt($('.cantidad').eq(i).val());

        if(isNaN(valor1))
        {
            valor1 = 1;
            $('.cantidad').eq(i).val(valor1);
        }

        /* FIN - CANTIDAD */


        /* CANTIDAD UF */
        var valor2 = parseInt($('.cant_uf').eq(i).val());

        if(isNaN(valor2))
        {
            valor2 = 0;
            $('.cant_uf').eq(i).val(valor2);
        }

        /* FIN - CANTIDAD UF */


        /* VALOR UNITARIO */
        var valor3 = parseInt($('.unitario').eq(i).val());

        if(isNaN(valor3))
        {
            if(valor2==0)
            valor3 = 1;
            else
            valor3 = 1*valorUfDia;
            $('.unitario').eq(i).val(valor3);
        }else{
            if(valor2!=0)
            valor3 = 1*valorUfDia;
            $('.unitario').eq(i).val(valor3);
        }
        /* FIN - VALOR UNITARIO */

        /* DESCUENTO */

        var valor5 = parseInt($('.descuento').eq(i).val());

        if(isNaN(valor5))
        {
            valor5 = 0;
            $('.descuento').eq(i).val(valor5);
        }

        /* FIN - DESCUENTO */

        /* VALOR NETO */
        var valor4 = parseInt($('.neto').eq(i).val());
        var valorDescuento = 0; // DESCUENTO

        if(isNaN(valor4))
        {
            if(valor2==0)
                valor4 = (valor1*valor3);
            else
                valor4 = (valor1*valor2)*valorUfDia;

            valorDescuento = Math.round((valor4*valor5)/100); // DESCUENTO
            $('.neto').eq(i).val(valor4-valorDescuento);
        }else{
            if(valor2==0)
                valor4 = (valor1*valor3);
            else
                valor4 = (valor1*valor2)*valorUfDia;

            valorDescuento = Math.round((valor4*valor5)/100); // DESCUENTO
            $('.neto').eq(i).val(valor4-valorDescuento);
        }
        /* FIN - VALOR UNITARIO */

        valorFinal+=valor4;
        valorDescuentoFinal+=valorDescuento;

    }


    valoresPresupuesto.subtotal=valorFinal;
    valoresPresupuesto.descuento=valorDescuentoFinal;
    valoresPresupuesto.neto=valorFinal-valorDescuentoFinal;
    valoresPresupuesto.impuesto=(valorFinal-valorDescuentoFinal)*19/100;
    valoresPresupuesto.total=valoresPresupuesto.impuesto+valoresPresupuesto.neto;

    totalesPresupuesto(valoresPresupuesto);
}


function totalesPresupuesto(valores)
{
    $('#subtotal_final').html(formato_miles(valores.subtotal.toFixed(0),1));
    $('#neto_final').html(formato_miles(valores.neto.toFixed(0),1));
    $('#descuento_final').html(formato_miles(valores.descuento.toFixed(0),1));
    $('#impuesto_final').html(formato_miles(valores.impuesto.toFixed(0),1));
    $('#total_final').html(formato_miles(valores.total.toFixed(0),1));
}

/*
function formato_miles(num,iva){
    var num = parseInt(num);
    if(isNaN(num))
    { return 0;}
    else
    {
        var cadena = ""; var aux;
        var cont = 1,m,k;
        if(num<0)
        {aux=1;}
        else
        {aux=0;}

        num=num.toString();

        for(m=num.length-1; m>=0; m--)
        {
            cadena = num.charAt(m) + cadena;

            if(cont%3 == 0 && m >aux)
            {cadena = "." + cadena;}
            else
            {cadena = cadena;}

            if(cont== 3)
            {cont = 1;}
            else
            {cont++;}

        }

        cadena = cadena.replace(/.,/,",");
        var resultado = '';
        if(iva){
            //resultado = cadena+' + IVA';
            resultado = cadena;
        }else{
            resultado = cadena;
        }
        return resultado;
    }

}
*/

/* COMENTARIOS */

function agregarComentario()
{
    var largo = $('.titulo_comentario').length;
    var estado = 0;

    if(largo==0)
        largo=1;

    do{
        if($('#comentario_'+largo).length==0) {
            estado = 1;
        }else{
            estado = 0;
            largo++;
        }

    }while(estado==0);

    var html = '';

    html+='<div class="row comentario_presupuesto" id="comentario_'+largo+'">';
    html+='<div class="col-md-12 form-group">';
    html+='<div class="col-md-3">Titulo</div>';
    html+='<div class="col-md-8"><input type="text" class="form-control titulo_comentario" ></div>';
    html+='<div class="col-md-1"><div class="btn btn-danger pull-right" onclick="removeComentario('+largo+')">X</div></div>';
    html+='</div>';
    html+='<div class="col-md-12 form-group">';
    html+='<div class="col-md-3">Detalle Comentario</div>';
    html+='<div class="col-md-9"><textarea class="form-control detalle_comentario"></textarea></div>';
    html+='</div>';
    html+='</div>';
    html+='<hr class="linea_comentario_'+largo+'">';

    $('#contenedor_comentario_presupuesto').append(html);
}


function agregarComentario2(titulo,detalle)
{
    var largo = $('.titulo_comentario').length;
    var estado = 0;

    if(largo==0)
        largo=1;

    do{
        if($('#comentario_'+largo).length==0) {
            estado = 1;
        }else{
            estado = 0;
            largo++;
        }

    }while(estado==0);

    if(largo)
    {
        var html = '';

        html+='<div class="row comentario_presupuesto" id="comentario_'+largo+'">';
        html+='<div class="col-md-12 form-group">';
        html+='<div class="col-md-3">Titulo</div>';
        html+='<div class="col-md-8"><input type="text" class="form-control titulo_comentario" value="'+titulo+'" ></div>';
        html+='<div class="col-md-1"><div class="btn btn-danger pull-right" onclick="removeComentario('+largo+')">X</div></div>';
        html+='</div>';
        html+='<div class="col-md-12 form-group">';
        html+='<div class="col-md-3">Detalle Comentario</div>';
        html+='<div class="col-md-9"><textarea class="form-control detalle_comentario">'+detalle+'</textarea></div>';
        html+='</div>';
        html+='</div>';
        html+='<hr class="linea_comentario_'+largo+'">';
    }else{
        $('.titulo_comentario').eq(0).val(titulo);
        $('.detalle_comentario').eq(0).html(detalle);
    }



    $('#contenedor_comentario_presupuesto').append(html);
}


function removeComentario(num)
{
    $('#comentario_'+num).remove();
    $('.linea_comentario_'+num).remove();
}


/* AGREGAR CLIENTE */

function ver_agregar_cliente()
{
    ver_div('#nuevo_cliente');
}


$(document).ready(function(){
    listaRegiones('region_cliente',0);
    //listaProvincias('provincia',0);
    //listaComunas('comuna',0);
});


function traerCiudades()
{
    var datos = {};

    datos.estado = 1;
    datos.id_region = $('#region_cliente').val();
    datos.id_provincia = 0;
    listaProvincias('provincia_cliente',0,datos);
    listaComunas('comuna_cliente',0,datos);
}

function traerComunas()
{
    var datos = {};

    datos.estado = 1;
    datos.id_provincia = $('#provincia_cliente').val();
    listaComunas('comuna_cliente',0,datos);
}



function registrar()
{

    var nombre = $.trim($('#nombre_cliente').val());
    var rut = $.trim($('#rut_cliente').val());
    var direccion = $.trim($('#direccion_cliente').val());

    var region = $.trim($('#region_cliente').val());
    var provincia = $.trim($('#provincia_cliente').val());
    var comuna = $.trim($('#comuna_cliente').val());

    var telefono = $.trim($('#telefono_cliente').val());
    var celular = $.trim($('#celular_cliente').val());

    var email = $.trim($('#email_cliente').val());

    var giro = $.trim($('#giro_cliente').val());


    if(nombre=='')
    {
        error(txt_tipo_cliente.texto1);
        $('#nombre_cliente').select().focus();
        return false;
    }
    if(!Rut(rut,'rut'))
    {
        $('#rut_cliente').select().focus();
        return false;
    }
    if(direccion=='')
    {
        error(txt_tipo_cliente.texto2);
        $('#direccion_cliente').select().focus();
        return false;
    }


    if(region==0)
    {
        error(txt_tipo_cliente.texto3);
        $('#region_cliente').select().focus();
        return false;
    }

    if(provincia==0)
    {
        error(txt_tipo_cliente.texto4);
        $('#provincia_cliente').select().focus();
        return false;
    }

    if(comuna==0)
    {
        error(txt_tipo_cliente.texto5);
        $('#comuna_cliente').select().focus();
        return false;
    }

    if(telefono=='')
    {
        error(txt_tipo_cliente.texto6);
        $('#telefono_cliente').select().focus();
        return false;
    }

    if(celular=='')
    {
        error(txt_tipo_cliente.texto7);
        $('#celular').select().focus();
        return false;
    }

    if(validarEmail(email))
    {
        error(txt_tipo_cliente.texto8);
        $('#email_cliente').select().focus();
        return false;
    }

    if(giro=='')
    {
        error(txt_tipo_cliente.texto9);
        $('#giro_cliente').select().focus();
        return false;
    }


    var data = {};

    data.nombre = nombre;
    data.rut = rut;
    data.direccion = direccion;


    data.id_region = region;
    data.id_provincia = provincia;
    data.id_comuna = comuna;

    data.telefono = telefono;
    data.celular = celular;

    data.email = email;
    data.giro = giro;

    var datos = {};

    datos.modulo = 'cliente';
    datos.metodo = 'registrarCliente';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            if(resp.estado == 1)
            {
                $('#nombre_cliente').val('');
                $('#rut_cliente').val('');
                $('#direccion_cliente').val('');

                $('#region_cliente').val(0);
                $('#provincia_cliente').val(0);
                $('#comuna_cliente').val(0);

                $('#telefono_cliente').val('');
                $('#celular_cliente').val('');

                $('#email_cliente').val('');

                $('#giro_cliente').val('');

                ok('Cliente Registrado');
                $.fancybox.close();
                listaClienteBusqueda();

            }else{
                error('Problemas al registrar el cliente.');
            }

        }
    });


}

function ver_pdf()
{
    var data = {};

    data.pdf_estado = 1;
    data.n_presupuesto = presupuesto_n;

    var datos = {};

    datos.modulo = 'pdf';
    datos.metodo = 'genPdf';
    datos.data = data;


    cargando.load(1);

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp) {

            cargando.load(0);
            $.fancybox({type:'iframe', href:"../pdf_ot/"+presupuesto_n+"_presupuesto.pdf", width: 800, height: 600});

        }
        });
}
