

function listaTipoUsuarios(div,id)
{

    var datos = {};

    datos.modulo = 'tipo_usuario';
    datos.metodo = 'traerTodosTipoUsuarios';

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            $('#'+div).html('<option value="0">'+txt_tipo_usuario.texto1+'</option>');

            if(resp.estado == 1)
            {
                for(var key in resp.tipo_usuarios)
                    $('#'+div).append('<option value="'+resp.tipo_usuarios[key].id+'">'+resp.tipo_usuarios[key].nombre+'</option>');

                if(id>0)
                {
                    $('#'+div).val(id);
                }

            }



        }
    });

}
