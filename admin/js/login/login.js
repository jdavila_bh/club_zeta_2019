var login = {

    /* DATOS FORM - INICIAR SESION */
    modulo:'wbs/wbs.php',
    metodo:'POST', // POST - GET
    vista_link:'sistema/home.php',
    /* DATOS FORM - RECUPERAR PASS */
    modulo2:'lib/modulos/recuperar_clave.php',
    metodo2:'POST', // POST - GET    

    /* ESTRUCTURA HTML */
    div_usuario:'usuario',
    div_password:'pass',
    div_enter:'boton_ingresar',
    div_recuperar_pass:'recuperar_contrasena',

    /* MENSAJES */
    nombre_input_usuario:'usuario',
    nombre_input_pass:'contraseña',

    titulo_mensaje_error:'Faltan Datos',
    mensaje_error:'Debe ingresar el dato ',

    titulo_mensaje_error2:'Usuario / Contraseña erronea',
    mensaje_error2:'intentelo nuevamente.',

    titulo_exito:'Login Usuario',
    mensaje_exito:'usuario encontrado espere un momento',

    titulo_recuperar_clave: 'Ingrese su email.',
    titulo_validar_email:'Error en mail',
    mensaje_validar_email:'Debe ingresar un correo valido.',


    /* DATOS */
    estado_val:false,


    dato_usuario:'',
    dato_pass:'',
    dato_email:'',

    /* FUNCIONES */

    validar_datos:function(){

        if($.trim($('#'+this.div_usuario).val())=='')
        {
            mensaje.mensaje(this.titulo_mensaje_error,this.mensaje_error+this.nombre_input_usuario,2);
            $('#'+this.div_usuario).focus();
            return false;
        }else if($.trim($('#'+this.div_password).val())==''){
            mensaje.mensaje(this.titulo_mensaje_error,this.mensaje_error+this.nombre_input_pass,2);
            $('#'+this.div_password).focus();
            return false;
        }
        this.dato_usuario = $.trim($('#'+this.div_usuario).val());
        this.dato_pass = $.trim($('#'+this.div_password).val());

        return true;
    },
    iniciar_sesion:function(){
        this.estado_val = this.validar_datos();
        if(this.estado_val==true)
        {
            this.ingresar_datos();
        }
    },
    ingresar_datos:function(){

        var data = {};

        data.user = this.dato_usuario;
        data.pass = this.dato_pass;

        var datos = {};
        datos.modulo = 'usuario_admin';
        datos.metodo = 'verificarUsuario';
        datos.data = data;

        cargando.load();
        $.ajax({
            type: this.metodo,
            url: this.modulo,
            data: datos,
            dataType: 'json',
            success: function(resp) {
                //console.log(a);
                if(resp.estado==1){
                    window.location.href = login.vista_link;
                    mensaje.mensaje(login.titulo_exito,login.mensaje_exito,1);
                }
                else
                {
                    mensaje.mensaje(login.titulo_mensaje_error2,login.mensaje_error2,0);
                }
                cargando.load();
            }
        })


    },
    init_login:function(){
        $(document).ready(function(){
            $('#'+login.div_usuario).focus();
            $('#'+login.div_enter).attr('onclick','login.iniciar_sesion()');
            $('#'+login.div_recuperar_pass).attr('onclick','login.recuperar_clave()');

            /* VALIDAMOS ENTER CUANDO ESTE EL MENSAJE */
            window.onkeydown = function(e){
                if(event.which==13) // 13 = ENTER
                {
                    login.estado_val = login.validar_datos();
                    if(login.estado_val==true)
                    {
                        login.ingresar_datos();
                    }
                }
            }
            /* FIN -  VALIDAMOS ENTER CUANDO ESTE EL MENSAJE */

        });

    },
    recuperar_clave:function(){

        this.dato_email = prompt(this.titulo_recuperar_clave);

        var emailReg = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
        if( !emailReg.test( this.dato_email ) ) {
            mensaje.mensaje(this.titulo_validar_email,this.mensaje_validar_email,0);
            return false;
        }

        cargando.load();
        $.ajax({
            type: this.metodo2,
            url: this.modulo2,
            data: 'tipo=recuperar_mail&email='+this.dato_email,
            success: function(a) {
                //console.log(a);
                if(a==1){
                    //top.location.href = this.vista_link;
                    mensaje.mensaje(this.titulo_exito,this.mensaje_exito,1);
                }
                else
                {
                    mensaje.mensaje(this.titulo_mensaje_error2,this.titulo_mensaje_error2,0);
                }
                cargando.load();

            }
        })


    },
    getDatosUsuario: function(){

        $.ajax({
            type: this.metodo,
            url: this.modulo,
            dataType: "json",
            data: 'idfuncion=2',
            success: function(a) {

                $('.usuario_logeado_nombre').html(a.user);

            }
        })

    }


}

login.init_login();