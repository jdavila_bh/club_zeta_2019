
var mensaje = {
    div:'<div id="mensaje"></div>',
    posicion: 1 ,
    mensaje_int:"",
    txt_mensaje: "",
    estado_mensaje:["#FF5942","#FF9900","#84C000"],
    estado: "",
    
    mensaje:function(texto,estado){
        this.txt_mensaje = texto;
        this.estado= this.estado_mensaje[estado];
        this.mostrar_mensaje();
    },
    
    mostrar_mensaje:function(){   
        
        $('#mensaje').css("color",this.estado);
        $('#mensaje').html(this.txt_mensaje);
        if($('#mensaje').css('display')=='none')
        $('#mensaje').slideDown();
        if(this.mensaje_int)
        clearTimeout(this.mensaje_int);
        
        this.mensaje_int = setTimeout(function(){
            $('#mensaje').slideUp();
        },3000);
    },
    fijar_posicion:function(pos){
        
        switch(pos){
        case 1:
            $('#mensaje').css('top',0);
        break;
        case 2:
            $('#mensaje').css('top','50%');
        break;
        case 3:
            $('#mensaje').css('bottom',0);
        break;
                
        }
        
    },
    
    evento:function(){
        $('#mensaje').bind('click',function(){ $('#mensaje').slideUp();});               
    },
    
    init:function(){
         $(document).ready(function(){  
            $("body").append(mensaje.div);   
            mensaje.evento();
            mensaje.fijar_posicion(mensaje.posicion);
         });  
    }
    
};

// instanciamos el obj
mensaje.posicion=3;
mensaje.init();



  
    
