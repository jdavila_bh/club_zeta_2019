
var mensaje = {
    div:'<div id="contenedor_mensaje"><div id="caja_mensaje"><div id="bg_caja_mensaje1"></div><div id="bg_caja_mensaje2"></div><div id="titulo_mensaje_obj">titulo mensaje</div><div id="texto_mensaje_obj">se genero un error al intentar modificar el contenido de un div, para poder solucionar el problema debe volver a intentarlo.</div><button class="btn btn-danger" id="boton_cerrar_mensaje">cerrar</button></div></div>',
    posicion: 1 ,
    mensaje_int:"",
    titulo_mensaje:"",
    txt_mensaje: "",
    estado_mensaje1:["#e3503e","#4cb64c","#f19a1f","#46b8da","#e3503e"],
    estado_mensaje2:["#df3823","#42a142","#e38b0e","#2caed5","#df3823"],
    estado_mensaje3:["btn btn-danger","btn btn-success","btn btn-warning","btn btn-info","btn btn-primary"],
    estado_ico_mensaje:["ico3.png","ico2.png","ico4.png","ico5.png","ico1.png"],
    estado1: "",
    estado2: "",
    estado3: "",
    estado_ico: "",
    estado_mensaje: 0,
    
    mensaje:function(titulo,texto,estado){
        this.titulo_mensaje = titulo;
        this.txt_mensaje = texto;
        
        this.estado1= this.estado_mensaje1[estado];
        this.estado2= this.estado_mensaje2[estado];
        
        this.estado3= this.estado_mensaje3[estado];

        this.estado_ico= this.estado_ico_mensaje[estado];
        
        this.mostrar_mensaje();
    },
    
    mostrar_mensaje:function(){   
         if($('#contenedor_mensaje').css('display')=='none')
         {
             //$('#mensaje').css("color",this.estado);
            $('#titulo_mensaje_obj').html(this.titulo_mensaje);
            $('#texto_mensaje_obj').html(this.txt_mensaje);

            $('#bg_caja_mensaje1').css('background-image','url(../js/mensaje/images/'+this.estado_ico+')')
            $('#bg_caja_mensaje1').css('background-color',this.estado1);
            $('#bg_caja_mensaje2').css('background-color',this.estado2);
            $('#boton_cerrar_mensaje').attr('class',this.estado3);


            if($('#contenedor_mensaje').css('display')=='none')                
            $('#contenedor_mensaje').fadeIn('fast');            
        }
        /*
        if(this.mensaje_int)
        clearTimeout(this.mensaje_int);
        
        /*
        this.mensaje_int = setTimeout(function(){        
            $('#contenedor_mensaje').fadeOut('fast');
        },3000);
        */
    },
    fijar_posicion:function(pos){
        
        switch(pos)
        {
        case 1:
            $('#mensaje').css('top',0);
        break;
        case 2:
            $('#mensaje').css('top','50%');
        break;
        case 3:
            $('#mensaje').css('bottom',0);
        break;                
        }
        
    },
    
    evento:function(){
        $('#boton_cerrar_mensaje').bind('click',function(){ $('#contenedor_mensaje').fadeOut();});               
        $('#contenedor_mensaje').bind('click',function(){ $('#contenedor_mensaje').fadeOut();});               
    },
    
    init:function(){
         
        $(document).ready(function(){  
            $("body").append(mensaje.div);   
            mensaje.evento();
            //mensaje.fijar_posicion(mensaje.posicion);                        
         });  
        
        /* VALIDAMOS ENTER CUANDO ESTE EL MENSAJE */ 
          /*  document.onkeydown = function(e){

               if(event.which==13) // 13 = ENTER
                {	if($('#contenedor_mensaje').css('display')!='none')
                    {
                        $('#contenedor_mensaje').fadeOut();                                                
                    }
                } 
            }*/
        /* FIN -  VALIDAMOS ENTER CUANDO ESTE EL MENSAJE */ 
        
    }
    
};

// instanciamos el obj
//mensaje.posicion=3;
mensaje.init();

  