$(document).ready(function(e) {
	
	$('#datepicker').datepicker({
        format: "dd/mm/yyyy",
        weekStart: 1,
        language: "es"

    });

    $('#sandbox-container .input-daterange').datepicker({
        orientation: "bottom auto"
    });

	/* evento */
	$('#boton_exportar').click(function(){
		var resp=confirm("¿Desea exportar?")
		if(resp)
		window.location='moduloexcel.php?excel=exportar_evento';
	});
	/*
    $('#boton_buscar').click(function(){
		var busqueda=$('#input_busqueda').val();
		var fecha1 = $('#fecha1').val();
		var fecha2 = $('#fecha2').val();

		window.location.href="vistas.php?id=home_busqueda&busqueda="+busqueda+"&fecha1="+fecha1+"&fecha2="+fecha2;
	});*/
	/* fin evento */
	
	/* cotizacion 1*/
	$('#boton_buscar2').click(function(){
		var busqueda=$('#input_busqueda').val();
		var fecha1 = $('#fecha1').val();
		var fecha2 = $('#fecha2').val();

		window.location.href="vistas.php?id=cot1_busqueda&busqueda="+busqueda+"&fecha1="+fecha1+"&fecha2="+fecha2;
	});
	$('#boton_exportar2').click(function(){
		var resp=confirm("¿Desea exportar?")
		if(resp)
		window.location='moduloexcel.php?excel=exportar_cot1';
	});
	/* fin cotizacion 1*/
	
	/* cotizacion 2*/
	$('#boton_buscar3').click(function(){
		var busqueda=$('#input_busqueda').val();
		var fecha1 = $('#fecha1').val();
		var fecha2 = $('#fecha2').val();

		window.location.href="vistas.php?id=cot2_busqueda&busqueda="+busqueda+"&fecha1="+fecha1+"&fecha2="+fecha2;
	});
	$('#boton_exportar3').click(function(){
		var resp=confirm("¿Desea exportar?")
		if(resp)
		window.location='moduloexcel.php?excel=exportar_cot2';
	});
	/* fin cotizacion 2*/
	
	/* financiamiento 1*/
	$('#boton_buscar4').click(function(){
		var busqueda=$('#input_busqueda').val();
		var fecha1 = $('#fecha1').val();
		var fecha2 = $('#fecha2').val();

		window.location.href="vistas.php?id=finan_busqueda&busqueda="+busqueda+"&fecha1="+fecha1+"&fecha2="+fecha2;
	});
	$('#boton_exportar4').click(function(){
		var resp=confirm("¿Desea exportar?")
		if(resp)
		window.location='moduloexcel.php?excel=exportar_finan';
	});
	/* fin financiamiento 1*/
	
	
	/* PRECIOS */
	$('#boton_buscar_precio').click(function(){
		var busqueda=$('#input_busqueda').val();

		window.location.href="vistas.php?id=precios_busq&busqueda="+busqueda;
	});
	
	/* FIN PRECIOS */
});
function get_precio(tipo){
	if(tipo==1){
		var id = $('#sel_yam').val();
	}else{
		var id = $('#sel_euro').val();
	}
	if(id==-1){
		$('#input_precio2').val('');
		$('#input_precio1').val('');
	}else{
		if(tipo==1){
			$('#input_precio1').val('Cargando...');
		}else{
			$('#input_precio2').val('Cargando...');
		}
		jQuery.ajax({
			type: "POST", 
			url: "modulo_registro.php",
			data: 'idfuncion=1&tipo='+tipo+'&id_modelo='+id,
			success: function(a) {
				//console.log(a);
				if(tipo==1){
					$('#input_precio1').val('');
					$('#input_precio1').val($.trim(a));
				}else{
					$('#input_precio2').val('');
					$('#input_precio2').val($.trim(a));
				}
			}
		});
	}
}
function update_p(tipo){
	if(tipo==1){
		var id = $('#sel_yam').val();
		var precio = $('#input_precio1').val();
		if(precio==''){
			alert('Debes ingresar el precio.');
			$('#input_precio1').focus();
			return;
		}
	}else{
		var id = $('#sel_euro').val();
		var precio = $('#input_precio2').val();
		if(precio==''){
			alert('Debes ingresar el precio.');
			$('#input_precio2').focus();
			return;
		}
	}
	if(id!=-1){
		jQuery.ajax({
			type: "POST", 
			url: "modulo_registro.php",
			data: 'idfuncion=2&tipo='+tipo+'&id_modelo='+id+'&precio='+precio,
			success: function(a) {
				//console.log(a);
				if(parseInt(a)==1){
					alert('Precio cambiado');
				}else{
					alert('Error, vuelva a intentarlo más tarde.');
				}
			}
		});
	}else{
		alert('Debes elegir un modelo.');	
	}
}
//uso: onkeypress="return justNumbers(event);"
function justNumbers(e){
	var keynum = window.event ? window.event.keyCode : e.which;

	if((keynum == 8) || (keynum == 46)){
		return true;
	}else{
		return /\d/.test(String.fromCharCode(keynum));
	}

}
