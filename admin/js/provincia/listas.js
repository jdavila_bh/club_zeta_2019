

function listaProvincias(div,id,data)
{

    var datos = {};

    datos.modulo = 'provincia';
    datos.metodo = 'traerTodosProvincias';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            $('#'+div).html('<option value="0">Seleccione una Provincia</option>');

            if(resp.estado == 1)
            {
                for(var key in resp.registros)
                    $('#'+div).append('<option value="'+resp.registros[key].id+'">'+resp.registros[key].nombre+'</option>');

                if(id>0)
                {
                    $('#'+div).val(id);
                }

            }



        }
    });

}


function listaProvincias2(div,div2,id,id2)
{

    var datos = {};

    datos.modulo = 'provincia';
    datos.metodo = 'traerTodosProvincias';

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            $('#'+div).html('<option value="0">Seleccione una Provincia</option>');

            if(resp.estado == 1)
            {
                for(var key in resp.registros)
                    $('#'+div).append('<option value="'+resp.registros[key].id+'">'+resp.registros[key].nombre+'</option>');

                if(id>0)
                {
                    $('#'+div).val(id);
                }

                listaComunas2(div2,id2)

            }



        }
    });

}
