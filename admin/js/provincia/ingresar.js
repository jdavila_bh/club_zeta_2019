$(document).ready(function()
{
    listaRegiones('region',0);
});

// AGREGAR
function registrar()
{
    var nombre = $.trim($('#nombre').val());
    var id_region = $.trim($('#region').val());

    if(nombre=='')
    {
        $('#nombre').select().focus();
        error("Debe Ingresar el nombre Ciudad.");
        return false;
    }

    if(id_region==0)
    {
        $('#region').select().focus();
        error("Debe seleccionar una region.");
        return false;
    }


    var data = {};

    data.nombre = nombre;
    data.id_region = id_region;

    var datos = {};

    datos.modulo = 'provincia';
    datos.metodo = 'registrarProvincia';
    datos.data = data;

    cargando.load();

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            cargando.load();
            if(resp.estado==1)
            {
                ok("Registro creado satisfactoriamente.");
                $('#nombre').val('');
                $('#region').val(0);
            }else{
                error("problema al crear el registro, vuelva a intentarlo.");
            }
            tabla_registros();

        }
    });
}

