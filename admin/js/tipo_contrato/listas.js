

function listaTipoContrato(div,id)
{

    var datos = {};

    datos.modulo = 'tipo_contrato';
    datos.metodo = 'traerTodosTipoContratos';

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            $('#'+div).html('<option value="0">Seleccione un Tipo de Contrato</option>');

            if(resp.estado == 1)
            {
                for(var key in resp.registros)
                    $('#'+div).append('<option value="'+resp.registros[key].id+'">'+resp.registros[key].nombre+'</option>');

                if(id>0)
                {
                    $('#'+div).val(id);
                }

            }



        }
    });

}
