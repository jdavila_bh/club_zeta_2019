$(document).ready(function()
{

});

// AGREGAR
function registrar()
{
    var nombre = $.trim($('#nombre').val());

    if(nombre=='')
    {
        $('#nombre').select().focus();
        error("Debe Ingresar el tipo de Contrato.");
        return false;
    }


    var data = {};

    data.nombre = nombre;

    var datos = {};

    datos.modulo = 'tipo_contrato';
    datos.metodo = 'registrarTipoContrato';
    datos.data = data;

    cargando.load();

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            cargando.load();
            if(resp.estado==1)
            {
                ok("Registro creado satisfactoriamente.");
                $('#nombre').val('');
            }else{
                error("problema al crear el registro, vuelva a intentarlo.");
            }
            tabla_registros();

        }
    });
}

