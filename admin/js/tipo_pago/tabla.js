$(document).ready(function(){
    tabla_registros(); // CARGA TABLA DATOS
});


function tabla_registros()
{

    var data = {};

    var datos = {};

    datos.modulo = 'tipo_pago';
    datos.metodo = 'traerTodosTipoPagos';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            var color_estados = ['danger','success'];
            var titulos = ['Id','Nombre','Fecha Ingreso','Fecha Modificación','Estado','Modificar'];
            var html = '';
            var div_tabla = 'tabla_registros';
            html+= armar_tabla_head(titulos,div_tabla);

            if(resp.estado==1)
            {
                for(var key in resp.registros)
                {
                    html+='<tr>';
                    html+='<td>'+resp.registros[key].id+'</td>';
                    html+='<td id="nombre_reg_'+resp.registros[key].id+'">'+resp.registros[key].nombre+'</td>';
                    html+='<td>'+resp.registros[key].fecha_ingreso+'</td>';
                    html+='<td>'+resp.registros[key].fecha_modificacion+'</td>';
                    html+='<td><button id="reg_'+resp.registros[key].id+'" class="btn btn-'+color_estados[resp.registros[key].estado]+' menu-icon fa fa fa-check boton" onclick="estado_registro('+resp.registros[key].id+','+resp.registros[key].estado+')"></button></td>';
                    html+='<td><button class="btn btn-info menu-icon fa fa-pencil boton" onclick="editar_registro('+resp.registros[key].id+')"></button></td>';
                    html+='</tr>';
                }

            }

            $('#tabla_registro').html(html);
            dataTable(div_tabla);


        }
    });

}


// MODIFICAR DATO
function editar_registro(id){
    cargando.load();
    $('#id_registro_mod').val(id);
    $('#nombre').val($('#nombre_reg_'+id).html());

    cambiar_botones(false);
    $(document).scrollTop(0);
    cargando.load();
}

// CAMBIAR ESTADO - CAMBIAR BOTONES
function cambiar_botones(estado)
{
    if(estado){
        $('#boton_acciones').html('<button class="btn btn-success" onclick="registrar()">Crear Registro</button>');
    }else{
        $('#boton_acciones').html('');
        $('#boton_acciones').append('<button class="btn btn-success" onclick="editar_registro_guardar()">Modifcar Registro</button>');
        $('#boton_acciones').append('<button class="btn btn-danger" onclick="cancerlar_modificar()" style="margin-left: 5px;">Cancelar Cambios</button>');
    }
}

// CANCELAR MODIFICACION
function cancerlar_modificar(){
    $('#id_registro_mod').val(0);
    $('#nombre').val('');
    cambiar_botones(true);
}



// MODIFICAR REGISTRO
function editar_registro_guardar()
{

    var id_registro = $.trim($('#id_registro_mod').val());
    var nombre = $.trim($('#nombre').val());

    if(nombre=='')
    {
        error('Debe Ingresar el Nombre.');
        $('#nombre').select().focus();
        return false;
    }

    var data = {};

    data.id_registro = id_registro;
    data.nombre = nombre;

    var datos = {};

    datos.modulo = 'tipo_pago';
    datos.metodo = 'modificarTipoPago';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado == 1)
            {
                $('#nombre').val('');

                ok('Registro Actualizado.');
                $.fancybox.close();

                var tag = ["nombre_reg_"];
                var value = [nombre];
                actualizarTablaReg(tag,value,id_registro);
                cancerlar_modificar();

            }else{
                error('Problema al actualizar el registro.');
            }
        }
    });
}

function estado_registro(id,estado)
{

    if(estado)
        estado=0;
    else
        estado=1;

    var data = {};

    data.id_registro = id;
    data.nuevo_estado = estado;

    var datos = {};

    datos.modulo = 'tipo_pago';
    datos.metodo = 'cambiarEstado';
    datos.data = data;

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp)
        {
            if(resp.estado==1)
            {
                var color_estado = ['btn-success','btn-danger'];
                var color_estado2 = ['btn-danger','btn-success'];

                $('#reg_'+id).removeClass(color_estado[estado]).addClass(color_estado2[estado]).attr('onclick','estado_registro('+id+','+estado+')');

                ok('Cambios de estado, exitoso.');

            }else{
                ok('Problemas al cambiar el estado, vuelva a intentarlo.');
            }
        }
    });

}