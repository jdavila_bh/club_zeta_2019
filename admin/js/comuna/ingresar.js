$(document).ready(function()
{
    listaProvincias('provincia',0);
});

// AGREGAR
function registrar()
{
    var nombre = $.trim($('#nombre').val());
    var id_provincia = $.trim($('#provincia').val());

    if(nombre=='')
    {
        $('#nombre').select().focus();
        error("Debe Ingresar el nombre Comuna.");
        return false;
    }

    if(id_provincia==0)
    {
        $('#provincia').select().focus();
        error("Debe seleccionar una Ciudad.");
        return false;
    }


    var data = {};

    data.nombre = nombre;
    data.id_provincia = id_provincia;

    var datos = {};

    datos.modulo = 'comuna';
    datos.metodo = 'registrarComuna';
    datos.data = data;

    cargando.load();

    $.ajax({
        type: 'POST',
        url: URL_WBS,
        data: datos,
        dataType: 'json',
        success: function(resp){

            cargando.load();
            if(resp.estado==1)
            {
                ok("Registro creado satisfactoriamente.");
                $('#nombre').val('');
                $('#provincia').val(0);
            }else{
                error("problema al crear el registro, vuelva a intentarlo.");
            }
            tabla_registros();

        }
    });
}

