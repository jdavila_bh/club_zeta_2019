
function filtrar_tabla(div){
    $('#'+div).dataTable({
        "language": {
            "lengthMenu": "Por página: _MENU_",
            "zeroRecords": "Sin resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de MAX registros en total)"
        },
        "order": [[ 0, 'desc' ]]
    });
    $('#'+div+' .dataTables_filter input').attr('placeholder', 'Buscar');

}

function ver_div(div){

    $.fancybox({
        'href': div,
        fitToView	: false,
        autoSize	: false,
        width		: '90%',
        height		: 'auto',
        closeBtn	: true,
        closeClick	: false,
        scrolling	: 'yes',
        openEffect	: 'elastic',
        closeEffect	: 'elastic',

        beforeShow: function(){
            $("body").css({'overflow-y':'visible'});
        },
        afterClose: function(){
            $("body").css({'overflow-y':'visible'});
        }
     });
}

//VALIDAR SOLO NUMEROS
function justNumbers(e){
    var keynum = window.event ? window.event.keyCode : e.which;
    if((keynum == 8) || (keynum == 46)){
        return true;
    }else{
        return /\d/.test(String.fromCharCode(keynum));
    }
}

function revisarDigito( dvr )
{
    dv = dvr + ""
    if ( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
    {
        alert("Debe ingresar un digito verificador valido");
        window.document.form1.rut.focus();
        window.document.form1.rut.select();
        return false;
    }
    return true;
}

function revisarDigito2( crut ,div)
{
    largo = crut.length;
    if ( largo < 2 )
    {
        error("Debe ingresar el rut completo");
        $('#'+div).focus();
        $('#'+div).select();
        return false;
    }
    if ( largo > 2 )
        rut = crut.substring(0, largo - 1);
    else
        rut = crut.charAt(0);
    dv = crut.charAt(largo-1);
    revisarDigito( dv );

    if ( rut == null || dv == null )
        return 0;

    var dvr = '0';
    suma = 0;
    mul  = 2;

    for (i= rut.length -1 ; i >= 0; i--)
    {
        suma = suma + rut.charAt(i) * mul;
        if (mul == 7)
            mul = 2;
        else
            mul++;
    }
    res = suma % 11;
    if (res==1)
        dvr = 'k';
    else if (res==0)
        dvr = '0';
    else
    {
        dvi = 11-res;
        dvr = dvi + ""
    }
    if ( dvr != dv.toLowerCase() )
    {
        error("EL rut es incorrecto");
        $('#'+div).focus();
        $('#'+div).select();
        return false
    }

    return true
}

function Rut(texto,div)
{
    //console.log('texto rut '+texto);
    var tmpstr = "";
    for ( i=0; i < texto.length ; i++ )
        if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
            tmpstr = tmpstr + texto.charAt(i);
    texto = tmpstr;
    largo = texto.length;

    if ( largo < 2 )
    {
        error("Debe ingresar el rut completo");
        $('#'+div).focus();
        $('#'+div).select();
        return false;
    }

    for (i=0; i < largo ; i++ )
    {
        if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
        {
            error("El valor ingresado no corresponde a un R.U.T valido");
            $('#'+div).focus();
            $('#'+div).select();
            return false;
        }
    }

    var invertido = "";
    for ( i=(largo-1),j=0; i>=0; i--,j++ )
        invertido = invertido + texto.charAt(i);
    var dtexto = "";
    dtexto = dtexto + invertido.charAt(0);
    dtexto = dtexto + '-';
    cnt = 0;

   // console.log(dtexto+' -  '+invertido);

    for ( i=1,j=2; i<largo; i++,j++ )
    {
        //alert("i=[" + i + "] j=[" + j +"]" );
        if ( cnt == 3 )
        {
            dtexto = dtexto + '.';
            j++;
            dtexto = dtexto + invertido.charAt(i);
            cnt = 1;
        }
        else
        {
            dtexto = dtexto + invertido.charAt(i);
            cnt++;
        }
    }

    invertido = "";
    for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )
        invertido = invertido + dtexto.charAt(i);

    $('#'+div).val(invertido.toUpperCase());

    if ( revisarDigito2(texto,div) )
        return true;

    return false;
}

function validarEmail(valor) {
    var estado = 0;
    if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(valor)){
        estado=0;
    } else {
        estado=1;
    }

    return estado;
}


// Retorna la estructura Cabecera de la tabla
function armar_tabla_head(datos,div_taget){
    var html = '<table class="table table-responsive table-info table-bordered table-hover" id="'+div_taget+'">';

    html+='<thead>';
    html+='<tr>';
    for(var i = 0; i<datos.length; i++)
        html+='<th>'+datos[i]+'</th>';

    html+='</tr>';
    html+='</thead>';
    html+='<tbody>';

    return html;
}

function dataTable(div){
    $('#'+div).dataTable({

        "language": {
            "lengthMenu": "Por página: _MENU_",
            "zeroRecords": "Sin resultados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros en total)"
        },
        "order": [[ 0, 'desc' ]]

    });
    $('#tabla_cliente .table-caption').text('');
    $('#tabla_cliente_filter input').attr('placeholder', 'Buscar...');

}

function ver_div(div){

    $.fancybox({
        'href': div,
        fitToView	: false,
        autoSize	: false,
        width		: '90%',
        height		: 'auto',
        closeBtn	: true,
        closeClick	: false,
        scrolling	: 'yes',
        openEffect	: 'elastic',
        closeEffect	: 'elastic',

        beforeShow: function(){
            $("body").css({'overflow-y':'visible'});
        },
        afterClose: function(){
            $("body").css({'overflow-y':'visible'});
        }
    });
}


function indicador_uf(div_value)
{
    $.get('http://mindicador.cl/api/uf',function(resp){
        $('#'+div_value).val(parseInt(resp.serie[0].valor));
    })

}

function indicador_uf_html(div_value)
{
    $.get('http://mindicador.cl/api/uf',function(resp){
        $('#'+div_value).html(formato_miles(parseInt(resp.serie[0].valor).toFixed(0),1));
    })

}


function downloadURI(uri, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
}

function openNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}


function formato_miles(num,iva){
    var num = parseInt(num);
    if(isNaN(num))
    { return 0;}
    else
    {
        var cadena = ""; var aux;
        var cont = 1,m,k;
        if(num<0)
        {aux=1;}
        else
        {aux=0;}

        num=num.toString();

        for(m=num.length-1; m>=0; m--)
        {
            cadena = num.charAt(m) + cadena;

            if(cont%3 == 0 && m >aux)
            {cadena = "." + cadena;}
            else
            {cadena = cadena;}

            if(cont== 3)
            {cont = 1;}
            else
            {cont++;}

        }

        cadena = cadena.replace(/.,/,",");
        var resultado = '';
        if(iva){
            //resultado = cadena+' + IVA';
            resultado = cadena;
        }else{
            resultado = cadena;
        }
        return resultado;
    }

}