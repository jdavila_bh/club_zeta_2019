var fecha_hora = {
    hora:"",
    fecha:"",
    div_hora:"",
    div_fecha:"",
    int_hora:"",
    int_fecha:"",
    init_fecha_hora : function(){
        $(document).ready(function(){
            fecha_hora.iniciar_fecha();
            fecha_hora.iniciar_reloj();  
        });
    },
    iniciar_fecha : function(){
            this.fecha = fecha_hora.capturar_fecha();
            $('#'+this.div_fecha).html(this.fecha);        
    },
    iniciar_reloj : function(){        
            fecha_hora.int_hora = setInterval(function(){
                fecha_hora.hora = fecha_hora.capturar_hora();    
                $('#'+fecha_hora.div_hora).html(fecha_hora.hora);
            },1000);            
    },
    capturar_hora : function(){
        var hora = new Date();        
        var hora_cap = fecha_hora.formato_2_digit(hora.getHours())+':'+fecha_hora.formato_2_digit(hora.getMinutes())+':'+fecha_hora.formato_2_digit(hora.getSeconds());
        return hora_cap+' hrs.';
    },
    capturar_fecha : function(){
        var fecha = new Date();
        var fecha_cap = fecha_hora.formato_2_digit(fecha.getDate())+'/'+fecha_hora.formato_2_digit((fecha.getMonth()+1))+'/'+fecha.getFullYear();
        return fecha_cap;
        
    },
	formato_2_digit:function(num){
		if(parseInt(num)<10)
		{
		return '0'+num;
		}else{
		return num;	
		}
	}
    
    
}

fecha_hora.div_fecha="fecha";
fecha_hora.div_hora="hora";
fecha_hora.init_fecha_hora();