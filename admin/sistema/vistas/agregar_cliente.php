<script type="text/javascript" src="../js/region/listas.js"></script>
<script type="text/javascript" src="../js/provincia/listas.js"></script>
<script type="text/javascript" src="../js/comuna/listas.js"></script>

<script type="text/javascript" src="../js/cliente/idioma.js"></script>
<script type="text/javascript" src="../js/cliente/ingresar.js"></script>


<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Agregar Cliente</span></h1>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Nombre <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="nombre_d">
               <input id="nombre" class="form-control" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Rut <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="rut_d">
                <input id="rut" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Dirección <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="direccion_d">
                <input id="direccion" class="form-control" type="text">
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-2 control-label">Region <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="region_d">
                <select id="region" class="form-control" type="text" onchange="traerCiudades()"></select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Ciudad <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="ciudad_d">
                <select id="provincia" class="form-control" type="text" onchange="traerComunas()"></select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Comuna <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="comuna_d">
                <select id="comuna" class="form-control" type="text"></select>
            </div>
        </div>



        <div class="form-group">
            <label class="col-md-2 control-label">Teléfono</label>
            <div class="col-md-10" id="telefono_d">
                <input id="telefono" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Celular</label>
            <div class="col-md-10" id="celular_d">
                <input id="celular" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Email <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="email_d">
                <input id="email" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Giro <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="celular_d">
                <input id="giro" class="form-control" type="text">
            </div>
        </div>
		
		<div class="form-group">
            <label class="col-md-2 control-label"><span class="colorRojo">Campos Obligatorio (*)</span></label>
            <div class="col-md-10" id="">                
            </div>
        </div>


        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-labeled btn-success pull-right" onClick="registrar()"><span class="btn-label icon fa fa-save"></span>GUARDAR</button>
                </div>
            </div>
        </div>
    </div>
</div>