
<script type="text/javascript" src="../js/usuario/idioma.js"></script>
<script type="text/javascript" src="../js/tipo_usuario/listas.js"></script>
<script type="text/javascript" src="../js/usuario/ingresar.js"></script>

<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Agregar Usuario</span></h1>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Nombre <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="nombre_d">
               <input id="nombre" class="form-control" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Apellido Paterno <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="apellido_d">
                <input id="apellidop" class="form-control" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Apellido Materno <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="apellido_d">
                <input id="apellidom" class="form-control" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Rut <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="rut_d">
                <input id="rut" class="form-control" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Email <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="email_d">
                <input id="email" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Dirección <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="direccion_d">
                <input id="direccion" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Teléfono <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="telefono_d">
                <input id="telefono" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Celular <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="celular_d">
                <input id="celular" class="form-control" type="text">
            </div>
        </div>

        <hr>

        <div class="form-group">
            <label class="col-md-2 control-label">Usuario <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="usuario_d">
                <input id="user" class="form-control" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Contraseña <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="contraseña_d">
                <input id="pass" class="form-control" type="password">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Repetir Contraseña <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="contraseña_d">
                <input id="pass2" class="form-control" type="password">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Tipo Usuario <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="tipo_d">
                <select id="tipo" class="form-control" type="text"></select>
            </div>
        </div>
		
		<div class="form-group">
            <label class="col-md-2 control-label"><span class="colorRojo">Campos Obligatorio (*)</span></label>
            <div class="col-md-10" id="">                
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-labeled btn-success pull-right" onClick="registrar()"><span class="btn-label icon fa fa-save"></span>GUARDAR</button>
                </div>
            </div>
        </div>
    </div>
</div>