<script type="text/javascript" src="../js/cliente/idioma.js"></script>

<script type="text/javascript" src="../js/region/listas.js"></script>
<script type="text/javascript" src="../js/provincia/listas.js"></script>
<script type="text/javascript" src="../js/comuna/listas.js"></script>

<script type="text/javascript" src="../js/cliente/tabla.js"></script>

<link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox.css" media="screen">
<link rel="stylesheet" type="text/css" href="../css/usuario.css">
<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Lista Clientes</span></h1>
    </div>

    <div class="table table-bordered table-primary">
    	<div class="scroll100">
            <div class="scrollauto" id="tabla_registro">
            </div>
        </div>
    	
    </div>
</div>
<div id="tabla_editar_registro" class="panel panel-warning panel-dark" style="display:none;">
	<div class="panel-heading">
    	<span class="panel-title">Editar Cliente</span>
    </div>
    <div class="panel-body">
            <div class="form-group">
                <label class="col-md-2 control-label">ID</label>
                <div class="col-md-10" id="id_d">
                    <div id="id_registro"></div>
                </div>
            </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Nombre <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="nombre_d">
                <input id="nombre" class="form-control" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Rut <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="rut_d">
                <input id="rut" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Dirección <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="direccion_d">
                <input id="direccion" class="form-control" type="text">
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-2 control-label">Region <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="region_d">
                <select id="region" class="form-control" type="text" onchange="traerCiudades()"></select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Ciudad <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="ciudad_d">
                <select id="provincia" class="form-control" type="text" onchange="traerComunas()"></select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Comuna <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="comuna_d">
                <select id="comuna" class="form-control" type="text"></select>
            </div>
        </div>



        <div class="form-group">
            <label class="col-md-2 control-label">Teléfono</label>
            <div class="col-md-10" id="telefono_d">
                <input id="telefono" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Celular</label>
            <div class="col-md-10" id="celular_d">
                <input id="celular" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Email <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="email_d">
                <input id="email" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Giro <span class="colorRojo">(*)</span></label>
            <div class="col-md-10" id="celular_d">
                <input id="giro" class="form-control" type="text">
            </div>
        </div>
		
		<div class="form-group">
            <label class="col-md-2 control-label"><span class="colorRojo">Campos Obligatorio (*)</span></label>
            <div class="col-md-10" id="">                
            </div>
        </div>

        <button id="boton_guardar" class="btn btn-success" onClick="editar_registro_guardar()">Guardar</button></td>

    </div>
</div>
