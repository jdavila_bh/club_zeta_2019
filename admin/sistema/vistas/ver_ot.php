<script type="text/javascript" src="../js/ot/tabla.js"></script>

<link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox.css" media="screen">

<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Lista OT</span></h1>
    </div>
    <div class="panel-body">
    <div class="row form-group">
        <div class="col-md-1">
            <span>Estados</span>
        </div>
        <div class="col-md-2">
            <select id="estado_ot" class="form-control" onchange="tabla_registros()">
                <option value="-1">Ver Todos</option>
                <option value="0">Por Validar</option>
                <option value="1">Aceptada</option>
                <option value="2">Rechazada</option>
            </select>
        </div>
    </div>
    </div>
    <div class="table table-bordered table-primary">
        <div class="scroll100">
            <div class="scrollauto" id="tabla_registro">
            </div>
        </div>

    </div>
</div>

<div class="hidden" id="link_pdf"></div>

<!-- Informacion Cliente -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="titulo_box"></h4>
            </div>
            <div class="modal-body" id="texto_box">
                <p></p>
            </div>
            <div class="modal-footer" id="footer_box">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>