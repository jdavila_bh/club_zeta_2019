<script src="../js/tipo_usuario/ingresar.js" type="text/javascript" ></script>
<script src="../js/tipo_usuario/tabla.js" type="text/javascript" ></script>
<script src="../js/funciones/funciones.js" type="text/javascript" ></script>

<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Tipo Usuarios</h1>
    </div>
    <div class="panel-body">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title" id="titulo_accion_temporada">Crear Tipo Usuario</span>
                </div>
                <div class="panel-body buttons-with-margins">
                    <label for="inputEmail2" class="col-sm-2 control-label">Nombre Tipo Usuario</label>
                    <div class="col-sm-10">
                        <input type="hidden" id="id_registro_mod">
                        <input type="text" class="form-control" id="nombre" placeholder="Nombre Tipo Usuario" value="">
                    </div>
                </div>
                <div class="panel-body buttons-with-margins" id="boton_acciones">
                    <button class="btn btn-success" onclick="registrar()">Crear Nuevo</button>
                </div>
            </div>

            <div class="panel buttons-with-margins">
                <div class="panel-heading">
                    <span class="panel-title" >Lista Tipo Usuarios</span>
                </div>
                <div class="table">
                    <div id="tabla_registro">

                    </div>
                </div>
            </div>
    </div>
</div>
