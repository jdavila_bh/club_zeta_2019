<script type="text/javascript" src="../js/provincia/listas.js"></script>

<script src="../js/comuna/ingresar.js" type="text/javascript" ></script>
<script src="../js/comuna/tabla.js" type="text/javascript" ></script>
<script src="../js/funciones/funciones.js" type="text/javascript" ></script>

<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Comunas</h1>
    </div>
    <div class="panel-body">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title" id="titulo_accion_temporada">Crear Una Comuna</span>
                </div>
                <div class="panel-body buttons-with-margins">
                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Nombre Comuna</label>
                        <div class="col-sm-10">
                            <input type="hidden" id="id_registro_mod">
                            <input type="text" class="form-control" id="nombre" placeholder="Nombre Comuna" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Ciudad</label>
                        <div class="col-md-10" id="region_d">
                            <select id="provincia" class="form-control" type="text"></select>
                        </div>
                    </div>
                </div>
                <div class="panel-body buttons-with-margins" id="boton_acciones">
                    <button class="btn btn-success" onclick="registrar()">Crear Nuevo</button>
                </div>
            </div>

            <div class="panel buttons-with-margins">
                <div class="panel-heading">
                    <span class="panel-title" >Lista Comunas</span>
                </div>
                <div class="table">
                    <div id="tabla_registro">

                    </div>
                </div>
            </div>
    </div>
</div>
