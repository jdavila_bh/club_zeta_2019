<script type="text/javascript" src="../js/comprobante/tabla.js"></script>

<link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox.css" media="screen">


<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Comprobante</span></h1>
    </div>
    <div class="panel-body">
        <div class="row form-group">

            <!-- busqueda por  -->
            <div class="row form-group">
                <div class="col-xs-2"><span>Rut: </span></div>
                <div class="col-md-3">
                    <input type="text" id="f_rut"  placeholder="RUT" class="m-from" data-bind="value:replyNumber"/>
                </div>
                <div class="col-md-2">
                    <button id="btn_rut" name="btn_rut" class="btn btn-block">Buscar</button>
                </div>
            </div>

            <!-- busqueda por  -->
            <div class="row form-group">
                <div class="col-xs-2"><span>N° de Orden: </span></div>
                <div class="col-md-3">
                    <input  type="text" id="f_orden" placeholder="N° Orden" class="m-from" data-bind="value:replyNumber"/>
                </div>
                <div class="col-md-2">
                    <button id="btn_orden" class="btn btn-block">Buscar</button>
                </div>
            </div>

            <!--
            <div class="col-md-1">
                <span>Estados</span>
            </div>
            <div class="col-md-2">
                <select id="estado_ot" class="form-control" onchange="tabla_registros()">
                    <option value="-1">Ver Todos</option>
                    <option value="0">Por Validar</option>
                    <option value="1">Aceptada</option>
                    <option value="2">Rechazada</option>
                </select>
            </div>
            -->
        </div>
    </div>
    <div class="table table-bordered table-primary">
        <div class="scroll100">
            <div class="scrollauto" id="tabla_registro">
            </div>
        </div>

    </div>
</div>

<div class="hidden" id="link_pdf"></div>

<!-- Informacion Cliente -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="titulo_box"></h4>
            </div>
            <div class="modal-body" id="texto_box">
                <p></p>
            </div>
            <div class="modal-footer" id="footer_box">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- ver comprobante -->
<!--<div id="verComprobante" class="panel panel-warning panel-dark" style="display:none;"-->
<div id="verComprobante"  role="dialog" class="modal fade">
    <div class="modal-dialog" style="width: 750px;height: auto;">
        <div id="compra_exitosa" class="bg_blanco">

            <!--<link rel="stylesheet" type="text/css" href="../css/comprobante.css" media="screen">-->

            <table style="font-family: 'Arial', sans-serif;border-collapse: collapse;border-spacing: 0;color: #444444;width: 750px; background-image: url('../images/comprobante/fondo.png');background-repeat: no-repeat;background-position: center;background-size: 350px; background-color: #ffffff;">
                <tr>
                    <td colspan="3">
                        <table style=" width: 100%;">
                            <tr>
                                <td colspan="2" style="font-size:28px;padding-top: 10px;padding-right: 10px;padding-left: 10px;">Talonario válido sólo para 2018</td>
                                <td style="vertical-align: bottom;text-align: left; background-image: url('../images/comprobante/baner1.jpg');background-repeat: no-repeat;"><img src="../images/comprobante/logo.png" style="width: 80px; margin-top: 10px;"><span class="azul_oscuro size-sub-titulo">Servicio de alimentación</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;" >
                        <span style="font-size:28px;color: #74B913;">Comprobante</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;padding-bottom: 20px;padding-top: 20px;padding-right: 20px;padding-left: 20px;">
                        <span style="color: #0A4187;">Imprima y presente este comprobante en su casino. Impresión no válida para canje de almuerzo, se debe retirar cuponeras correspodientes en casino. Canje al portador de este documento.</span>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Nombre Comprador:</span>&nbsp;<span id="ver_nombre"></span></td>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Código de autorización:</span>&nbsp;<span id="ver_cod_aut"></span></td>
                    <td rowspan="7" style="text-align: center;vertical-align: bottom;"><img src="../images/comprobante/pagado.png" class="" style=" width: 100px;"></td>
                </tr>
                <tr>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Rut:</span>&nbsp;<span id="ver_rut"></span></td>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Tipo de Transacción:</span>&nbsp;<span id="ver_tipo_tran"></span></td>

                </tr>
                <tr>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Fecha:</span>&nbsp;<span id="fecha"></span></td>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Producto:</span>&nbsp;<span id="ver_prod"></span></td>

                </tr>
                <tr>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Alumno:</span>&nbsp;<span id="ver_nomb_alum"></span></td>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Precio Unitario:</span>&nbsp;<span id="ver_precio_uni"></span></td>

                </tr>
                <tr>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Nº orden:</span>&nbsp;<span id="nro_orden"></span></td>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Monto total:</span>&nbsp;<span id="ver_monto_total"></span></td>

                </tr>
                <tr>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span style="color: #0A4187;">Número de Cuotas:</span>&nbsp;<span id="ver_nro_cuotas"></span></td>
                    <td>&nbsp;</td>

                </tr>
                <tr>
                    <td style="padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><span id="ver_tarjeta" style="color: #0A4187;"></span>&nbsp;<span id="nro_tarjeta"></span></td>
                    <td>&nbsp;</td>

                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"style="text-align: left;padding-bottom: 20px;padding-top: 20px;padding-right: 30px;padding-left: 30px;">
                        <span>"En caso de requerir y/o Reembolsos, favor de contactar a Departamento Finanzas al teléfono (+56-2) 29 57 41 53 (Anexo 231) o al Mail finanzas@clubzeta.cl.</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" bgcolor="#0A4187" style="background-color: #0A4187;text-align: center;padding-bottom: 10px;padding-top: 10px;padding-right: 10px;padding-left: 10px;"><a href="www.clubzeta.cl" class="" style="color: #FFFFFF;text-decoration: none;">www.clubzeta.cl</a></td>
                </tr>
            </table>
        </div>
        <button id="btn_imp_cmprb" class="btn fa fa-print" style="margin: 2px 0px 0px 20px;"> Imprimir</button>
        <button id="btn_ver_cerrar" class="btn fa fa-close" style="margin: 2px 0px 0px 20px;">Cerrar</button>
    </div>
</div>