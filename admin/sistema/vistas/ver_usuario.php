<script type="text/javascript" src="../js/usuario/idioma.js"></script>
<script type="text/javascript" src="../js/tipo_usuario/listas.js"></script>
<script type="text/javascript" src="../js/usuario/tabla.js"></script>

<link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox.css" media="screen">
<link rel="stylesheet" type="text/css" href="../css/usuario.css">

<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Lista Usuarios</span></h1>
    </div>

    <div class="table table-bordered table-primary">
    	<div class="scroll100">
            <div class="scrollauto" id="tabla_registro">
            </div>
        </div>
    	
    </div>
</div>

<div id="tabla_editar_usuario" class="panel panel-warning panel-dark" style="display:none;">
	<div class="panel-heading">
    	<span class="panel-title">Editar Usuario</span>
    </div>
    <div class="panel-body">
            <div class="form-group">
                <label class="col-md-2 control-label">ID</label>
                <div class="col-md-10" id="nombre_d">
                    <div id="id_usuario"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Nombre <span class="colorRojo">(*)</span></label>
                <div class="col-md-10" id="nombre_d">
                    <input id="nombre" class="form-control" type="text">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Apellido Paterno <span class="colorRojo">(*)</span></label>
                <div class="col-md-10" id="apellido_d">
                    <input id="apellidop" class="form-control" type="text">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Apellido Materno <span class="colorRojo">(*)</span></label>
                <div class="col-md-10" id="apellido_d">
                    <input id="apellidom" class="form-control" type="text">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Rut <span class="colorRojo">(*)</span></label>
                <div class="col-md-10" id="rut_d">
                    <input id="rut" class="form-control" type="text">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Email <span class="colorRojo">(*)</span></label>
                <div class="col-md-10" id="email_d">
                    <input id="email" class="form-control" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Dirección <span class="colorRojo">(*)</span></label>
                <div class="col-md-10" id="direccion_d">
                    <input id="direccion" class="form-control" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Teléfono <span class="colorRojo">(*)</span></label>
                <div class="col-md-10" id="telefono_d">
                    <input id="telefono" class="form-control" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Celular <span class="colorRojo">(*)</span></label>
                <div class="col-md-10" id="celular_d">
                    <input id="celular" class="form-control" type="text">
                </div>
            </div>

            <hr>

            <div class="form-group">
                <label class="col-md-2 control-label">Tipo Usuario <span class="colorRojo">(*)</span></label>
                <div class="col-md-10" id="tipo_d">
                    <select id="tipo" class="form-control" type="text"></select>
                </div>
            </div>
			
			<div class="form-group">
            <label class="col-md-2 control-label"><span class="colorRojo">Campos Obligatorio (*)</span></label>
            <div class="col-md-10" id="">                
            </div>
			</div>

            <button id="boton_guardar" class="btn btn-success" onClick="editar_registro_guardar()">Guardar</button></td>

    </div>
</div>

<div id="contenedor_cambio_clave" class="panel panel-warning panel-dark" style="display:none;">
	<div class="panel-heading">
    	<span class="panel-title">Cambio de Contraseña</span>
    </div>
    <div class="panel-body">
    	<table class="table table-bordered">
            <tr>
                <td colspan="2"><input type="hidden" id="id_usuario_hidden"/></td>
            </tr>
            <tr>
            	<th>Contraseña Nueva</th>
                <td><input id="clave_nueva" class="form-control" type="password"></td>
            </tr>
            <tr>
                <th>Repetir Contraseña</th>
                <td><input id="clave_nueva2" class="form-control" type="password"></td>
            </tr>
            <tr>
            	<th></th>
                <td><button class="btn btn-success" onClick="guardar_clave()">Guardar</button></td>
            </tr>            
        </table>
    </div>
</div>