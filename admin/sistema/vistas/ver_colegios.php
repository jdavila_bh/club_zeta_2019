<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo 5
 * Date: 11-04-2018
 * Time: 12:22
 */
?>
<script type="text/javascript" src="../js/colegios/tabla.js"></script>

<link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox.css" media="screen">


<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Colegios</span></h1>
    </div>

    <div class="table table-bordered table-primary">
        <div class="scroll100">
            <div class="scrollauto" id="tabla_registro">
            </div>
        </div>

    </div>
</div>


<!-- Informacion Colegios-->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="titulo_box"></h4>
            </div>
            <div class="modal-body" id="texto_box">
                <p></p>
            </div>
            <div class="modal-footer" id="footer_box">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

