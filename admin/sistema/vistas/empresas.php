<script src="../js/empresa/ingresar.js" type="text/javascript" ></script>
<script src="../js/empresa/modificar.js" type="text/javascript" ></script>
<script src="../js/empresa/tabla.js" type="text/javascript" ></script>
<script src="../js/funciones/funciones.js" type="text/javascript" ></script>

<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Empresas</h1>
    </div>
    <div class="panel-body">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title" id="titulo_accion_temporada">Crear una Empresa</span>
                </div>
                <div class="panel-body buttons-with-margins">
                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="hidden" id="id_registro_mod">
                            <input type="text" class="form-control" id="nombre" placeholder="Nombre de Empresa" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">RUT</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rut" placeholder="RUT de Empresa" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Direccion</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="direccion" placeholder="Dirección de Empresa" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Teléfono</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="telefono" placeholder="Teléfono de Empresa" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Giro</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="giro" placeholder="Giro de Empresa" value="">
                        </div>
                    </div>

                    <!--<div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Logo</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="logo" placeholder="Logo de Empresa" value="">
                        </div>
                    </div>-->
                </div>
                <div class="panel-body buttons-with-margins" id="boton_acciones">
                    <button class="btn btn-success" onclick="registrar()">Crear Nuevo</button>
                </div>
            </div>

            <div class="panel buttons-with-margins">
                <div class="panel-heading">
                    <span class="panel-title" >Lista de Empresas</span>
                </div>
                <div class="table">
                    <div id="tabla_registro">

                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="editar_registro" role="dialog">
                <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Editar Empresa</h4>
                </div>
                <div class="modal-body">
                 <!-- INPUTS -->
                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="hidden" id="id_registro_mod">
                            <input type="text" class="form-control" id="nombre_mod" placeholder="Nombre de Empresa" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">RUT</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rut_mod" placeholder="RUT de Empresa" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Direccion</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="direccion_mod" placeholder="Dirección de Empresa" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Teléfono</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="telefono_mod" placeholder="Teléfono de Empresa" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Giro</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="giro_mod" placeholder="Giro de Empresa" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-success" onclick="modificar_registro()">Guardar</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
              
            </div>
          </div>
          
        </div>
    </div>
</div>
