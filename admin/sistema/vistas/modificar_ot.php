<script type="text/javascript" src="../js/region/listas.js"></script>
<script type="text/javascript" src="../js/provincia/listas.js"></script>
<script type="text/javascript" src="../js/comuna/listas.js"></script>
<script type="text/javascript" src="../js/tipo_pago/listas.js"></script>
<script type="text/javascript" src="../js/tipo_contrato/listas.js"></script>
<!-- OT -->
<script type="text/javascript" src="../js/ot/modificar.js"></script>
<!-- Idioma Validador Cliente -->
<script type="text/javascript" src="../js/cliente/idioma.js"></script>

<script type="text/javascript" src="../js/jqueryui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="../js/jqueryui/jquery-ui.css">
<link rel="stylesheet" href="../css/sistema.css">

<div class="panel">
    <div class="page-header">
        <h1><span class="text-blck">Modificar Presupuesto - <span class="colorAzul text-bold">PRESUPUESTO Nº: <span id="n_presupuesto" class=""><?php echo $_GET['id_registro']; ?></span></span></span></h1>
        <input type="hidden" id="id_ot" value="<?php echo $_GET['id_registro']; ?>">
    </div>
    <div class="panel-body">
        <!-- PASOS - Crear presupuesto -->
        <div class="row mb30">
            <div class="col-md-2"><div class="paso1 paso-top center-block pointer" onclick="menu_presupuesto(1)"></div> </div>
            <div class="col-md-2"><div class="paso-flecha  center-block mt38"></div> </div>
            <div class="col-md-2"><div class="paso2 paso-bottom center-block pointer" onclick="menu_presupuesto(2)"></div> </div>
            <div class="col-md-2"><div class="paso-flecha paso-bottom center-block mt38"></div> </div>
            <div class="col-md-2"><div class="paso3 paso-bottom center-block pointer" onclick="menu_presupuesto(3)"></div> </div>
        </div>
        <!-- FIN PASOS - Crear presupuesto -->

        <!-- Paso 1 -->
        <div class="paso1_datos">
            <div class="row">
                <div class="col-md-16 hidden">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Buscar Cliente</label>
                        <div class="col-md-5" id="nombre_d">
                           <input id="nombre" class="form-control" type="text">
                        </div>
                        <div class="col-md-2">
                            <div class="btn btn-success" onclick="ver_agregar_cliente()">+</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <h2 class="panel-title pull-left">Información Cliente</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-info">
                        <tr>
                            <td>Nombre Cliente:</td>
                            <td id="nombre_cli"></td>
                        </tr>
                        <tr>
                            <td width="150">Rut:</td>
                            <td id="rut_cli"></td>
                        </tr>
                        <tr>
                            <td>Giro:</td>
                            <td id="giro_cli"></td>
                        </tr>
                        <tr>
                            <td>Dirección:</td>
                            <td id="direccion_cli"></td>
                        </tr>
                        <tr>
                            <td>Region:</td>
                            <td id="region_cli"></td>
                        </tr>
                        <tr>
                            <td>Ciudad:</td>
                            <td id="ciudad_cli"></td>
                        </tr>
                        <tr>
                            <td>Comuna:</td>
                            <td id="comuna_cli"></td>
                        </tr>
                        <tr>
                            <td>Telefono:</td>
                            <td id="telefono_cli"></td>
                        </tr>
                        <tr>
                            <td>Celular:</td>
                            <td id="celular_cli"></td>
                        </tr>

                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <h2 class="panel-title pull-left">Información Solicitante</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Solicitud</label>
                        <div class="col-md-10" id="nombre_d">
                            <input id="solicitud" class="form-control" type="text">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Solicitado Por</label>
                        <div class="col-md-10" id="nombre_d">
                            <input id="solicitadopor" class="form-control" type="text">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 form-group">
                    <h2 class="panel-title pull-left">Información Adicional Presupuesto</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Comentario</label>
                        <div class="col-md-10" id="nombre_d">
                            <textarea id="comentario" class="form-control" type="text" rows="6"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Nota</label>
                        <div class="col-md-10" id="nombre_d">
                            <textarea id="nota" class="form-control" type="text" rows="6"></textarea>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-labeled btn-success pull-right" onclick="menu_presupuesto(2)"><span class="btn-label icon fa fa-arrow-circle-right"></span>Siguiente</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN - Paso 1 -->


        <!-- Paso 2 -->
        <div class="paso2_datos">
            <div class="row form-group">
                <div class="col-md-12">
                    <div class="btn btn-success" onclick="agregarItem()">Agregar Item</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>Cantidad</td>
                                <td>Descripción</td>
                                <td>UF</td>
                                <td>Unitario</td>
                                <td>Descuento (%)</td>
                                <td>Neto</td>
                                <td>Eliminar</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="detalle_item" id="row_1" >
                                <td><input type="text" class="cantidad form-control" onkeyup="calcularValores()"  onkeypress="return justNumbers(event);"></td>
                                <td><textarea class="descripcion form-control"></textarea> </td>
                                <td><input type="text" class="cant_uf form-control" onkeyup="calcularValores()"  onkeypress="return justNumbers(event);"></td>
                                <td><input type="text" class="unitario form-control" onkeyup="calcularValores()" onkeypress="return justNumbers(event);"></td>
                                <td><input type="text" class="descuento form-control" onkeyup="calcularValores()" onkeypress="return justNumbers(event);" maxlength="3"></td>
                                <td><input type="text" class="neto form-control" onkeypress="return justNumbers(event);"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="4" rowspan="5"></td>
                                <td colspan="1">Subtotal</td>
                                <td colspan="2" id="subtotal_final">0</td>
                            </tr>
                            <tr>
                                <td colspan="1">Descuento</td>
                                <td colspan="2" id="descuento_final">0</td>
                            </tr>
                            <tr>
                                <td colspan="1">Neto</td>
                                <td colspan="2" id="neto_final">0</td>
                            </tr>
                            <tr>
                                <td colspan="1">Impuesto <span id="impuesto" class="colorRojo">19%</span></td>
                                <td colspan="2" id="impuesto_final">0</td>
                            </tr>
                            <tr>
                                <td colspan="1"><strong>Total Proyecto</strong></td>
                                <td colspan="2" id="total_final">0</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Forma Pago</label>
                        <div class="col-md-9" id="formapago_d">
                            <select id="formapago" class="form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-3 control-label">UF del Día</label>
                        <div class="col-md-9" id="uf_dia_d">
                            <input type="text" id="uf_dia" class="form-control" placeholder="UF del día" onkeypress="return justNumbers(event);">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Valor Hora</label>
                        <div class="col-md-9" id="valorhora_d">
                            <input type="text" id="valorhora" class="form-control" placeholder="Valor Hora" onkeypress="return justNumbers(event);">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tipo Contrato</label>
                        <div class="col-md-9" id="tipocontrato_d">
                            <select id="tipocontrato" class="form-control"></select>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nº Factura</label>
                        <div class="col-md-9" id="nfactura_d">
                            <input type="text" id="nfactura" class="form-control" placeholder="Numero Factura" onkeypress="return justNumbers(event);">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Timing en dias</label>
                        <div class="col-md-9" id="timingdia_d">
                            <input type="text" id="timingdia" class="form-control" placeholder="Numero de dias" onkeyup="calcularHoras()" onkeypress="return justNumbers(event);">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Timing en horas</label>
                        <div class="col-md-9" id="valorhora_d">
                            <input type="text" id="timinghora" class="form-control" placeholder="Numero de horas" onkeypress="return justNumbers(event);">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-labeled btn-success pull-right ml10" onclick="menu_presupuesto(3)"><span class="btn-label icon fa fa-arrow-circle-right"></span>Siguiente</button>
                        <button class="btn btn-labeled btn-success pull-right ml10" onclick="menu_presupuesto(1)"><span class="btn-label icon fa fa-arrow-circle-left"></span>Volver</button>
						<button class="btn btn-labeled btn-success pull-right" onclick="ver_pdf()"><span class="btn-label icon fa fa-file"></span>Vista Previa</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- FIN - Paso 2 -->

        <!-- Paso 3 -->
        <div class="paso3_datos">
            <div class="row form-group">
                <div class="col-md-12">
                    <div class="btn btn-success" onclick="agregarComentario()">Agregar comentario</div>
                </div>
            </div>
            <div id="contenedor_comentario_presupuesto">
                <hr>
                <!--
                <div class="row comentario_presupuesto" id="comentario_1">
                    <div class="col-md-12 form-group">
                        <div class="col-md-3">Titulo</div>
                        <div class="col-md-8"><input type="text" class="form-control titulo_comentario" ></div>
                        <div class="col-md-1"><div class="btn btn-danger pull-right" onclick="removeComentario(1)">X</div></div>
                    </div>
                    <div class="col-md-12 form-group">
                        <div class="col-md-3">Detalle Comentario</div>
                        <div class="col-md-9"><textarea class="form-control detalle_comentario"></textarea></div>
                    </div>
                </div>
                <hr class="linea_comentario_1">
                -->


            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-labeled btn-success pull-right ml10" onclick="guardarComentario()"><span class="btn-label icon fa fa-save"></span>Guardar Presupuesto</button>
                        <button class="btn btn-labeled btn-success pull-right ml10" onclick="menu_presupuesto(2)"><span class="btn-label icon fa fa-arrow-circle-left"></span>Volver</button>
						<button class="btn btn-labeled btn-success pull-right" onclick="ver_pdf()"><span class="btn-label icon fa fa-file"></span>Vista Previa</button>

                    </div>
                </div>
            </div>
        </div>
        <!-- FIN - Paso 3 -->
    </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="titulo_box">Modal Header</h4>
            </div>
            <div class="modal-body" id="texto_box">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer" id="footer_box">

            </div>
        </div>

    </div>
</div>


<div class="panel display_none" id="nuevo_cliente">
    <div class="page-header">
        <h1><span class="text-blck">Agregar Cliente</span></h1>
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-2 control-label">Nombre</label>
            <div class="col-md-10" id="nombre_d">
                <input id="nombre_cliente" class="form-control" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Rut</label>
            <div class="col-md-10" id="rut_d">
                <input id="rut_cliente" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Dirección</label>
            <div class="col-md-10" id="direccion_d">
                <input id="direccion_cliente" class="form-control" type="text">
            </div>
        </div>


        <div class="form-group">
            <label class="col-md-2 control-label">Region</label>
            <div class="col-md-10" id="region_d">
                <select id="region_cliente" class="form-control" type="text" onchange="traerCiudades()"></select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Ciudad</label>
            <div class="col-md-10" id="ciudad_d">
                <select id="provincia_cliente" class="form-control" type="text" onchange="traerComunas()"></select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Comuna</label>
            <div class="col-md-10" id="comuna_d">
                <select id="comuna_cliente" class="form-control" type="text"></select>
            </div>
        </div>



        <div class="form-group">
            <label class="col-md-2 control-label">Teléfono</label>
            <div class="col-md-10" id="telefono_d">
                <input id="telefono_cliente" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Celular</label>
            <div class="col-md-10" id="celular_d">
                <input id="celular_cliente" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Email</label>
            <div class="col-md-10" id="email_d">
                <input id="email_cliente" class="form-control" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">Giro</label>
            <div class="col-md-10" id="celular_d">
                <input id="giro_cliente" class="form-control" type="text">
            </div>
        </div>


        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-labeled btn-success pull-right" onClick="registrar()"><span class="btn-label icon fa fa-save"></span>GUARDAR</button>
                </div>
            </div>
        </div>
    </div>
</div>