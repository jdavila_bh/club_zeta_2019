<?php

/**
 * Created by PhpStorm.
 * User: Desarrollo 5
 * Date: 06-02-2018
 * Time: 9:52
 */

$html ='';

$html .='<ul class="navigation">';
    /*detalle de compra*/
    if($_SESSION['permiso1']==1 || $_SESSION['sesion_usuario_activa']['id_tipo_usuario_admin']==1){
    $html .='<li>
                <a href="javascript:void(0);" tabindex="-1" class="pointer" onclick="vista(\'ver_detalle_compra\')"><i class="menu-icon fa fa-list"></i><span class="mm-text">Detalle Compras</span></a>
            </li>';
    }

    /*grafica*/
    if($_SESSION['permiso2']==1 || $_SESSION['sesion_usuario_activa']['id_tipo_usuario_admin']==1) {
        $html .= '<li>
                <a href="javascript:void(0);" tabindex="-1" class="pointer" onclick="vista(\'ver_grafico\')"><i class="menu-icon fa fa-bar-chart-o" ></i><span class="mm-text">Graficos</span></a>
            </li>';
    }

    /*colegios*/
    if($_SESSION['permiso3']==1 || $_SESSION['sesion_usuario_activa']['id_tipo_usuario_admin']==1) {
        $html .= '<li class="mm-dropdown">
                <a href="#"><i class="menu-icon fa fa fa-angle-double-right"></i><span class="mm-text">Colegios</span></a>
                <ul>
                    <li>
                        <a href="javascript:void(0);" tabindex="-1" class="pointer" onclick="vista(\'agregar_colegio\')"><i class="menu-icon fa fa-check"></i><span class="mm-text">Agregar Colegio</span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" tabindex="-1" class="pointer" onclick="vista(\'ver_colegios\')"><i class="menu-icon fa fa-list"></i><span class="mm-text">Ver colegio</span></a>
                    </li>
                </ul>
            </li>';
    }

    /*comprobante*/
    if($_SESSION['permiso4']==1 || $_SESSION['sesion_usuario_activa']['id_tipo_usuario_admin']==1) {
        $html .= '<li>
                <a href="javascript:void(0);" tabindex="-1" class="pointer" onclick="vista(\'ver_comprobante\')"><i class="menu-icon fa fa-paste"></i><span class="mm-text">Ver Comprobante</span></a>
            </li>';
    }

    /*usurios*/
    if($_SESSION['permiso5']==1 || $_SESSION['sesion_usuario_activa']['id_tipo_usuario_admin']==1) {
        $html .= '<li class="mm-dropdown">
                <a href="#"><i class="menu-icon fa fa-user"></i><span class="mm-text">Usuarios</span></a>
                <ul>
                    <li>
                        <a href="javascript:void(0);" tabindex="-1" class="pointer" onclick="vista(\'agregar_usuario_admin\')"><i class="menu-icon fa fa-check"></i><span class="mm-text">Agregar Usuario</span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" tabindex="-1" class="pointer" onclick="vista(\'ver_usuarios\')"><i class="menu-icon fa fa-list"></i><span class="mm-text">Ver Usuarios</span></a>
                    </li>
                </ul>
            </li>';
    }
$html .='</ul>';

echo $html;