<!-- / #content-wrapper -->
<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
<script type="text/javascript"> window.jQuery || document.write('<script src="../assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
<script type="text/javascript"> window.jQuery || document.write('<script src="../js/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->

<!-- Fancy_Box css -->
<link href="../js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" >
<link href="../js/fancybox/helpers/jquery.fancybox-buttons.css" rel="stylesheet" type="text/css" >
<link href="../js/fancybox/helpers/jquery.fancybox-thumbs.css"  rel="stylesheet" type="text/css">


<!-- Fancy_Box js  -->

<script src="../js/fancybox/jquery.fancybox.pack.js"             ></script>
<script src="../js/fancybox/jquery.fancybox.js"                  ></script>
<script src="../js/fancybox/jquery.mousewheel-3.0.6.pack.js"     ></script>


<!-- LanderApp's javascripts -->
<script src="../assets/javascripts/bootstrap.min.js"></script>
<script src="../assets/javascripts/landerapp.min.js"></script>

<script type="text/javascript">
    init.push(function () {
        // Javascript code here
    })
    window.LanderApp.start(init);


    window.onbeforeunload = function() { return "You work will be lost."; };
    history.pushState(null, null, 'home.php');
    window.addEventListener('popstate', function(event) {
        history.pushState(null, null, 'home.php');
        $.get("vistas/home.php", function( data ){
            $("#content-wrapper" ).html( data );
        });
    });


    var URL_WBS = 'http://clubzeta.cl.local/admin/wbs/wbs.php';
</script>


<?php include('scripts.php'); ?>
<script>
    // UF DIA
    $('#uf_os').html('<a href="javascript:void(0)"><span class="icon fa fa-money"></span> UF DIA: <span id="uf_os_dia"></span></a>');
    indicador_uf_html('uf_os_dia');
</script>
</body>
</html>