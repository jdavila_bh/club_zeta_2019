<?php
function getStamp(){
    $now = (string)microtime();
    $now = explode(' ', $now);
    $mm = explode('.', $now[0]);
    $mm = $mm[1];
    $now = $now[1];
    $segundos = $now % 60;
    $segundos = $segundos < 10 ? "$segundos" : $segundos;
    return strval(date("YmdHi",mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"))) . "$segundos$mm");
}
//echo getStamp();

$ds          = '/';
$ruta_save_file = 'imagenes_galeria_recetas/';
$storeFolder = '../../../'.$ruta_save_file.urldecode($_POST['carpeta_destino']);

if (!empty($_FILES)) {

    $tempFile = $_FILES['file']['tmp_name'];

    //$targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;
    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder;
    $file_name_no_ext = explode('.',$_FILES['file']['name']);
    $max_file_name = count($file_name_no_ext);
    $file_ext = '.'.$file_name_no_ext[$max_file_name-1];
    $file_ext = strtolower($file_ext);
    $nombre_nuevo_file = getStamp();
    $targetFile =  $targetPath.$nombre_nuevo_file.$file_ext;

    move_uploaded_file($tempFile,$targetFile);
    echo $nombre_nuevo_file.$file_ext;
}