<?php
include('../../class/config.php');
include('../../class/db.php');

$bd = new db(); //conexion a la base de datos

extract($_REQUEST);

switch($idfuncion) {

    case 1: // TRAER DATOS

        //ESTRUCTURA
        $columnas  = array( "id_tipo_usuario","nombre_tipo_usuario","fecha_creacion_tipo_usuario","estado");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_usuario');
        $bd->setFiltro('1');
        $bd->setAgrupar('id_tipo_usuario');
        $bd->setOrden('id_tipo_usuario DESC');

        $bd->select();
        $bd->ejecutar();
        $datos = array();
        while($row = $bd->resultado())
        {
            array_push($datos,array(
                "id"=>$row['id_tipo_usuario'],
                "nombre"=>utf8_encode($row['nombre_tipo_usuario']),
                "fecha"=>$row['fecha_creacion_tipo_usuario'],
                "estado"=>$row['estado']
            ));

        }

        echo json_encode($datos);

    break;

    case 2: // INSERTAR REGISTRO

        //ESTRUCTURA TABLA DATOS
        $columnas  = array( "id_tipo_usuario","nombre_tipo_usuario");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_usuario');
        $bd->setFiltro('1');
        $bd->setAgrupar('id_tipo_usuario');
        $bd->setOrden('id_tipo_usuario');

        $valores = array(0,"'".$nombre."'");
        $bd->setValores($valores);
        $bd->insert();
        $datos = array();
        array_push($datos,array("estado"=>$bd->getResultadoInsert()));
        echo json_encode($datos);


        break;

    case 3: // CAMBIAR ESTADO REGISTRO 0 a 1

        //ESTRUCTURA TABLA ARCHIVOS
        $columnas  = array( "id_tipo_usuario","nombre_tipo_usuario","estado");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_usuario');
        $bd->setFiltro('1');
        $bd->setAgrupar('id_tipo_usuario');
        $bd->setOrden('id_tipo_usuario');

        // CAMBIAMOS TIPO DE ARCHIVO AL SELECCIONADO
        $valores = array('estado'=>$nuevo_estado);
        $bd->setUpdateValores($valores);
        $bd->setFiltro('id_tipo_usuario='.$id_registro);
        $bd->update();
        $datos = array();
        array_push($datos,array("estado"=>$bd->getResultadoUpdate()));
        echo json_encode($datos);

        break;

    case 4: // UPDATE REGISTRO

        //ESTRUCTURA TABLA ARCHIVOS
        $columnas  = array( "id_tipo_usuario","nombre_tipo_usuario");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_usuario');
        $bd->setAgrupar('id_tipo_usuario');
        $bd->setOrden('id_tipo_usuario');

        // CAMBIAMOS TIPO DE ARCHIVO AL SELECCIONADO
        $valores = array('nombre_tipo_usuario'=>"'".$nombre."'");
        $bd->setUpdateValores($valores);
        $bd->setFiltro('id_tipo_usuario='.$id_registro);
        $bd->update();
        $datos = array();
        array_push($datos,array("estado"=>$bd->getResultadoUpdate()));
        echo json_encode($datos);


        break;

    case 5: // TRAER DATOS PAISES ACTIVAS

        //ESTRUCTURA
        $columnas  = array( "id_tipo_usuario", "nombre_tipo_usuario");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_usuario');
        $bd->setFiltro('estado=1');
        $bd->setAgrupar('id_tipo_usuario');
        $bd->setOrden('id_tipo_usuario DESC');

        $bd->select();
        $bd->ejecutar();
        $datos = array();
        while($row = $bd->resultado())
        {
            array_push($datos,array(
                "id"=>$row['id_tipo_usuario'],
                "nombre"=>$row['nombre_tipo_usuario']
            ));

        }

        echo json_encode($datos);

        break;


    case 6: // TRAER LISTA DE MERCADOS

        //ESTRUCTURA
        $columnas  = array( "id_tipo_usuario", "nombre_tipo_usuario");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_usuario');
        $bd->setFiltro('estado=1');
        $bd->setAgrupar('id_tipo_usuario');
        $bd->setOrden('id_tipo_usuario DESC');

        $bd->select();
        $bd->ejecutar();
        $datos = array();
        while($row = $bd->resultado())
        {
            array_push($datos,array(
                "id"=>$row['id_tipo_usuario'],
                "nombre"=>utf8_encode($row['nombre_tipo_usuario'])
            ));

        }

        echo json_encode($datos);

    break;

}

?>