<?php
include('../../class/config.php');
include('../../class/db.php');

$bd = new db(); //conexion a la base de datos

extract($_REQUEST);

switch($idfuncion) {

    case 1: // TRAER DATOS

        //ESTRUCTURA
        $columnas  = array( "id_nave","nombre_nave","fecha_ingreso_nave","estado");

        $bd->setColumnas($columnas);
        $bd->setTabla('nave');
        $bd->setFiltro('1');
        $bd->setAgrupar('id_nave');
        $bd->setOrden('id_nave DESC');

        $bd->select();
        $bd->ejecutar();
        $datos = array();
        while($row = $bd->resultado())
        {
            array_push($datos,array(
                "id"=>$row['id_nave'],
                "nombre"=>utf8_encode($row['nombre_nave']),
                "fecha"=>$row['fecha_ingreso_nave'],
                "estado"=>$row['estado']
            ));

        }

        echo json_encode($datos);

        break;

    case 2: // INSERTAR REGISTRO

        //ESTRUCTURA TABLA DATOS
        $columnas  = array( "id_nave","nombre_nave");

        $bd->setColumnas($columnas);
        $bd->setTabla('nave');
        $bd->setFiltro('1');
        $bd->setAgrupar('id_nave');
        $bd->setOrden('id_nave');

        $valores = array(0,"'".$nombre."'");
        $bd->setValores($valores);
        $bd->insert();
        $datos = array();
        array_push($datos,array("estado"=>$bd->getResultadoInsert()));
        echo json_encode($datos);


        break;

    case 3: // CAMBIAR ESTADO REGISTRO 0 a 1

        //ESTRUCTURA TABLA ARCHIVOS
        $columnas  = array( "id_nave","nombre_nave");

        $bd->setColumnas($columnas);
        $bd->setTabla('nave');
        $bd->setFiltro('1');
        $bd->setAgrupar('id_nave');
        $bd->setOrden('id_nave');

        // CAMBIAMOS TIPO DE ARCHIVO AL SELECCIONADO
        $valores = array('estado'=>$nuevo_estado);
        $bd->setUpdateValores($valores);
        $bd->setFiltro('id_nave='.$id_registro);
        $bd->update();
        $datos = array();
        array_push($datos,array("estado"=>$bd->getResultadoUpdate()));
        echo json_encode($datos);

        break;

    case 4: // UPDATE REGISTRO

        //ESTRUCTURA TABLA ARCHIVOS
        $columnas  = array( "id_nave","nombre_nave");

        $bd->setColumnas($columnas);
        $bd->setTabla('nave');
        $bd->setAgrupar('id_nave');
        $bd->setOrden('id_nave');

        // CAMBIAMOS TIPO DE ARCHIVO AL SELECCIONADO
        $valores = array('nombre_nave'=>"'".$nombre."'");
        $bd->setUpdateValores($valores);
        $bd->setFiltro('id_nave='.$id_registro);
        $bd->update();
        $datos = array();
        array_push($datos,array("estado"=>$bd->getResultadoUpdate()));
        echo json_encode($datos);


        break;

    case 5: // TRAER DATOS PAISES ACTIVAS

        //ESTRUCTURA
        $columnas  = array( "id_nave", "nombre_nave");

        $bd->setColumnas($columnas);
        $bd->setTabla('nave');
        $bd->setFiltro('estado=1');
        $bd->setAgrupar('id_nave');
        $bd->setOrden('id_nave DESC');

        $bd->select();
        $bd->ejecutar();
        $datos = array();
        while($row = $bd->resultado())
        {
            array_push($datos,array(
                "id"=>$row['id_nave'],
                "nombre"=>$row['nombre_nave']
            ));

        }

        echo json_encode($datos);

        break;


    case 6: // TRAER LISTA DE MERCADOS

        //ESTRUCTURA
        $columnas  = array( "id_nave", "nombre_nave");

        $bd->setColumnas($columnas);
        $bd->setTabla('nave');
        $bd->setFiltro('estado=1');
        $bd->setAgrupar('id_nave');
        $bd->setOrden('id_nave DESC');

        $bd->select();
        $bd->ejecutar();
        $datos = array();
        while($row = $bd->resultado())
        {
            array_push($datos,array(
                "id"=>$row['id_nave'],
                "nombre"=>utf8_encode($row['nombre_nave'])
            ));

        }

        echo json_encode($datos);

        break;

}

?>