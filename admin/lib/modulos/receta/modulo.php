<?php
include('../../class/config.php');
include('../../class/db.php');

$bd2 = new db(); //conexion a la base de datos

extract($_REQUEST);

switch($idfuncion) {

    case 1: // TRAER DATOS
        include('listas.php');

        //ESTRUCTURA
        $columnas  = array( "id_receta","nombre","descripcion","ingredientes","preparacion","url_foto","id_categoria","id_tipo","id_tiempo_preparacion","id_tipo_cocina","id_estacion","id_origen","id_nutricion","pax","id_dificultad","estado","estado_web","estado_app","estado_home");

        $bd2->setColumnas($columnas);
        $bd2->setTabla('receta');
        $bd2->setFiltro('1');
        $bd2->setAgrupar('id_receta');
        $bd2->setOrden('id_receta DESC');

        $bd2->select();
        $bd2->ejecutar();
        $datos = array();
        while($row = $bd2->resultado())
        {
            $imagen_cap = explode('.',$row['url_foto']);
            if(count($imagen_cap)<2)
            {
                $imagen_cap = 'sin_imagen_big.png';
            }else{
                $imagen_cap = $imagen_cap[0].'.'.$imagen_cap[1];
            }

            array_push($datos,array(
                "id"=>base64_encode($row['id_receta']),
                "nombre"=>base64_encode(($row['nombre'])),
                "descripcion"=>base64_encode(($row['descripcion'])),
                "ingredientes"=>base64_encode(($row['ingredientes'])),
                "preparacion"=>base64_encode(($row['preparacion'])),
                "url_foto"=>base64_encode($imagen_cap),
                "id_categoria"=>base64_encode($lista_categoria[$row['id_categoria']]),
                "id_tipo"=>base64_encode($lista_tipo[$row['id_tipo']]),
                "id_tiempo_preparacion"=>base64_encode($lista_tiempo_preparacion[$row['id_tiempo_preparacion']]),
                "id_tipo_cocina"=>base64_encode($lista_tipo_cocina[$row['id_tipo_cocina']]),
                "id_estacion"=>base64_encode($lista_estacion[ $row['id_estacion']]),
                "id_origen"=> base64_encode($lista_origen[$row['id_origen']]),
                "id_nutricion"=>base64_encode($lista_nutricion[ $row['id_nutricion']]),
                "pax"=>base64_encode($row['pax']),
                "id_dificultad"=>base64_encode($lista_dificultad[ $row['id_dificultad']]),
                "estado" => base64_encode($row['estado']),
                "estado_web" => base64_encode($row['estado_web']),
                "estado_app" => base64_encode($row['estado_app']),
                "estado_home" => base64_encode($row['estado_home'])
            ));

        }

        echo json_encode($datos);

        break;

    case 2: // INSERTAR REGISTRO

        //ESTRUCTURA TABLA DATOS
        $columnas  = array( "id_receta","nombre","descripcion","ingredientes","preparacion","url_foto","id_categoria","id_tipo","id_tiempo_preparacion","id_tipo_cocina","id_estacion","id_origen","id_nutricion","pax","id_dificultad");

        $bd2->setColumnas($columnas);
        $bd2->setTabla('receta');
        $bd2->setFiltro('1');
        $bd2->setAgrupar('id_receta');
        $bd2->setOrden('id_receta');

        /*
         *  'nombre': nombre,
        'receta': receta,
        '$url_foto': url_foto,
        '$tiempo_preparacion':tiempo,
        '$tipo_cocina':cocina,
        '$estacion':estacion,
        '$origen':origen,
        '$nutricion':nutricion,
        '$pax':pax,
        '$dificultad':dificultad
         *
         * */

        $valores = array(0,"'".utf8_decode($nombre)."'","'".utf8_decode($descripcion)."'","'".utf8_decode($ingredientes)."'","'".utf8_decode($preparacion)."'","'".$url_foto."'","'".$categoria."'","'".$tipo."'","'".$tiempo_preparacion."'","'".$tipo_cocina."'","'".$estacion."'","'".$origen."'","'".$nutricion."'","'".$pax."'","'".$dificultad."'");
        $bd2->setValores($valores);
        $bd2->insert();
        $datos = array();
        $datos['estado'] = array("estado"=>$bd2->getResultadoInsert(),'sql' => $bd2->consulta);
        echo json_encode($datos);


        break;

    case 3: // CAMBIAR ESTADO REGISTRO 0 a 1

        //ESTRUCTURA TABLA ARCHIVOS
        $columnas  = array( "id_receta","estado");

        $bd2->setColumnas($columnas);
        $bd2->setTabla('receta');
        $bd2->setFiltro('1');
        $bd2->setAgrupar('id_receta');
        $bd2->setOrden('id_receta');

        // CAMBIAMOS TIPO DE ARCHIVO AL SELECCIONADO
        $valores = array($tipo_estado=>$nuevo_estado);
        $bd2->setUpdateValores($valores);
        $bd2->setFiltro('id_receta='.$id_registro);
        $bd2->update();
        $datos = array();
        array_push($datos,array("estado"=>$bd2->getResultadoUpdate()));
        echo json_encode($datos);

        break;

    case 4: // UPDATE REGISTRO

        //ESTRUCTURA TABLA ARCHIVOS
        //$columnas  = array("nombre","receta","url_foto","id_tiempo_preparacion","id_tipo_cocina","id_estacion","id_origen","id_nutricion","pax","id_dificultad");

        //$bd2->setColumnas($columnas);
        $bd2->setTabla('receta');
        $bd2->setAgrupar('id_receta');
        $bd2->setOrden('id_receta');

        // CAMBIAMOS TIPO DE ARCHIVO AL SELECCIONADO
        $valores = array(
            'nombre'=>"'".utf8_decode($nombre)."'",
            'descripcion'=>"'".utf8_decode($descripcion)."'",
            'ingredientes'=>"'".utf8_decode($ingredientes)."'",
            'preparacion'=>"'".utf8_decode($preparacion)."'",
            'url_foto'=>"'".$url_foto."'",
            'id_categoria'=>"'".$categoria."'",
            'id_tipo'=>"'".$tipo."'",
            'id_tiempo_preparacion'=>"'".$tiempo_preparacion."'",
            'id_tipo_cocina'=>"'".$tipo_cocina."'",
            'id_estacion'=>"'".$estacion."'",
            'id_origen'=>"'".$origen."'",
            'id_nutricion'=>"'".$nutricion."'",
            'pax'=>"'".$pax."'",
            'id_dificultad'=>"'".$dificultad."'"
            );
        $bd2->setUpdateValores($valores);
        $bd2->setFiltro('id_receta='.$id);
        $bd2->update();
        $datos = array();
        $datos['estado'] =array("estado"=>$bd2->getResultadoUpdate());
        echo json_encode($datos);


        break;

    case 5: // TRAER LISTA

        //ESTRUCTURA
        $columnas  = array( $id, $nombre);

        $bd2->setColumnas($columnas);
        $bd2->setTabla($tabla);
        $bd2->setFiltro('estado=1');
        $bd2->setAgrupar($id);
        $bd2->setOrden($id.' ASC');

        $bd2->select();
        $bd2->ejecutar();
        $datos = array();
        while($row = $bd2->resultado())
        {
            array_push($datos,array(
                "id"=>base64_encode($row[$id]),
                "nombre"=>base64_encode($row[$nombre])
            ));

        }
        echo json_encode($datos);

        break;


    case 6: // TRAER REGISTRO

        //ESTRUCTURA
        $columnas  = array( "id_receta","nombre","descripcion","ingredientes","preparacion","url_foto","id_categoria","id_tipo","id_tiempo_preparacion","id_tipo_cocina","id_estacion","id_origen","id_nutricion","pax","id_dificultad","estado");

        $bd2->setColumnas($columnas);
        $bd2->setTabla('receta');
        $bd2->setFiltro(' id_receta = '.$id);
        $bd2->setAgrupar('id_receta');
        $bd2->setOrden('id_receta DESC');

        $bd2->select();
        $bd2->ejecutar();
        $datos = array();
        $row = $bd2->resultado();

            $imagen_cap = explode('.',$row['url_foto']);
            if(count($imagen_cap)<2)
            {
                $imagen_cap = 'sin_imagen_big.png';
            }else{
                $imagen_cap = $imagen_cap[0].'.'.$imagen_cap[1];
            }

            array_push($datos,array(
                "id"=>base64_encode($row['id_receta']),
                "nombre"=>utf8_encode(base64_encode($row['nombre'])),
                "descripcion"=>base64_encode($row['descripcion']),
                "ingredientes"=>base64_encode($row['ingredientes']),
                "preparacion"=>base64_encode($row['preparacion']),
                "url_foto"=>base64_encode($imagen_cap),
                "id_categoria"=>base64_encode($row['id_categoria']),
                "id_tipo"=>base64_encode($row['id_tipo']),
                "id_tiempo_preparacion"=>base64_encode($row['id_tiempo_preparacion']),
                "id_tipo_cocina"=>base64_encode($row['id_tipo_cocina']),
                "id_estacion"=>base64_encode($row['id_estacion']),
                "id_origen"=> base64_encode($row['id_origen']),
                "id_nutricion"=> base64_encode($row['id_nutricion']),
                "pax"=>base64_encode($row['pax']),
                "id_dificultad"=>base64_encode($row['id_dificultad']),
                "estado" => base64_encode($row['estado'])
            ));



        echo json_encode($datos);

    break;

}


?>