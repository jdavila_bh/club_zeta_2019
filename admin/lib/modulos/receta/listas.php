<?php
$bd2 = new db(); //conexion a la base de datos

$id = 'id_tiempo_preparacion';
$nombre = 'tiempo_preparacion';
$tabla = 'tiempo_preparacion';

//ESTRUCTURA
$columnas  = array($id, $nombre);

$bd2->setColumnas($columnas);
$bd2->setTabla($tabla);
$bd2->setFiltro('estado=1');
$bd2->setAgrupar($id);
$bd2->setOrden($id.' ASC');

$bd2->select();
$bd2->ejecutar();

$lista_tiempo_preparacion[0] = utf8_decode('Sin Información');

while($row = $bd2->resultado())
{
    $lista_tiempo_preparacion[$row[0]] = ($row[1]);
}

$id = 'id_tipo_cocina';
$nombre = 'tipo_cocina';
$tabla = 'tipo_cocina';

//ESTRUCTURA
$columnas  = array($id, $nombre);

$bd2->setColumnas($columnas);
$bd2->setTabla($tabla);
$bd2->setFiltro('estado=1');
$bd2->setAgrupar($id);
$bd2->setOrden($id.' ASC');

$bd2->select();
$bd2->ejecutar();

$lista_tipo_cocina[0] = utf8_decode('Sin Información');

while($row = $bd2->resultado())
{
    $lista_tipo_cocina[$row[0]] = ($row[1]);
}

$id = 'id_estacion';
$nombre = 'estacion';
$tabla = 'estacion';

//ESTRUCTURA
$columnas  = array($id, $nombre);

$bd2->setColumnas($columnas);
$bd2->setTabla($tabla);
$bd2->setFiltro('estado=1');
$bd2->setAgrupar($id);
$bd2->setOrden($id.' ASC');

$bd2->select();
$bd2->ejecutar();

$lista_estacion[0] = utf8_decode('Sin Información');

while($row = $bd2->resultado())
{
    $lista_estacion[$row[0]] =($row[1]);
}

$id = 'id_origen';
$nombre = 'origen';
$tabla = 'origen';

//ESTRUCTURA
$columnas  = array($id, $nombre);

$bd2->setColumnas($columnas);
$bd2->setTabla($tabla);
$bd2->setFiltro('estado=1');
$bd2->setAgrupar($id);
$bd2->setOrden($id.' ASC');

$bd2->select();
$bd2->ejecutar();

$lista_origen[0] = utf8_decode('Sin Información');

while($row = $bd2->resultado())
{
    $lista_origen[$row[0]] = ($row[1]);
}

$id = 'id_nutricion';
$nombre = 'nutricion';
$tabla = 'nutricion';

//ESTRUCTURA
$columnas  = array($id, $nombre);

$bd2->setColumnas($columnas);
$bd2->setTabla($tabla);
$bd2->setFiltro('estado=1');
$bd2->setAgrupar($id);
$bd2->setOrden($id.' ASC');

$bd2->select();
$bd2->ejecutar();

$lista_nutricion[0] = utf8_decode('Sin Información');

while($row = $bd2->resultado())
{
    $lista_nutricion[$row[0]] =($row[1]);
}


$id = 'id_dificultad';
$nombre = 'dificultad';
$tabla = 'dificultad';

//ESTRUCTURA
$columnas  = array($id, $nombre);

$bd2->setColumnas($columnas);
$bd2->setTabla($tabla);
$bd2->setFiltro('estado=1');
$bd2->setAgrupar($id);
$bd2->setOrden($id.' ASC');

$bd2->select();
$bd2->ejecutar();

$lista_dificultad[0] = utf8_decode('Sin Información');

while($row = $bd2->resultado())
{
    $lista_dificultad[$row[0]] =($row[1]);
}

$id = 'id_categoria';
$nombre = 'categoria';
$tabla = 'categoria';

//ESTRUCTURA
$columnas  = array($id, $nombre);

$bd2->setColumnas($columnas);
$bd2->setTabla($tabla);
$bd2->setFiltro('estado=1');
$bd2->setAgrupar($id);
$bd2->setOrden($id.' ASC');

$bd2->select();
$bd2->ejecutar();

$lista_categoria[0] = utf8_decode('Sin Información');

while($row = $bd2->resultado())
{
    $lista_categoria[$row[0]] =($row[1]);
}


$id = 'id_tipo';
$nombre = 'tipo';
$tabla = 'tipo';

//ESTRUCTURA
$columnas  = array($id, $nombre);

$bd2->setColumnas($columnas);
$bd2->setTabla($tabla);
$bd2->setFiltro('estado=1');
$bd2->setAgrupar($id);
$bd2->setOrden($id.' ASC');

$bd2->select();
$bd2->ejecutar();

$lista_tipo[0] = utf8_decode('Sin Información');

while($row = $bd2->resultado())
{
    $lista_tipo[$row[0]] =($row[1]);
}
