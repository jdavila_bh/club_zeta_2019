<?php
include('../../class/config.php');
include('../../class/db.php');

$bd = new db(); //conexion a la base de datos

extract($_REQUEST);

switch($idfuncion) {

    case 1: // TRAER DATOS

        //ESTRUCTURA
        $columnas  = array( "id_tipo_cocina","tipo_cocina","fecha_ingreso","estado");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_cocina');
        $bd->setFiltro('1');
        $bd->setAgrupar('id_tipo_cocina');
        $bd->setOrden('id_tipo_cocina DESC');

        $bd->select();
        $bd->ejecutar();
        $datos = array();
        while($row = $bd->resultado())
        {
            array_push($datos,array(
                "id"=>$row['id_tipo_cocina'],
                "nombre"=>utf8_encode($row['tipo_cocina']),
                "fecha"=>$row['fecha_ingreso'],
                "estado"=>$row['estado']
            ));

        }

        echo json_encode($datos);

    break;

    case 2: // INSERTAR REGISTRO

        //ESTRUCTURA TABLA DATOS
        $columnas  = array( "id_tipo_cocina","tipo_cocina");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_cocina');
        $bd->setFiltro('1');
        $bd->setAgrupar('id_tipo_cocina');
        $bd->setOrden('id_tipo_cocina');

        $valores = array(0,"'".utf8_decode($nombre)."'");
        $bd->setValores($valores);
        $bd->insert();
        $datos = array();
        array_push($datos,array("estado"=>$bd->getResultadoInsert()));
        echo json_encode($datos);


        break;

    case 3: // CAMBIAR ESTADO REGISTRO 0 a 1

        //ESTRUCTURA TABLA ARCHIVOS
        $columnas  = array( "id_tipo_cocina","tipo_cocina","estado");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_cocina');
        $bd->setFiltro('1');
        $bd->setAgrupar('id_tipo_cocina');
        $bd->setOrden('id_tipo_cocina');

        // CAMBIAMOS TIPO DE ARCHIVO AL SELECCIONADO
        $valores = array('estado'=>$nuevo_estado);
        $bd->setUpdateValores($valores);
        $bd->setFiltro('id_tipo_cocina='.$id_registro);
        $bd->update();
        $datos = array();
        array_push($datos,array("estado"=>$bd->getResultadoUpdate()));
        echo json_encode($datos);

        break;

    case 4: // UPDATE REGISTRO

        //ESTRUCTURA TABLA ARCHIVOS
        $columnas  = array( "id_tipo_cocina","tipo_cocina");

        $bd->setColumnas($columnas);
        $bd->setTabla('tipo_cocina');
        $bd->setAgrupar('id_tipo_cocina');
        $bd->setOrden('id_tipo_cocina');

        // CAMBIAMOS TIPO DE ARCHIVO AL SELECCIONADO
        $valores = array('tipo_cocina'=>"'".utf8_decode($nombre)."'");
        $bd->setUpdateValores($valores);
        $bd->setFiltro('id_tipo_cocina='.$id_registro);
        $bd->update();
        $datos = array();
        array_push($datos,array("estado"=>$bd->getResultadoUpdate()));
        echo json_encode($datos);


        break;


}

?>