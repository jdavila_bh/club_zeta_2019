<?php
session_start();
include('../../class/config.php');
include('../../class/db.php');

$bd = new db(); //conexion a la base de datos

extract($_REQUEST);


switch($_POST['idfuncion'])  
{

	case "eventos":
            $idtabla = date('YmdHis');
            $html='';
        $html.= utf8_decode('<script>$("#jq-datatables-'.$idtabla.'").dataTable({"language": {"lengthMenu": "Por página: _MENU_","zeroRecords": "Sin resultados","info": "Mostrando página PAGE de _PAGES_","infoEmpty": "No hay registros disponibles","infoFiltered": "(Filtrado de MAX registros en total)"}});$("#jq-datatables-'.$idtabla.'_wrapper .table-caption").text("");    $("#jq-datatables-'.$idtabla.'_wrapper .dataTables_filter input").attr("placeholder", "Buscar...");</script>');

            $html.= utf8_decode('<div class="table-primary">
                  <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-'.$idtabla.'">
                    <thead>
                    <tr>
                        <th scope="col">Horario</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido Paterno</th>
                        <th scope="col">Apellido Materno</th>
                        <th scope="col">Rut</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Región</th>
                        <th scope="col">Comuna</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Referencia</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">IP</th>
                        <th scope="col">Navegador</th>
                        <th scope="col">Evento</th>
                        <th scope="col">Fecha_evento</th>
                    </tr>
                    </thead>
                    <tbody>');
                    $columnas  = array( "id",
                        "horario",
                        "nombre",
                        "apellidoPaterno",
                        "apellidoMaterno",
                        "rut",
                        "direccion",
                        "region",
                        "comuna",
                        "telefono",
                        "email",
                        "referencia",
                        "fecha",
                        "ip",
                        "navegador",
                        "nombre_evento",
                        "fecha_evento");

                    $bd->setColumnas($columnas);
                    $bd->setTabla('eventos_registros');
                    $bd->setFiltro('1');
                    $bd->setAgrupar('id');
                    $bd->setOrden('fecha DESC');

                    $bd->select();
                    $bd->ejecutar();

                    while($row = $bd->resultado())
                    {
                        $html.= '<tr id="fila_'.$row[0].'">
                              <td>'.($row[1]).'</td>
                              <td>'.($row[2]).'</td>
                              <td>'.($row[3]).'</td>
                              <td>'.($row[4]).'</td>
                              <td>'.($row[5]).'</td>
                              <td>'.($row[6]).'</td>
                              <td>'.($row[7]).'</td>
                              <td>'.($row[8]).'</td>
                              <td>'.($row[9]).'</td>
                              <td>'.($row[10]).'</td>
                              <td>'.($row[11]).'</td>
                              <td>'.($row[12]).'</td>
                              <td>'.($row[13]).'</td>
                              <td>'.($row[14]).'</td>
                              <td>'.($row[15]).'</td>
                              <td>'.($row[16]).'</td>
                             </tr>';
                    }

                $html.= '</tbody></table></div>';

            echo base64_encode($html);





    break;

    case "eventos_busqueda":
        $idtabla = date('YmdHis');
        $html='';
        $html.= utf8_decode('<script>$("#jq-datatables-'.$idtabla.'").dataTable({"language": {"lengthMenu": "Por página: _MENU_","zeroRecords": "Sin resultados","info": "Mostrando página PAGE de _PAGES_","infoEmpty": "No hay registros disponibles","infoFiltered": "(Filtrado de MAX registros en total)"}});$("#jq-datatables-'.$idtabla.'_wrapper .table-caption").text("");    $("#jq-datatables-'.$idtabla.'_wrapper .dataTables_filter input").attr("placeholder", "Buscar...");</script>');

        $html.= utf8_decode('<div class="table-primary">
                  <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-'.$idtabla.'">
                    <thead>
                    <tr>
                        <th scope="col">Horario</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido Paterno</th>
                        <th scope="col">Apellido Materno</th>
                        <th scope="col">Rut</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Región</th>
                        <th scope="col">Comuna</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Referencia</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">IP</th>
                        <th scope="col">Navegador</th>
                        <th scope="col">Evento</th>
                        <th scope="col">Fecha_evento</th>
                    </tr>
                    </thead>
                    <tbody>');
        $columnas  = array( "id",
            "horario",
            "nombre",
            "apellidoPaterno",
            "apellidoMaterno",
            "rut",
            "direccion",
            "region",
            "comuna",
            "telefono",
            "email",
            "referencia",
            "fecha",
            "ip",
            "navegador",
            "nombre_evento",
            "fecha_evento");

        if((empty($_POST['fecha1']))&&(empty($_POST['fecha2']))){
            $filtro_fecha='';
        }else{
            $fecha_sep1 = explode('/',$_POST['fecha1']);
            $fecha_sep2 = explode('/',$_POST['fecha2']);

            $fecha1_t = $fecha_sep1[2].'-'.$fecha_sep1[1].'-'.$fecha_sep1[0].' 00:00:00';
            $fecha2_t = $fecha_sep2[2].'-'.$fecha_sep2[1].'-'.$fecha_sep2[0].' 23:59:59';

            $filtro_fecha=" AND (fecha>='".$fecha1_t."' AND fecha<='".$fecha2_t."')";
        }

        //echo  $filtro_fecha;
        $bd->setColumnas($columnas);
        $bd->setTabla('eventos_registros');
        $bd->setFiltro('1 '.$filtro_fecha);
        $bd->setAgrupar('id');
        $bd->setOrden('fecha DESC');

        $bd->select();
        $bd->ejecutar();
        //echo $bd->consulta;

        while($row = $bd->resultado())
        {
            $html.= '<tr id="fila_'.$row[0].'">
                              <td>'.($row[1]).'</td>
                              <td>'.($row[2]).'</td>
                              <td>'.($row[3]).'</td>
                              <td>'.($row[4]).'</td>
                              <td>'.($row[5]).'</td>
                              <td>'.($row[6]).'</td>
                              <td>'.($row[7]).'</td>
                              <td>'.($row[8]).'</td>
                              <td>'.($row[9]).'</td>
                              <td>'.($row[10]).'</td>
                              <td>'.($row[11]).'</td>
                              <td>'.($row[12]).'</td>
                              <td>'.($row[13]).'</td>
                              <td>'.($row[14]).'</td>
                              <td>'.($row[15]).'</td>
                              <td>'.($row[16]).'</td>
                             </tr>';
        }

        $html.= '</tbody></table></div>';

        echo base64_encode($html);





        break;


}


?>      
