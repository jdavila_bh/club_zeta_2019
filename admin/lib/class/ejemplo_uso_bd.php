<?php
require_once('config.php');
require_once('db.php');

//CONEXION
$bd = new db();
//$bd->getDatos();


//ESTRUCTURA
$columnas  = array( "id_dic", "nombre_dic", "detalle_dic", "estado");

$bd->setColumnas($columnas);
$bd->setTabla('diccionario');
$bd->setFiltro('1');
$bd->setAgrupar('id_dic');
$bd->setOrden('id_dic');

$bd->select();
$bd->ejecutar();


//UPDATE VALORES

$valores = array('nombre_dic'=>"'update'",'detalle_dic'=>"'cambiando un valor'");

$bd->setUpdateValores($valores);
$bd->setFiltro('id_dic=1');
$bd->update();
echo $bd->resultadoUpdate;
echo $bd->getUpdateValores();


// BORRAR UN REGISTRO
//$bd->setFiltro('id_dic>0');
//$bd->delete();


// INSERTAR UN REGISTRO
$valores = array(0,"'Paul'","'prueba'",1);

$bd->setValores($valores);
$bd->insert();

//echo $bd->resultado;
//echo $bd->resultadoInsert;


// VER REGISTROS
//echo $bd->num_registros;
//echo var_dump($bd->registros);
/*
while($row = $bd->resultado())
{
    echo 'Clave: '.$row[0].' Valor: '.$row['nombre_dic'].'</br>';

}

// VER CONSULTA SELECT
echo $bd->getSelect();

*/




