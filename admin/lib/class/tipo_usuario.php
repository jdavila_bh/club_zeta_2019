<?php
//include('config.php');
//include('db.php');
//include('tools.php');


class tipoUsuario extends tools{

    public $id;
    public $nombre_tipo;

    public $estado_tipo;

    public function __construct($id){

        $this->id = $id;
        $this->nombre_tipo = '';

        $this->obtenerDatos();
    }

    public function obtenerDatos(){

        $bd = new db();

        // TABLA RECETA
        $columnas_tabla  = array( "id_tipo_usuario","nombre_tipo_usuario");

        $bd->setColumnas($columnas_tabla); // Columna de la tabla
        $bd->setTabla('tipo_usuario'); // Tabla Receta
        $bd->setFiltro('id_tipo_usuario='.$this->id);

        $bd->setAgrupar('id_tipo_usuario');
        $bd->setOrden('id_tipo_usuario ASC');

        $bd->select();
        $bd->ejecutar();

        if($bd->num_registros>0) {
            while ($row = $bd->resultado()) {
                $this->nombre_tipo = $row['nombre_tipo_usuario'];
            }
            $this->estado_tipo=1;
        }else{
            $this->estado_tipo=0;
        }

    }


    public function getNombreTipo(){
        return array("estado_tipo"=>$this->nombre_tipo);
    }


}

//$tipo_user = new tipoUsuario(1);
//var_dump($tipo_user->getNombreTipo());

