<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 17/03/2017
 * Time: 17:11
 */

class request_url {


    private $url;
    private $data;
    private $response;

    function __construct($url,$data)
    {
        $this->url = $url;
        $this->data = $data;
        $this->response = array();
    }


    public function request()
    {
        //url contra la que atacamos
        $ch = curl_init($this->getUrl());
        //a true, obtendremos una respuesta de la url, en otro caso,
        //true si es correcto, false si no lo es
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //establecemos el verbo http que queremos utilizar para la petición
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        //enviamos el array data CURLOPT_URL
        curl_setopt($ch, CURLOPT_POSTFIELDS,@http_build_query($this->getData(), '', '&'));
        //obtenemos la respuesta
        $response = curl_exec($ch);
        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($ch);

        $this->setResponse($response);
    }

    public function getResponseJson()
    {
        return json_encode(json_decode($this->getResponse(),true));
    }

    public function getResponseArray()
    {
        return json_decode($this->getResponse(),true);
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

}

/*
$data = array(
    'address'=> 'airport',
    'country'=> 'Chile',
    'city'=> 'Santiago'
);

$request_url = new request_url('http://54.163.117.128/api/index.php/validateAddress/checkAddress',$data);

$request_url->request();

var_dump($request_url->getResponseArray());
*/
