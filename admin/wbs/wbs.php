<?php
session_start();
header('Content-Type: application/json');
include_once('../lib/class/config.php');
//var_dump($_REQUEST);

 $url_api = URL_API.$_REQUEST['modulo'].'/'.$_REQUEST['metodo'].'/'.$_REQUEST['id_reg'];

 //datos a enviar
 
 //url contra la que atacamos
 $ch = curl_init($url_api);
 //a true, obtendremos una respuesta de la url, en otro caso, 
 //true si es correcto, false si no lo es
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 //establecemos el verbo http que queremos utilizar para la petición
 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $_SERVER['REQUEST_METHOD']);
 //enviamos el array data CURLOPT_URL
 curl_setopt($ch, CURLOPT_POSTFIELDS,@http_build_query($_REQUEST['data'], '', '&'));
 //obtenemos la respuesta
 $response = curl_exec($ch);
 // Se cierra el recurso CURL y se liberan los recursos del sistema
 curl_close($ch);

 $datos_response = json_decode($response,true);

if($datos_response['estado']==1&&$_REQUEST['metodo']=='verificarUsuario')
{
    $_SESSION['sesion_usuario_activa'] = $datos_response['sesionActiva'];
    $_SESSION['sesion_usuario_activa_estado'] = 1;
}

 // RESPONSE
 echo $response;
 //var_dump($response);
?>
