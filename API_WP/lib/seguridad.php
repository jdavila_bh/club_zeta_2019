<?php
$metodo = strtoupper($_SERVER['REQUEST_METHOD']);
//Revisa si el token generado coincide con el de sesion
if($metodo=='POST'||$metodo=='GET')
{
	if(($_SESSION['token_app']!=$_REQUEST['token_app'])||empty($_REQUEST['token_app']))
	{	
		$datos = array();
		
		$datos = array(
		'codigo' 	=> 200,
		'estado' 	=> 0,
		'mensaje' 	=> 'Debe autentificar app, necesita activar token',
		'pasa' =>$_GET,
		'token pass' => $_SESSION['token_app']
		
		);
		
		echo json_encode($datos);	
		exit();
	}
}else{
	
	$headers = apache_request_headers();// Trae datos de la cabecera	
	if(($_SESSION['token_app']!=$headers['token_app'])||empty($headers['token_app']))
	{	
		$datos = array();
		
		$datos = array(
		'codigo' 	=> 200,
		'estado' 	=> 0,
		'mensaje' 	=> 'Debe autentificar app, necesita activar token',
		'token params' =>$headers['token_app']
		
		);
		
		echo json_encode($datos);	
		exit();
	}
	
}


?>