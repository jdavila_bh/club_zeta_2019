<?php
//include('config.php');
//include('db.php');
//include('tools.php');

class login extends tools {
    
    public $user;
    public $pass;
    
    public $id;
    public $nombre;
    public $apellido;
    public $email;
    public $rut;
    public $id_tipo;
    public $fecha_creacion;
    public $fecha_modificacion;

    public $estado_usuario;
    
    public function __construct($user,$pass)
    {
        $this->user = $user;
        $this->pass = $pass;

        $this-> id='';
        $this-> nombre='';
        $this-> apellido='';
        $this-> email='';
        $this-> rut='';
        $this-> id_tipo='';
        $this-> fecha_creacion='';
        $this-> fecha_modificacion='';

        $this->estado_usuario=0;

        $this->obtenerDatos();
    }
    
    public function obtenerDatos(){
        $bd = new db();
        
        // TABLA RECETA
        $columnas_tabla  = array( "id_usuario", "nombre_usuario", "apellido_usuario", "email_usuario", "user_usuario", "pass_usuario", "rut_usuario", "id_tipo_usuario", "fecha_creacion_usuario", "fecha_modificacion_usuario");

        $bd->setColumnas($columnas_tabla); // Columna de la tabla
        $bd->setTabla('usuario'); // Tabla Receta
        $bd->setFiltro('user_usuario="'.$this->user.'" AND pass_usuario="'.$this->pass.'"');
        $bd->setAgrupar('id_usuario');
        $bd->setOrden('id_usuario ASC');

        $bd->select();
        $bd->ejecutar();

        if($bd->num_registros>0) {
            while ($row = $bd->resultado()) {
                $this->id = $row['id_usuario'];
                $this->nombre = $row['nombre_usuario'];
                $this->apellido = $row['apellido_usuario'];
                $this->email = $row['email_usuario'];
                $this->rut = $row['rut_usuario'];
                $this->id_tipo = $row['id_tipo_usuario'];
                $this->fecha_creacion = $row['fecha_creacion_usuario'];
                $this->fecha_modificacion = $row['fecha_modificacion_usuario'];
            }
            $this->estado_usuario=1;
        }else{
            $this->estado_usuario=0;
        }
    }
    
    public function getDatos(){
        return array(   "id"=>$this->id,
                        "nombre"=>$this->nombre,
                        "apellido"=>$this->apellido,
                        "email"=>$this->email,
                        "rut"=>$this->rut,
                        "id_tipo"=>$this->id_tipo,
                        "fecha_creacion"=>$this->fecha_creacion,
                        "fecha_modificacion"=>$this->fecha_modificacion,
                        "user"=>$this->user,
                        "pass"=>$this->pass
                    );
    }

    public function iniciarSesion(){
        $_SESSION['id_usuario_sesion']=$this->id;
        $_SESSION['nombre_usuario_sesion']=$this->nombre;
        $_SESSION['apellido_usuario_sesion']=$this->apellido;
        $_SESSION['id_tipo_usuario_sesion']=$this->id_tipo;
        $_SESSION['online_usuario_sesion']=true;
    }
    
    
    

}    

//$login = new login('admin','admin');
//$usuario = $login->getDatos();
//var_dump($usuario);



	
?>