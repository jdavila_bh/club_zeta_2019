<?php

class tools{

    public $id= array();

    public $cantidad_registros = 0;
    public $last_key = 0;
    public $signo_acortar_texto = '...';

    public function __construct(){
    }

    // SET

    public function setCantidadRegistros(){

        $this->cantidad_registros = count($this->id);
    }

    public function setSignoAcortarTexto($signo){

        $this->signo_acortar_texto = $signo;

    }


    // GET

    public function getCantidadRegistros(){
        return $this->cantidad_registros;
    }

    public function getkey($key){
        return array_search($key,$this->id);
    }

    public function getIds(){
        return $this->id;
    }

    public function getLastKey(){

        return $this->last_key;
    }

    public function getSignoAcortarTexto(){

        return $this->signo_acortar_texto;
    }

    public function cortarNombre($nombre,$largo){

        if(strlen($nombre)>$largo)
        {
            return substr($nombre,0,$largo).$this->getSignoAcortarTexto();
        }

    }

    // Funciones


    public function explode($delimitador,$texto)
    {
        return explode($delimitador,$texto);

    }

    public function implode($delimitador,$texto)
    {
        return implode($delimitador,$texto);

    }

    public function validarRut($rut){

        if(strpos($rut,"-")==false){
            $RUT[0] = substr($rut, 0, -1);
            $RUT[1] = substr($rut, -1);
        }else{
            $RUT = explode("-", trim($rut));
        }
        $elRut = str_replace(".", "", trim($RUT[0]));
        $factor = 2;
        for($i = strlen($elRut)-1; $i >= 0; $i--):
            $factor = $factor > 7 ? 2 : $factor;
            $suma += $elRut{$i}*$factor++;
        endfor;
        $resto = $suma % 11;
        $dv = 11 - $resto;
        if($dv == 11){
            $dv=0;
        }else if($dv == 10){
            $dv="k";
        }else{
            $dv=$dv;
        }
        if($dv == trim(strtolower($RUT[1]))){
            return 1;
        }else{
            return 0;
        }

    }


    public function validarEmail($email){

        $Sintaxis='#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
        if(preg_match($Sintaxis,$email))
            return true;
        else
            return false;

    }


}