<?php

/**
 * @author     Allware Ltda. (http://www.allware.cl)
 * @copyright  2015 Transbank S.A. (http://www.tranbank.cl)
 * @date       Jan 2015
 * @license    GNU LGPL
 * @version    2.0.1
 */

/** Contiene datos de integración para realizar pruebas de conectividad */
return $certificate = array(

    /** Ambiente */ //
    "environment" => ENVIRONMENT, // INTEGRACION - CERTIFICACION - PRODUCCION
    
    /** Llave Privada */
    "private_key" => PRIVATE_KEY,

    /** Certificado Publico */
    "public_cert" => PUBLIC_CERT,

    /** Certificado Server */
    "webpay_cert" => WEBPAY_CERT,

    /** Codigo Comercio */
    "commerce_code" => COMMERCE_CODE, // ID CERTIFICACION 597020000541

);
