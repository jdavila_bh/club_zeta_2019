<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 09/01/2017
 * Time: 11:08
 */

class usuario extends db{

    private $id;
    private $nombre;
    private $apellido;
    private $rut;
    private $empresa;

    private $id_region;
    private $id_ciudad;
    private $id_comuna;

    private $codigo_postal;
    private $direccion;
    private $telefono;
    private $email;
    private $clave;
    private $estado;
    private $fecha_ingreso;

    private $campos;


    function __construct()
    {
        $this->id = 0;
        $this->nombre = '';
        $this->apellido = '';
        $this->rut = '';
        $this->empresa = '';

        $this->id_region = 0;
        $this->id_ciudad = 0;
        $this->id_comuna = 0;

        $this->codigo_postal = '';
        $this->direccion = '';
        $this->telefono = '';
        $this->email = '';
        $this->clave = '';
        $this->estado = 0;
        $this->fecha_ingreso = '';

        $this->campos = [
            'id_usuario',
            'nombre_usuario',
            'apellido_usuario',
            'rut_usuario',
            'empresa_usuario',
            'id_region',
            'id_ciudad',
            'id_comuna',
            'codigo_postal',
            'direccion_usuario',
            'telefono_usuario',
            'email_usuario',
            'clave_usuario',
            'estado_usuario',
            'fecha_registro'
        ];
    }


    public function traerRegistroUsuario($id)
    {
        $bd = new db();

        //ESTRUCTURA TABLA DATOS
        $columnas  = $this->campos;

        $bd->setColumnas($columnas);
        $bd->setTabla('usuario');
        $bd->setFiltro('id_usuario='.$id);
        $bd->setAgrupar('id_usuario');
        $bd->setLimit(0,1);
        $bd->setOrden('id_usuario DESC');

        $bd->select();
        $bd->ejecutar();

        $datos = array();
        if($bd->num_registros>0)
        {
            $row = $bd->resultado();
        }else{

        }




    }

    public function setRegistroUsuario($usuario)
    {

    }

    /**
     * @return mixed
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param mixed $fecha_ingreso
     */
    public function setFechaIngreso($fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * @param mixed $apellido
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    /**
     * @return mixed
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * @param mixed $rut
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return mixed
     */
    public function getIdRegion()
    {
        return $this->id_region;
    }

    /**
     * @param mixed $id_region
     */
    public function setIdRegion($id_region)
    {
        $this->id_region = $id_region;
    }

    /**
     * @return mixed
     */
    public function getIdCiudad()
    {
        return $this->id_ciudad;
    }

    /**
     * @param mixed $id_ciudad
     */
    public function setIdCiudad($id_ciudad)
    {
        $this->id_ciudad = $id_ciudad;
    }

    /**
     * @return mixed
     */
    public function getIdComuna()
    {
        return $this->id_comuna;
    }

    /**
     * @param mixed $id_comuna
     */
    public function setIdComuna($id_comuna)
    {
        $this->id_comuna = $id_comuna;
    }

    /**
     * @return mixed
     */
    public function getCodigoPostal()
    {
        return $this->codigo_postal;
    }

    /**
     * @param mixed $codigo_postal
     */
    public function setCodigoPostal($codigo_postal)
    {
        $this->codigo_postal = $codigo_postal;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * @param mixed $clave
     */
    public function setClave($clave)
    {
        $this->clave = $clave;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }




}