<?php
include('../conex.php');
date_default_timezone_set('America/Santiago');
$metodo_fun = array(
		'init' 	    => 'POST',
		'getResult' => 'POST'
);

if($metodo!=$metodo_fun[$funcion_modulo])
{
	$datos = array(
		'codigo' => 404,
		'estado' => 0,
		'mensaje'=> 'Metodo no registrado en el modulo.',
		'metodo' => $funcion_modulo,
		'method' => $metodo
	);	
}else{

extract($_REQUEST);


/** Configuracion parametros de la clase Webpay */
$sample_baseurl = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

$configuration = new Configuration();
$configuration->setEnvironment($certificate['environment']);
$configuration->setCommerceCode($certificate['commerce_code']);
$configuration->setPrivateKey($certificate['private_key']);
$configuration->setPublicCert($certificate['public_cert']);
$configuration->setWebpayCert($certificate['webpay_cert']);

/** Creacion Objeto Webpay */
$webpay = new Webpay($configuration);

//$action = isset($_GET["action"]) ? $_GET["action"] : 'init';

$post_array = false;
$datos = array();
$estado_oc_activa = 0; // 1 se puede usar - 0 no se puede usar // DEJAR EN 1 PARA NO VERIFICAR ESTADO OC
$estado_validar_oc = 1; // 1 validar oc en BD - 0 no validar oc en BD // ACTIVAR PARA VALIDAR OC
extract($_REQUEST);

switch ($funcion_modulo) {

    case 'init': //PASO 1

        $tx_step = "Init";

        /** Monto de la transacción */
        $amount = $monto_orden_compra;

        /** Orden de compra de la tienda */
        $buyOrder = $id_orden_compra;

        /** Código comercio de la tienda entregado por Transbank */
        $sessionId = uniqid();
        
        /** URL de retorno */
        $urlReturn = $url_return.CIERRE;
        
        /** URL Final */
	    $urlFinal  = $url_final.EXITO.'?orden='.$buyOrder;

        /* TIPO TRANSACCION */
        $wSTransactionType = 'TR_NORMAL_WS';

        /*
        $request = array(
            "wSTransactionType" => $wSTransactionType,
            "commerceCode" => $configuration->getCommerceCode(),
            "buyOrder"  => $buyOrder,
            "sessionId" => $sessionId,
            "urlReturn" => $urlReturn,
            "urlFinal"  => $urlFinal,
            "transactionDetails" => [
                'sharesAmount' => 'null',
                'sharesNumber' => 'null',
                'amount' => $amount,
                'commerceCode' => $configuration->getCommerceCode(),
                'buyOrder' => $buyOrder
            ]

        );*/

        /* VALIDAR OC OCUPADA */
        if($estado_validar_oc==1)
        {
        //$query_val = "SELECT id_orden_compra FROM orden_compra_wbs WHERE ocupado_orden_compra='' AND id_orden_compra=".$buyOrder; // CERTIFICACION
        $query_val = "SELECT id_orden_compra FROM orden_compra WHERE ocupado_orden_compra='' AND id_orden_compra=".$buyOrder; // PRODUCCION
        $result = mysql_query($query_val,$link);
        if((int)@mysql_num_rows($result)>0)
        {

            //$query_activar="UPDATE orden_compra_wbs SET ocupado_orden_compra=0 WHERE id_orden_compra=".$buyOrder; //CERTIFICACION
            $query_activar="UPDATE orden_compra SET ocupado_orden_compra=0 WHERE id_orden_compra=".$buyOrder; //PRODUCCION
            if(mysql_query($query_activar,$link))
            {
                $estado_oc_activa=1;
            }else{
                $estado_oc_activa=0;
            }

        }else{
            $estado_oc_activa=0;
        }
        }
        /* FIN - VALIDAR OC OCUPADA */

        if($estado_oc_activa==1)
        {
            /** Iniciamos Transaccion */

            $result = $webpay->getNormalTransaction()->initTransaction($wSTransactionType,$amount, $buyOrder, $sessionId, $urlReturn, $urlFinal);

            @log_wbs($buyOrder.'InitRequest',print_r($webpay->getNormalTransaction()->getInitTransaction(), TRUE));
            @log_wbs($buyOrder.'InitResponse',print_r($result, TRUE));

            /** Verificamos respuesta de inicio en webpay */
            if (!empty($result->token) && isset($result->token)) {
                $datos['msg'] = "Sesion iniciada con exito en Webpay";
                $datos['token'] = $result->token;
                $datos['url'] = $result->url;
                $datos['estado'] = 1;
                $datos['html'] = base64_encode('<form id="webPayWbs" action="'.$result->url.'" method="post"><input type="hidden" name="token_ws" value="'.$result->token.'"></form>');
            } else {
                $datos['msg'] = "Webpay no disponible";
                $datos['html'] = base64_encode('<form id="webPayWbs" action="compra_online2.php?orden='.$buyOrder.'" method="post"></form>');
                $datos['estado'] = 2;
            }
        }else{
            $datos['msg'] = "OC no disponible";
            $datos['html'] = base64_encode('<form id="webPayWbs" action="compra_online2.php?orden='.$buyOrder.'" method="post"></form>');
            $datos['estado'] = 2;
        }
        
        break;

    case 'getResult': //PASO 2

        //if (!isset($_POST["token_ws"]))
        //    break;

        /** Token de la transacción */
        //$token = filter_input(INPUT_POST, 'token_ws');
        
        //$request = array(
         //   "token" => filter_input(INPUT_POST, 'token_ws')
        //);

        /** Rescatamos resultado y datos de la transaccion */
        $result = $webpay->getNormalTransaction()->getTransactionResult($token_ws);

        @log_wbs($result->buyOrder.'NormalTransactionRequest',print_r(array('tokenInput' =>$token_ws), TRUE));
        @log_wbs($result->buyOrder.'NormalTransactionResponse',print_r($result, TRUE));

        @log_wbs($result->buyOrder.'acknowledgeTransactionRequest',print_r(array('tokenInput' => $webpay->getNormalTransaction()->getRequestAcknowledgeTransaction()), TRUE));
        @log_wbs($result->buyOrder.'acknowledgeTransactionResponse',print_r($webpay->getNormalTransaction()->getResponseAcknowledgeTransaction(), TRUE));
        
        /** Verificamos resultado  de transacción */
        if ($result->detailOutput->responseCode === 0) {

            /** propiedad de HTML5 (web storage), que permite almacenar datos en nuestro navegador web */
            $script = '';
            $script .= '<script>window.localStorage.clear();</script>';
            $script .= '<script>localStorage.setItem("authorizationCode", '.$result->detailOutput->authorizationCode.')</script>';
            $script .= '<script>localStorage.setItem("amount", '.$result->detailOutput->amount.')</script>';
            $script .= '<script>localStorage.setItem("buyOrder", '.$result->buyOrder.')</script>';

            $datos['script'] =base64_encode($script);
            $datos['msg'] ="ACEPTADO";
            $datos['url'] =$result->urlRedirection;
            $datos['estado'] =1;
            $datos['result'] =$result;

        } else {
            $datos['msg'] = "RECHAZADO";
            $datos['estado'] =0;
            $datos['result'] =$result;
        }


        break;
    
    case "end": //PASO 3
        
        $post_array = true;

        $tx_step = "End";
        $request = "";
        $result = $_POST;
        
        $message = "Transacion Finalizada";
        $next_page = $sample_baseurl."?action=nullify";
        $button_name = "Anular Transacci&oacute;n &raquo;";

        break;   

    
    case "nullify":

        $tx_step = "nullify";
        
        $request = $_POST;
        
        /** Codigo de Comercio */
        $commercecode = null;

        /** Código de autorización de la transacción que se requiere anular */
        $authorizationCode = filter_input(INPUT_POST, 'authorizationCode');

        /** Monto autorizado de la transacción que se requiere anular */
        $amount =  filter_input(INPUT_POST, 'amount');

        /** Orden de compra de la transacción que se requiere anular */
        $buyOrder =  filter_input(INPUT_POST, 'buyOrder');
        
        /** Monto que se desea anular de la transacción */
        $nullifyAmount = 200;

        $request = array(
            "authorizationCode" => $authorizationCode, // Código de autorización
            "authorizedAmount" => $amount, // Monto autorizado
            "buyOrder" => $buyOrder, // Orden de compra
            "nullifyAmount" => $nullifyAmount, // idsession local
            "commercecode" => $configuration->getCommerceCode(), // idsession local
        );
        
        $result = $webpay->getNullifyTransaction()->nullify($authorizationCode, $amount, $buyOrder, $nullifyAmount, $commercecode);
        
        /** Verificamos resultado  de transacción */
        if (!isset($result->authorizationCode)) {
            $message = "webpay no disponible";
        } else {
            $message = "Transaci&oacute;n Finalizada";
        }

        $next_page = '';
        
        break;
}

}

function log_wbs($archivo,$datos){
    $myfile = fopen('log/'.$archivo.".txt", "a") or die("no se puede abrir el archivo");
    $txt = $datos;
    fwrite($myfile, "\n". $txt);
    fclose($myfile);
}



?>
