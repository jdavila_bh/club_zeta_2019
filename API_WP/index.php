<?php
session_start();
//CONFIG WEBPAY
include('configWP.php');

require_once('lib/class/libwebpay/webpay.php');
require_once('lib/class/certificates/cert-normal.php');
include('lib/header.php');
include('lib/conex.php');
include('lib/log_request.php');
include('lib/class/PHPMailer-compress/class.phpmailer.php');
//include('lib/seguridad.php');



$datos = array(); // RESPONSE

$peticion = explode('/',$_GET['PATH_INFO']); // CAPTURAMOS RUTA
$funcion_modulo = $peticion[1]; // METODO DEL MODULO
$id_reg_cap = $peticion[2]; // REGISTRO
// Obtener recurso
$recurso = array_shift($peticion); // SACAMOS EL RECURSO QUE APUNTA // MODULO
// MODULOS
$recursos_existentes = array(
    'webpay'
    );

// Comprobar si existe el recurso
if(!in_array($recurso, $recursos_existentes)){

    $datos['status'] = array(
        'codigo'=>405,
        'mensaje'=>'Metodo no existe, vuelva intentarlo'
   );

}else{

    // METODO UTILIZADO GET,POST,PUT,DELETE,PATCH
    $metodo = strtoupper($_SERVER['REQUEST_METHOD']);
	//$metodo = $_POST['method'];


    // LINK MODULO
    include('lib/modulo/'.$recurso.'/modulo.php');
}


// RESPONSE API
echo json_encode($datos);