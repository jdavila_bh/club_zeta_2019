<?php

$menu_header=1;
$carousel2 = 1;
$miga_pan = 'HOME';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('SEO/seo.php'); ?>
<?php include('script.php'); ?>

</head>

<body>

<!-- Header -->
<?php include('header.php'); ?>
<!-- Fin - Header -->

<div class="container-fluid pl0 pr0">
    <div id="slider_home" class="owl-carousel">
       <div class="pointer" onclick="abrir_log();"><img src="images/slider/desktop/img1.jpg"> </div>
       <div><img src="images/slider/desktop/img2.jpg"> </div>
    </div>
</div>
<!--
<div class="container-fluid fondo_home">
    <div class="container mr0 ml0 pl0 pr0">
        <div class="row hide-md mr0  pr0">
            <div class="col-md-12 mt124 mr0  pr0">
                <p class="size60 verde_home_A6BD4D bitter text-left  mr0 ml0 pl0 pr0">SERVICIOS DE<br> ALIMENTACIÓN</p>
                <p class="size30 azul_oscuro bitter text-left">Nuestra experiencia nos avala,<br> el sabor y la calidad de nuestra<br> comida nos respalda.</p>
                <hr style="width: 61px;height: 1px;margin: 40px 0;border: none;background: #0A4187;" />
                <?php
                echo (empty($_SESSION["id_usuario"]))?
                    '<button type="button" class="boton_1 pull-left" onclick="abrir_log();">Ingresar</button></div>'
                    :'<button type="button" class="boton_1 pull-left"  onclick="alcomprar();">Ingresar</button></div>';
                ?>
            </div>
        </div>

        <div class="row hide-sm  mr0  pr0">
            <div class="col-md-12 mt40  mr0  pr0">
                <p class="size50 verde_home_A6BD4D bitter text-center  mr0 ml0 pl0 pr0">SERVICIOS DE<br> ALIMENTACIÓN</p>
                <p class="size30 azul_oscuro bitter text-center">Nuestra experiencia nos avala, el sabor y la calidad de nuestra comida nos respalda.</p>
                <hr style="width: 61px;height: 1px;margin: 0 auto;border: none;background: #0A4187;" />
                <?php
                echo (empty($_SESSION["id_usuario"]))?
                    '<button type="button" class="boton_1 center-block mt30" onclick="abrir_log();">Ingresar</button></div>'
                    :'<button type="button" class="boton_1 center-block mt30"  onclick="alcomprar();">Ingresar</button></div>';
                ?>
            </div>
        </div>
    </div>
</div>
-->
<!-- Footer -->
<?php include('footer.php'); ?>
<!-- Fin - Footer -->
</body>
</html>
